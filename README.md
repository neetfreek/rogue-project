# rogue-project
This is a 2D top-down dungeon crawler game. Written in C# and built with MonoGame (implements the Microsoft XNA 4). This game uses the Dawnlike 16x16 Universal Rogue-like tileset v1.81, created by DawnBringer (palette), DragonDePlatino (tileset), registered under the CC-BY 4.0 license.

## Overview

- `\Main` includes the top-level program management classes, most centrally `Game1.cs` (initialise main components like the graphics display device, contain major classes, and contain the game loop logic as well as drawing. 
- `\Common` contains various globally-used values, enums, and helper methods.
- The remaining folders contain component-specific classes. For instance, `\GUI` contains all classes related to the creation of, and interaction with GUI elements, while `\Terrain` contains various sections of dictionaries, enums, and classes for handling sprites' (from sprite texture atlases) organisation and retrieval. 