﻿/****************************************************
* Responsible for spawning items in the game world. *
*   Dropping an item means assigning the item as    *
*   the target tile's Item property.                *
*****************************************************/
using System.Collections.Generic;
using RogueProject.Terrain;
using System;
using RogueProject.Common;

namespace RogueProject.Items
{
    public static class ItemDropper
    {
        private static Random random = new Random();


        // Roll whether to drop item or not
        public static bool RollDrop(int dropChance)
        {
            int roll = random.Next(0, dropChance + 1);
            if (roll <= dropChance)
            {
                return true;
            }
            return false;
        }
        // Create new item, drop at tile. Return item name for message
        public static string DropNewItem(Tile tile)
        {
            Item item = ItemMaker.NewItem();
            DropItem(tile, item);

            return item.Name;
        }
        // Drop item at tile. Return item name for message
        public static string DropItem(Tile tile, Item item)
        {
            Tile dropTile = DropTile(tile, item);
            item.Tile = dropTile;
            dropTile.Item = item;

            return item.Name;
        }


        // Return tile to drop item at
        private static Tile DropTile(Tile tile, Item item)
        {
            Tile tileDrop = tile;
            List<Tile> dropTilesPossible = new List<Tile>();
            int rollTile = 0;
            int gridSize = 1;

            while (!tileDrop.Walkable)
            {
                gridSize += 2;
                dropTilesPossible = DropTilesAroundTile(gridSize, tileDrop);
                if (dropTilesPossible.Count > 0)
                {
                    rollTile = random.Next(0, dropTilesPossible.Count);

                    tileDrop = dropTilesPossible[rollTile];
                    return tileDrop;
                }
            }

            return tileDrop;
        }
        // Return list of empty (Tile.Walkable) tiles appropriate for dropping items into
        private static List<Tile> DropTilesAroundTile(int sizeGrid, Tile tileCenter)
        {
            List<Tile> tilesDropPossible = new List<Tile>();

            int positionModifier = sizeGrid / 2;
            Tile tileStart = tileCenter.Neighbour(Direction.Up, positionModifier).Neighbour(Direction.Left, positionModifier);
            Tile tileProgress = tileStart;
            Tile tileDrop = tileStart;

            for (int counterRows = 0; counterRows < sizeGrid; counterRows++)
            {
                if (tileStart.Neighbour(Direction.Down, counterRows) != null)
                {
                    tileProgress = tileStart.Neighbour(Direction.Down, counterRows);
                }

                for (int counterCols = 0; counterCols < sizeGrid; counterCols++)
                {
                    if (tileStart.Neighbour(Direction.Right, counterCols) != null)
                    {
                        tileDrop = tileProgress.Neighbour(Direction.Right, counterCols);
                    }

                    if (tileDrop.Walkable && tileDrop.Item == null)
                    {
                        tilesDropPossible.Add(tileDrop);
                    }
                }
            }

            return tilesDropPossible;
        }
    }
}