﻿namespace RogueProject.Items
{
    public enum QualityItem
    {
        None,
        Terrible,
        Low,
        Normal,
        High,
        Superior,
    }

    public enum CategoryItem
    {
        None,
        Armour,
        Consumable,
        Weapon,
    }

    public enum CategoryArmour
    {
        None,
        Helmet,
        Chest,
        Gloves,
        Boots,
        Shield,
        Amulet,
        Ring,
    }

    public enum CategoryWeapon
    {
        None,
        Melee,
        Ranged,
    }

    public enum TypeArmour
    {
        None,
        // Trinkets
        Ring,
        Amulet,
        Charm,
        // Boot
        Shoe,
        Boot,
        BootPlate,
        // Chest
        Chainmail,
        ChainmailFull,
        Leather,
        LeatherHardened,
        Platemail,
        PlatemailFull,
        Robe,
        RobeHooded,
        // Glove
        GloveLeather,
        GloveLeatherThick,
        GlovePlate,
        // Helm
        Circlet,
        Crown,
        Helmet,
        HelmetFull,
        Hat,
        Pot,
        Skullcap,
        // Shield
        Buckler,
        Bouche,
        Heater,
        Kite,
        Pavise,
    }

    public enum TypeWeapon
    {
        None,
        // Ranged
        Bow,
        BowLong,
        BowShort,
        Crossbow,
        Sling,
        // Melee
        Axe,
        AxeDouble,
        Bardiche,
        Blade,
        Cutlass,
        Club,
        Dagger,
        Dirk,
        Flail,
        Glaive,
        GreatSword,
        Halberd,
        Knife,
        LongSword,
        Mallet,
        Mace,
        Pickaxe,
        Polearm,
        Rod,
        Scythe,
        ShortSword,
        Spear,
        Stiletto,
        Whip,
    }
}