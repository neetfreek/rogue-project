﻿/********************************************************************
* Returns armour rating values stored as Vector2 variables based    *
*   on CategoryArmour argument                                      *
*********************************************************************/

using Microsoft.Xna.Framework;

namespace RogueProject.Items
{
    public static class ArmourRatingArmour
    {
        public static Vector2 ArmourRating(TypeArmour type)
        {
            Vector2 ratingArmour = new Vector2(0f, 0f);
            switch (type)
            {
                // Boot
                case TypeArmour.Shoe:
                    ratingArmour = new Vector2(1f, 3f);
                    break;
                case TypeArmour.Boot:
                    ratingArmour = new Vector2(2f, 6f);
                    break;
                case TypeArmour.BootPlate:
                    ratingArmour = new Vector2(3f, 9f);
                    break;
                // Chest
                case TypeArmour.Robe:
                    ratingArmour = new Vector2(2f, 8f);
                    break;
                case TypeArmour.RobeHooded:
                    ratingArmour = new Vector2(4f, 10f);
                    break;
                case TypeArmour.Leather:
                    ratingArmour = new Vector2(4f, 10f);
                    break;
                case TypeArmour.LeatherHardened:
                    ratingArmour = new Vector2(7f, 13f);
                    break;
                case TypeArmour.Chainmail:
                    ratingArmour = new Vector2(10f, 16f);
                    break;
                case TypeArmour.ChainmailFull:
                    ratingArmour = new Vector2(13f, 19f);
                    break;
                case TypeArmour.Platemail:
                    ratingArmour = new Vector2(16, 22f);
                    break;
                case TypeArmour.PlatemailFull:
                    ratingArmour = new Vector2(19f, 25f);
                    break;
                // Glove
                case TypeArmour.GloveLeather:
                    ratingArmour = new Vector2(1, 5);
                    break;
                case TypeArmour.GloveLeatherThick:
                    ratingArmour = new Vector2(3f, 8f);
                    break;
                case TypeArmour.GlovePlate:
                    ratingArmour = new Vector2(6f, 11);
                    break;
                // Helm
                case TypeArmour.Pot:
                    ratingArmour = new Vector2(3f, 7f);
                    break;
                case TypeArmour.Hat:
                    ratingArmour = new Vector2(1f, 5f);
                    break;
                case TypeArmour.Circlet:
                    ratingArmour = new Vector2(2f, 6f);
                    break;
                case TypeArmour.Crown:
                    ratingArmour = new Vector2(3f, 8f);
                    break;
                case TypeArmour.Skullcap:
                    ratingArmour = new Vector2(4f, 10f);
                    break;
                case TypeArmour.Helmet:
                    ratingArmour = new Vector2(7f, 13f);
                    break;
                case TypeArmour.HelmetFull:
                    ratingArmour = new Vector2(10, 15f);
                    break;
                // Shield
                case TypeArmour.Buckler:
                    ratingArmour = new Vector2(2f, 8f);
                    break;
                case TypeArmour.Kite:
                    ratingArmour = new Vector2(4f, 10f);
                    break;
                case TypeArmour.Pavise:
                    ratingArmour = new Vector2(2f, 12f);
                    break;
                case TypeArmour.Bouche:
                    ratingArmour = new Vector2(10, 16f);
                    break;
                case TypeArmour.Heater:
                    ratingArmour = new Vector2(14f, 20f);
                    break;
            }

            return ratingArmour;
        }
    }
}
