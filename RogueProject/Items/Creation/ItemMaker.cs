﻿/****************************************************************
* Responsible for creating new items, deciding item category    *
*   (e.g. CategoryItem.Weapon), sub-category (e.g.              *
*   CategoryWeapon.Melee), the type (e.g.                       *
*   TypeWeapon.ShortSword) the quality of the item (e.g.        *
*   QualityItem.Terrible), the primary statistic (e.g. Damage), *
*   and sprite (e.g. WeaponSprite.ShortSwordGold                *
*****************************************************************/
using System;
using Microsoft.Xna.Framework;

namespace RogueProject.Items
{
    public static class ItemMaker
    {
        private static Random random = new Random();


        public static Item NewItem()
        {
            Item item = new Item();

            CategoryItem itemCategory = SetCategoryItem();
            switch (itemCategory)
            {
                case CategoryItem.Armour:
                    item = NewArmour();
                    break;
                case CategoryItem.Weapon:
                    item = NewWeapon();
                    break;
            }
            return item;
        }
        

        // Create new items
        private static Armour NewArmour()
        {
            Armour armour = new Armour
            {
                CategoryItem = CategoryItem.Armour,
                CategoryArmour = SetCategoryArmour(),
                Quality = SetQualityItem(),
            };
            armour.TypeArmour = HandleSetTypeArmour(armour.CategoryArmour);
            armour.Name = ItemDisplayName.DisplayName(armour);
            armour.SpriteArmour = ArmourSpriteTexture.SpriteArmour(armour.TypeArmour);

            Vector2 ratingArmourRange = new Vector2(ArmourRatingArmour.ArmourRating(armour.TypeArmour).X * QualityModifier(armour.Quality), ArmourRatingArmour.ArmourRating(armour.TypeArmour).Y * QualityModifier(armour.Quality));
            ratingArmourRange.X = (float)Math.Round((decimal)(ratingArmourRange.X), 0, MidpointRounding.AwayFromZero);
            ratingArmourRange.Y = (float)Math.Round((decimal)(ratingArmourRange.Y), 0, MidpointRounding.AwayFromZero);
            ratingArmourRange = EnsureMinimumStat(ratingArmourRange);
            armour.RatingArmour = random.Next((int)ratingArmourRange.X, (int)ratingArmourRange.Y + 1);

            return armour;
        }
        private static Weapon NewWeapon()
        {
            Weapon weapon = new Weapon
            {
                CategoryItem = CategoryItem.Weapon,
                CategoryWeapon = SetCategoryWeapon(),
                Quality = SetQualityItem(),
            };
            weapon.TypeWeapon = HandleSetTypeWeapon(weapon.CategoryWeapon);
            weapon.Name = ItemDisplayName.DisplayName(weapon);
            weapon.Range = WeaponRange.Range(weapon.TypeWeapon);
            weapon.SpriteWeapon = WeaponSpriteTexture.SpriteWeapon(weapon.TypeWeapon);

            Vector2 damage = new Vector2(WeaponDamage.Damage(weapon.TypeWeapon).X * QualityModifier(weapon.Quality), WeaponDamage.Damage(weapon.TypeWeapon).Y * QualityModifier(weapon.Quality));
            damage.X = (float)Math.Round((decimal)(damage.X), 0, MidpointRounding.AwayFromZero);
            damage.Y = (float)Math.Round((decimal)(damage.Y), 0, MidpointRounding.AwayFromZero);
            damage = EnsureMinimumStat(damage);
            weapon.Damage = damage;

            return weapon;
        }
        // TODO: Consumable

        private static Vector2 EnsureMinimumStat(Vector2 itemStatistic)
        {
            Vector2 itemStatLocal = itemStatistic;
            if (itemStatLocal.X < 1)
            {
                itemStatLocal.X = 1;
            }
            if (itemStatLocal.Y < 1)
            {
                itemStatLocal.Y = 1;
            }

            if (itemStatLocal.Y < itemStatLocal.X)
            {
                itemStatLocal.Y = itemStatLocal.X;
            }

            return itemStatLocal;
        }
        private static float QualityModifier(QualityItem qualityItem)
        {
            float modifier = 0f; // Percentage change applied to base statistic
            switch (qualityItem)
            {
                case QualityItem.Terrible:
                    modifier = random.Next((int)ItemVariables.QUALITY_MODIFIER_TERRIBLE.X,
                        (int)ItemVariables.QUALITY_MODIFIER_TERRIBLE.Y);
                    break;
                case QualityItem.Low:
                    modifier = random.Next((int)ItemVariables.QUALITY_MODIFIER_LOW.X,
                        (int)ItemVariables.QUALITY_MODIFIER_LOW.Y);
                    break;
                case QualityItem.Normal:
                    modifier = random.Next((int)ItemVariables.QUALITY_MODIFIER_NORMAL.X,
                        (int)ItemVariables.QUALITY_MODIFIER_NORMAL.Y);
                    break;
                case QualityItem.High:
                    modifier = random.Next((int)ItemVariables.QUALITY_MODIFIER_HIGH.X,
                        (int)ItemVariables.QUALITY_MODIFIER_HIGH.Y);
                    break;
                case QualityItem.Superior:
                    modifier = random.Next((int)ItemVariables.QUALITY_MODIFIER_SUPERIOR.X,
                        (int)ItemVariables.QUALITY_MODIFIER_SUPERIOR.Y);
                    break;
            }

            modifier *= 0.01f;

            return modifier;
        }


        // Set categories
        private static CategoryItem SetCategoryItem()
        {
            CategoryItem category = CategoryItem.None;

            int rollCategory = random.Next(0, 100);
            if (rollCategory < ItemVariables.DROP_ROLL_ARMOUR)
            {
                category = CategoryItem.Armour;

            }
            else if (rollCategory < ItemVariables.DROP_ROLL_WEAPON)
            {
                category = CategoryItem.Weapon;

            }
            //else if (rollCategory < ItemVariables.DROP_ROLL_CONSUMABLE)
            //{
            //    category = CategoryItem.Consumable;
            //}

            return category;
        }
        private static QualityItem SetQualityItem()
        {
            QualityItem qualityItem = QualityItem.None;

            int roll = random.Next(0, 100);
            if (roll < ItemVariables.QUALITY_ROLL_TERRIBLE)
            {
                qualityItem = QualityItem.Terrible;
            }
            else if (roll < ItemVariables.QUALITY_ROLL_LOW)
            {
                qualityItem = QualityItem.Low;
            }
            else if (roll < ItemVariables.QUALITY_ROLL_NORMAL)
            {
                qualityItem = QualityItem.Normal;
            }
            else if (roll < ItemVariables.QUALITY_ROLL_HIGH)
            {
                qualityItem = QualityItem.High;
            }
            else if (roll < ItemVariables.QUALITY_ROLL_SUPERIOR)
            {
                qualityItem = QualityItem.Superior;
            }

            return qualityItem;
        }
        // Armour
        private static CategoryArmour SetCategoryArmour()
        {
            CategoryArmour category = CategoryArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_HELM)
            {
                category = CategoryArmour.Helmet;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CHEST)
            {
                category = CategoryArmour.Chest;
            }
            else if (rollType < ItemVariables.DROP_ROLL_GLOVES)
            {
                category = CategoryArmour.Gloves;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOOTS)
            {
                category = CategoryArmour.Boots;
            }
            else if (rollType < ItemVariables.DROP_ROLL_SHIELD)
            {
                category = CategoryArmour.Shield;
            }
            else if (rollType < ItemVariables.DROP_ROLL_AMULET)
            {
                category = CategoryArmour.Amulet;
            }
            else if (rollType < ItemVariables.DROP_ROLL_RING)
            {
                category = CategoryArmour.Ring;
            }

            return category;
        }
        private static TypeArmour HandleSetTypeArmour(CategoryArmour category)
        {
            TypeArmour type = TypeArmour.None;
            switch (category)
            {
                case CategoryArmour.Helmet:
                    type = SetTypeHelmet();
                    break;
                case CategoryArmour.Chest:
                    type = SetTypeChest();
                    break;
                case CategoryArmour.Gloves:
                    type = SetTypeGloves();
                    break;
                case CategoryArmour.Boots:
                    type = SetTypeBoots();
                    break;
                case CategoryArmour.Shield:
                    type = SetTypeShield();
                    break;
                case CategoryArmour.Amulet:
                    type = TypeArmour.Amulet;
                    break;
                case CategoryArmour.Ring:
                    type = TypeArmour.Ring;
                    break;
            }

            return type;
        }
        private static TypeArmour SetTypeHelmet()
        {
            TypeArmour type = TypeArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_POT)
            {
                type = TypeArmour.Pot;
            }
            else if (rollType < ItemVariables.DROP_ROLL_HAT)
            {
                type = TypeArmour.Hat;
            }
            else if (rollType < ItemVariables.DROP_ROLL_SKULLCAP)
            {
                type = TypeArmour.Skullcap;
            }
            else if (rollType < ItemVariables.DROP_ROLL_HELMET)
            {
                type = TypeArmour.Helmet;
            }
            else if (rollType < ItemVariables.DROP_ROLL_HELMETFULL)
            {
                type = TypeArmour.HelmetFull;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CIRCLET)
            {
                type = TypeArmour.Circlet;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CROWN)
            {
                type = TypeArmour.Crown;
            }
            return type;
        }
        private static TypeArmour SetTypeChest()
        {
            TypeArmour type = TypeArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_ROBE)
            {
                type = TypeArmour.Robe;
            }
            else if (rollType < ItemVariables.DROP_ROLL_ROBEHOODED)
            {
                type = TypeArmour.RobeHooded;
            }
            else if (rollType < ItemVariables.DROP_ROLL_LEATHER)
            {
                type = TypeArmour.Leather;
            }
            else if (rollType < ItemVariables.DROP_ROLL_LEATHERHARDENED)
            {
                type = TypeArmour.LeatherHardened;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CHAINMAIL)
            {
                type = TypeArmour.Chainmail;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CHAINMAILFULL)
            {
                type = TypeArmour.ChainmailFull;
            }
            else if (rollType < ItemVariables.DROP_ROLL_PLATEMAIL)
            {
                type = TypeArmour.Platemail;
            }
            else if (rollType < ItemVariables.DROP_ROLL_PLATEMAILFULL)
            {
                type = TypeArmour.PlatemailFull;
            }
            return type;
        }
        private static TypeArmour SetTypeGloves()
        {
            TypeArmour type = TypeArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_GLOVELEATHER)
            {
                type = TypeArmour.GloveLeather;
            }
            else if (rollType < ItemVariables.DROP_ROLL_GLOVELEATHERTHICK)
            {
                type = TypeArmour.GloveLeatherThick;
            }
            else if (rollType < ItemVariables.DROP_ROLL_GLOVEPLATE)
            {
                type = TypeArmour.GlovePlate;
            }

            return type;
        }
        private static TypeArmour SetTypeBoots()
        {
            TypeArmour type = TypeArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_SHOE)
            {
                type = TypeArmour.Shoe;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOOT)
            {
                type = TypeArmour.Boot;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOOTPLATE)
            {
                type = TypeArmour.BootPlate;
            }

            return type;
        }
        private static TypeArmour SetTypeShield()
        {
            TypeArmour type = TypeArmour.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_BUCKLER)
            {
                type = TypeArmour.Buckler;
            }
            else if (rollType < ItemVariables.DROP_ROLL_KITE)
            {
                type = TypeArmour.Kite;
            }
            else if (rollType < ItemVariables.DROP_ROLL_PAVISE)
            {
                type = TypeArmour.Pavise;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOUCHE)
            {
                type = TypeArmour.Bouche;
            }
            else if (rollType < ItemVariables.DROP_ROLL_HEATER)
            {
                type = TypeArmour.Heater;
            }

            return type;
        }


        // Weapons
        private static CategoryWeapon SetCategoryWeapon()
        {
            CategoryWeapon category = CategoryWeapon.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_RANGED)
            {
                category = CategoryWeapon.Ranged;
            }
            else if (rollType < ItemVariables.DROP_ROLL_MELEE)
            {
                category = CategoryWeapon.Melee;
            }

            return category;
        }
        private static TypeWeapon HandleSetTypeWeapon(CategoryWeapon category)
        {
            TypeWeapon type = TypeWeapon.None;
            switch (category)
            {
                case CategoryWeapon.Melee:
                    type = SetTypeMelee();
                    break;
                case CategoryWeapon.Ranged:
                    type = SetTypeRanged();
                    break;
            }

            return type;
        }
        private static TypeWeapon SetTypeMelee()
        {
            TypeWeapon type = TypeWeapon.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_AXE)
            {
                type = TypeWeapon.Axe;
            }
            else if (rollType < ItemVariables.DROP_ROLL_AXEDOUBLE)
            {
                type = TypeWeapon.AxeDouble;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BARDICHE)
            {
                type = TypeWeapon.Bardiche;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BLADE)
            {
                type = TypeWeapon.Blade;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CUTLASS)
            {
                type = TypeWeapon.Cutlass;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CLUB)
            {
                type = TypeWeapon.Club;
            }
            else if (rollType < ItemVariables.DROP_ROLL_DAGGER)
            {
                type = TypeWeapon.Dagger;
            }
            else if (rollType < ItemVariables.DROP_ROLL_DIRK)
            {
                type = TypeWeapon.Dirk;
            }
            else if (rollType < ItemVariables.DROP_ROLL_FLAIL)
            {
                type = TypeWeapon.Flail;
            }
            else if (rollType < ItemVariables.DROP_ROLL_GLAIVE)
            {
                type = TypeWeapon.Glaive;
            }
            else if (rollType < ItemVariables.DROP_ROLL_GREATSWORD)
            {
                type = TypeWeapon.GreatSword;
            }
            else if (rollType < ItemVariables.DROP_ROLL_HALBERD)
            {
                type = TypeWeapon.Halberd;
            }
            else if (rollType < ItemVariables.DROP_ROLL_KNIFE)
            {
                type = TypeWeapon.Knife;
            }
            else if (rollType < ItemVariables.DROP_ROLL_LONGSWORD)
            {
                type = TypeWeapon.LongSword;
            }
            else if (rollType < ItemVariables.DROP_ROLL_MALLET)
            {
                type = TypeWeapon.Mallet;
            }
            else if (rollType < ItemVariables.DROP_ROLL_MACE)
            {
                type = TypeWeapon.Mace;
            }
            else if (rollType < ItemVariables.DROP_ROLL_PICKAXE)
            {
                type = TypeWeapon.Pickaxe;
            }
            else if (rollType < ItemVariables.DROP_ROLL_POLEARM)
            {
                type = TypeWeapon.Polearm;
            }
            else if (rollType < ItemVariables.DROP_ROLL_ROD)
            {
                type = TypeWeapon.Rod;
            }
            else if (rollType < ItemVariables.DROP_ROLL_SCYTHE)
            {
                type = TypeWeapon.Scythe;
            }
            else if (rollType < ItemVariables.DROP_ROLL_SHORTSWORD)
            {
                type = TypeWeapon.ShortSword;
            }
            else if (rollType < ItemVariables.DROP_ROLL_SPEAR)
            {
                type = TypeWeapon.Spear;
            }
            else if (rollType < ItemVariables.DROP_ROLL_ROD)
            {
                type = TypeWeapon.Stiletto;
            }
            else if (rollType < ItemVariables.DROP_ROLL_WHIP)
            {
                type = TypeWeapon.Whip;
            }

            return type;
        }
        private static TypeWeapon SetTypeRanged()
        {
            TypeWeapon type = TypeWeapon.None;
            int rollType = random.Next(0, 100);
            if (rollType < ItemVariables.DROP_ROLL_SLING)
            {
                type = TypeWeapon.Sling;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOWSHORT)
            {
                type = TypeWeapon.BowShort;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOW)
            {
                type = TypeWeapon.Bow;
            }
            else if (rollType < ItemVariables.DROP_ROLL_BOWLONG)
            {
                type = TypeWeapon.BowLong;
            }
            else if (rollType < ItemVariables.DROP_ROLL_CROSSBOW)
            {
                type = TypeWeapon.Crossbow;
            }
            return type;
        }


        // Temporary test methods
        public static void TestMakeWeapons()
        {
           for (int counter = 1000; counter > 0; counter--)
            {

                Weapon weapon = NewWeapon();
                Console.WriteLine($"{Environment.NewLine}{Environment.NewLine}***NEW ITEM***");
                Console.WriteLine($"name: {weapon.Name}");
                Console.WriteLine($"category: {weapon.CategoryWeapon}");
                Console.WriteLine($"type: {weapon.TypeWeapon}");
                Console.WriteLine($"quality: {weapon.Quality}");
                Console.WriteLine($"damage: {weapon.Damage}");
                Console.WriteLine($"sprite: {weapon.SpriteWeapon}");
            }
        }
        public static void TestMakeArmour()
        {
            for (int counter = 1000; counter > 0; counter--)
            {
                Armour armour= NewArmour();
                Console.WriteLine($"{Environment.NewLine}{Environment.NewLine}***NEW ITEM***");
                Console.WriteLine($"name: {armour.Name}");
                Console.WriteLine($"category: {armour.CategoryArmour}");            
                Console.WriteLine($"type: {armour.TypeArmour}");
                Console.WriteLine($"quality: {armour.Quality}");
                Console.WriteLine($"defence: {armour.RatingArmour}");
                Console.WriteLine($"sprite: {armour.SpriteArmour}");
            }
        }
    }
}