﻿/********************************************************************
* Returns weapon damage values stored as Vector2 variables based    *
*   on CategoryWeapon argument                                      *
*********************************************************************/

using Microsoft.Xna.Framework;

namespace RogueProject.Items
{
    public static class WeaponDamage
    {
        public static Vector2 Damage(TypeWeapon type)
        {
            Vector2 damage = new Vector2(0f, 0f);
            switch (type)
            {
                // Ranged
                    // 1-handed
                case TypeWeapon.Crossbow:
                    damage = new Vector2(3, 5);
                    break;
                case TypeWeapon.Sling:
                    damage = new Vector2(1, 6);
                    break;
                // 2-handed
                case TypeWeapon.Bow:
                    damage = new Vector2(5, 8);
                    break;
                case TypeWeapon.BowLong:
                    damage = new Vector2(4, 10);
                    break;
                case TypeWeapon.BowShort:
                    damage = new Vector2(2, 6);
                    break;
                case TypeWeapon.Rod:
                    damage = new Vector2(1, 4);
                    break;
                // Melee
                // 1-handed
                // 1 range
                case TypeWeapon.Axe:
                    damage = new Vector2(4, 10);
                    break;
                case TypeWeapon.Blade:
                    damage = new Vector2(4, 8);
                    break;
                case TypeWeapon.Cutlass:
                    damage = new Vector2(3, 6);
                    break;
                case TypeWeapon.Club:
                    damage = new Vector2(2, 5);
                    break;
                case TypeWeapon.Dagger:
                    damage = new Vector2(1, 6);
                    break;
                case TypeWeapon.Dirk:
                    damage = new Vector2(1, 5);
                    break;
                case TypeWeapon.Flail:
                    damage = new Vector2(2, 8);
                    break;
                case TypeWeapon.Knife:
                    damage = new Vector2(2, 4);
                    break;
                case TypeWeapon.LongSword:
                    damage = new Vector2(6, 8);
                    break;
                case TypeWeapon.Mace:
                    damage = new Vector2(3, 6);
                    break;
                case TypeWeapon.ShortSword:
                    damage = new Vector2(2, 6);
                    break;
                case TypeWeapon.Stiletto:
                    damage = new Vector2(3, 5);
                    break;
                        // 2 range
                case TypeWeapon.Whip:
                    damage = new Vector2(1, 10);
                    break;
                // 2-handed
                    // 1 range                
                case TypeWeapon.AxeDouble:
                    damage = new Vector2(4, 12);
                    break;
                case TypeWeapon.Mallet:
                    damage = new Vector2(4, 10);
                    break;
                case TypeWeapon.Pickaxe:
                    damage = new Vector2(4, 8);
                    break;
                    // 2 range
                case TypeWeapon.Bardiche:
                    damage = new Vector2(6, 10);
                    break;
                case TypeWeapon.Glaive:
                    damage = new Vector2(4, 12);
                    break;
                case TypeWeapon.GreatSword:
                    damage = new Vector2(6, 12);
                    break;
                case TypeWeapon.Halberd:
                    damage = new Vector2(2, 14);
                    break;                
                case TypeWeapon.Polearm:
                    damage = new Vector2(3, 10);
                    break;
                case TypeWeapon.Scythe:
                    damage = new Vector2(2, 9);
                    break;
                case TypeWeapon.Spear:
                    damage = new Vector2(3, 10);
                    break;
            }

            return damage;
        }
    }
}