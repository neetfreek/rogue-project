﻿/********************************************************************
* Returns item display name. Depending on item argument, tries to   *
*   get string value (name) corresponding to key (item) from the    *
*   appropriate key (e.g. CategoryWeapon in the case of weapons)    *
*********************************************************************/
using System.Collections.Generic;

namespace RogueProject.Items
{
    public static class ItemDisplayName
    {
        public static string DisplayName(Item item)
        {
            string name = "";

            switch (item.CategoryItem)
            {
                case CategoryItem.Weapon:
                    WeaponDisplayNames.TryGetValue(item.TypeWeapon, out name);
                    break;
                case CategoryItem.Armour:
                    ArmourDisplayNames.TryGetValue(item.TypeArmour, out name);
                    break;
            }

            return name;
        }

        public static Dictionary<TypeWeapon, string> WeaponDisplayNames = new Dictionary<TypeWeapon, string>()
        {
                {TypeWeapon.Bow,        "Bow"},
                {TypeWeapon.BowLong,    "Long Bow"},
                {TypeWeapon.BowShort,   "Short Bow"},
                {TypeWeapon.Crossbow,   "Crossbow"},
                {TypeWeapon.Sling,      "Sling"},
                {TypeWeapon.Axe,        "Axe"},
                {TypeWeapon.AxeDouble,  "Double Axe"},
                {TypeWeapon.Bardiche,   "Bardiche"},
                {TypeWeapon.Blade,      "Blade"},
                {TypeWeapon.Cutlass,    "Cutlass"},
                {TypeWeapon.Club,       "Club"},
                {TypeWeapon.Dagger,     "Dagger"},
                {TypeWeapon.Dirk,       "Dirk"},
                {TypeWeapon.Flail,      "Flail"},
                {TypeWeapon.Glaive,     "Glaive"},
                {TypeWeapon.GreatSword, "Great Sword"},
                {TypeWeapon.Halberd,    "Halberd"},
                {TypeWeapon.Knife,       "Knife"},
                {TypeWeapon.LongSword,  "Long Sword"},
                {TypeWeapon.Mallet,     "Mallet"},
                {TypeWeapon.Mace,       "Mace"},
                {TypeWeapon.Pickaxe,    "Pickaxe"},
                {TypeWeapon.Polearm,    "Polearm"},
                {TypeWeapon.Rod,        "Rod"},
                {TypeWeapon.Scythe,     "Scythe"},
                {TypeWeapon.ShortSword, "Short Sword"},
                {TypeWeapon.Spear,      "Spear"},
                {TypeWeapon.Stiletto,   "Stiletto"},
                {TypeWeapon.Whip,       "Whip"},
        };

        public static Dictionary<TypeArmour, string> ArmourDisplayNames = new Dictionary<TypeArmour, string>()
        {            
                {TypeArmour.Amulet,             "Amulet"},
                {TypeArmour.Shoe,               "Shoes"},
                {TypeArmour.Boot,               "Boots"},
                {TypeArmour.BootPlate,          "Plated Boots"},
                {TypeArmour.Chainmail,          "Chainmail"},
                {TypeArmour.ChainmailFull,      "Full Chainmail"},
                {TypeArmour.Leather,            "Leather Armour"},
                {TypeArmour.LeatherHardened,    "Hardened Leather Armour"},
                {TypeArmour.Platemail,          "Platemail"},
                {TypeArmour.PlatemailFull,      "Full Platemail"},
                {TypeArmour.Robe,               "Robe"},
                {TypeArmour.RobeHooded,         "Hooded Robe"},
                {TypeArmour.GloveLeather,       "Leather Gloves"},
                {TypeArmour.GloveLeatherThick,  "Thick Leather Gloves"},
                {TypeArmour.GlovePlate,         "Plated Gloves"},
                {TypeArmour.Circlet,            "Circlet"},
                {TypeArmour.Crown,              "Crown"},
                {TypeArmour.Helmet,             "Helmet"},
                {TypeArmour.HelmetFull,         "Full Helmet"},
                {TypeArmour.Hat,                "Hat"},
                {TypeArmour.Pot,                "Pot"},
                {TypeArmour.Skullcap,           "Skullcap"},
                {TypeArmour.Ring,               "Ring"},
                {TypeArmour.Buckler,            "Buckler"},
                {TypeArmour.Bouche,             "Bouche"},
                {TypeArmour.Heater,             "Heater"},
                {TypeArmour.Kite,               "Kite Shield"},
                {TypeArmour.Pavise,             "Pavise"},
        };                      
    }                           
}                      