﻿/*******************************************************
* Returns armour sprite for CategoryArmour argument  *
********************************************************/
using System;
using RogueProject.Textures;

namespace RogueProject.Items
{
    public static class ArmourSpriteTexture
    {
        private static readonly Random random = new Random();
        
        public static ArmourSprite SpriteArmour(TypeArmour type)
        {
            int roll = 0;
            ArmourSprite sprite = ArmourSprite.None;
            switch (type)
            {
                case TypeArmour.Circlet:
                    sprite = ArmourSprite.Circlet;
                    break;
                case TypeArmour.Crown:
                    roll = random.Next(2, 4);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Helmet:
                    roll = random.Next(4, 7);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.HelmetFull:
                    roll = random.Next(7, 10);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Hat:
                    roll = random.Next(10, 13);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Pot:
                    sprite = ArmourSprite.Pot;
                    break;
                case TypeArmour.Skullcap:
                    roll = random.Next(14, 16);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Leather:
                    roll = random.Next(16, 18);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.LeatherHardened:
                    roll = random.Next(18, 20);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Chainmail:
                    roll = random.Next(20, 23);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.ChainmailFull:
                    roll = random.Next(23, 27);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Platemail:
                    roll = random.Next(27, 31);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.PlatemailFull:
                    roll = random.Next(31, 34);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Robe:
                    roll = random.Next(34, 38);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.RobeHooded:
                    roll = random.Next(38, 42);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.GloveLeather:
                    roll = random.Next(42, 44);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.GloveLeatherThick:
                    sprite = ArmourSprite.GloveLeatherThick;
                    break;
                case TypeArmour.GlovePlate:
                    sprite = ArmourSprite.GlovePlate;
                    break;
                case TypeArmour.Buckler:
                    sprite = ArmourSprite.Buckler;
                    break;
                case TypeArmour.Bouche:
                    sprite = ArmourSprite.Bouche;
                    break;
                case TypeArmour.Heater:
                    sprite = ArmourSprite.Buckler;
                    break;
                case TypeArmour.Kite:
                    roll = random.Next(49, 52);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll); break;
                case TypeArmour.Pavise:
                    sprite = ArmourSprite.Pavise;
                    break;
                case TypeArmour.Shoe:
                    roll = random.Next(53, 57);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Boot:
                    roll = random.Next(57, 61);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.BootPlate:
                    roll = random.Next(61, 63);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Amulet:
                    roll = random.Next(63, 78);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
                case TypeArmour.Ring:
                    roll = random.Next(79, 122);
                    sprite = (ArmourSprite)Enum.ToObject(typeof(ArmourSprite), roll);
                    break;
            }
            return sprite;
        }
    }
}