﻿/*******************************************************
* Retuerns weapon sprite for CategoryWeapon argument   *
********************************************************/
using System;
using RogueProject.Textures;

namespace RogueProject.Items
{
    public static class WeaponSpriteTexture
    {
        private static readonly Random random = new Random();

        public static WeaponSprite SpriteWeapon(TypeWeapon type)
        {
            int roll = 0;
            WeaponSprite sprite = WeaponSprite.None;
            switch (type)
            {
                case TypeWeapon.Crossbow:
                    sprite = WeaponSprite.Crossbow;
                    break;
                case TypeWeapon.Sling:
                    sprite = WeaponSprite.Sling;
                    break;
                case TypeWeapon.Bow:
                    sprite = WeaponSprite.Bow;
                    break;
                case TypeWeapon.BowLong:
                    roll = random.Next(2, 4);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.BowShort:
                    sprite = WeaponSprite.BowShort;
                    break;
                case TypeWeapon.Rod:
                    roll = random.Next(37, 39);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Axe:
                    sprite = WeaponSprite.Axe;
                    break;
                case TypeWeapon.Blade:
                    roll = random.Next(10, 12);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Cutlass:
                    roll = random.Next(12, 14);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Club:
                    roll = random.Next(14, 17);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Dagger:
                    roll = random.Next(19, 22);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Dirk:
                    roll = random.Next(17, 19);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Flail:
                    sprite = WeaponSprite.Flail;
                    break;
                case TypeWeapon.Knife:
                    roll = random.Next(27, 29);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.LongSword:
                    sprite = WeaponSprite.LongSword;
                    break;
                case TypeWeapon.Mace:
                    roll = random.Next(31, 33);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.ShortSword:
                    roll = random.Next(40, 42);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Stiletto:
                    sprite = WeaponSprite.Stiletto;
                    break;
                case TypeWeapon.Whip:
                    roll = random.Next(47, 48);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;             
                case TypeWeapon.AxeDouble:
                    sprite = WeaponSprite.AxeDouble;
                    break;
                case TypeWeapon.Mallet:
                    sprite = WeaponSprite.Mallet;
                    break;
                case TypeWeapon.Pickaxe:
                    roll = random.Next(33, 36);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll); break;
                case TypeWeapon.Bardiche:
                    sprite = WeaponSprite.Bardiche;
                    break;
                case TypeWeapon.Glaive:
                    sprite = WeaponSprite.Glaive;
                    break;
                case TypeWeapon.GreatSword:
                    roll = random.Next(24, 26);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
                case TypeWeapon.Halberd:
                    sprite = WeaponSprite.Halberd;
                    break;
                case TypeWeapon.Polearm:
                    sprite = WeaponSprite.Polearm;
                    break;
                case TypeWeapon.Scythe:
                    sprite = WeaponSprite.Scythe;
                    break;
                case TypeWeapon.Spear:
                    roll = random.Next(43, 46);
                    sprite = (WeaponSprite)Enum.ToObject(typeof(WeaponSprite), roll);
                    break;
            }

            return sprite;
        }
    }
}