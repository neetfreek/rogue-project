﻿/********************************************************************
* Returns weapon damage values stored as integer variables based    *
*   on CategoryWeapon argument                                      *
*********************************************************************/

namespace RogueProject.Items
{
    public static class WeaponRange
    {
        public static int Range(TypeWeapon type)
        {
            int range = 1;
            switch (type)
            {
                // Ranged
                    // 1-handed
                case TypeWeapon.Crossbow:
                    range = 4;
                    break;
                case TypeWeapon.Sling:
                    range = 5;
                    break;
                // 2-handed
                case TypeWeapon.Bow:
                    range = 7;
                    break;
                case TypeWeapon.BowLong:
                    range = 8;
                    break;
                case TypeWeapon.BowShort:
                    range = 6;
                    break;
                case TypeWeapon.Rod:
                    range = 7;
                    break;
                // Melee
                // 1-handed
                // 1 range
                case TypeWeapon.Axe:
                    range = 1;
                    break;
                case TypeWeapon.Blade:
                    range = 1;
                    break;
                case TypeWeapon.Cutlass:
                    range = 1;
                    break;
                case TypeWeapon.Club:
                    range = 1;
                    break;
                case TypeWeapon.Dagger:
                    range = 1;
                    break;
                case TypeWeapon.Dirk:
                    range = 1;
                    break;
                case TypeWeapon.Flail:
                    range = 1;
                    break;
                case TypeWeapon.Knife:
                    range = 1;
                    break;
                case TypeWeapon.LongSword:
                    range = 1;
                    break;
                case TypeWeapon.Mace:
                    range = 1;
                    break;
                case TypeWeapon.ShortSword:
                    range = 1;
                    break;
                case TypeWeapon.Stiletto:
                    range = 1;
                    break;
                        // 2 range
                case TypeWeapon.Whip:
                    range = 2;
                    break;
                    // 2-handed
                        // 1 range
                case TypeWeapon.AxeDouble:
                    range = 1;
                    break;
                case TypeWeapon.GreatSword:
                    range = 2;
                    break;
                case TypeWeapon.Mallet:
                    range = 1;
                    break;
                case TypeWeapon.Pickaxe:
                    range = 1;
                    break;
                        // 2 range
                case TypeWeapon.Bardiche:
                    range = 2;
                    break;
                case TypeWeapon.Glaive:
                    range = 2;
                    break;
                case TypeWeapon.Halberd:
                    range = 2;
                    break;
                case TypeWeapon.Polearm:
                    range = 2;
                    break;
                case TypeWeapon.Scythe:
                    range = 2;
                    break;
                case TypeWeapon.Spear:
                    range = 2;
                    break;
            }

            return range;
        }
    }
}