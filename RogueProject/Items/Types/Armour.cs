﻿/*******************************************************
* Contains properties of armour objects in the game.   *
********************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Textures;

namespace RogueProject.Items
{
    public class Armour : Item
    {
        public int RatingArmour { get => ratingArmour; set => ratingArmour = value; }

        private int ratingArmour;
    }
}