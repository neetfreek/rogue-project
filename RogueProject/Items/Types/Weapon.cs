﻿/*******************************************************
* Contains properties of weapon objects in the game.   *
********************************************************/
using Microsoft.Xna.Framework;

namespace RogueProject.Items
{
    public class Weapon : Item
    {
        public Vector2 Damage { get => damage; set => damage = value; }
        public int Range { get => range; set => range = value; }

        private Vector2 damage;
        private int range;
    }
}