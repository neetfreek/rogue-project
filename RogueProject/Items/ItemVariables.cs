﻿/************************************************
* Contains quality roll chances and modifiers,  *      
*   drop rate chances for CategoryItems,        *
*   TypeArmour, CategoryArmour                  *
*************************************************/
using Microsoft.Xna.Framework;

namespace RogueProject.Items
{
    public static class ItemVariables
    {
        // Quality roll chances for QualityItem (roll 0-99, if under lowest value const select that, else if under next, etc.)
        public const float QUALITY_ROLL_TERRIBLE = 10f;
        public const float QUALITY_ROLL_LOW = 30f;
        public const float QUALITY_ROLL_NORMAL = 70f;
        public const float QUALITY_ROLL_HIGH = 90f;
        public const float QUALITY_ROLL_SUPERIOR = 100;

        // Inspection words
        public const string TEXT_QUALITY_TERRIBLE = "Rusty";
        public const string TEXT_QUALITY_LOW = "Poor";
        public const string TEXT_QUALITY_NORMAL = "Standard";
        public const string TEXT_QUALITY_HIGH = "Fine";
        public const string TEXT_QUALITY_SUPERIOR = "Superior";

        public const string TEXT_RATING_ARMOUR = "Armour rating:";
        public const string TEXT_DAMAGE = "Damage:";
        public const string TEXT_RANGE = "Range:";

        // Quality percentage modifiers, percentage range applied to base stat
        public static Vector2 QUALITY_MODIFIER_TERRIBLE = new Vector2(20f, 40f);
        public static Vector2 QUALITY_MODIFIER_LOW = new Vector2(50f, 90f);
        public static Vector2 QUALITY_MODIFIER_NORMAL = new Vector2(100f, 100f);
        public static Vector2 QUALITY_MODIFIER_HIGH = new Vector2(110f, 150f);
        public static Vector2 QUALITY_MODIFIER_SUPERIOR = new Vector2(160f, 180f);

        // Drop roll chances for CategoryItems (roll 0-99, if under lowest value const select that, else if under next, etc.)
        public const int DROP_ROLL_ARMOUR = 50;
        public const int DROP_ROLL_WEAPON = 100;
        //public const int DROP_ROLL_CONSUMABLE = 100;

        // Drop roll chances for CategoryItems sub-categories (roll 0-99, if under lowest value const select that, else if under next, etc.)
        // CategoryArmour
        public const float DROP_ROLL_HELM = 17.5f;
        public const float DROP_ROLL_CHEST = 47.5f;
        public const float DROP_ROLL_GLOVES = 62.5f;
        public const float DROP_ROLL_BOOTS = 80f;
        public const float DROP_ROLL_SHIELD = 90f;
        public const float DROP_ROLL_AMULET = 95f;
        public const float DROP_ROLL_RING = 100f;
            // TypeArmour
                // Helmet
        public const float DROP_ROLL_POT = 5f;
        public const float DROP_ROLL_HAT = 30f;
        public const float DROP_ROLL_SKULLCAP = 53f;
        public const float DROP_ROLL_HELMET = 70f;
        public const float DROP_ROLL_HELMETFULL = 80f;
        public const float DROP_ROLL_CIRCLET = 90f;
        public const float DROP_ROLL_CROWN = 100f;
                // Chest
        public const float DROP_ROLL_ROBE = 18f;
        public const float DROP_ROLL_ROBEHOODED = 28f;
        public const float DROP_ROLL_LEATHER = 45f;
        public const float DROP_ROLL_LEATHERHARDENED = 57f;
        public const float DROP_ROLL_CHAINMAIL = 72f;
        public const float DROP_ROLL_CHAINMAILFULL = 83f;
        public const float DROP_ROLL_PLATEMAIL = 93f;
        public const float DROP_ROLL_PLATEMAILFULL = 100f;
                // Glove
        public const float DROP_ROLL_GLOVELEATHER = 34f;
        public const float DROP_ROLL_GLOVELEATHERTHICK = 67f;
        public const float DROP_ROLL_GLOVEPLATE = 100f;
                // Boot
        public const float DROP_ROLL_SHOE = 35f;
        public const float DROP_ROLL_BOOT = 70f;
        public const float DROP_ROLL_BOOTPLATE = 100f;
                // Shield
        public const float DROP_ROLL_BUCKLER = 35f;
        public const float DROP_ROLL_KITE = 60f;
        public const float DROP_ROLL_PAVISE = 75f;
        public const float DROP_ROLL_BOUCHE = 88f;
        public const float DROP_ROLL_HEATER = 100f;

            // CategoryWeapon
        public const float DROP_ROLL_RANGED = 40f;
        public const float DROP_ROLL_MELEE = 100f;
            // TypeWeapon
                // Ranged
        public const float DROP_ROLL_SLING = 23f;
        public const float DROP_ROLL_BOWSHORT = 45f;
        public const float DROP_ROLL_BOW = 66f;
        public const float DROP_ROLL_BOWLONG = 78f;
        public const float DROP_ROLL_CROSSBOW = 100f;
                // Melee
        public const float DROP_ROLL_AXE = 4f;
        public const float DROP_ROLL_AXEDOUBLE = 8f;
        public const float DROP_ROLL_BARDICHE = 12f;
        public const float DROP_ROLL_BLADE = 16f;
        public const float DROP_ROLL_CUTLASS = 20f;
        public const float DROP_ROLL_CLUB = 25f;
        public const float DROP_ROLL_DAGGER = 30f;
        public const float DROP_ROLL_DIRK = 34f;
        public const float DROP_ROLL_FLAIL = 38f;
        public const float DROP_ROLL_GLAIVE = 42f;
        public const float DROP_ROLL_GREATSWORD = 46f;
        public const float DROP_ROLL_HALBERD = 50f;
        public const float DROP_ROLL_KNIFE = 55f;
        public const float DROP_ROLL_LONGSWORD = 59f;
        public const float DROP_ROLL_MALLET = 63f;
        public const float DROP_ROLL_MACE = 67f;
        public const float DROP_ROLL_PICKAXE = 71f;
        public const float DROP_ROLL_POLEARM = 75f;
        public const float DROP_ROLL_ROD = 79f;
        public const float DROP_ROLL_SCYTHE = 83f;
        public const float DROP_ROLL_SHORTSWORD = 88f;
        public const float DROP_ROLL_SPEAR = 92f;
        public const float DROP_ROLL_STILETTO = 96f;
        public const float DROP_ROLL_WHIP = 100f;
    }
}