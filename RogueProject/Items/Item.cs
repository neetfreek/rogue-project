﻿/********************************************
* Represents various properties of an item  *
*********************************************/
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject.Items
{
    public class Item
    {
        // General
        public Tile Tile { get => tile; set => tile = value; }
        // Item overview
        public string Name { get => name; set => name = value; }
        public CategoryItem CategoryItem { get => categoryItem; set => categoryItem = value; }
        public QualityItem Quality { get => quality; set => quality = value; }
        // Armour
        public CategoryArmour CategoryArmour { get => categoryArmour; set => categoryArmour = value; }
        public TypeArmour TypeArmour { get => typeArmour; set => typeArmour = value; }
        public ArmourSprite SpriteArmour { get => spriteArmour; set => spriteArmour = value; }
        // Weapon
        public CategoryWeapon CategoryWeapon { get => categoryWeapon; set => categoryWeapon = value; }
        public TypeWeapon TypeWeapon { get => typeWeapon; set => typeWeapon = value; }
        public WeaponSprite SpriteWeapon { get => spriteWeapon; set => spriteWeapon = value; }
        // Stats
        public int Strength { get => strength; set => strength = value; }
        public int Magic { get => magic; set => magic = value; }
        public int Agility { get => agility; set => agility = value; }
        private Tile tile;

        private string name;
        private CategoryItem categoryItem;
        private QualityItem quality;
        private CategoryArmour categoryArmour;
        private TypeArmour typeArmour;
        private ArmourSprite spriteArmour;
        private CategoryWeapon categoryWeapon;
        private TypeWeapon typeWeapon;
        private WeaponSprite spriteWeapon;
        private int strength;
        private int magic;
        private int agility;
    }
}