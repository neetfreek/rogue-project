﻿/********************************************
* Represents a GUI rectangle used for e.g.  * 
*   buttons, menus                          * 
*********************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Textures;

namespace RogueProject.GUI
{
    public class GUIRectangle
    {
        public GUIRectangle Parent;

        // Sub-components
        public List<GUIRectangle> Subcomponents = new List<GUIRectangle>();
        public int SubComponentColumns { get => subComponentColumns; }
        public int SubComponentRows { get => subComponentRows; }

        // States
        public bool Active;
        public bool ShowTextHeader;
        public bool ShowText;
        public bool IsIcon;
        public GUISpriteCategory SpriteCategory { get => spriteCategory; set => spriteCategory = value; }
        public GUIType GuiType { get => guiType; }
        public GUISpriteCategory SpriteCategoryDefault { get => spriteCategoryDefault; }
        // Text
        public string TextHeader { get => textHeader; }
        public string Text { get => text; }
        public float SizeText { get => sizeText; set => sizeText = value; }
        public Color TextHeaderColour = Color.BlanchedAlmond;
        public Color TextColour = Color.BlanchedAlmond;
        public Vector2 PositionText { get => positionText * GUIVariables.WIDTH_HEIGHT_TILE; }
        public Vector2 PositionTextHeader { get => GetPositionTextHeader(); }
        // Transform
        public float Layer { get => layer; }
        public Vector2 PositionScaled { get => position * GUIVariables.WIDTH_HEIGHT_TILE; }
        public Vector2 PositionUnScaled { get => position; }
        public Vector2 PositionOrginal { get => positionOriginal; }
        public Vector2 Position { get => position; }
        public Vector2 Size { get => size; }
        // Interact

        private readonly int subComponentColumns;
        private readonly int subComponentRows;
        private readonly string textHeader;
        private string text;
        private float sizeText;
        private readonly float layer;
        private Vector2 position;
        private Vector2 positionOriginal;
        private Vector2 positionTextHeader;
        private Vector2 positionText;
        private Vector2 size;
        private GUISpriteCategory spriteCategory;
        private readonly GUISpriteCategory spriteCategoryDefault;
        private GUIType guiType;


        public GUIRectangle(string text, Vector2 position, Vector2 size, GUISpriteCategory spriteCategory, GUIType guiType, Vector2 buttonsColsRows)
        {
            subComponentColumns = (int)buttonsColsRows.X;
            subComponentRows = (int)buttonsColsRows.Y;
            this.guiType = guiType;
            this.textHeader = text;
            this.size = size;
            this.position = position;
            positionOriginal = Position;
            spriteCategoryDefault = spriteCategory;
            this.spriteCategory = SpriteCategoryDefault;
            layer = SetLayer();
            SetTextHeaderPosition();
        }


        /********
        * Setup * 
        *********/
        private float SetLayer()
        {
            float layer = 0f;

            switch (GuiType)
            {
                case GUIType.Panel:
                    layer = GUIVariables.LAYER_BACKGROUND;
                    if (TextHeader == GUIVariables.HEADING_PANEL_NEWGAME)
                    {
                        layer = GUIVariables.LAYER_PANEL_BACK;
                    }
                    if (TextHeader == GUIVariables.HEADING_PANEL_NEWGAME_LEFT || TextHeader == GUIVariables.HEADING_PANEL_NEWGAME_RIGHT)
                    {
                        layer = GUIVariables.LAYER_PANEL_FRONT;
                    }
                    break;
                case GUIType.Menu:
                    layer = GUIVariables.LAYER_COMPONENT;
                    break;
                case GUIType.Button:
                    layer = GUIVariables.LAYER_SUBCOMPONENT;
                    break;
                case GUIType.BarBorder:
                    layer = GUIVariables.LAYER_BORDER;
                    break;
                case GUIType.Bar:
                    layer = GUIVariables.LAYER_SUBCOMPONENT;
                    break;
                case GUIType.TextBox:
                    layer = GUIVariables.LAYER_TEXT;
                    break;
            }

            return layer;
        }
        public void SetGUIType(GUIType guiType)
        {
            this.guiType = guiType;
        }
        public void SetTextHeaderPosition()
        {
            switch (GuiType)
            {
                // Move text to mid position X,Y of parent component
                case GUIType.Button:
                    positionTextHeader = position + size * 0.5f;
                    break;
                //Move text to mid position X of parent component
                case GUIType.Menu:
                    positionTextHeader = new Vector2(position.X + size.X * 0.5f, 0f);
                    break;
            }
        }
        public void SetText(string text)
        {
            this.text = text;
        }
        public void AddToText(string textToAdd)
        {
            text += textToAdd;
        }
        public void SetTextGrey(bool state)
        {
            if (state)
            {
                TextColour = Color.DarkGray;
            }
            else
            {
                TextColour = Color.BlanchedAlmond;
            }
        }
        public Vector2 GetPositionTextHeader()
        {
            Vector2 positionHeader = new Vector2(0f, 0f);
            if (Game1.InMainMenu)
            {
                positionHeader = positionTextHeader * GUIVariables.WIDTH_HEIGHT_TILE;
            }
            else
            {
                positionHeader = (positionTextHeader + new Vector2(Size.X * 0.5f , -0.3f))* GUIVariables.WIDTH_HEIGHT_TILE;
            }

            return positionHeader;
        }


        /************
        * Interaction  * 
        *************/
        private Rectangle Collider()
        {
            Rectangle rectangle = new Rectangle(0, 0, 0, 0);

            if (Game1.InGame)
            {
                rectangle = HUDCollider.ColliderGame(this);
            }
            else
            {
                rectangle = HUDCollider.ColliderMenu(this);
            }

            return rectangle;
        }
        public void Resize(Vector2 amount)
        {
            size += amount;
        }
        public void ToggleOn(bool state)
        {
            if (state)
            {
                size = new Vector2(GUIVariables.WIDTH_INSPECTION, GUIVariables.HEIGHT_INSPECTION);
            }
            else
            {
                size = new Vector2(0f, 0f);
            }
        }


        /************
        * Movement  * 
        *************/
        public void MoveComponentTo(Vector2 position)
        {
            this.position = position;
            positionTextHeader = position;
        }
        public void MoveComponentBy(Vector2 amount)
        {
            position += amount;
            positionTextHeader += amount;
        }

        public void MoveTextHeaderTo(Vector2 position)
        {
            positionTextHeader = position;

        }
        public void MoveTextHeaderBy(Vector2 amount)
        {
            positionTextHeader += amount;
        }
        public void MoveTextTo(Vector2 position)
        {
            positionText = position;

        }
        public void MoveTextBy(Vector2 amount)
        {
            positionText += amount;
        }


        /****************
        * Interaction   * 
        *****************/
        public bool MouseOverComponent(Vector2 positionMouse)
        {
            Rectangle collider = Collider();
            if (collider.Contains(positionMouse.X, positionMouse.Y))
            {
                return true;
            }
            return false;
        }
    }
}