﻿/************************************************
* Contain list of GUI components for Draw.cs    *
*   to draw                                     *
*************************************************/
using System.Collections.Generic;

namespace RogueProject.GUI
{
    public static class GUILists
    {
        // Main menu GUI component lists
        public static List<GUIRectangle> GUIComponentsMenuMain = new List<GUIRectangle>();
        public static List<GUIRectangle> GUIComponentsMenuSave = new List<GUIRectangle>();
        public static List<GUIRectangle> GUIComponentsMenuLoad = new List<GUIRectangle>();
        public static List<GUIRectangle> GUIComponentsMenuNewGame = new List<GUIRectangle>();
        
        // In game GUI component lists
        public static List<GUIRectangle> GUIComponentsHUD = new List<GUIRectangle>();

        public static GUIRectangle GetHUDComponentByName(string nameSubComponent)
        {
            GUIRectangle component = GUIComponentsHUD[0];

            foreach (GUIRectangle componentInList in GUIComponentsHUD)
            {
                if (componentInList.TextHeader == nameSubComponent)
                {
                    component = componentInList;
                }
            }
            return component;
        }

        public static void ResetGUILists()
        {
            GUIComponentsMenuMain = new List<GUIRectangle>();
            GUIComponentsMenuSave = new List<GUIRectangle>();
            GUIComponentsMenuLoad = new List<GUIRectangle>();
            GUIComponentsMenuNewGame = new List<GUIRectangle>();        
            GUIComponentsHUD = new List<GUIRectangle>();
        }
    }
}