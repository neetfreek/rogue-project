﻿/****************************************************************************
* Handles converting player input in New Game menu into player statistics   *
* A new object of this class is created every time the New Game panel is    *
*   openend. This calls to populate all fields in the New Game panel with   *
*   default values.                                                         *
* Allows for the player to set their character's name, gender, and          *
*   statistics (agility, magic, strength). The player can re-roll,          *
*   increase and decreate the stat points.                                  *
* The player can begin the game with the new character on clicking the      *
*   Start button. To begin, the player must have entered a name, selected a *
*   gender, and allocated any unallocated stat points.                      *
* Beginning the game calls PlayerSetup.SetupNewPlayer to assign properties  *
*   to character (in character stats).                                      *
*****************************************************************************/
using System;
using System.Text;
using Microsoft.Xna.Framework.Input;
using RogueProject.PlayerCharacter;

namespace RogueProject.GUI
{
    public class NewGameMenu
    {
        private readonly Game1 game;
        private readonly GUIRectangle menuStart;
        private readonly Random random = new Random();
        private readonly StringBuilder stringBuilder = new StringBuilder();

        private GUIRectangle subComponentName;
        private GUIRectangle subComponentGender;
        private GUIRectangle subComponentAgility;
        private GUIRectangle subComponentMagic;
        private GUIRectangle subComponentStrength;
        private GUIRectangle subComponentStatsRemaining;

        public Gender Gender { get => gender; set => gender = value; }        
        public string Name { get => name; set => name = value; }
        public int Agility { get => agility; }
        public int Magic { get => magic; }
        public int Strength { get => strength; }

        private Gender gender;
        private string name;
        private int agility;
        private int magic;
        private int strength;
        private int statsRemaining;

        private bool adjustedName;
        private bool pressedShift;
        private bool pressedCapsLock;
        private string nameOld = "";


        public NewGameMenu(Game1 game, GUIRectangle menuStart)
        {
            this.game = game;
            this.menuStart = menuStart;
            SetupMenuStart();
        }


        public bool SetName(KeyboardState state)
        {
            bool keyPressed = false;

            if (state.GetPressedKeys().Length > 0)
            {
                keyPressed = true;
                nameOld = name;
                CheckClearName();

                HandleKeyPress(state.GetPressedKeys()[0]);
            }
            if (name.Length < PlayerVariables.MAXIMUM_CHARS_NAME && stringBuilder.Length >= 0)
            {
                name = stringBuilder.ToString();
                subComponentName.SetText(name);
            }
            if (nameOld.Length != name.Length)
            {
                nameOld = name;
            }

            return keyPressed;
        }
        public void CheckClearName()
        {
            if (!adjustedName)
            {
                name = "";
                subComponentName.SetTextGrey(false);
                Console.WriteLine($"Cleared name");
                adjustedName = true;

            }
        }
        public void SetGender(Gender gender)
        {
            this.gender = gender;
            subComponentGender.SetText(gender.ToString());
            subComponentGender.SetTextGrey(false);
        }
        public void ReRollStats()
        {
            RollAllStats();
            SetStatTexts();
        }
        public void PlusStat(Stat stat)
        {
            if (statsRemaining > 0)
            {
                switch (stat)
                {
                    case Stat.Agility:
                        if (agility < PlayerVariables.MAXIMUM_STAT)
                        {
                            agility += 1;
                            statsRemaining -= 1;
                        }
                        break;
                    case Stat.Magic:
                        if (magic < PlayerVariables.MAXIMUM_STAT)
                        {
                            magic += 1;
                            statsRemaining -= 1;
                        }
                        break;
                    case Stat.Strength:
                        if (strength < PlayerVariables.MAXIMUM_STAT)
                        {
                            strength += 1;
                            statsRemaining -= 1;
                        }
                        break;
                }
                SetStatTexts();
            }
        }
        public void MinusStat(Stat stat)
        {
            switch (stat)
            {
                case Stat.Agility:
                    if (Agility - 1 >= PlayerVariables.MINIMUM_STAT)
                    {
                        agility -= 1;
                        statsRemaining += 1;
                    }
                    break;
                case Stat.Magic:
                    if (Magic - 1 >= PlayerVariables.MINIMUM_STAT)
                    {
                        magic -= 1;
                        statsRemaining += 1;                        
                    }
                    break;
                case Stat.Strength:
                    if (Strength - 1 >= PlayerVariables.MINIMUM_STAT)
                    {
                        strength -= 1;
                        statsRemaining += 1;
                    }
                    break;
            }
            SetStatTexts();
        }

        public void StartNameGame()
        {
            if (name.Length > 0 && statsRemaining == 0 &&  Gender != Gender.None)
            {
                game.NewGame();
                game.PlayerSetup.SetupNewPlayer(Name, Gender, Agility, Magic, Strength);
            }
        }

        /********
        * Setup *
        *********/
        private void SetupMenuStart()
        {
            SetSubComponentReferences();
            SetupSubComponents();
        }
        private void SetSubComponentReferences()
        {
            foreach (GUIRectangle subComponent in menuStart.Subcomponents)
            {
                switch (subComponent.TextHeader)
                {
                    case GUIVariables.HEADING_FIELD_NAME:
                        subComponentName = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_GENDER:
                        subComponentGender = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_AGILITY:
                        subComponentAgility = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_MAGIC:
                        subComponentMagic = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_STRENGTH:
                        subComponentStrength = subComponent;
                        break;
                    case GUIVariables.TEXT_PROMPT_STATS_REMAINING:
                        subComponentStatsRemaining = subComponent;
                        break;
                }
            }
        }
        private void SetupSubComponents()
        {
            // Name
            name = GUIVariables.TEXT_PROMPT_CLICK;
            nameOld = GUIVariables.TEXT_PROMPT_CLICK;
            subComponentName.SetTextGrey(true);
            subComponentName.SetText(GUIVariables.TEXT_PROMPT_CLICK);
            // Gender
            subComponentGender.SetTextGrey(true);
            subComponentGender.SetText(GUIVariables.TEXT_PROMPT_SELECT_ICON);
            // Stats
            RollAllStats();
            SetStatTexts();
        }


        /****************
        * Name helpers  *
        *****************/
        private void HandleKeyPress(Keys key)
        {
            bool keySpecial = false;

            switch (key)
            {
                case Keys.Back:
                    if (stringBuilder.Length - 1 >= 0)
                    {
                        stringBuilder.Remove(stringBuilder.Length - 1, 1);
                        name = stringBuilder.ToString();
                        subComponentName.SetText(name);
                        nameOld = name;
                    }
                    keySpecial = true;
                    break;
                case Keys.CapsLock:
                    pressedCapsLock = !pressedCapsLock;
                    keySpecial = true;
                    break;
                case Keys.Delete:
                    stringBuilder.Clear();
                    name = stringBuilder.ToString();
                    subComponentName.SetText(name);
                    nameOld = name;
                    keySpecial = true;
                    break;
                case Keys.LeftShift:
                    pressedShift = true;
                    keySpecial = true;
                    break;
                case Keys.RightShift:
                    pressedShift = true;
                    keySpecial = true;
                    break;
                case Keys.Space:
                    stringBuilder.Append(" ");
                    keySpecial = true;
                    break;
            }

            if (!keySpecial)
            {
                if (ValidKey(key) && name.Length + 1 <= PlayerVariables.MAXIMUM_CHARS_NAME)
                {
                    string keyPressed = key.ToString();
                    AddTextToStringBuilder(keyPressed);
                }
            }

            if (key != Keys.LeftShift && key != Keys.RightShift)
            {
                pressedShift = false;
            }
        }
        private void AddTextToStringBuilder(string keyPressed)
        {
            if (!pressedCapsLock)
            {
                if (pressedShift)
                {
                    stringBuilder.Append(keyPressed);
                }
                else
                {
                    stringBuilder.Append(keyPressed.ToLower());
                }
            }
            else
            {
                if (pressedShift)
                {
                    stringBuilder.Append(keyPressed.ToLower());
                }
                else
                {
                    stringBuilder.Append(keyPressed);
                }
            }
        }
        private bool ValidKey(Keys key)
        {
            switch (key)
            {
                case Keys.A:
                    return true;
                case Keys.B:
                    return true;
                case Keys.C:
                    return true;
                case Keys.D:
                    return true;
                case Keys.E:
                    return true;
                case Keys.F:
                    return true;
                case Keys.G:
                    return true;
                case Keys.H:
                    return true;
                case Keys.I:
                    return true;
                case Keys.J:
                    return true;
                case Keys.K:
                    return true;
                case Keys.L:
                    return true;
                case Keys.M:
                    return true;
                case Keys.N:
                    return true;
                case Keys.O:
                    return true;
                case Keys.P:
                    return true;
                case Keys.Q:
                    return true;
                case Keys.R:
                    return true;
                case Keys.S:
                    return true;
                case Keys.T:
                    return true;
                case Keys.U:
                    return true;
                case Keys.V:
                    return true;
                case Keys.W:
                    return true;
                case Keys.X:
                    return true;
                case Keys.Y:
                    return true;
                case Keys.Z:
                    return true;
                case Keys.OemQuotes:
                    return true;
            }

            return false;
        }


        /************************
        * Stat point helpers    *
        *************************/
        private void RollAllStats()
        {
            agility = RollStat();
            magic = RollStat();
            strength = RollStat();
        }
        private int RollStat()
        {
            int roll = 0;
            roll = random.Next(PlayerVariables.MINIMUM_STAT, PlayerVariables.MAXIMUM_STAT + 1);
            return roll;
        }
        private void SetStatTexts()
        {
            subComponentAgility.SetText(Agility.ToString());
            subComponentMagic.SetText(Magic.ToString());
            subComponentStrength.SetText(Strength.ToString());
            subComponentStatsRemaining.SetText($"({statsRemaining.ToString()})");
            if (statsRemaining == 0)
            {
                subComponentStatsRemaining.SetTextGrey(true);
            }
            else
            {
                subComponentStatsRemaining.SetTextGrey(false);
            }
        }
    }
}