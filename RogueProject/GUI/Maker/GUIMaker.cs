﻿/********************************************************
* Handles creation of GUI components and subcomponents  *
*********************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Textures;

namespace RogueProject.GUI
{
    public class GUIMaker
    {
        private readonly Game1 game;

        public GUIMaker(Game1 game)
        {
            this.game = game; 
        }


        /********************************************
        * Handle create main menu GUI components    *
        *********************************************/
        public void CreateMenuMain()
        {
            int numberButtons = 4;
            if (Game1.GameStarted)
            {
                numberButtons = 5;
            }

            //Background panel
            GUILists.GUIComponentsMenuMain.Add(Panel(GUIVariables.HEADING_BACKGROUND, GUISpriteCategory.RectBlackGrey, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y)));
            // Main menu
            GUILists.GUIComponentsMenuMain.Add(MenuWithButtons(GUIVariables.HEADING_MENU_MAIN,
                GUISpriteCategory.RectGreyLight, GUISpriteCategory.RectGrey,
                new Vector2(GUIVariables.WIDTH_BUTTON_GIANT, GUIVariables.HEIGHT_BUTTON), new Vector2(1, numberButtons)));

            game.Camera.Move(new Vector2(0f, 0f));
        }
        public void CreateMenuSave()
        {
            GUILists.GUIComponentsMenuSave.Add(Panel(GUIVariables.HEADING_BACKGROUND, GUISpriteCategory.RectBlackGrey, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y)));
            GUILists.GUIComponentsMenuSave.Add(MenuWithButtons(GUIVariables.HEADING_MENU_SAVE,
                GUISpriteCategory.RectGreyLight, GUISpriteCategory.RectGrey,
                new Vector2(GUIVariables.WIDTH_BUTTON_GIANT, GUIVariables.HEIGHT_BUTTON), new Vector2(1, 5)));

            game.Camera.Move(new Vector2(0f, 0f));
        }
        public void CreateMenuLoad()
        {
            GUILists.GUIComponentsMenuLoad.Add(Panel(GUIVariables.HEADING_BACKGROUND, GUISpriteCategory.RectBlackGrey, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y)));
            GUILists.GUIComponentsMenuLoad.Add(MenuWithButtons(GUIVariables.HEADING_MENU_LOAD,
                GUISpriteCategory.RectGreyLight, GUISpriteCategory.RectGrey,
                new Vector2(GUIVariables.WIDTH_BUTTON_GIANT, GUIVariables.HEIGHT_BUTTON), new Vector2(1, 5)));

            game.Camera.Move(new Vector2(0f, 0f));
        }
        public void CreateMenuNewGame()
        {

            // Create panels
            GUIRectangle panelBackground = Panel(GUIVariables.HEADING_BACKGROUND, GUISpriteCategory.RectBlackGrey, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            GUIRectangle panelMenu = Panel(GUIVariables.HEADING_PANEL_NEWGAME, GUISpriteCategory.RectGreyLight, new Vector2(GUIVariables.WIDTH_PANEL_NEWGAME, GUIVariables.HEIGHT_PANEL_NEWGAME));
            panelMenu.SetGUIType(GUIType.Panel);
            panelMenu.ShowTextHeader = true;;
                // Set text
            panelMenu.MoveTextHeaderBy(new Vector2((panelMenu.Size.X * 0.5f) - ((game.Textures.Font.MeasureString(panelMenu.TextHeader).X * 0.5f) / GlobalVariables.ScaledSprite), (game.Textures.Font.MeasureString(panelMenu.TextHeader).Y) / GlobalVariables.ScaledSprite * 2f));
            AddNewGamePanelButtons(panelMenu);
            AddNewGamePanelFields(panelMenu);
            AddNewGamePanelPanels(panelMenu);
            AddNewGamePanelText(panelMenu);

            // Move subComponents
            foreach (GUIRectangle subComponent in panelMenu.Subcomponents)
            {
                if (subComponent.TextHeader != GUIVariables.HEADING_PANEL_NEWGAME_LEFT &&
                    subComponent.TextHeader != GUIVariables.HEADING_PANEL_NEWGAME_RIGHT &&
                    subComponent.TextHeader != GUIVariables.HEADING_FIELD_AGILITY &&
                    subComponent.TextHeader != GUIVariables.HEADING_FIELD_MAGIC &&
                    subComponent.TextHeader != GUIVariables.HEADING_FIELD_STRENGTH &&
                    subComponent.TextHeader != GUIVariables.HEADING_FIELD_GENDER &&
                    subComponent.TextHeader != GUIVariables.HEADING_FIELD_NAME &&
                    subComponent.TextHeader != GUIVariables.TEXT_PROMPT_STATS_REMAINING)
                {
                    subComponent.ShowTextHeader = true;
                }
                subComponent.Parent = panelMenu;
                subComponent.MoveComponentTo(GUIMakerHelper.SetNewGameSubComponentPositions(subComponent));
                subComponent.MoveTextHeaderTo(subComponent.Position);
                if (subComponent.GuiType == GUIType.Button)
                {
                    GUIMakerHelper.CenterComponentHeader(subComponent);
                    subComponent.Active = true;
                }
                if (subComponent.TextHeader == GUIVariables.HEADING_FIELD_AGILITY ||
                    subComponent.TextHeader == GUIVariables.HEADING_FIELD_MAGIC ||
                    subComponent.TextHeader == GUIVariables.HEADING_FIELD_STRENGTH ||
                    subComponent.TextHeader == GUIVariables.HEADING_FIELD_NAME ||
                    subComponent.TextHeader == GUIVariables.HEADING_FIELD_GENDER ||
                    subComponent.TextHeader == GUIVariables.TEXT_PROMPT_STATS_REMAINING)
                {
                    subComponent.SetText("");
                    subComponent.ShowText = true;
                    subComponent.MoveTextTo(GUIMakerHelper.SetNewGameSubComponentPositions(subComponent));
                }
            }

            GUILists.GUIComponentsMenuNewGame.Add(panelBackground);
            GUILists.GUIComponentsMenuNewGame.Add(panelMenu);

            // Create new NewGameMenu object in GUIAction when making new New Game menu 
            game.GUIAction.NewGameMenu = new NewGameMenu(game, panelMenu);

            game.Camera.Move(new Vector2(0f, 0f));
        }
        private void AddNewGamePanelPanels(GUIRectangle panelMenu)
        {
            // Add panels
            GUIRectangle panelFrontLeft = Panel(GUIVariables.HEADING_PANEL_NEWGAME_LEFT, GUISpriteCategory.RectGreyDark, new Vector2(GUIVariables.WIDTH_PANEL_NEWGAME_LEFT, GUIVariables.HEIGHT_PANEL_NEWGAME_LEFT));
            panelMenu.SetGUIType(GUIType.Panel);
            panelMenu.Subcomponents.Add(panelFrontLeft);
            GUIRectangle panelFrontRight = Panel(GUIVariables.HEADING_PANEL_NEWGAME_RIGHT, GUISpriteCategory.RectGreyDark, new Vector2(GUIVariables.WIDTH_PANEL_NEWGAME_RIGHT, GUIVariables.HEIGHT_PANEL_NEWGAME_RIGHT));
            panelMenu.SetGUIType(GUIType.Panel);
            panelMenu.Subcomponents.Add(panelFrontRight);
        }
        private void AddNewGamePanelText(GUIRectangle panelMenu)
        {
            // Add text
            GUIRectangle textName = Panel(GUIVariables.TEXT_PROMPT_NAME, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textName);
            GUIRectangle textGender = Panel(GUIVariables.TEXT_PROMPT_GENDER, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textGender);
            GUIRectangle textStats = Panel(GUIVariables.TEXT_PROMPT_STATS, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textStats);
            GUIRectangle textAgility = Panel(GUIVariables.TEXT_AGILITY, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textAgility);
            GUIRectangle textMagic = Panel(GUIVariables.TEXT_MAGIC, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textMagic);
            GUIRectangle textStrenth = Panel(GUIVariables.TEXT_STRENGTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(textStrenth);
        }
        private void AddNewGamePanelButtons(GUIRectangle panelMenu)
        {

            // Buttons icons
            Vector2 buttonSize = new Vector2(GUIVariables.WIDTH_HEIGHT_ICON, GUIVariables.WIDTH_HEIGHT_ICON);
            GUIRectangle iconFemale = ButtonPanel(GUIVariables.TEXT_ICON_FEMALE, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconFemale);
            iconFemale.IsIcon = true;
            GUIRectangle iconMale = ButtonPanel(GUIVariables.TEXT_ICON_MALE, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconMale);
            iconMale.IsIcon = true;
            // NEW PLUS MINUS ICONS
            GUIRectangle iconPlusAgility= ButtonPanel(GUIVariables.TEXT_ICON_PLUS_AGILITY, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconPlusAgility);
            iconPlusAgility.IsIcon = true;
            GUIRectangle iconPlusMagic = ButtonPanel(GUIVariables.TEXT_ICON_PLUS_MAGIC, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconPlusMagic);
            iconPlusMagic.IsIcon = true;
            GUIRectangle iconPlusStrength = ButtonPanel(GUIVariables.TEXT_ICON_PLUS_STRENGTH, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconPlusStrength);
            iconPlusStrength.IsIcon = true;
            GUIRectangle icoMinusAgility = ButtonPanel(GUIVariables.TEXT_ICON_MINUS_AGILITY, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(icoMinusAgility);
            icoMinusAgility.IsIcon = true;
            GUIRectangle iconMinusMagic = ButtonPanel(GUIVariables.TEXT_ICON_MINUS_MAGIC, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconMinusMagic);
            iconMinusMagic.IsIcon = true;
            GUIRectangle iconMinusStrength = ButtonPanel(GUIVariables.TEXT_ICON_MINUS_STRENGTH, panelMenu, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(iconMinusStrength);
            iconMinusStrength.IsIcon = true;
            // Buttons standard
            GUIRectangle buttonStart = ButtonPanel(GUIVariables.TEXT_START, panelMenu, new Vector2(GUIVariables.WIDTH_BUTTON_LARGE, GUIVariables.HEIGHT_BUTTON), new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(buttonStart);
            GUIRectangle buttonRoll = ButtonPanel(GUIVariables.TEXT_PROMPT__STATS_ROLL, panelMenu, new Vector2(GUIVariables.WIDTH_BUTTON_MEDIUM, GUIVariables.HEIGHT_BUTTON), new Vector2(0f, 0f), GUISpriteCategory.RectGrey, 0);
            panelMenu.Subcomponents.Add(buttonRoll);
        }
        private void AddNewGamePanelFields(GUIRectangle panelMenu)
        {
            // Add text fields
            GUIRectangle fieldName = Panel(GUIVariables.HEADING_FIELD_NAME, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));

            fieldName.Active = true;
            panelMenu.Subcomponents.Add(fieldName);
            GUIRectangle fieldGender = Panel(GUIVariables.HEADING_FIELD_GENDER, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(fieldGender);
            GUIRectangle fieldAgility = Panel(GUIVariables.HEADING_FIELD_AGILITY, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            panelMenu.Subcomponents.Add(fieldAgility);
            GUIRectangle fieldMagic = Panel(GUIVariables.HEADING_FIELD_MAGIC, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            fieldAgility.ShowText = true;
            panelMenu.Subcomponents.Add(fieldMagic);
            GUIRectangle fieldStrength = Panel(GUIVariables.HEADING_FIELD_STRENGTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            fieldAgility.ShowText = true;
            panelMenu.Subcomponents.Add(fieldStrength);
            GUIRectangle fieldStatsRemaining = Panel(GUIVariables.TEXT_PROMPT_STATS_REMAINING, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            fieldStatsRemaining.ShowText = true;
            panelMenu.Subcomponents.Add(fieldStatsRemaining);
        }

        /************************************************************
        * Handle create toggling in-game HUD GUI component parts    *
        *************************************************************/
        public void CreateCharacterSheet()
        {
            GUIRectangle characterSheet = Panel(GUIVariables.HEADING_CHARACTER_SHEET, GUISpriteCategory.RectBlackGrey, new Vector2(GUIVariables.WIDTH_CHARACTER_SHEET, GUIVariables.HEIGHT_CHARACTER_SHEET));
            characterSheet.ShowTextHeader = true;
            characterSheet.SizeText = GUIVariables.SCALE_HEADING;
            CreateCharacterSheetHeadings(characterSheet);
            CreateCharacterSheetFields(characterSheet);

            GUILists.GUIComponentsHUD.Add(characterSheet);
            game.PlayerStats.characterSheet.AssignCharacterSheetComponents(characterSheet);
        }
        private void CreateCharacterSheetHeadings(GUIRectangle characterSheet)
        {
            // Stats
            GUIRectangle textAgility = Panel(GUIVariables.TEXT_AGILITY, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textAgility);
            GUIRectangle textMagic = Panel(GUIVariables.TEXT_MAGIC, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textMagic);
            GUIRectangle textStrength = Panel(GUIVariables.TEXT_STRENGTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textStrength);
            // Vital stats
            GUIRectangle textHealth = Panel(GUIVariables.HEADING_HEALTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textHealth);
            GUIRectangle textMana = Panel(GUIVariables.HEADING_MANA, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textMana);
            // Combat
            GUIRectangle textDamage = Panel(GUIVariables.HEADING_DAMAGE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textDamage);
            GUIRectangle textDefence = Panel(GUIVariables.HEADING_DEFENCE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textDefence);
            // Game info
            GUIRectangle textExperience = Panel(GUIVariables.HEADING_EXPERIENCE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textExperience);
            GUIRectangle textArea = Panel(GUIVariables.HEADING_AREA, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textArea);

            foreach (GUIRectangle subComponent in characterSheet.Subcomponents)
            {
                subComponent.SizeText = GUIVariables.SCALE_INSPECTION;

                if (GUIMakerHelper.SetCharacterSheetText(subComponent) != null)
                {
                    if (GUIMakerHelper.SetCharacterSheetText(subComponent) != "")
                    {
                        subComponent.SetText(GUIMakerHelper.SetCharacterSheetText(subComponent));
                        subComponent.TextColour = Color.Gray;
                    }
                }
                subComponent.ShowText = true;
            }
        }
        private void CreateCharacterSheetFields(GUIRectangle characterSheet)
        {
            // Name
            GUIRectangle textName = Panel(GUIVariables.HEADING_FIELD_NAME, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, textName);
            textName.SetText(game.PlayerStats.Name);
            // Stats
            GUIRectangle fieldAgility = Panel(GUIVariables.HEADING_FIELD_AGILITY, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldAgility);
            GUIRectangle fieldMagic = Panel(GUIVariables.HEADING_FIELD_MAGIC, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldMagic);
            GUIRectangle fieldStrength = Panel(GUIVariables.HEADING_FIELD_STRENGTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldStrength);
            // Vital stats
            GUIRectangle fieldHealth = Panel(GUIVariables.FIELD_HEALTH, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldHealth);
            GUIRectangle fieldMana = Panel(GUIVariables.FIELD_MANA, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldMana);
            // Combat
            GUIRectangle fieldDamage = Panel(GUIVariables.FIELD_DAMAGE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldDamage);
            GUIRectangle fiedDefence = Panel(GUIVariables.FIELD_DEFENCE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fiedDefence);
            // Game info
            GUIRectangle fieldExperience = Panel(GUIVariables.FIELD_EXPERIENCE, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldExperience);
            GUIRectangle fieldArea = Panel(GUIVariables.FIELD_AREA, GUISpriteCategory.None, new Vector2(GlobalVariables.TILES_SCREEN_X, GlobalVariables.TILES_SCREEN_Y));
            SetParent(characterSheet, fieldArea);

            foreach (GUIRectangle subComponent in characterSheet.Subcomponents)
            {
                subComponent.SizeText = GUIVariables.SCALE_INSPECTION;

                if (subComponent.TextHeader.Contains("Field"))
                {
                    subComponent.SetText("");
                    subComponent.TextColour = Color.BlanchedAlmond;
                    subComponent.ShowText = true;
                }
            }
            textName.SetText(game.PlayerStats.Name);
            textName.SizeText = GUIVariables.SCALE_STANDARD;
            textName.TextColour = Color.DarkGoldenrod;
        }
        public void CreateInventory()
        {
            Vector2 sizeCellGrid = new Vector2(GUIVariables.WIDTH_HEIGHT_ICON, GUIVariables.WIDTH_HEIGHT_ICON);

            GUIRectangle inventory = Grid(GUIVariables.HEADING_INVENTORY, GUISpriteCategory.RectBlackGrey, GUISpriteCategory.Square, sizeCellGrid, new Vector2(GUIVariables.WIDTH_INVENTORY, GUIVariables.HEIGHT_INVENTORY));
            GUIRectangle equipment = Grid(GUIVariables.HEADING_EQUIPMENT, GUISpriteCategory.RectBlackGrey, GUISpriteCategory.Square, sizeCellGrid, new Vector2(GUIVariables.WIDTH_EQUIPMENT, GUIVariables.HEIGHT_EQUIPMENT));
            GUIRectangle trinkets = Grid(GUIVariables.HEADING_TRINKETS, GUISpriteCategory.RectBlackGrey, GUISpriteCategory.Square, sizeCellGrid, new Vector2(GUIVariables.WIDTH_TRINKETS, GUIVariables.HEIGHT_TRINKETS));
            GUIRectangle inspection = Panel(GUIVariables.HEADING_INSEPCTION, GUISpriteCategory.RectBlackGrey, new Vector2(GUIVariables.WIDTH_INSPECTION, GUIVariables.HEIGHT_INSPECTION));
            inspection.ShowText = true;

            GUILists.GUIComponentsHUD.Add(inventory);
            GUILists.GUIComponentsHUD.Add(equipment);
            GUILists.GUIComponentsHUD.Add(trinkets);
            GUILists.GUIComponentsHUD.Add(inspection);

            //CreateInspector();
        }

        /********************************************************
        * Handle create static in-game HUD GUI component parts  *
        *********************************************************/
        public void HandleCreateHUD()
        {   // Top panel
            CreateTopPanel();
            CreateBottomPanel();
            CreateTextBoxes();
        }
        private void CreateTopPanel()
        {
            GUIRectangle topPanel = HUDPanel(GUIVariables.HEADING_PANEL_HUD_TOP, GUISpriteCategory.RectBlackGrey,
                new Vector2(GlobalVariables.TILES_SCREEN_X, 2f), 3);

            GUIRectangle borderBarExperience = Bar(GUIVariables.HEADING_BAR_BORDER_EXPERIENCE, GUISpriteCategory.BarBorder,
                GUIType.BarBorder, new Vector2(7, 1));
            GUIRectangle barExperience= Bar(GUIVariables.HEADING_BAR_EXPERIENCE, GUISpriteCategory.BarYellow, GUIType.Bar,
                borderBarExperience.Size);
            barExperience.Active = true;

            GUILists.GUIComponentsHUD.Add(topPanel);
            topPanel.Subcomponents.Add(borderBarExperience);
            topPanel.Subcomponents.Add(barExperience);
        }
        private void CreateBottomPanel()
        {
            GUIRectangle bottomPanel = HUDPanel(GUIVariables.HEADING_PANEL_HUD_BOTTOM, GUISpriteCategory.RectBlackGrey, new Vector2(GlobalVariables.TILES_SCREEN_X, 2f), 0);

            GUIRectangle borderBarHealth = Bar(GUIVariables.HEADING_BAR_BORDER_HEALTH, GUISpriteCategory.BarBorder, GUIType.BarBorder, new Vector2(5, 1));
            GUIRectangle barHealth = Bar(GUIVariables.HEADING_BAR_HEALTH, GUISpriteCategory.BarRed, GUIType.Bar, borderBarHealth.Size);
            barHealth.Active = true;

            GUIRectangle borderBarMana = Bar(GUIVariables.HEADING_BAR_BORDER_MANA, GUISpriteCategory.BarBorder, GUIType.BarBorder, new Vector2(5, 1));
            GUIRectangle barMana = Bar(GUIVariables.HEADING_BAR_MANA, GUISpriteCategory.BarBlue, GUIType.Bar, borderBarMana.Size);
            barMana.Active = true;

            GUILists.GUIComponentsHUD.Add(bottomPanel);
            bottomPanel.Subcomponents.Add(borderBarHealth);
            bottomPanel.Subcomponents.Add(borderBarMana);
            bottomPanel.Subcomponents.Add(barHealth);
            bottomPanel.Subcomponents.Add(barMana);
        }
        private void CreateTextBoxes()
        {
            // Messages textbox
            GUIRectangle textBox = HUDPanel(GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE, GUISpriteCategory.RectBlackGrey, new Vector2(10, 4f), 0);
            textBox.SetGUIType(GUIType.TextBox);
            TextBox textBoxMessage = new TextBox(game, textBox);

            if (HUDVariables.MessageTemporary.Count > 0)
            {
                textBoxMessage.RepopulateList(HUDVariables.MessageTemporary);
            }
            textBoxMessage.UpdateMessage($"{HUDVariables.ENTRANCE} {"Area Name"}");

            textBox.ShowText = true;
            game.TextBoxes.TextBoxMessages = textBoxMessage;

            // Status textbox
            GUIRectangle textBoxStatus = HUDPanel(GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS, GUISpriteCategory.RectBlackGrey, new Vector2(4, 2f), 0);
            textBoxStatus.SetGUIType(GUIType.TextBox);
            TextBox textBoxStatusmessage = new TextBox(game, textBoxStatus);
            game.TextBoxes.TextBoxStatus = textBoxStatusmessage;
            textBoxStatus.ShowText = true;
            game.PlayerAction.UpdateStatusTextNewTurn();

            GUILists.GUIComponentsHUD.Add(textBox);
            GUILists.GUIComponentsHUD.Add(textBoxStatus);
        }

        /****************************
        * Create menu components    *
        *****************************/
        private GUIRectangle MenuWithButtons(string textMenuHeading, GUISpriteCategory spriteCategoryMenu,
            GUISpriteCategory spriteCategoryButtons, Vector2 buttonSize, Vector2 buttonsColsRows)
        {
            float width = (buttonSize.X * buttonsColsRows.X)
                + ((GUIVariables.BORDER_COMPONENT * buttonsColsRows.X) + GUIVariables.BORDER_COMPONENT);
            float height = (buttonSize.Y * buttonsColsRows.Y)
                + ((GUIVariables.BORDER_COMPONENT * buttonsColsRows.Y) + GUIVariables.BORDER_COMPONENT);

            GUIRectangle menuRectangle = new GUIRectangle(textMenuHeading, new Vector2(0f, 0f), new Vector2(width, height), spriteCategoryMenu, GUIType.Menu, buttonsColsRows)
            {
                ShowTextHeader = true
            };
            menuRectangle.MoveComponentBy(-menuRectangle.Size * 0.5f);

            for (int numberButton = 0; numberButton < buttonsColsRows.X * buttonsColsRows.Y; numberButton++)
            {
                menuRectangle.Subcomponents.Add(ButtonMenu(GUIMakerHelper.ButtonText(menuRectangle, numberButton), menuRectangle, buttonSize, spriteCategoryButtons, numberButton, buttonsColsRows));
            }

            return menuRectangle;
        }
        private GUIRectangle Grid(string textMenuHeading, GUISpriteCategory spriteCategoryPanel,
            GUISpriteCategory spriteCategoryButtons, Vector2 buttonSize, Vector2 buttonsColsRows)
        {
            float width = (GUIVariables.BORDER_COMPONENT + (buttonSize.X * buttonsColsRows.X) + GUIVariables.BORDER_COMPONENT);
            float height = (buttonSize.Y * buttonsColsRows.Y + GUIVariables.BORDER_GRID_BOT);

            GUIRectangle grid = new GUIRectangle(textMenuHeading, new Vector2(0f, 0f), new Vector2(width, height), spriteCategoryPanel, GUIType.Menu, buttonsColsRows)
            {
                ShowTextHeader = true                
            };

            for (int numberButton = 0; numberButton < buttonsColsRows.X * buttonsColsRows.Y; numberButton++)
            {
                GUIRectangle button = ButtonGrid(GUIMakerHelper.ButtonText(grid, numberButton), grid, buttonSize, spriteCategoryButtons, numberButton, buttonsColsRows);
                grid.Subcomponents.Add(button);
                button.Parent = grid;
            }

            return grid;
        }
        private GUIRectangle Panel(string textPanelHeading, GUISpriteCategory spriteCategoryRectangle, Vector2 size)
        {
            GUIRectangle panel = new GUIRectangle(textPanelHeading, new Vector2(0f, 0f), size, spriteCategoryRectangle, GUIType.Panel, new Vector2(0f,0f));
            panel.MoveComponentTo(GUIMakerHelper.MenuPanelPosition(panel));

            return panel;
        }

        private GUIRectangle Bar(string textPanelHeading, GUISpriteCategory spriteCategoryRectangle,
            GUIType guiType, Vector2 size)
        {
            GUIRectangle border = new GUIRectangle(textPanelHeading, new Vector2(0f, 0f), size, spriteCategoryRectangle, guiType, new Vector2(0f, 0f));

            return border;
        }

        /************************
        * Create HUD components *
        *************************/
        // Top bar, bottom bar, textbox
        private GUIRectangle HUDPanel(string textPanelHeading, GUISpriteCategory spriteCategoryRectangle,
            Vector2 size, int numberButtons)
        {
            GUIRectangle hudPanel = Panel(textPanelHeading, spriteCategoryRectangle, size);

            Vector2 buttonSize = new Vector2(GUIVariables.WIDTH_HEIGHT_ICON, GUIVariables.WIDTH_HEIGHT_ICON);

            for (int buttonNumber = 0; buttonNumber < numberButtons; buttonNumber++)
            {
                GUIRectangle buttonPanel = ButtonPanel(GUIMakerHelper.ButtonText(hudPanel, buttonNumber), hudPanel, buttonSize, new Vector2(0f, 0f), GUISpriteCategory.RectGreyDark, buttonNumber);
                buttonPanel.IsIcon = true;
                hudPanel.Subcomponents.Add(buttonPanel);
            }

            return hudPanel;
        }

        /************************
        * Create sub-components *
        *************************/
        private GUIRectangle ButtonMenu(string text, GUIRectangle rectangle, Vector2 size, GUISpriteCategory spriteCategory,
            int buttonNumber, Vector2 buttonsColsRows)
        {
            Vector2 buttonPosition = GUIMakerHelper.ButtonPositionMenu(rectangle, buttonNumber, buttonsColsRows, size);

            GUIRectangle buttonRectangle = new GUIRectangle(text, buttonPosition, size, spriteCategory, GUIType.Button, new Vector2(0f, 0f));
            GUIMakerHelper.SetButtonActiveState(rectangle, buttonRectangle, buttonNumber);
            buttonRectangle.ShowTextHeader = true;
            return buttonRectangle;
        }
        private GUIRectangle ButtonGrid(string text, GUIRectangle rectangle, Vector2 size, GUISpriteCategory spriteCategory,
            int buttonNumber, Vector2 buttonsColsRows)
        {
            Vector2 buttonPosition = rectangle.Position + GUIMakerHelper.ButtonPositionGrid(rectangle, buttonNumber, buttonsColsRows, size);
            GUIRectangle buttonRectangle = new GUIRectangle(text, buttonPosition, size, spriteCategory,
                GUIType.Button, new Vector2(0f, 0f))
            {
                Active = true
            };
            return buttonRectangle;
        }

        private GUIRectangle ButtonPanel(string text, GUIRectangle rectangle, Vector2 size,Vector2 position,
            GUISpriteCategory spriteCategory, int buttonNumber)
        {
            GUIRectangle buttonRectangle = new GUIRectangle(text, position, size, spriteCategory, GUIType.Button, new Vector2(0f, 0f));
            GUIMakerHelper.SetButtonActiveState(rectangle, buttonRectangle, buttonNumber);

            return buttonRectangle;
        }


        /************
        * Helpers   *
        *************/
        private void SetParent(GUIRectangle parent, GUIRectangle child)
        {
            child.Parent = parent;
            parent.Subcomponents.Add(child);
        }
    }
}