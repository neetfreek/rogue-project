﻿/********************************************
* Contains all references to GUI variables  * 
*********************************************/
using RogueProject.Common;

namespace RogueProject.GUI
{
    public class GUIVariables
    {
        // Sizes
            // General
        public const int WIDTH_HEIGHT_TILE = (int)(GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE);
            // Text
        public const float SCALE_HEADING = 1.25f;
        public const float SCALE_INSPECTION = 0.75f;
        public const float SCALE_STANDARD = 1f;
        public const float SCALE_TEXT_BOX = 0.65f;
            // Buttons
        public const float BORDER_HEADING = 1.5f;
        public const float BORDER_COMPONENT = 1f;
        public const float BORDER_GRID_BOT = 2f;
        public const float HEIGHT_BUTTON = 2f;
        public const float WIDTH_BUTTON_SMALL = 2f;
        public const float WIDTH_BUTTON_MEDIUM = 3f;
        public const float WIDTH_BUTTON_LARGE = 4f;
        public const float WIDTH_BUTTON_GIANT = 5f;
            // Menu
        public const float WIDTH_PANEL_NEWGAME = 24f;
        public const float WIDTH_PANEL_NEWGAME_LEFT = 11f;
        public const float WIDTH_PANEL_NEWGAME_RIGHT = 11f;
        public const float HEIGHT_PANEL_NEWGAME = 12f;
        public const float HEIGHT_PANEL_NEWGAME_LEFT = 5f;
        public const float HEIGHT_PANEL_NEWGAME_RIGHT = 8f;
            // HUD panels
        public const float WIDTH_INSPECTION = 5f;
        public const float HEIGHT_INSPECTION = 6f;
        public const float WIDTH_CHARACTER_SHEET = 10f;
        public const float HEIGHT_CHARACTER_SHEET = 8f;
        // Grid
        public const float CAPACITY_INVENTORY = 32f;
        public const float CAPACITY_EQUIPMENT = 6f;
        public const float CAPACITY_TRINKETS = 4f;
        public const float WIDTH_INVENTORY = 8f;
        public const float WIDTH_EQUIPMENT = 6f;
        public const float WIDTH_TRINKETS = 4f;
        public static float HEIGHT_INVENTORY = CAPACITY_INVENTORY / WIDTH_INVENTORY;
        public static float HEIGHT_EQUIPMENT = CAPACITY_EQUIPMENT / WIDTH_EQUIPMENT;
        public static float HEIGHT_TRINKETS = CAPACITY_TRINKETS / WIDTH_TRINKETS;
            // Icons
        public const float WIDTH_HEIGHT_ICON = 1f;
       
        // Layers
        public const float LAYER_BACKGROUND = 0.5f;
        public const float LAYER_PANEL_BACK = 0.55f;
        public const float LAYER_PANEL_FRONT = 0.58f;
        public const float LAYER_COMPONENT = 0.6f;
        public const float LAYER_SUBCOMPONENT = 0.7f;
        public const float LAYER_BORDER = 0.8f;
        public const float LAYER_TEXT = 0.9f;

        // Heading text values
            // Menus
        public const string HEADING_MENU_MAIN = "Main Menu";
        public const string HEADING_MENU_SAVE = "Save Game";
        public const string HEADING_MENU_LOAD = "Load Game";
        public const string HEADING_BACKGROUND = "Background";
        public const string HEADING_PANEL_NEWGAME = "Character Creation";
        public const string HEADING_PANEL_NEWGAME_LEFT = "Panel Left";
        public const string HEADING_PANEL_NEWGAME_RIGHT = "Panel Right";
        public const string HEADING_FIELD_AGILITY = "Agility Field";
        public const string HEADING_FIELD_MAGIC = "Magic Field";
        public const string HEADING_FIELD_STRENGTH = "Strength Field";
        public const string HEADING_FIELD_NAME = "Name Field";
        public const string HEADING_FIELD_GENDER = "Gender Field";
            // HUD, displayed
        public const string HEADING_CHARACTER_SHEET = "Character Sheet";
        public const string HEADING_INVENTORY = "Inventory";
        public const string HEADING_INSEPCTION = "Inspection";
        public const string HEADING_EQUIPMENT = "Equipment";
        public const string HEADING_TRINKETS = "Trinkets";
        public const string HEADING_HEALTH = "Health:";
        public const string FIELD_HEALTH = "Field Health:";
        public const string HEADING_MANA = "Mana:";
        public const string FIELD_MANA = "Field Mana:";
        public const string HEADING_EXPERIENCE = "Experience:";
        public const string FIELD_EXPERIENCE = "Field Experience:";
        public const string HEADING_AREA = "Area:";
        public const string FIELD_AREA = "Field Area:";
        public const string HEADING_DAMAGE = "Damage:";
        public const string FIELD_DAMAGE = "Field Damage:";
        public const string HEADING_DEFENCE= "Defence:";
        public const string FIELD_DEFENCE = "Field Defence:";
        // HUD, not displayed
        public const string HEADING_CELL_INVENTORY = "Inventory Cell";
        public const string HEADING_CELL_EQUIPMENT = "Equipment Cell";
        public const string HEADING_CELL_TRINKETS = "Trinket Cell";
        public const string HEADING_PANEL_TEXT_BOX_MESSAGE = "HUD Textbox";
        public const string HEADING_PANEL_TEXT_BOX_STATUS = "HUD Status Textbox";
        public const string HEADING_PANEL_HUD_TOP = "HUD Top";
        public const string HEADING_PANEL_HUD_BOTTOM= "HUD Bottom";
        public const string HEADING_BAR_EXPERIENCE= "Experience Bar";
        public const string HEADING_BAR_HEALTH= "Health Bar";
        public const string HEADING_BAR_MANA= "Mana Bar";
        public const string HEADING_BAR_BORDER_EXPERIENCE= "Border Experience Bar";
        public const string HEADING_BAR_BORDER_HEALTH= "Border Health Bar";
        public const string HEADING_BAR_BORDER_MANA= "Border Mana Bar";

        // Text values
        public const string SEPARATOR = "/";
        public const string TEXT_BAR_EXPERIENCE = "Current experience:";
        public const string TEXT_BAR_HEALTH = "Current health:";
        public const string TEXT_BAR_MANA = "Current mana:";

        // New game panel text values
            // Prompts
        public const string TEXT_PROMPT_GENDER = "CHOOSE YOUR GENDER:";
        public const string TEXT_PROMPT_NAME = "ENTER YOUR NAME:";
        public const string TEXT_PROMPT_STATS = "ASSIGN STAT POINTS:";
        public const string TEXT_PROMPT__STATS_ROLL = "Re-roll";
        public const string TEXT_PROMPT_STATS_REMAINING = "STAT POINTS REMAINING:";
        public const string TEXT_PROMPT_CLICK = "Click me";
        public const string TEXT_PROMPT_SELECT_ICON = "Select icon";
            // Stats
        public const string TEXT_AGILITY = "Agility:";
        public const string TEXT_MAGIC = "Magic:";
        public const string TEXT_STRENGTH = "Strength:";
            // Menu Buttons
        public const string TEXT_START= "Start";
        public const string TEXT_ICON_FEMALE= "Female";
        public const string TEXT_ICON_MALE= "Male";
        public const string TEXT_ICON_PLUS_AGILITY= "Plus Agility";
        public const string TEXT_ICON_PLUS_STRENGTH = "Plus Strength";
        public const string TEXT_ICON_PLUS_MAGIC = "Plus Magic";
        public const string TEXT_ICON_MINUS_AGILITY = "Minus Agility";
        public const string TEXT_ICON_MINUS_STRENGTH = "Minus Strength";
        public const string TEXT_ICON_MINUS_MAGIC = "Minus Magic";
            // HUD buttons
        public const string TEXT_ICON_MAIN_MENU = "Main menu icon";
        public const string TEXT_ICON_INVENTORY = "Inventory icon";
        public const string TEXT_ICON_CHARACTER_SHEET = "Character Sheet icon";
        // Menu button text values
        // General
        public const string TEXT_BACK = "Return";
            // Main menu
        public const string TEXT_NEW_GAME = "New Game";
        public const string TEXT_SAVE = "Save";
        public const string TEXT_LOAD = "Load";
        public const string TEXT_QUIT = "Quit";
            // Save, load menu
        public const string TEXT_SAVE_SLOT_1 = "Slot 1";
        public const string TEXT_SAVE_SLOT_2 = "Slot 2";
        public const string TEXT_SAVE_SLOT_3 = "Slot 3";
        public const string TEXT_SAVE_SLOT_4 = "Quick Save";


    }
}