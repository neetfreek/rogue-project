﻿using System.IO;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Data;

namespace RogueProject.GUI
{
    public static class GUIMakerHelper
    {

        /********************************************
        * Return component top-left corner position * 
        *********************************************/
        public static Vector2 OriginComponent(GUIRectangle rectangle)
        {
            return new Vector2(rectangle.Size.X * -0.5f, rectangle.Size.Y * -0.5f);
        }


        /********************************
        * GUIMaker component helpers    *
        *********************************/
        public static Vector2 MenuPanelPosition(GUIRectangle rectangle)
        {
            Vector2 position = new Vector2(0f, 0f);

            switch (rectangle.TextHeader)
            {
                case GUIVariables.HEADING_BACKGROUND:
                    position = (-rectangle.Size * 0.5f);
                    break;
                case GUIVariables.HEADING_MENU_LOAD:
                    position = (-rectangle.Size * 0.5f);
                    break;
                case GUIVariables.HEADING_MENU_MAIN:
                    position = (-rectangle.Size * 0.5f);
                    break;
                case GUIVariables.HEADING_MENU_SAVE:
                    position = (-rectangle.Size * 0.5f);
                    break;
                case GUIVariables.HEADING_PANEL_NEWGAME:
                    position = (-rectangle.Size * 0.5f);
                    break;
            }

            return position;
        }
        public static Vector2 SetNewGameSubComponentPositions(GUIRectangle subComponent)
        {
            Vector2 position = subComponent.Parent.Position;
            float modifierHeight = position.Y * -0.3f;
            float modifierWidthLeftAlign = position.X * -0.1f;
            float modifierWidthRightAlight = modifierWidthLeftAlign + (subComponent.Parent.Size.X * 0.5f);

            switch (subComponent.TextHeader)
            {
                // Left-aligned
                case GUIVariables.HEADING_PANEL_NEWGAME_LEFT:
                    position += new Vector2(modifierWidthLeftAlign * 0.5f, modifierHeight * 1.5f);
                    break;
                case GUIVariables.HEADING_PANEL_NEWGAME_RIGHT:
                    position += new Vector2(modifierWidthRightAlight * 0.94f, modifierHeight * 1.5f);
                    break;
                case GUIVariables.TEXT_PROMPT_NAME:
                    position += new Vector2(modifierWidthLeftAlign, modifierHeight * 2f);
                    break;
                case GUIVariables.HEADING_FIELD_NAME:
                    position += new Vector2(modifierWidthLeftAlign * 5.5f, modifierHeight * 2f);
                    break;
                case GUIVariables.TEXT_PROMPT_GENDER:
                    position += new Vector2(modifierWidthLeftAlign, modifierHeight * 3f);
                    break;
                case GUIVariables.HEADING_FIELD_GENDER:
                    position += new Vector2(modifierWidthLeftAlign * 6.275f, modifierHeight * 3f);
                    break;
                case GUIVariables.TEXT_ICON_FEMALE:
                    position += new Vector2(modifierWidthLeftAlign * 1.5f, modifierHeight * 3.5f);
                    break;
                case GUIVariables.TEXT_ICON_MALE:
                    position += new Vector2(modifierWidthLeftAlign * 4f, modifierHeight * 3.5f);
                    break;
                case GUIVariables.TEXT_START:
                    position += new Vector2(modifierWidthLeftAlign * 3.4f, modifierHeight * 4.9f);
                    break;
                // Right-aligned
                case GUIVariables.TEXT_PROMPT_STATS:
                    position += new Vector2(modifierWidthRightAlight, modifierHeight * 2f);
                    break;
                case GUIVariables.TEXT_PROMPT_STATS_REMAINING:
                    position += new Vector2(modifierWidthRightAlight * 1.48f, modifierHeight * 2f);
                    break;
                case GUIVariables.TEXT_AGILITY:
                    position += new Vector2(modifierWidthRightAlight, modifierHeight * 3f);
                    break;
                case GUIVariables.HEADING_FIELD_AGILITY:
                    position += new Vector2(modifierWidthRightAlight * 1.25f, modifierHeight * 3f);
                    break;
                case GUIVariables.TEXT_ICON_PLUS_AGILITY:
                    position += new Vector2(modifierWidthRightAlight * 1.275f, modifierHeight * 3f - (subComponent.Size.Y * 0.5f));
                    break;
                case GUIVariables.TEXT_ICON_MINUS_AGILITY:
                    position += new Vector2(modifierWidthRightAlight * 1.325f, modifierHeight * 3f - (subComponent.Size.Y * 0.5f));
                    break;

                case GUIVariables.TEXT_MAGIC:
                    position += new Vector2(modifierWidthRightAlight, modifierHeight * 4f);
                    break;
                case GUIVariables.HEADING_FIELD_MAGIC:
                    position += new Vector2(modifierWidthRightAlight * 1.25f, modifierHeight * 4f);
                    break;
                case GUIVariables.TEXT_ICON_PLUS_MAGIC:
                    position += new Vector2(modifierWidthRightAlight * 1.275f, modifierHeight * 4f - (subComponent.Size.Y * 0.5f));
                    break;
                case GUIVariables.TEXT_ICON_MINUS_MAGIC:
                    position += new Vector2(modifierWidthRightAlight * 1.325f, modifierHeight * 4f - (subComponent.Size.Y * 0.5f));
                    break;

                case GUIVariables.TEXT_STRENGTH:
                    position += new Vector2(modifierWidthRightAlight, modifierHeight * 5f);
                    break;
                case GUIVariables.HEADING_FIELD_STRENGTH:
                    position += new Vector2(modifierWidthRightAlight * 1.25f, modifierHeight * 5f);
                    break;
                case GUIVariables.TEXT_ICON_PLUS_STRENGTH:
                    position += new Vector2(modifierWidthRightAlight * 1.275f, modifierHeight * 5f - (subComponent.Size.Y * 0.5f));
                    break;
                case GUIVariables.TEXT_ICON_MINUS_STRENGTH:
                    position += new Vector2(modifierWidthRightAlight * 1.325f, modifierHeight * 5f - (subComponent.Size.Y * 0.5f));
                    break;

                case GUIVariables.TEXT_PROMPT__STATS_ROLL:
                    position += new Vector2(modifierWidthRightAlight * 1.5f, modifierHeight * 4.5f);
                    break;
            }
            return position;
        }

        /************************************************************************************************************
        * GUIMaker sub-component helpers. Use index number to identify, can't use name as button not created yet    *        ***********************************************************************************************************/
        public static Vector2 ButtonPositionMenu(GUIRectangle rectangle, int buttonNumber, Vector2 buttonsColsRows, Vector2 size)
        {
            Vector2 position = new Vector2(0f, 0f);
            Vector2 sizeButton = size;
            int counter = 0;

            for (int counterRow = 0; counterRow < buttonsColsRows.Y; counterRow++)
            {
                for (int counterCol = 0; counterCol < buttonsColsRows.X; counterCol++)
                {
                    if (counter == buttonNumber)
                    {
                        position.X = OriginComponent(rectangle).X + GUIVariables.BORDER_COMPONENT
                            + ((counterCol * sizeButton.X) + (GUIVariables.BORDER_COMPONENT * counterCol));
                        position.Y = OriginComponent(rectangle).Y + GUIVariables.BORDER_HEADING + sizeButton.Y
                            * counterRow + counterRow;
                    }
                    counter++;
                }
            }

            return position;
        }
        public static Vector2 ButtonPositionGrid(GUIRectangle rectangle, int buttonNumber, Vector2 buttonsColsRows, Vector2 size)
        {
            Vector2 position = new Vector2(0f, 0f);
            Vector2 sizeButton = size;
            int counter = 0;

            for (int counterRow = 0; counterRow < buttonsColsRows.Y; counterRow++)
            {
                for (int counterCol = 0; counterCol < buttonsColsRows.X; counterCol++)
                {
                    if (counter == buttonNumber)
                    {
                        position.X = OriginComponent(rectangle).X + GUIVariables.BORDER_COMPONENT + (counterCol * sizeButton.X);
                        position.Y = OriginComponent(rectangle).Y + GUIVariables.BORDER_HEADING + sizeButton.Y
                            * counterRow;
                    }
                    counter++;
                }
            }

            return position;
        }
        public static void CenterComponentHeader(GUIRectangle rectangle)
        {
            rectangle.MoveTextHeaderBy(new Vector2(rectangle.Size.X *0.5f, rectangle.Size.Y * 0.5f));
        }

        public static float ButtonWidth(GUIRectangle rectangle)
        {
            float widthButton = rectangle.Size.X - 1f;

            return widthButton;
        }

        public static void SetButtonActiveState(GUIRectangle menu, GUIRectangle button, int buttonNumber)
        {
            // Set default button text colour, active state
            button.TextHeaderColour = Color.BlanchedAlmond;
            button.Active = true;

            // Set main menu button colours
            if (menu.TextHeader == GUIVariables.HEADING_MENU_MAIN)
            {
                SetButtonActiveStateMenuMain(button, buttonNumber);
            }

            // Set load menu button colours
            if (menu.TextHeader == GUIVariables.HEADING_MENU_LOAD)
            {
                SetButtonActiveStateMenuSaveLoad(button, buttonNumber);
            }
        }
        public static void SetButtonActiveStateMenuMain(GUIRectangle button, int buttonNumber)
        {
            Color inactiveColour = Color.DarkGray;

            switch (buttonNumber)
            {
                case 1:
                    if (!Game1.GameStarted)
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
                case 2:
                    if (!File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_1)) &&
                        !File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_2)) &&
                        !File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_3)) &&
                        !File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_4)))
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
            }
        }
        public static void SetButtonActiveStateMenuSaveLoad(GUIRectangle button, int buttonNumber)
        {
            Color inactiveColour = Color.DarkGray;

            switch (buttonNumber)
            {
                case 0:
                    if (!File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_1)))
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
                case 1:
                    if (!File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_2)))
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
                case 2:
                    if (!File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_3)))
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
                case 3:
                    if (!File.Exists(SaveGame.SaveDataPath(GUIVariables.TEXT_SAVE_SLOT_4)))
                    {
                        button.TextHeaderColour = inactiveColour;
                        button.Active = false;
                    }
                    break;
            }
        }


        /************************************************************************************************************************
        * Assign text values to button sub-components. Use index number to identify, can't use name as button not created yet   * 
        *************************************************************************************************************************/
        public static string ButtonText(GUIRectangle menuRectangle, int buttonNumber)
        {
            string buttonText = "";
            switch (menuRectangle.TextHeader)
            {
                case GUIVariables.HEADING_MENU_MAIN:
                    if (Game1.GameStarted)
                    {
                        buttonText = ButtonTextMenuMainGameStarted(buttonNumber);
                    }
                    else
                    {
                        buttonText = ButtonTextMenuMainGameNotStarted(buttonNumber);
                    }
                    break;
                case GUIVariables.HEADING_MENU_SAVE:
                    buttonText = ButtonTextMenuSaveLoad(buttonNumber);
                    break;
                case GUIVariables.HEADING_MENU_LOAD:
                    buttonText = ButtonTextMenuSaveLoad(buttonNumber);
                    break;
                case GUIVariables.HEADING_PANEL_HUD_TOP:
                    buttonText = ButtonTextPanelHUDTop(buttonNumber);
                    break;
                //case GUIVariables.HEADING_PANEL_HUD_BOTTOM:
                //    buttonText = ButtonTextPanelHUDBottom(buttonNumber);
                //    break;
                case GUIVariables.HEADING_INVENTORY:
                    buttonText = GUIVariables.HEADING_CELL_INVENTORY;
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    buttonText = GUIVariables.HEADING_CELL_EQUIPMENT;
                    break;
                case GUIVariables.HEADING_TRINKETS:
                    buttonText = GUIVariables.HEADING_CELL_TRINKETS;
                    break;
            }
            return buttonText;
        }
        private static string ButtonTextMenuMainGameNotStarted(int buttonNumber)
        {
            string buttonText = "";
            switch (buttonNumber)
            {
                case 0:
                    buttonText = GUIVariables.TEXT_NEW_GAME;
                    break;
                case 1:
                    buttonText = GUIVariables.TEXT_SAVE;
                    break;
                case 2:
                    buttonText = GUIVariables.TEXT_LOAD;
                    break;
                case 3:
                    buttonText = GUIVariables.TEXT_QUIT;
                    break;
            }

            return buttonText;
        }
        private static string ButtonTextMenuMainGameStarted(int buttonNumber)
        {
            string buttonText = "";
            switch (buttonNumber)
            {
                case 0:
                    buttonText = GUIVariables.TEXT_BACK;
                    break;
                case 1:
                    buttonText = GUIVariables.TEXT_NEW_GAME;
                    break;
                case 2:
                    buttonText = GUIVariables.TEXT_SAVE;
                    break;
                case 3:
                    buttonText = GUIVariables.TEXT_LOAD;
                    break;
                case 4:
                    buttonText = GUIVariables.TEXT_QUIT;
                    break;
            }

            return buttonText;
        }

        private static string ButtonTextMenuSaveLoad(int buttonNumber)
        {
            string buttonText = "";

            switch (buttonNumber)
            {
                case 0:
                    buttonText = GUIVariables.TEXT_SAVE_SLOT_1;
                    break;
                case 1:
                    buttonText = GUIVariables.TEXT_SAVE_SLOT_2;
                    break;
                case 2:
                    buttonText = GUIVariables.TEXT_SAVE_SLOT_3;
                    break;
                case 3:
                    buttonText = GUIVariables.TEXT_SAVE_SLOT_4;
                    break;
                case 4:
                    buttonText = GUIVariables.TEXT_BACK;
                    break;
            }

            return buttonText;
        }
        private static string ButtonTextPanelHUDTop(int buttonNumber)
        {
            string buttonText = "";

            switch (buttonNumber)
            {
                case 0:
                    buttonText = GUIVariables.TEXT_ICON_MAIN_MENU;
                    break;
                case 1:
                    buttonText = GUIVariables.TEXT_ICON_INVENTORY;
                    break;
                case 2:
                    buttonText = GUIVariables.TEXT_ICON_CHARACTER_SHEET;
                    break;
            }

            return buttonText;
        }


        /****************************
        * Set Character Sheet texts * 
        *****************************/
        public static string SetCharacterSheetText(GUIRectangle subComponent)
        {
            string text = "";
            switch (subComponent.TextHeader)
            {
                case GUIVariables.TEXT_AGILITY:
                    text = GUIVariables.TEXT_AGILITY.ToString();
                    break;
                case GUIVariables.TEXT_MAGIC:
                    text = GUIVariables.TEXT_MAGIC.ToString();
                    break;
                case GUIVariables.TEXT_STRENGTH:
                    text = GUIVariables.TEXT_STRENGTH.ToString();
                    break;
                case GUIVariables.HEADING_HEALTH:
                    text = GUIVariables.HEADING_HEALTH.ToString();
                    break;
                case GUIVariables.HEADING_MANA:
                    text = GUIVariables.HEADING_MANA.ToString();
                    break;
                case GUIVariables.HEADING_DAMAGE:
                    text = GUIVariables.HEADING_DAMAGE.ToString();
                    break;
                case GUIVariables.HEADING_DEFENCE:
                    text = GUIVariables.HEADING_DEFENCE.ToString();
                    break;
                case GUIVariables.HEADING_EXPERIENCE:
                    text = GUIVariables.HEADING_EXPERIENCE.ToString();
                    break;
                case GUIVariables.HEADING_AREA:
                    text = GUIVariables.HEADING_AREA.ToString();
                    break;
            }

            return text;
        }
    }
}