﻿namespace RogueProject.GUI
{
    public enum GUIType
    {
        Panel,
        BarBorder,
        Bar,
        Button,
        Menu,
        TextBox,
    }
}