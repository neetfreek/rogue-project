﻿/************************************************************
* Handles moving in-game HUD GUI components, sub-components *
*   such as when player moves.                              *
*************************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Common;

namespace RogueProject.GUI
{
    public static class HUDMove
    {
        /****************
        * GUI HUD Move  *
        *****************/
        public static void MoveHUD(Vector2 position)
        {
            GUIRectangle inventory;

            foreach (GUIRectangle component in GUILists.GUIComponentsHUD)
            {
                Vector2 modifierPosition = new Vector2(0f, 0f);
                component.MoveComponentTo(position); // Set to player position
                switch (component.TextHeader)
                {
                    // Top panel
                    case GUIVariables.HEADING_PANEL_HUD_TOP:
                        component.MoveComponentBy(new Vector2(GlobalVariables.TILES_SCREEN_X * -0.5f, (GlobalVariables.TILES_SCREEN_Y * -0.5f)));
                        component.MoveComponentBy(new Vector2(0f, -1f));
                        MoveHUDTopBarSubComponents(component, position);
                        break;
                    // Bottom panel
                    case GUIVariables.HEADING_PANEL_HUD_BOTTOM:
                        component.MoveComponentBy(new Vector2(GlobalVariables.TILES_SCREEN_X * -0.5f, (GlobalVariables.TILES_SCREEN_Y * +0.5f)));
                        component.MoveComponentBy(new Vector2(0f, -2f));
                        MoveHUDBotBarSubComponents(component, position);
                        break;
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE:
                        component.MoveComponentBy(new Vector2(GlobalVariables.TILES_SCREEN_X * -0.5f,
                            (GlobalVariables.TILES_SCREEN_Y * + 0.5f) - component.Size.Y));
                        component.MoveComponentBy(new Vector2(0f, -2f));
                        MoveBottomBarTextMessageBox(component, position);
                        break;
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS:
                        component.MoveComponentBy(new Vector2(GlobalVariables.TILES_SCREEN_X * 0.5f - component.Size.X,
                            (GlobalVariables.TILES_SCREEN_Y * +0.5f) - component.Size.Y));
                        component.MoveComponentBy(new Vector2(0f, -2f));
                        MoveBottomBarTextStatusBox(component, position);
                        break;
                    // Character Sheet
                    case GUIVariables.HEADING_CHARACTER_SHEET:
                        component.MoveComponentBy(new Vector2(-component.Size.X * 0.5f,
                            -component.Size.Y * 0.5f));
                        component.MoveTextHeaderBy(new Vector2(-component.Size.X * 0.275f, 0.65f));
                        MoveCharacterSheetSubComponents(component);
                        break;
                    // Inventory
                    case GUIVariables.HEADING_INVENTORY:
                        component.MoveComponentBy(new Vector2(-component.Size.X * 0.5f,
                            - component.Size.Y * 0.5f));
                        MoveGridSubComponents(component, component.TextHeader);
                        break;
                    case GUIVariables.HEADING_EQUIPMENT:
                        inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);

                        component.MoveComponentBy(new Vector2(-component.Size.X * 0.5f,
                            - ((inventory.Size.Y * 0.5f) + (component.Size.Y)) ));
                        MoveGridSubComponents(component, component.TextHeader);
                        break;
                    case GUIVariables.HEADING_TRINKETS:
                        inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);

                        component.MoveComponentBy(new Vector2(-component.Size.X * 0.5f,
                            (inventory.Size.Y * 0.5f)));
                        MoveGridSubComponents(component, component.TextHeader);
                        break;
                    case GUIVariables.HEADING_INSEPCTION:
                        modifierPosition = new Vector2(component.Size.X * 0.75f, 0f);
                        inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);
                        component.MoveComponentBy(new Vector2(-(component.Size.X + (inventory.Size.X * 0.5f) + 1f), -((component.Size.Y * 0.5f) + 1.5f)));
                        component.MoveTextTo(component.Position);
                        break;
                }
            }
        }

        private static void MoveHUDTopBarSubComponents(GUIRectangle component, Vector2 positionPlayer)
        {
            Vector2 positionModifier = new Vector2(0f, 0f);
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                if (subComponent.TextHeader == GUIVariables.TEXT_ICON_CHARACTER_SHEET)
                {
                    positionModifier = new Vector2(+0.1f + subComponent.Size.X, -0.1f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2(-GlobalVariables.TILES_SCREEN_X * 0.5f,
                        (-GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }
                if (subComponent.TextHeader == GUIVariables.TEXT_ICON_INVENTORY)
                {
                    positionModifier = new Vector2(+0.1f, -0.1f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2(-GlobalVariables.TILES_SCREEN_X * 0.5f,
                        (-GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }
                if (subComponent.TextHeader == GUIVariables.TEXT_ICON_MAIN_MENU)
                {
                    positionModifier = new Vector2(-subComponent.Size.X, 0f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2(GlobalVariables.TILES_SCREEN_X * 0.5f,
                        (-GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }

                if (subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_EXPERIENCE ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_EXPERIENCE)                
                {
                    positionModifier = new Vector2(0f, -0.06f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2(-subComponent.Size.X * 0.5f,
                        (- GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }
            }
        }
        private static void MoveHUDBotBarSubComponents(GUIRectangle component, Vector2 positionPlayer)
        {
            Vector2 positionModifier = new Vector2(0.1f, 0.1f);
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                if (subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_HEALTH ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_HEALTH)
                {
                    positionModifier = new Vector2(subComponent.Size.X * -0.40f, -1.5f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2(-GlobalVariables.TILES_SCREEN_X * 0.5f + subComponent.Size.X * 0.5f,
                        (GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }

                if (subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_MANA ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_MANA)
                {
                    positionModifier = new Vector2(subComponent.Size.X * 0.40f, -1.5f);
                    subComponent.MoveComponentTo(positionPlayer);
                    subComponent.MoveComponentBy(new Vector2((GlobalVariables.TILES_SCREEN_X * 0.5f) - subComponent.Size.X * 1.5f,
                        (GlobalVariables.TILES_SCREEN_Y * 0.5f)) + positionModifier);
                }
            }
        }
        private static void MoveBottomBarTextMessageBox(GUIRectangle component, Vector2 positionPlayer)
        {
            Vector2 positionModifier = new Vector2(0.2f, -0.1f);
            component.MoveTextTo(component.Position + positionModifier);
        }
        private static void MoveBottomBarTextStatusBox(GUIRectangle component, Vector2 positionPlayer)
        {          
            Vector2 positionModifier = new Vector2(0.3f, 0.3f);
            component.MoveTextTo(component.Position + positionModifier);
        }

        private static void MoveGridSubComponents(GUIRectangle component, string nameComponent)
        {
            switch (nameComponent)
            {
                case GUIVariables.HEADING_EQUIPMENT:
                    MoveGridSubComponents(component, (int)GUIVariables.CAPACITY_EQUIPMENT, (int)GUIVariables.WIDTH_EQUIPMENT, (int)GUIVariables.HEIGHT_EQUIPMENT);
                    break;
                case GUIVariables.HEADING_INVENTORY:
                    MoveGridSubComponents(component, (int)GUIVariables.CAPACITY_INVENTORY, (int)GUIVariables.WIDTH_INVENTORY, (int)GUIVariables.HEIGHT_INVENTORY);
                    break;
                case GUIVariables.HEADING_TRINKETS:
                    MoveGridSubComponents(component, (int)GUIVariables.CAPACITY_TRINKETS, (int)GUIVariables.WIDTH_TRINKETS, (int)GUIVariables.HEIGHT_TRINKETS);
                    break;
            }
        }
        private static void MoveGridSubComponents(GUIRectangle component, int capacityGrid, int widthGrid, int heightGrid)
        {
            int counter = 0;
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                // Set cells in grid positions
                Vector2 positionModifier = PositionCellGrid(counter, widthGrid, heightGrid);
                // Adjust cells position to center in background panel (by default have space for border)
                positionModifier += subComponent.Size;
                // Move cells
                subComponent.MoveComponentTo(component.Position + positionModifier);
                counter++;
            }
        }
        private static Vector2 PositionCellGrid(int indexCell, int widthGrid, int heightGrid)
        {
            int indexIterated = 0;
            Vector2 positionGrid = new Vector2(0f, 0f);
            for (int counterRows = 0; counterRows < heightGrid; counterRows++)
            {
                for (int counterCols = 0; counterCols < widthGrid; counterCols++)
                {
                    positionGrid = new Vector2(counterCols, counterRows);
                    if (indexIterated == indexCell)
                    {
                        return positionGrid;
                    }
                    indexIterated++;
                }
            }
            return positionGrid;
        }

        /********************
        * HUD fields move   *
        *********************/
        private static void MoveCharacterSheetSubComponents(GUIRectangle characterSheet)
        {
            Vector2 modifierPositionVerticalLeft = new Vector2(characterSheet.Size.X * 0.08f, 0f);
            Vector2 modifierPositionVerticalRight = new Vector2(characterSheet.Size.X * 0.55f, 0f);
            Vector2 modifyPositionHorizontal = new Vector2(0f, characterSheet.Size.Y * 0.25f);
            Vector2 modifyHeightStats = new Vector2(0f, characterSheet.Size.X * 0.1f);
            Vector2 modifyPositionStats = new Vector2(characterSheet.Size.X * 0.25f, 0f);
            Vector2 modifyPositionRightFields = new Vector2(characterSheet.Size.X * 0.225f, 0f);
            Vector2 modiftPositionArea = new Vector2(characterSheet.Size.X * 0.155f, 0f);
            Vector2 modiftPositionExperience = new Vector2(characterSheet.Size.X * 0.295f, 0f);

            foreach (GUIRectangle subComponent in characterSheet.Subcomponents)
            {
                switch (subComponent.TextHeader)
                {
                        // Headings
                    case GUIVariables.TEXT_AGILITY:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionHorizontal);
                        break;
                    case GUIVariables.TEXT_MAGIC:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionHorizontal + (modifyHeightStats * 1));
                        break;
                    case GUIVariables.TEXT_STRENGTH:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionHorizontal + (modifyHeightStats * 2));
                        break;
                    case GUIVariables.HEADING_EXPERIENCE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionHorizontal + (modifyHeightStats * 4));
                        break;
                    case GUIVariables.HEADING_AREA:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionHorizontal + (modifyHeightStats * 5));
                        break;

                    case GUIVariables.HEADING_HEALTH:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionHorizontal);
                        break;
                    case GUIVariables.HEADING_MANA:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionHorizontal + (modifyHeightStats * 1));
                        break;
                    case GUIVariables.HEADING_DAMAGE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionHorizontal + (modifyHeightStats * 2));
                        break;
                    case GUIVariables.HEADING_DEFENCE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionHorizontal + (modifyHeightStats * 3));
                        break;
                    // Fields
                    case GUIVariables.HEADING_FIELD_NAME:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(new Vector2(subComponent.Size.X * 0.17f, subComponent.Size.Y * 0.06f));
                        break;

                    case GUIVariables.HEADING_FIELD_AGILITY:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionStats + modifyPositionHorizontal);
                        break;
                    case GUIVariables.HEADING_FIELD_MAGIC:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionStats + modifyPositionHorizontal + (modifyHeightStats * 1));
                        break;
                    case GUIVariables.HEADING_FIELD_STRENGTH:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modifyPositionStats + modifyPositionHorizontal + (modifyHeightStats * 2));
                        break;

                    case GUIVariables.FIELD_HEALTH:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionRightFields + modifyPositionHorizontal);
                        break;
                    case GUIVariables.FIELD_MANA:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionRightFields + modifyPositionHorizontal + (modifyHeightStats * 1));
                        break;
                    case GUIVariables.FIELD_DAMAGE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionRightFields + modifyPositionHorizontal + (modifyHeightStats * 2));
                        break;
                    case GUIVariables.FIELD_DEFENCE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalRight + modifyPositionRightFields + modifyPositionHorizontal + (modifyHeightStats * 3));
                        break;

                    case GUIVariables.FIELD_EXPERIENCE:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modiftPositionExperience + modifyPositionHorizontal + (modifyHeightStats * 4));
                        break;
                    case GUIVariables.FIELD_AREA:
                        subComponent.MoveTextTo(subComponent.Parent.Position);
                        subComponent.MoveTextBy(modifierPositionVerticalLeft + modiftPositionArea + modifyPositionHorizontal + (modifyHeightStats * 5));
                        break;
                }
            }
        }
    }
}