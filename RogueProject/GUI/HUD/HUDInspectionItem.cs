﻿/*******************************************************************
* Responsible for setting the inspector GUI component's Text value *
*   when inspecting (mousing/keyboarding over) items in inventory. *
* Each portion of information  (name, damage, quality, etc)        *
*   assigned to an entry in textList, which is ultimately          *
*   converted to a string to which inspector's Text value is       *
*   assigned.                                                      *
* Lines exceeding GUIVariables.MAX_INSPECTOR_CHARACTERS length (18)*
*   split into multiple lines.                                     *
********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Items;

namespace RogueProject.GUI
{
    public class HUDInspectionItem
    {
        private GUIRectangle inspector;

        private List<string> textList = new List<string>();


        /********************
        * Item inspection   *
        *********************/
        public void DisplayInspectionItem(Item item)
        {
            inspector = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INSEPCTION);
            ResetTextList();

            switch (item.CategoryItem)
            {
                case CategoryItem.Armour:
                    InspectArmour(item);
                    break;
                case CategoryItem.Weapon:
                    InspectWeapon(item);
                    break;
                case CategoryItem.Consumable:
                    break;
            }
        }

        private void InspectArmour(Item item)
        {
            Armour armour = (Armour)item;
            string name = $"{PrefixQuality(item.Quality)} {item.Name}";
            AddTextToList($"{name.ToUpper()}{System.Environment.NewLine}");
            AddTextToList(item.CategoryArmour.ToString());
            AddTextToList($"{ItemVariables.TEXT_RATING_ARMOUR} {armour.RatingArmour.ToString()}");
            inspector.SetText(ListToText());
        }

        private void InspectWeapon(Item item)
        {
            Weapon weapon = (Weapon)item;
            string name = $"{PrefixQuality(item.Quality)} {item.Name}";
            AddTextToList($"{name.ToUpper()}{System.Environment.NewLine}");
            AddTextToList($"{item.CategoryWeapon.ToString()}");
            if (weapon.CategoryWeapon == CategoryWeapon.Ranged)
            {
                AddTextToList($"{ItemVariables.TEXT_RANGE} {weapon.Range.ToString()}");
            }
            AddTextToList($"{ItemVariables.TEXT_DAMAGE} {Helper.Vector2RangeToString(weapon.Damage)}");
            inspector.SetText(ListToText());
        }


        /************
        * Helpers   *
        *************/
        private void AddTextToList(string text)
        {
            int indexEOL = HUDVariables.MAX_INSPECTOR_CHARACTERS;
            string textToAdd = text;
            string lineToAdd;

            while (textToAdd.Length > HUDVariables.MAX_INSPECTOR_CHARACTERS)
            {
                indexEOL = HUDVariables.MAX_INSPECTOR_CHARACTERS;

                if (textToAdd[HUDVariables.MAX_INSPECTOR_CHARACTERS - 1] != ' ' ||
                    textToAdd[HUDVariables.MAX_INSPECTOR_CHARACTERS] != ' ')
                {
                    while (textToAdd[indexEOL] != ' ')
                    {
                        indexEOL--;
                    }
                    indexEOL++;
                    lineToAdd = textToAdd.Substring(0, indexEOL);
                }
                else
                {
                    lineToAdd = textToAdd.Substring(0, indexEOL);
                }
                textList.Add($"{lineToAdd}{System.Environment.NewLine}");
                textToAdd = textToAdd.Remove(0, lineToAdd.Length);
            }

            textList.Add($"{textToAdd}{System.Environment.NewLine}");

        }
        private string ListToText()
        {
            string text = "";
            foreach (string line in textList)
            {
                text += $"{line}{System.Environment.NewLine}";
            }

            return text;
        }
        private void ResetTextList()
        {
            textList.Clear();
        }

        private string PrefixQuality(QualityItem quality)
        {
            string qualityItem = "";
            switch (quality)
            {
                case QualityItem.Terrible:
                    qualityItem = ItemVariables.TEXT_QUALITY_TERRIBLE;
                    break;
                case QualityItem.Low:
                    qualityItem = ItemVariables.TEXT_QUALITY_LOW;
                    break;
                case QualityItem.Normal:
                    qualityItem = ItemVariables.TEXT_QUALITY_NORMAL;
                    break;
                case QualityItem.High:
                    qualityItem = ItemVariables.TEXT_QUALITY_HIGH;
                    break;
                case QualityItem.Superior:
                    qualityItem = ItemVariables.TEXT_QUALITY_SUPERIOR;
                    break;
            }

            return qualityItem;
        }
    }
}