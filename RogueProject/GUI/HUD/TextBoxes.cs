﻿/********************************************************************
* Represent HUD text-box objects, e.g. the message and status       *
*   text boxes where game information is presented to the player    *
*********************************************************************/
namespace RogueProject.GUI
{
    public class TextBoxes
    {
        public TextBox TextBoxMessages;
        public TextBox TextBoxStatus;
    }
}
