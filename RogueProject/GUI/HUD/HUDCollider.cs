﻿/****************************************************************
* Represent colliders for HUD UI components e.g. menu buttons   *
* Handle positioning of colliders                               *
*****************************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Common;

namespace RogueProject.GUI
{
    public static class HUDCollider
    {
        public static Rectangle ColliderMenu(GUIRectangle component)
        {
            Rectangle rectangle = new Rectangle();

            if (component.Parent != null)
            {
                rectangle = ColliderNewGameSubComponent(component);
            }
            else
            {
                rectangle.X = (int)(((GlobalVariables.TILES_SCREEN_X * 0.5f) + component.PositionUnScaled.X));
                rectangle.Y = (int)(((GlobalVariables.TILES_SCREEN_Y * 0.5f) + component.PositionUnScaled.Y));
                rectangle.Width = (int)component.Size.X * GlobalVariables.ScaledSprite;
                rectangle.Height = (int)component.Size.Y * GlobalVariables.ScaledSprite;
            }

            return rectangle;
        }
        // New game menu subComponents (e.g. gender button, stat arrows) hard-coded
        private static Rectangle ColliderNewGameSubComponent(GUIRectangle component)
        {
            Rectangle rectangle = new Rectangle();
            switch (component.TextHeader)
            {
                case GUIVariables.HEADING_FIELD_NAME:
                    rectangle.X = 2;
                    rectangle.Y = 6;
                    rectangle.Width = 10;
                    rectangle.Height = 1;
                    break;
                case GUIVariables.TEXT_ICON_FEMALE:
                    rectangle.X = 5;
                    rectangle.Y = 8;
                    rectangle.Width = 1;
                    rectangle.Height = 2;
                    break;
                case GUIVariables.TEXT_ICON_MALE:
                    rectangle.X = 8;
                    rectangle.Y = 8;
                    rectangle.Width = 1;
                    rectangle.Height = 2;
                    break;
                case GUIVariables.TEXT_PROMPT__STATS_ROLL:
                    rectangle.X = 23;
                    rectangle.Y = 10;
                    rectangle.Width = (int)GUIVariables.WIDTH_BUTTON_MEDIUM;
                    rectangle.Height = (int)GUIVariables.HEIGHT_BUTTON;
                    break;
                case GUIVariables.TEXT_ICON_PLUS_AGILITY:
                    rectangle.X = 20;
                    rectangle.Y = 7;
                    rectangle.Width = (int)(GUIVariables.WIDTH_HEIGHT_ICON);
                    rectangle.Height = (int)(GUIVariables.WIDTH_HEIGHT_ICON * 2f);
                    break;
                case GUIVariables.TEXT_ICON_MINUS_AGILITY:
                    rectangle.X = 21;
                    rectangle.Y = 7;
                    rectangle.Width = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    rectangle.Height = (int)(GUIVariables.WIDTH_HEIGHT_ICON * 2f);
                    break;
                case GUIVariables.TEXT_ICON_PLUS_MAGIC:
                    rectangle.X = 20;
                    rectangle.Y = 9;
                    rectangle.Width = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    rectangle.Height = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    break;
                case GUIVariables.TEXT_ICON_MINUS_MAGIC:
                    rectangle.X = 21;
                    rectangle.Y = 9;
                    rectangle.Width = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    rectangle.Height = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    break;
                case GUIVariables.TEXT_ICON_PLUS_STRENGTH:
                    rectangle.X = 20;
                    rectangle.Y = 11;
                    rectangle.Width = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    rectangle.Height = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    break;
                case GUIVariables.TEXT_ICON_MINUS_STRENGTH:
                    rectangle.X = 21;
                    rectangle.Y = 11;
                    rectangle.Width = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    rectangle.Height = (int)GUIVariables.WIDTH_HEIGHT_ICON;
                    break;
                case GUIVariables.TEXT_START:
                    rectangle.X = 7;
                    rectangle.Y = 11;
                    rectangle.Width = (int)GUIVariables.WIDTH_BUTTON_LARGE;
                    rectangle.Height = (int)GUIVariables.HEIGHT_BUTTON;
                    break;
            }

            return rectangle;
        }

        public static Rectangle ColliderGame(GUIRectangle component)
        {
            GUIRectangle inventory;
            float posModifier;
            float sizeX = component.Size.X * GlobalVariables.ScaledSprite;
            float sizeY = component.Size.Y * GlobalVariables.ScaledSprite;
            float posX = 0;
            float posY = 0;

            switch (component.TextHeader)
            {
                // Top panel
                case GUIVariables.HEADING_PANEL_HUD_TOP:
                    posX = 0;
                    posY = 0;
                    sizeY = GlobalVariables.ScaledSprite;
                    break;
                case GUIVariables.TEXT_ICON_INVENTORY:
                    posX = 0;
                    posY = 0;
                    break;
                case GUIVariables.TEXT_ICON_CHARACTER_SHEET:
                    posX = 0 + component.Size.X * GlobalVariables.ScaledSprite;
                    posY = 0;
                    break;
                case GUIVariables.TEXT_ICON_MAIN_MENU:
                    posX = GlobalVariables.WidthScreenActual - (component.Size.X * GlobalVariables.ScaledSprite);
                    posY = 0;
                    break;
                case GUIVariables.HEADING_BAR_BORDER_EXPERIENCE:
                    sizeX = 0f;
                    sizeY = 0f;
                    break;
                case GUIVariables.HEADING_BAR_EXPERIENCE:
                    posX = (GlobalVariables.WidthScreenActual * 0.5f) - ((component.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    posY = -GlobalVariables.ScaledSprite * 1;
                    sizeY = GlobalVariables.ScaledSprite * 4;
                    break;
                // Character Sheet
                case GUIVariables.HEADING_CHARACTER_SHEET:
                    posX = (GlobalVariables.WidthScreenActual * 0.5f) - ((component.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    posY = (GlobalVariables.HeightScreenActual * 0.5f) - ((component.Size.Y * GlobalVariables.ScaledSprite) * 0.5f);
                    sizeX = component.Size.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    sizeY = component.Size.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    break;
                // Inventory
                case GUIVariables.HEADING_INVENTORY:
                    posX = (GlobalVariables.WidthScreenActual * 0.5f) - ((component.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    posY = (GlobalVariables.HeightScreenActual * 0.5f) - ((component.Size.Y * GlobalVariables.ScaledSprite) * 0.5f);
                    sizeX = component.Size.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    sizeY = component.Size.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    break;
                case GUIVariables.HEADING_CELL_INVENTORY:
                    posModifier = GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    posX = PositionCellGrid(component, (int)GUIVariables.WIDTH_INVENTORY,
                        (int)GUIVariables.HEIGHT_INVENTORY).X + posModifier;
                    posY = PositionCellGrid(component, (int)GUIVariables.WIDTH_INVENTORY,
                        (int)GUIVariables.HEIGHT_INVENTORY).Y + posModifier;
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);                    
                    posX = (GlobalVariables.WidthScreenActual * 0.5f) - ((component.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    posY = (GlobalVariables.HeightScreenActual * 0.5f) - (((inventory.Size.Y * 0.5f) + (component.Size.Y)) * GlobalVariables.ScaledSprite);
                    sizeX = component.Size.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    sizeY = component.Size.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    break;
                case GUIVariables.HEADING_CELL_EQUIPMENT:
                    posModifier = GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;

                    posX = PositionCellGrid(component, (int)GUIVariables.WIDTH_EQUIPMENT,
                        (int)GUIVariables.HEIGHT_EQUIPMENT).X + posModifier;
                    posY = PositionCellGrid(component, (int)GUIVariables.WIDTH_EQUIPMENT,
                        (int)GUIVariables.HEIGHT_EQUIPMENT).Y + posModifier;
                    break;
                case GUIVariables.HEADING_TRINKETS:
                    inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);
                    posX = (GlobalVariables.WidthScreenActual * 0.5f) - ((component.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    posY = (GlobalVariables.HeightScreenActual * 0.5f) + ((inventory.Size.Y * 0.5f) * GlobalVariables.ScaledSprite);
                    sizeX = component.Size.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    sizeY = component.Size.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    break;
                case GUIVariables.HEADING_CELL_TRINKETS:
                    posModifier = GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;

                    posX = PositionCellGrid(component, (int)GUIVariables.WIDTH_TRINKETS,
                        (int)GUIVariables.HEIGHT_TRINKETS).X + posModifier;
                    posY = PositionCellGrid(component, (int)GUIVariables.WIDTH_TRINKETS,
                        (int)GUIVariables.HEIGHT_TRINKETS).Y + posModifier;
                    break;
                // Bottom panel
                case GUIVariables.HEADING_PANEL_HUD_BOTTOM:
                    posX = 0;
                    posY = GlobalVariables.HeightScreenActual - GlobalVariables.ScaledSprite * component.Size.Y;
                    break;
                case GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE:
                    posX = 0;
                    posY = GlobalVariables.HeightScreenActual - GlobalVariables.ScaledSprite * (component.Size.Y + 2);
                    sizeY = GlobalVariables.ScaledSprite * component.Size.Y;
                    break;
                case GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS:
                    posX = GlobalVariables.WidthScreenActual - component.Size.X * GlobalVariables.ScaledSprite;
                    posY = GlobalVariables.HeightScreenActual - GlobalVariables.ScaledSprite * (component.Size.Y + 2);
                    sizeY = component.Size.Y * GlobalVariables.ScaledSprite;
                    break;
                case GUIVariables.HEADING_BAR_BORDER_HEALTH:
                    sizeX = 0f;
                    sizeY = 0f;
                    break;
                case GUIVariables.HEADING_BAR_HEALTH:
                    posX = 0;
                    posY = GlobalVariables.HeightScreenActual - GlobalVariables.ScaledSprite * 1;
                    sizeY = GlobalVariables.ScaledSprite * 4;
                    break;
                case GUIVariables.HEADING_BAR_MANA:
                    posX = GlobalVariables.WidthScreenActual - GlobalVariables.ScaledSprite * 5;
                    posY = GlobalVariables.HeightScreenActual - GlobalVariables.ScaledSprite * 1;
                    sizeY = GlobalVariables.ScaledSprite * 4;
                    break;
                case GUIVariables.HEADING_BAR_BORDER_MANA:
                    sizeX = 0f;
                    sizeY = 0f;
                    break;
            }

            Rectangle rectangle = new Rectangle((int)posX, (int)posY, (int)sizeX, (int)sizeY);
            return rectangle;
        }

        private static Rectangle Disabled()
        {
            return new Rectangle(0, 0, 0, 0);
        }

        private static Vector2 PositionCellGrid(GUIRectangle component, int widthGrid, int heightGrid)
        {
            int counter = 0;
            Vector2 positionParent = PositionParent(component.Parent);
            Vector2 positionCell = PositionParent(component.Parent);

            for (int counterRows = 0; counterRows < heightGrid; counterRows++)
            {
                for (int counterCols = 0; counterCols < widthGrid; counterCols++)
                {
                    if (counter == component.Parent.Subcomponents.IndexOf(component))
                    {
                        return positionCell;
                    }

                    positionCell.X += 1 * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
                    counter++;
                }
                positionCell.X = positionParent.X;
                positionCell.Y += 1 * GlobalVariables.WIDTH_HEIGHT_SPRITE * GlobalVariables.SCALE_SPRITE;
            }

            return positionCell;
        }
        private static Vector2 PositionParent(GUIRectangle parent)
        {
            GUIRectangle inventory;
            Vector2 positionParent = new Vector2(0f, 0f);
            switch (parent.TextHeader)
            {
                case GUIVariables.HEADING_INVENTORY:
                    positionParent.X = (GlobalVariables.WidthScreenActual * 0.5f) - ((parent.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    positionParent.Y = (GlobalVariables.HeightScreenActual * 0.5f) - ((parent.Size.Y * GlobalVariables.ScaledSprite) * 0.5f);
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);

                    positionParent.X = (GlobalVariables.WidthScreenActual * 0.5f) - ((parent.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    positionParent.Y = (GlobalVariables.HeightScreenActual * 0.5f) - 
                        ((parent.Size.Y * GlobalVariables.ScaledSprite) + ((inventory.Size.Y * 0.5f) * GlobalVariables.ScaledSprite));
                    break;
                case GUIVariables.HEADING_TRINKETS:
                    inventory = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);

                    positionParent.X = (GlobalVariables.WidthScreenActual * 0.5f) - ((parent.Size.X * GlobalVariables.ScaledSprite) * 0.5f);
                    positionParent.Y = (GlobalVariables.HeightScreenActual * 0.5f) +
                        ((inventory.Size.Y * 0.5f) * GlobalVariables.ScaledSprite);
                    break;
            }

            return positionParent;
        }
    }
}