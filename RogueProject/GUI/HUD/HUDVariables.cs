﻿/****************************************************************
* Contain commonly-used HUD consts like common names or numbers *
*****************************************************************/
using System.Collections.Generic;

namespace RogueProject.GUI
{
    public static class HUDVariables
    {
        // Messages measurements
        public const int MAX_MESSAGE_CHARACTERS = 47;
        public const int MAX_STATUS_CHARACTERS = 16;
        public const int MINIMSED_LINES_MESSAGEBOX = 1;
        public const int MAXIMISED_LINES_MESSAGEBOX = 8;

        // Inspector measurements
        public const int MAX_INSPECTOR_CHARACTERS = 18;

        // Enemy health status
        public const string HEALTHY = " (healthy) ";
        public const string INJURED = " (injured) ";
        public const string BADLY_INJURED = " (badly injured) ";
        public const string NEAR_DEATH= " (near-death) ";

        // Common message vocabulary
        // Combat
        public const string EMPHASIS_ACTION = "**";
        public const string HIT = "hit";
        public const string MISS = "missed";
        public const string FOR = "for";
        public const string DMG = "dmg";
        public const string DEFEAT_FOE = "defeated";
        public const string DEFEAT_PLAYER= "defeated. Game over!";
        public const string PLUS = "+";
        public const string EXPERIENCE = "xp";
        public const string MISS_CRITICAL = "critically missed";
            // Interactions
        public const string OPEN_DOOR = "opened door";
        public const string ENTRANCE = "Entering";
        public const string MOVES_STATUS =     "Moves:  ";
        public const string MOVES =     "Moves:";
        public const string ACTIONS_STATUS =   "Actions:";
        public const string PICKUP =   "Picked up";
        public const string PICKUP_NO_SPACE =   "Inventory full. Can't pick up";

            // Turns
        public static string NEXT_ACTION_END = $"New turn on{System.Environment.NewLine}action";
        public static string NEXT_MOVE_END = $"New turn on{System.Environment.NewLine}move";
        public static string CLICK_TO_END_TURN = $"Click again to{System.Environment.NewLine}{System.Environment.NewLine}end turn";
        public static string PROCESS_ENEMIES_TURN = $"{System.Environment.NewLine}" +
            $"{System.Environment.NewLine}   Enemy turn";

        public static List<string> MessageTemporary = new List<string>();
    }
}
