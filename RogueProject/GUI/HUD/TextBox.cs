﻿/************************************************************
* Handles updating the text displayed in the in-game HUD    *
*   GUI text box component.                                 *
*************************************************************/
using System;
using System.Collections.Generic;
using RogueProject.Common;

namespace RogueProject.GUI
{
    public class TextBox
    {
        private Game1 game;
        private GUIRectangle textBox;
        
        public List<string> TextList = new List<string>();
        public int LineLimit { get => lineLimit; set => lineLimit = value; }
        private int scrollAmount;
        private int lineCurrent;

        private int lineLimit = HUDVariables.MAXIMISED_LINES_MESSAGEBOX;

        private string text;
        private int indexEOLPrevious;

        public TextBox(Game1 game, GUIRectangle component)
        {
            this.game = game;
            textBox = component;
        }

        public void Setup(GUIRectangle component)
        {
            textBox = component;
        }
        public void ResetList()
        {
            TextList.Clear();
        }


        /********************************************************
        * Receive message information, send to HandleUpdateText *
        *********************************************************/
        public void UpdateMessage(string message)
        {
            AddTextToTextList(message);
            HandleUpdateText();
        }
        public void ReplaceMessage(string message)
        {
            text = "";
            text = message;
            textBox.SetText(text);
        }

        public void UpdateAttackerHit(string nameAttacker, string nameDefender, int damage, float healthPercentDefender)
        {
            string line = "";
            if (nameDefender == game.PlayerStats.Name)
            {
                line = $"{nameAttacker} {HUDVariables.HIT} {nameDefender} {HUDVariables.FOR} {damage}{HUDVariables.DMG}";
            }
            else
            {
                line = $"{nameAttacker} {HUDVariables.HIT} {nameDefender}{HealthStatus(healthPercentDefender)}{HUDVariables.FOR} {damage}{HUDVariables.DMG}";
            }

            AddTextToTextList(line);
            HandleUpdateText();
        }
        public void UpdateAttackerMiss (string nameAttacker, string nameDefender)
        {
            string line = $"{nameAttacker} {HUDVariables.MISS} {nameDefender}";

            AddTextToTextList(line);
            HandleUpdateText();
        }
        public void UpdateCriticalMiss(string nameAttacker, string nameDefender, int damage)
        {
            string line = $"{nameAttacker} {HUDVariables.MISS_CRITICAL} {nameDefender} {HUDVariables.FOR} {-damage}";
            AddTextToTextList(line);
            HandleUpdateText();
        }
        public void UpdateFoeDefeated(string nameDefeated, int experience)
        {
            UpdateMessage($"{HUDVariables.EMPHASIS_ACTION}{nameDefeated} {HUDVariables.DEFEAT_FOE}. {HUDVariables.PLUS}{experience}{HUDVariables.EXPERIENCE}{HUDVariables.EMPHASIS_ACTION}");
        }
        public void UpdatePlayerDefeated(string namePlayer)
        {
            UpdateMessage($"{HUDVariables.EMPHASIS_ACTION}{namePlayer} {HUDVariables.DEFEAT_PLAYER}{HUDVariables.EMPHASIS_ACTION}");
        }

        public void UpdateItemDropped(string nameDefeated, string nameItem)
        {
            UpdateMessage($"{HUDVariables.EMPHASIS_ACTION}{nameDefeated} dropped {nameItem}{HUDVariables.EMPHASIS_ACTION}");
        }


        /****************************************
        * Determine what to write to text box   *
        *****************************************/
        private void AddTextToTextList(string text)
        {
            string textToAdd = text;
            string textInLine = "";
            indexEOLPrevious = 0;

            if (textToAdd.Length > HUDVariables.MAX_MESSAGE_CHARACTERS)
            {
                while (textToAdd.Length > HUDVariables.MAX_MESSAGE_CHARACTERS)
                {
                    textInLine = SplitLine(textToAdd);
                    TextList.Add(textInLine);
                    textToAdd = textToAdd.Remove(0, indexEOLPrevious);
                }
                if (textToAdd.Length > 0)
                {
                    TextList.Add(textToAdd);
                }
            }
            else
            {
                TextList.Add(textToAdd);
            }

            lineCurrent = TextList.Count;
        }

        public void HandleUpdateText()
        {
            text = "";

            if (TextList.Count <= LineLimit)
            {
                text = UpdatePartialBox();
            }
            else
            {
                text = UpdateFullBox(scrollAmount);
            }

            textBox.SetText(text);
            if (TextList.Count > 100)
            {
            }
        }
        private string UpdatePartialBox()
        {
            string text = "";

            foreach (string lineOfText in TextList)
            {
                text += $"{Environment.NewLine}{Environment.NewLine}{lineOfText}";
            }

            return text;
        }
        private string UpdateFullBox(int scroll)
        {
            string text = "";

            for (int counter = LineLimit; counter > 0; counter--)
            {
                text += $"{Environment.NewLine}{Environment.NewLine}{TextList[(lineCurrent - counter)]}";
            }

            return text;
        }


        /************
        * Helpers   *
        *************/
        public string SeparatorSpaces(int numberDigits)
        {
            string seperator = " ";
            string seperatorFormatted = "";
            int spaces = numberDigits;

            while (spaces > 0)
            {
                seperatorFormatted += seperator;
                spaces--;
            }

            return seperatorFormatted;
        }

        public void RepopulateList(List<string> previousList)
        {
            foreach (string entry in previousList)
            {
                AddTextToTextList(entry);
            }
        }

        private string SplitLine(string text)
        {
            int indexEOL = HUDVariables.MAX_MESSAGE_CHARACTERS;
            string line = "";

            if (text.Length > HUDVariables.MAX_MESSAGE_CHARACTERS)
            {
                // If EOL splits word, split line to end of last word that fits
                if (text[HUDVariables.MAX_MESSAGE_CHARACTERS] != ' ' &&
                    text[HUDVariables.MAX_MESSAGE_CHARACTERS + 1] != ' ')
                {
                    while (text[indexEOL] != ' ')
                    {
                        indexEOL--;
                    }
                    line = text.Substring(0, indexEOL);
                    indexEOLPrevious = indexEOL;
                }
                // Otherwise return entire line that fits
                else
                {
                    line = text.Substring(0, HUDVariables.MAX_MESSAGE_CHARACTERS);
                    indexEOLPrevious = HUDVariables.MAX_MESSAGE_CHARACTERS;
                }
            }

            return line;
        }

        public void ScrollTextBox(Direction direction)
        {
            if (TextList.Count > HUDVariables.MAXIMISED_LINES_MESSAGEBOX)
            {
                int scrollToAdd = 0;
                if (direction == Direction.Up)
                {
                    scrollAmount -= 1;
                    scrollToAdd = -1;
                }
                else
                {
                    scrollAmount += 1;
                    scrollToAdd = 1;
                }

                int lineCurrentTemp = lineCurrent + scrollToAdd;

                if (lineCurrentTemp >= LineLimit &&
                    lineCurrentTemp < TextList.Count)
                {
                    scrollAmount += scrollToAdd;

                    lineCurrent = lineCurrentTemp;
                    HandleUpdateText();
                }
            }
        }

        public void UpdateStatusEnemyTurns(int activeEnemyCurrent, int activeEnemiesThusTurn)
        {
            string textToCenter = ($"({ activeEnemyCurrent}/{ activeEnemiesThusTurn})");
            string centeredText = CenterEnemiesTurns(textToCenter);
            ReplaceMessage($"{HUDVariables.PROCESS_ENEMIES_TURN} " +
            $"{Environment.NewLine} {centeredText}");
        }
        private string CenterEnemiesTurns(string text)
        {
            string textCentered = "";
            int lengthText = text.Length;
            int midPointStatus = (int)(HUDVariables.MAX_STATUS_CHARACTERS * 0.5f);
            int indexStart = (int)((midPointStatus) - (text.Length * 0.5f));
            string spaces = SeparatorSpaces(indexStart - 1);
            textCentered = $"{spaces}{text}";
            return textCentered;
        }

        private string HealthStatus(float defenderHealthPercent)
        {
            string healthStatus = " ";
            if (defenderHealthPercent >= 0.75f)
            {
                healthStatus = HUDVariables.HEALTHY;
            }
            else if (defenderHealthPercent >= 0.50f)
            {
                healthStatus = HUDVariables.INJURED;
            }
            else if (defenderHealthPercent >= 0.25f)
            {
                healthStatus = HUDVariables.BADLY_INJURED;
            }
            else if (defenderHealthPercent > 0f)
            {
                healthStatus = HUDVariables.NEAR_DEATH;
            }

            return healthStatus;
        }
    }
}