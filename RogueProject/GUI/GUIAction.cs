﻿/************************************************************
* Handles GUI-interactions by player like selecting buttons *
*************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Items;
using RogueProject.PlayerCharacter;

namespace RogueProject.GUI
{
    public class GUIAction
    {
        private Game1 game;

        // New Game menu object, created, assigned in CreateMenuNewGame()
        private NewGameMenu newGameMenu;

        // Current active component lists containing current component e.g. GUIComponentsMenuMain
        public List<GUIRectangle> CurrentComponentsList { get => currentComponentsList;  }
        // Current active component with possible subcomponents e.g. MainMenu
        public GUIRectangle CurrentComponent { get => currentComponent; }
        public GUIRectangle CurrentSubComponent { get => currentSubComponent; }
        internal NewGameMenu NewGameMenu { get => newGameMenu; set => newGameMenu = value; }

        // Components
        private List<GUIRectangle> currentComponentsList;
        private GUIRectangle currentComponent;
        private GUIRectangle currentSubComponent;
        // Item dragging
        public Item itemDragged;
        private GUIRectangle itemDraggedComponent;
        private Item itemToSwitch;
        private int indexCellOrigin;
        private int indexCellDestination;
        // Navigation
        private GUIRectangle componentNavigation;
        private static Vector2 widthHeightGrid;
        private int capacityGrid = (int)widthHeightGrid.X * (int)widthHeightGrid.Y;
        private int indexNavigation;
        // Statuses
        public bool ClickedStatusBox;
        private int currentSubComponentIndex;
        public bool inventoryOpen;        
        private bool characterSheetOpen;        

        public GUIAction(Game1 game)
        {
            this.game = game;
        }


        /********************************
        * In-menu component selection   * 
        *********************************/
        public void SetCurrentComponent(List<GUIRectangle> currentComponentsList, GUIRectangle component)
        {
            this.currentComponentsList = currentComponentsList;

            currentComponent = component;
        }
        public void UnsetCurrentComponent()
        {
            SetCurrentSubComponent(currentSubComponent, false);
            currentSubComponent = null;
            currentComponent = null;
            currentComponentsList = null;
        }
        public void SetCurrentSubComponent(GUIRectangle subComponent, bool state)
        {
            if (state)
            {
                currentSubComponent = subComponent;
                if (NeedToHilightSubComponent())
                {
                    HighlightButton(subComponent, state);
                }
            }
            else if (subComponent != null)
            {
                if (NeedToHilightSubComponent())
                {
                    HighlightButton(subComponent, state);
                }
            }
        }
        public void UnsetCurrentSubComponent()
        {
            if (currentSubComponent != null)
            {
                currentSubComponent.SpriteCategory = currentSubComponent.SpriteCategoryDefault;
                currentSubComponent = null;
            }
        }
        private bool NeedToHilightSubComponent()
        {
            if (CurrentComponent.TextHeader == GUIVariables.HEADING_PANEL_NEWGAME)
            {
                return false;
            }
            return true;
        }
        private void HighlightButton(GUIRectangle subComponent, bool state)
        {
            if (state)
            {
                subComponent.SpriteCategory = Textures.GUISpriteCategory.RectBlackGrey;
            }
            else
            {
                currentSubComponent.SpriteCategory = currentSubComponent.SpriteCategoryDefault;
            }

        }


        /********************************
        * In-game component selection   * 
        *********************************/
        public void SetCurrentComponentGame(GUIRectangle component)
        {
            currentComponent = component;
        }
        public void UnsetCurrentComponentGame()
        {
            currentComponent = null;
        }
        public void SetCurrentSubComponentGame(GUIRectangle subComponent)
        {
            currentSubComponent = subComponent;
            if (currentSubComponent.TextHeader == GUIVariables.HEADING_CELL_EQUIPMENT ||
                currentSubComponent.TextHeader == GUIVariables.HEADING_CELL_INVENTORY ||
                currentSubComponent.TextHeader == GUIVariables.HEADING_CELL_TRINKETS)
            {
                indexNavigation = currentSubComponent.Parent.Subcomponents.IndexOf(currentSubComponent);
                HandleOverItem();
            }
        }


        /********************************
        * Keyboard navigation in menu   * 
        *********************************/
        public void SetCurrentSubComponentKeyboard(Direction direction)
        {
            int indexChange = UpdateKeyboardIndexSelection(direction);
            SetCurrentSubComponent(CurrentSubComponent, false);

            currentSubComponent = CurrentComponent.Subcomponents[indexChange];
            currentSubComponentIndex = indexChange;
            SetCurrentSubComponent(currentSubComponent, true);
        }
        private int UpdateKeyboardIndexSelection(Direction direction)
        {
            int indexChange = IndexChange(direction);
            int indexNew = currentSubComponentIndex + indexChange;

            if (direction == Direction.Up || direction == Direction.Down)
            {
                indexNew = IndexVerticalIteration(indexNew);
            }
            if (direction == Direction.Right || direction == Direction.Left)
            {
                indexNew = IndexHorizontalIteration(indexNew, indexChange);
            }

            return indexNew;
        }
        private int IndexChange(Direction direction)
        {
            int indexChange = 0;

            if (direction == Direction.Down)
            {
                indexChange = CurrentComponent.SubComponentColumns;
            }
            if (direction == Direction.Up)
            {
                indexChange = -CurrentComponent.SubComponentColumns;
            }
            if (direction == Direction.Right)
            {
                indexChange = 1;
            }
            if (direction == Direction.Left)
            {
                indexChange = -1;
            }

            return indexChange;
        }
        private int IndexVerticalIteration(int indexNew)
        {
            // Iterate from last col, row to first col, row
            if (currentSubComponentIndex == CurrentComponent.Subcomponents.Count - 1)
            {
                if (indexNew > CurrentComponent.Subcomponents.Count - 1)
                {
                    indexNew = 0;
                }
            }
            // Iterate from first col, row to last col, row
            else if (currentSubComponentIndex == 0)
            {
                if (indexNew < 0)
                {
                    indexNew = CurrentComponent.Subcomponents.Count - 1;
                }
            }
            // Iterate normally
            else if (currentSubComponentIndex < CurrentComponent.Subcomponents.Count - 1)
            {
                // Iterate from bottom col to top column on next row
                if (indexNew > CurrentComponent.Subcomponents.Count - 1)
                {
                    indexNew -= CurrentComponent.Subcomponents.Count - 1;
                }
                // Iterate from top col to bottom col on previous row
                else if (indexNew < 0)
                {
                    indexNew += CurrentComponent.Subcomponents.Count - 1;
                }
            }

            return indexNew;
        }
        private int IndexHorizontalIteration(int indexNew, int indexChange)
        {
            // Iterate from last col, row to first col, row
            if (currentSubComponentIndex == CurrentComponent.Subcomponents.Count - 1)
            {
                if (indexNew > CurrentComponent.Subcomponents.Count - 1)
                {
                    indexNew = 0;
                }
            }
            // Iterate from first col, row to last col, row
            else if (currentSubComponentIndex == 0)
            {
                if (indexNew < 0)
                {
                    indexNew = CurrentComponent.Subcomponents.Count - 1;
                }
            }
            // Iterate normally
            else if (currentSubComponentIndex < CurrentComponent.Subcomponents.Count)
            {
                indexNew = currentSubComponentIndex + indexChange;
            }

            return indexNew;
        }


        /************************************
        * Keyboard grid navigation in game  * 
        *************************************/
        public void NavigateHUDComponentKeyboard(Direction direction)
        {
            UpdateGridNavVariables(componentNavigation);

            if (direction == Direction.Up)
            {
                NavigateGridUp();
            }
            if (direction == Direction.Right)
            {
                NavigateGridRight();
            }
            if (direction == Direction.Down)
            {
                NavigateGridDown();
            }
            if (direction == Direction.Left)
            {
                NavigateGridLeft();
            }
            if (currentComponent != null)
            {
                currentSubComponent = currentComponent.Subcomponents[indexNavigation];
            }

            HandleOverItem();
        }
        public void KeyboardHUDComponentInteract()
        {
            switch (componentNavigation.TextHeader)
            {
                case GUIVariables.HEADING_INVENTORY:
                    InteractInventory();
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    InteractEquipment();
                    break;
            }
        }
        private void UpdateGridNavVariables(GUIRectangle component)
        {
            switch (component.TextHeader)
            {
                case GUIVariables.HEADING_INVENTORY:
                    widthHeightGrid.X = GUIVariables.WIDTH_INVENTORY;
                    widthHeightGrid.Y = GUIVariables.HEIGHT_INVENTORY;
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    widthHeightGrid.X = GUIVariables.WIDTH_EQUIPMENT;
                    widthHeightGrid.Y = GUIVariables.HEIGHT_EQUIPMENT;
                    break;
            }
            capacityGrid = (int)widthHeightGrid.X * (int)widthHeightGrid.Y;
        }
        private void ChangeCurrentNavComponent(GUIRectangle component)
        {
            switch (component.TextHeader)
            {
                case GUIVariables.HEADING_INVENTORY:
                    componentNavigation = GUILists.GetHUDComponentByName(GUIVariables.HEADING_EQUIPMENT);
                    break;
                case GUIVariables.HEADING_EQUIPMENT:
                    componentNavigation = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);
                    break;
            }
            currentComponent = componentNavigation;
            indexNavigation = 0;
            currentSubComponent = currentComponent.Subcomponents[indexNavigation];
        }

        private void NavigateGridUp()
        {
            // Top row of grid
            if (indexNavigation < widthHeightGrid.X)
            {
                if (currentComponent.TextHeader == GUIVariables.HEADING_INVENTORY ||
                    currentComponent.TextHeader == GUIVariables.HEADING_EQUIPMENT)
                {
                    ChangeCurrentNavComponent(currentComponent);
                }
                else
                {
                    // NAVIGATE TO BOTTOM OF GRID
                }
            }
            else
            {
                indexNavigation -= (int)(widthHeightGrid.X);
            }
        }
        private void NavigateGridRight()
        {
            // Not on right edge
            if ((indexNavigation + 1) % (int)widthHeightGrid.X != 0 || indexNavigation == 0)
            {
                indexNavigation += 1;
            }
            else
            {
                indexNavigation -= (int)(widthHeightGrid.X - 1);
            }
        }
        private void NavigateGridDown()
        {
            // Bottom row
            if (indexNavigation >= (capacityGrid - widthHeightGrid.X))
            {
                if (currentComponent.TextHeader == GUIVariables.HEADING_INVENTORY ||
                    currentComponent.TextHeader == GUIVariables.HEADING_EQUIPMENT)
                {
                    ChangeCurrentNavComponent(currentComponent);
                }
                else
                {
                    // NAVIGATE TO TOP OF GRID
                }
            }
            else
            {
                indexNavigation += (int)(widthHeightGrid.X);
            }
        }
        private void NavigateGridLeft()
        {
            // Not on left edge
            if (indexNavigation % widthHeightGrid.X != 0)
            {
                indexNavigation -= 1;
            }
            else
            {
                indexNavigation += (int)(widthHeightGrid.X - 1);
            }
        }


        /********************************
        * Mouse sub-component selection * 
        *********************************/
        public void SetCurrentSubComponentMouse(GUIRectangle subComponent)
        {
            SetCurrentSubComponent(CurrentSubComponent, false);

            currentSubComponent = subComponent;
            currentSubComponentIndex = CurrentComponent.Subcomponents.IndexOf(currentSubComponent);
            SetCurrentSubComponent(currentSubComponent, true);
        }


        /************************
        * Component interaction * 
        *************************/
        public void InteractComponent()
        {
            if (currentComponent != null)
            {
                switch (CurrentComponent.TextHeader)
                {
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE:
                        InteractTextBoxMessage(currentComponent);
                        break;
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS:
                        InteractTextBoxStatus(currentComponent);
                        break;
                }
                CheckResetStatusTextBox();
            }
        }
        public void InteractSubComponent()
        {
            if (currentSubComponent != null)
            {
                if (currentSubComponent.Active)
                {
                    switch (CurrentComponent.TextHeader)
                    {
                        // Menus
                        case GUIVariables.HEADING_MENU_MAIN:
                            InteractMainMenu();
                            break;
                        case GUIVariables.HEADING_MENU_SAVE:
                            InteractSaveMenu();
                            break;
                        case GUIVariables.HEADING_MENU_LOAD:
                            InteractLoadMenu();
                            break;
                        case GUIVariables.HEADING_PANEL_NEWGAME:
                            InteractNewGameMenu();
                            break;
                        // HUD
                        case GUIVariables.HEADING_PANEL_HUD_TOP:
                            InteractHUDTop();
                            break;
                        case GUIVariables.HEADING_INVENTORY:
                            InteractInventory();
                            break;
                        case GUIVariables.HEADING_EQUIPMENT:
                            InteractEquipment();
                            break;
                        case GUIVariables.HEADING_TRINKETS:
                            InteractTrinkets();
                            break;
                        case GUIVariables.HEADING_PANEL_HUD_BOTTOM:
                            InteractHUDBottom();
                            break;
                    }
                }
            }
            CheckResetStatusTextBox();
        }


        /************************************
        * Menu GUI component interactions   * 
        *************************************/
        private void InteractMainMenu()
        {
            if (currentComponent != null)
            {
                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.TEXT_BACK:
                        MenuMainToGame();
                        break;
                    case GUIVariables.TEXT_NEW_GAME:
                        ToggleMainMenu(false);
                        ToggleNewGame(true);
                        //game.NewGame();
                        break;
                    case GUIVariables.TEXT_SAVE:
                        ToggleMainMenu(false);
                        ToggleSave(true);
                        break;
                    case GUIVariables.TEXT_LOAD:
                        ToggleMainMenu(false);
                        ToggleLoad(true);
                        break;
                    case GUIVariables.TEXT_QUIT:
                        game.Exit();
                        break;
                }
            }
        }
        private void InteractSaveMenu()
        {
            if (currentComponent != null)
            {
                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.TEXT_SAVE_SLOT_1:
                        game.Save(GUIVariables.TEXT_SAVE_SLOT_1);
                        MenuMainToGame();
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_2:
                        game.Save(GUIVariables.TEXT_SAVE_SLOT_2);
                        MenuMainToGame();
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_3:
                        game.Save(GUIVariables.TEXT_SAVE_SLOT_3);
                        MenuMainToGame();
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_4:
                        game.Save(GUIVariables.TEXT_SAVE_SLOT_4);
                        MenuMainToGame();
                        break;
                    case GUIVariables.TEXT_BACK:
                        ToggleSave(false);
                        ToggleMainMenu(true);
                        break;
                }
            }
        }
        private void InteractLoadMenu()
        {
            if (currentComponent != null)
            {
                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.TEXT_SAVE_SLOT_1:
                        game.Load(GUIVariables.TEXT_SAVE_SLOT_1);
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_2:
                        game.Load(GUIVariables.TEXT_SAVE_SLOT_2);
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_3:
                        game.Load(GUIVariables.TEXT_SAVE_SLOT_3);
                        break;
                    case GUIVariables.TEXT_SAVE_SLOT_4:
                        game.Load(GUIVariables.TEXT_SAVE_SLOT_4);
                        break;
                    case GUIVariables.TEXT_BACK:
                        ToggleLoad(false);
                        ToggleMainMenu(true);
                        break;
                }
            }
        }
        private void InteractNewGameMenu()
        {
            if (currentComponent != null)
            {
                if (currentSubComponent.TextHeader == GUIVariables.HEADING_FIELD_NAME)
                {
                    game.InputKeyboard.UserIsTyping = true;
                }
                else
                {
                    game.InputKeyboard.UserIsTyping = false;
                }

                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.TEXT_ICON_FEMALE:
                        newGameMenu.SetGender(Gender.Female);
                        break;
                    case GUIVariables.TEXT_ICON_MALE:
                        newGameMenu.SetGender(Gender.Male);
                        break;
                    case GUIVariables.TEXT_PROMPT__STATS_ROLL:
                        newGameMenu.ReRollStats();
                        break;
                    case GUIVariables.TEXT_START:
                        newGameMenu.StartNameGame();
                        break;

                    case GUIVariables.TEXT_ICON_PLUS_AGILITY:
                        newGameMenu.PlusStat(Stat.Agility);
                        break;
                    case GUIVariables.TEXT_ICON_PLUS_MAGIC:
                        newGameMenu.PlusStat(Stat.Magic);
                        break;
                    case GUIVariables.TEXT_ICON_PLUS_STRENGTH:
                        newGameMenu.PlusStat(Stat.Strength);
                        break;
                    case GUIVariables.TEXT_ICON_MINUS_AGILITY:
                        newGameMenu.MinusStat(Stat.Agility);
                        break;
                    case GUIVariables.TEXT_ICON_MINUS_MAGIC:
                        newGameMenu.MinusStat(Stat.Magic);
                        break;
                    case GUIVariables.TEXT_ICON_MINUS_STRENGTH:
                        newGameMenu.MinusStat(Stat.Strength);
                        break;
                }

                currentSubComponent = null;
            }
        }


        /************************************
        * Game GUI component interactions   * 
        *************************************/
        private void InteractHUDTop()
        {
            if (currentComponent != null)
            {
                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.HEADING_BAR_EXPERIENCE:
                        game.TextBoxes.TextBoxMessages.UpdateMessage($"{GUIVariables.TEXT_BAR_EXPERIENCE} {game.PlayerStats.ExperienceCurrent}{GUIVariables.SEPARATOR}{game.PlayerStats.ExperienceFull}");
                        break;
                    case GUIVariables.TEXT_ICON_CHARACTER_SHEET:
                        ToggleCharacterSheet();
                        break;
                    case GUIVariables.TEXT_ICON_INVENTORY:
                        ToggleInventory();
                        break;
                    case GUIVariables.TEXT_ICON_MAIN_MENU:
                        GameToMenuMain();
                        break;
                }
            }
        }
        private void InteractInventory()
        {
            int indexCellClicked = currentSubComponent.Parent.Subcomponents.IndexOf(currentSubComponent);
            // If not dragging item
            if (itemDragged == null)
            {
                InteractInventoryCellNoDrag(indexCellClicked);
            }
            // If dragging item
            else
            {
                indexCellDestination = indexCellClicked;
                InteractInventoryCellDrag(indexCellClicked);
            }
            
        }
        private void InteractEquipment()
        {
            int indexCellClicked = currentSubComponent.Parent.Subcomponents.IndexOf(currentSubComponent);
            // If not dragging item
            if (itemDragged == null)
            {
                InteractEquipmentCellNoDrag(indexCellClicked);
            }
            // If dragging item
            else
            {
                indexCellDestination = indexCellClicked;
                if (game.PlayerInventory.EquipRequirements.AbleToEquipItemEquipment(indexCellDestination, itemDragged))
                {
                    InteractEquipmentCellDrag(indexCellClicked);
                }
            }

        }
        private void InteractTrinkets()
        {
            int indexCellClicked = currentSubComponent.Parent.Subcomponents.IndexOf(currentSubComponent);
            // If not dragging item
            if (itemDragged == null)
            {
                InteractTrinketCellNoDrag(indexCellClicked);
            }
            // If dragging item
            else
            {
                indexCellDestination = indexCellClicked;
                if (game.PlayerInventory.EquipRequirements.AbleToEquipItemTrinket(indexCellDestination, itemDragged))
                {
                    InteractTrinketCellDrag(indexCellClicked);
                }
            }
        }

        private void InteractInventoryCellNoDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Inventory[indexCellClicked] != null)
            {
                indexCellOrigin = indexCellClicked;
                itemDragged = game.PlayerInventory.Inventory[indexCellClicked];
                itemDraggedComponent = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);
            }
        }
        private void InteractInventoryCellDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Inventory[indexCellClicked] != null)
            {
                if (game.PlayerInventory.Inventory[indexCellClicked] != itemDragged)
                {
                    itemToSwitch = game.PlayerInventory.Inventory[indexCellClicked];
                    game.PlayerInventory.SwitchItemsInventory(itemDragged, itemToSwitch, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                    itemDragged = null;
                    itemToSwitch = null;
                }
            }
            // Unoccupied cell
            else
            {
                game.PlayerInventory.MoveItemToInventoryCell(itemDragged, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                itemDragged = null;
            }
        }
        private void InteractEquipmentCellNoDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Equipment[indexCellClicked] != null)
            {
                indexCellOrigin = indexCellClicked;
                itemDragged = game.PlayerInventory.Equipment[indexCellClicked];
                itemDraggedComponent = GUILists.GetHUDComponentByName(GUIVariables.HEADING_EQUIPMENT);
            }
        }
        private void InteractEquipmentCellDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Equipment[indexCellClicked] != null)
            {
                if (game.PlayerInventory.Equipment[indexCellClicked] != itemDragged)
                {
                    itemToSwitch = game.PlayerInventory.Equipment[indexCellClicked];
                    game.PlayerInventory.SwitchItemsEquipment(itemDragged, itemToSwitch, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                    itemDragged = null;
                    itemToSwitch = null;
                }
            }
            // Unoccupied cell
            else
            {
                game.PlayerInventory.MoveItemToEquipmentCell(itemDragged, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                itemDragged = null;
            }
        }
        private void InteractTrinketCellDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Trinkets[indexCellClicked] != null)
            {
                if (game.PlayerInventory.Trinkets[indexCellClicked] != itemDragged)
                {
                    itemToSwitch = game.PlayerInventory.Trinkets[indexCellClicked];
                    game.PlayerInventory.SwitchItemsTrinket(itemDragged, itemToSwitch, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                    itemDragged = null;
                    itemToSwitch = null;
                }
            }
            // Unoccupied cell
            else
            {
                game.PlayerInventory.MoveItemToTrinketCell(itemDragged, indexCellOrigin, indexCellDestination, itemDraggedComponent.TextHeader);
                itemDragged = null;
            }
        }
        private void InteractTrinketCellNoDrag(int indexCellClicked)
        {
            // Occupied cell
            if (game.PlayerInventory.Trinkets[indexCellClicked] != null)
            {
                indexCellOrigin = indexCellClicked;
                itemDragged = game.PlayerInventory.Trinkets[indexCellClicked];
                itemDraggedComponent = GUILists.GetHUDComponentByName(GUIVariables.HEADING_TRINKETS);
            }
        }
        private void InteractHUDBottom()
        {
            if (currentComponent != null)
            {
                switch (currentSubComponent.TextHeader)
                {
                    case GUIVariables.HEADING_BAR_HEALTH:
                        game.TextBoxes.TextBoxMessages.UpdateMessage($"{GUIVariables.TEXT_BAR_HEALTH} {game.PlayerStats.HealthCurrent}{GUIVariables.SEPARATOR}{game.PlayerStats.HealthTotal}");
                        break;
                    case GUIVariables.HEADING_BAR_MANA:
                        game.TextBoxes.TextBoxMessages.UpdateMessage($"{GUIVariables.TEXT_BAR_MANA} {game.PlayerStats.ManaCurrent}{GUIVariables.SEPARATOR}{game.PlayerStats.ManaTotal}");
                        break;
                }
            }
        }
        private void InteractTextBoxMessage(GUIRectangle component)
        {
            if (component != null)
            {
                switch (component.TextHeader)
                {
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE:
                        if (component.Size.Y > 1)
                        {
                            ToggleMaxmisiseMessageBox(false);
                        }
                        else
                        {
                            ToggleMaxmisiseMessageBox(true);
                        }
                        break;
                    case GUIVariables.HEADING_BAR_HEALTH:
                        game.TextBoxes.TextBoxMessages.UpdateMessage($"{GUIVariables.TEXT_BAR_HEALTH} {game.PlayerStats.HealthCurrent}");
                        break;
                    case GUIVariables.HEADING_BAR_MANA:
                        game.TextBoxes.TextBoxMessages.UpdateMessage($"{GUIVariables.TEXT_BAR_MANA} {game.PlayerStats.ManaCurrent}");
                        break;
                }
            }
        }
        private void InteractTextBoxStatus(GUIRectangle component)
        {
            if (component.TextHeader == GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS)
            {
                if (!ClickedStatusBox)
                {
                    game.TextBoxes.TextBoxStatus.ReplaceMessage(HUDVariables.CLICK_TO_END_TURN);
                    ClickedStatusBox = true;
                }
                else
                {
                    ClickedStatusBox = false;
                    game.PlayerAction.EndTurn();
                }
            }
        }
        public void CheckResetStatusTextBox()
        {
            if (CurrentComponent != null)
            {
                if (CurrentComponent.TextHeader != GUIVariables.HEADING_PANEL_TEXT_BOX_STATUS)
                {
                    game.PlayerAction.UpdateStatusTextNewTurn();
                    ClickedStatusBox = false;
                }
            }
            else
            {
                game.PlayerAction.UpdateStatusTextNewTurn();
                ClickedStatusBox = false;
            }
        }
        public void InteractScroll(Direction direction)
        {
            if (CurrentComponent != null)
            {
                switch (CurrentComponent.TextHeader)
                {
                    case GUIVariables.HEADING_PANEL_TEXT_BOX_MESSAGE:
                        game.TextBoxes.TextBoxMessages.ScrollTextBox(direction);
                        break;
                }
            }
        }

        /************************************
        * Toggle menu components on, off    * 
        *************************************/
        public void ToggleMainMenu(bool state)
        {
            if (state)
            {
                GUILists.ResetGUILists();
                if (game.TextBoxes.TextBoxMessages != null)
                {
                    HUDVariables.MessageTemporary = game.TextBoxes.TextBoxMessages.TextList;
                }
                game.GUIMaker.CreateMenuMain();
                SetCurrentComponent(GUILists.GUIComponentsMenuMain, GUILists.GUIComponentsMenuMain[1]);
            }
            else
            {
                UnsetCurrentComponent();
                GUILists.ResetGUILists();
            }
        }
        public void ToggleNewGame(bool state)
        {
            if (state)
            {
                GUILists.ResetGUILists();
                game.GUIMaker.CreateMenuNewGame();
                SetCurrentComponent(GUILists.GUIComponentsMenuNewGame, GUILists.GUIComponentsMenuNewGame[1]);
            }
            else
            {
                UnsetCurrentComponent();
                GUILists.ResetGUILists();
                game.InputKeyboard.UserIsTyping = false;
            }
        }
        public void ToggleSave(bool state)
        {
            if (state)
            {
                GUILists.ResetGUILists();
                game.GUIMaker.CreateMenuSave();
                SetCurrentComponent(GUILists.GUIComponentsMenuSave, GUILists.GUIComponentsMenuSave[1]);
            }
            else
            {
                UnsetCurrentComponent();
                GUILists.ResetGUILists();
            }
        }
        public void ToggleLoad(bool state)
        {
            if (state)
            {
                GUILists.ResetGUILists();
                game.GUIMaker.CreateMenuLoad();
                SetCurrentComponent(GUILists.GUIComponentsMenuLoad, GUILists.GUIComponentsMenuLoad[1]);
            }
            else
            {
                UnsetCurrentComponent();
                GUILists.ResetGUILists();
            }
        }


        /************************************
        * Toggle game components on, off    * 
        *************************************/
        public void ToggleHUD(bool state)
        {
            if (state)
            {
                GUILists.ResetGUILists();
                game.GUIMaker.HandleCreateHUD();
                SetCurrentComponent(GUILists.GUIComponentsHUD, GUILists.GUIComponentsHUD[0]);
                if (Game1.GameStarted)
                {
                    HUDMove.MoveHUD(game.Player.Tile.PositionGrid);
                }
            }
            else
            {
                UnsetCurrentComponent();
                GUILists.ResetGUILists();
            }
        }
        public void ToggleMaxmisiseMessageBox(bool state)
        {
            if (state)
            {
                CurrentComponent.Resize(new Vector2(0f, +3f));
                CurrentComponent.MoveComponentBy(new Vector2(0f, -3f));
                game.TextBoxes.TextBoxMessages.LineLimit = HUDVariables.MAXIMISED_LINES_MESSAGEBOX;
                game.TextBoxes.TextBoxMessages.HandleUpdateText();
                HUDMove.MoveHUD(game.Player.Tile.PositionGrid);
            }
            else
            {
                CurrentComponent.Resize(new Vector2(0f, -3f));
                CurrentComponent.MoveComponentBy(new Vector2(0f, +3f));
                game.TextBoxes.TextBoxMessages.LineLimit = HUDVariables.MINIMSED_LINES_MESSAGEBOX;
                game.TextBoxes.TextBoxMessages.HandleUpdateText();
                HUDMove.MoveHUD(game.Player.Tile.PositionGrid);
            }
        }


        /********************************
        * Open HUD sub-component panels * 
        *********************************/
        public void ToggleCharacterSheet()
        {
            characterSheetOpen = !characterSheetOpen;

            indexNavigation = 0;

            if (characterSheetOpen)
            {
                if (inventoryOpen)
                {
                    ToggleInventory();
                }

                game.GUIMaker.CreateCharacterSheet();
                SetCurrentComponentGame(GUILists.GetHUDComponentByName(GUIVariables.HEADING_CHARACTER_SHEET));
                game.PlayerStats.characterSheet.PopulateCharacterSheetFields();
                HUDMove.MoveHUD(game.Player.Tile.PositionGrid);
            }
            else
            {
                componentNavigation = null;
                GUILists.GUIComponentsHUD.Remove(GUILists.GetHUDComponentByName(GUIVariables.HEADING_CHARACTER_SHEET));
            }
        }
        public void ToggleInventory()
        {
            inventoryOpen = !inventoryOpen;

            indexNavigation = 0;

            if (inventoryOpen)
            {
                if (characterSheetOpen)
                {
                    ToggleCharacterSheet();
                }
                game.GUIMaker.CreateInventory();
                SetCurrentComponentGame(GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY));
                SetCurrentSubComponentGame(CurrentComponent.Subcomponents[0]);
                HUDMove.MoveHUD(game.Player.Tile.PositionGrid);
                componentNavigation = GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY);
                GUILists.GetHUDComponentByName(GUIVariables.HEADING_INSEPCTION).ToggleOn(false);
            }
            else
            {
                ClearDraggedItem();
                componentNavigation = null;
                GUILists.GUIComponentsHUD.Remove(GUILists.GetHUDComponentByName(GUIVariables.HEADING_INVENTORY));
                GUILists.GUIComponentsHUD.Remove(GUILists.GetHUDComponentByName(GUIVariables.HEADING_EQUIPMENT));
                GUILists.GUIComponentsHUD.Remove(GUILists.GetHUDComponentByName(GUIVariables.HEADING_TRINKETS));
                GUILists.GUIComponentsHUD.Remove(GUILists.GetHUDComponentByName(GUIVariables.HEADING_INSEPCTION));
            }
        }
        public void ToggleInspection(Item item)
        {
            if (item != null)
            {
                GUILists.GetHUDComponentByName(GUIVariables.HEADING_INSEPCTION).ToggleOn(true);
                game.PlayerInventory.hudInspectionItem.DisplayInspectionItem(item);
            }
            else
            {
                GUILists.GetHUDComponentByName(GUIVariables.HEADING_INSEPCTION).ToggleOn(false);
            }
        }


        /************************
        * Game state changes    * 
        *************************/
        public void ToggleEscape()
        {
            if (inventoryOpen)
            {
                ToggleInventory();
            }
            else if (characterSheetOpen)
            {
                ToggleCharacterSheet();
            }
            else
            {
                GameToMenuMain();
            }
        }
        private void GameToMenuMain()
        {
            if (inventoryOpen)
            {
                ToggleInventory();
            }
            Game1.InGame = false;
            ToggleMainMenu(true);
            Game1.InMainMenu = true;
        }
        public void MenuMainToGame()
        {
            UnsetCurrentComponent();

            GUILists.ResetGUILists();
            Game1.InMainMenu = false;
            game.Camera.Move(game.Player.Tile.Position);
            ToggleHUD(true);
            Game1.InGame = true;
        }

        /************
        * Helpers   * 
        *************/
        public void ClearDraggedItem()
        {
            if (itemDragged != null)
            {
                itemDragged = null;
            }
        }

        private void HandleOverItem()
        {
            ToggleInspection(IndexOverItem());
        }
        private Item IndexOverItem()
        {
            Item item = null;
            switch (currentSubComponent.TextHeader)
            {
                case GUIVariables.HEADING_CELL_EQUIPMENT:
                    item = game.PlayerInventory.Equipment[indexNavigation];
                    break;
                case GUIVariables.HEADING_CELL_INVENTORY:
                    item = game.PlayerInventory.Inventory[indexNavigation];
                    break;
                case GUIVariables.HEADING_CELL_TRINKETS:
                    item = game.PlayerInventory.Trinkets[indexNavigation];
                    break;
            }
            return item;
        }
    }
}