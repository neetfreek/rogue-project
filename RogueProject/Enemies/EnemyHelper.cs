﻿/********************************************************** *
* Helper functionality for common enemy game object tasks.  *
*************************************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Terrain;

namespace RogueProject.Enemies
{
    class EnemyHelper
    {
        public static bool InRangePlayer(Tile tilePlayer, Tile tileEnemy, int aggroRangeAdjustment)
        {
            float distanceX = Math.Abs(tilePlayer.PositionGrid.X - tileEnemy.PositionGrid.X);
            float distanceY = Math.Abs(tilePlayer.PositionGrid.Y - tileEnemy.PositionGrid.Y);
            if (distanceX <= EnemyVariables.AGGRO_RANGE_X + aggroRangeAdjustment &&
                distanceY <= EnemyVariables.AGGRO_RANGE_Y + aggroRangeAdjustment)
            {
                return true;
            }

            return false;
        }   
        
        public static Vector2 DistanceFromPlayer(Tile tilePlayer, Tile tileEnemy, int aggroRangeAdjustment)
        {
            Vector2 distance = tilePlayer.PositionGrid - tileEnemy.PositionGrid;
            distance.X = Math.Abs(distance.X);
            distance.Y = Math.Abs(distance.Y);

            return distance;
        }

        public static Tile CircumventObstacle(Tile tilePlayer, Tile tileEnemy, Direction directionTried)
        {
            Tile tileToMoveTo = tileEnemy;            

            // If tried Left, Right, try Up, Down
            if (directionTried == Direction.Left || directionTried == Direction.Right)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Up, 1), tileEnemy.Neighbour(Direction.Down, 1), tilePlayer, tileEnemy);
            }
            // If tried Up, Down, try Left, Right
            else if (directionTried == Direction.Up || directionTried == Direction.Down)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Left, 1), tileEnemy.Neighbour(Direction.Right, 1), tilePlayer, tileEnemy);
            }

            // If tried UpLeft, try Up, Left
            else if(directionTried == Direction.UpLeft)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Up, 1), tileEnemy.Neighbour(Direction.Left, 1), tilePlayer, tileEnemy);
            }

            // If tried DownLeft, try Down, Left
            else if(directionTried == Direction.DownLeft)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Down, 1), tileEnemy.Neighbour(Direction.Left, 1), tilePlayer, tileEnemy);
            }

            // If tried UpRight, try Up, Right
            else if(directionTried == Direction.UpRight)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Up, 1), tileEnemy.Neighbour(Direction.Right, 1), tilePlayer, tileEnemy);
            }

            // If tried DownRight, try Down, Right
            else if(directionTried == Direction.DownRight)
            {
                tileToMoveTo = TileCloserToPlayer(tileEnemy.Neighbour(Direction.Down, 1), tileEnemy.Neighbour(Direction.Right, 1), tilePlayer, tileEnemy);
            }

            return tileToMoveTo;
        }

        private static Tile TileCloserToPlayer(Tile tileOption1, Tile tileOption2, Tile tilePlayer, Tile tileEnemy)
        {
            Tile tile = tileOption1;

            bool tile1Walkable = true;
            bool tile2Walkable = true;

            if (!tileOption1.Walkable)
            {
                tile1Walkable = false;
            }
            if (!tileOption2.Walkable)
            {
                tile2Walkable = false;
            }

            // If both walkable, select by distance
            if (tile1Walkable && tile2Walkable)
            {
                if (Math.Abs(DistanceFromPlayer(tilePlayer, tileOption1, 0).X + DistanceFromPlayer(tilePlayer, tileOption1, 0).Y)
                    > Math.Abs(DistanceFromPlayer(tilePlayer, tileOption2, 0).X + DistanceFromPlayer(tilePlayer, tileOption2, 0).Y))
                {
                    tile = tileOption2;
                }
            }            
            else if (tile1Walkable && !tile2Walkable)
            {
                tile = tileOption1;
            }
            else if (tile2Walkable)
            {
                tile = tileOption2;
            }
            else
            {
                tile = tileEnemy;
            }

            return tile;
        }
    }
}