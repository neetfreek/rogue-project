﻿/************************************************************
* Contains various helper methods related to setting up     *
*   which EnemyCategories, EnemyTypes to add to an area.    *
* Called by EnemyMaker.cs                                   *
*************************************************************/
using System;
using System.Collections.Generic;
using RogueProject.Terrain;

namespace RogueProject.Enemies
{
    public static class EnemyHelperMaker
    {
        private static Random random;

        // Return EnemyCatergory EnemyType belongs to
        public static EnemyCategory EnemyCategory(EnemyType enemyType)
        {
            EnemyCategory enemyCategory = Enemies.EnemyCategory.None;

            foreach (EnemyCategoryAvian avian in (EnemyCategoryAvian[])Enum.GetValues(typeof(EnemyCategoryAvian)))
            {
                if (avian.ToString() == enemyType.ToString())
                {
                    return Enemies.EnemyCategory.Avian;
                }
            }

            foreach (EnemyCategoryPest pest in (EnemyCategoryPest[])Enum.GetValues(typeof(EnemyCategoryPest)))
            {
                if (pest.ToString() == enemyType.ToString())
                {
                    return Enemies.EnemyCategory.Pest;
                }
            }

            foreach (EnemyCategoryRodent rodent in (EnemyCategoryRodent[])Enum.GetValues(typeof(EnemyCategoryRodent)))
            {
                if (rodent.ToString() == enemyType.ToString())
                {
                    return Enemies.EnemyCategory.Rodent;
                }
            }
            return enemyCategory;
        }

        // Return number of EnemyCategories in EnemyType list
        public static int NumberEnemyCategoriesInList(List<EnemyType> enemyList)
        {
            List<EnemyType> enemyListToConsider = enemyList;
            List<EnemyCategory> enemyCategories = new List<EnemyCategory>();

            foreach (EnemyType enemy in enemyListToConsider)
            {
                EnemyCategory category = EnemyCategory(enemy);
                if (!enemyCategories.Contains(category))
                {
                    enemyCategories.Add(category);
                }
            }

            return enemyCategories.Count + 1;
        }

        // Return list of EnemyCategories in EnemyType list
        public static List<EnemyCategory> EnemyCategoriesInList(List<EnemyType> enemyList)
        {
            List<EnemyType> enemyListToConsider = enemyList;
            List<EnemyCategory> enemyCategories = new List<EnemyCategory>();

            foreach (EnemyType enemy in enemyListToConsider)
            {
                EnemyCategory category = EnemyCategory(enemy);
                if (!enemyCategories.Contains(category))
                {
                    enemyCategories.Add(category);
                }
            }

            return enemyCategories;
        }

        // Return numberEnemyCategories long EnemyCategory list of EnemyCategories from EnemyType list
        public static List<EnemyCategory> SelectEnemyCategories(List<EnemyType> enemyList, int numberEnemyCategories)
        {
            random = new Random();

            List<EnemyCategory> categoriesOptions = EnemyCategoriesInList(enemyList);
            List<EnemyCategory> selectedCategories = new List<EnemyCategory>();

            int categoriesToPick = numberEnemyCategories;

            while (categoriesToPick > 0)
            {
                EnemyCategory selection = categoriesOptions[random.Next(0, categoriesOptions.Count)];
                if (!selectedCategories.Contains(selection))
                {
                    selectedCategories.Add(selection);
                    categoriesToPick--;
                }
            }

            return selectedCategories;
        }

        // Return number of enemies of certain category within area list of enemies
        public static int EnemyTypesInCategory(List<EnemyType> enemyList, EnemyCategory category)
        {
            int numberEnemiesInCategory = 0;
            foreach (EnemyType enemy in enemyList)
            {
                if (EnemyCategory(enemy) == category)
                {
                    numberEnemiesInCategory++;
                }
            }

            return numberEnemiesInCategory;
        }

        // Add new EnemyCategory from enemyList to currentCategories list, return
        public static List<EnemyCategory> ExpandCategories(List<EnemyType> enemyList, List<EnemyCategory> currentCategories)
        {
            List<EnemyCategory> categories = currentCategories;
            List<EnemyCategory> categoriesPossible = EnemyCategoriesInList(enemyList);
            List<EnemyCategory> categoriesExpanded = currentCategories;

            foreach (EnemyCategory possibleCategory in categoriesPossible)
            {
                if (!categories.Contains(possibleCategory))
                {
                    categoriesExpanded.Add(possibleCategory);
                    return categoriesExpanded;
                }
            }

            return categoriesExpanded;
        }

        // Return EnemyType list of enemies for regionType
        public static List<EnemyType> AppropriateEnemyList(RegionType regionType)
        {
            List<EnemyType> enemyList = new List<EnemyType>();

            switch (regionType)
            {
                // Outdoor regions
                case RegionType.ForestLight:
                    enemyList = EnemyRegionLists.EnemiesForestLight;
                    break;
                case RegionType.ForestDark:
                    enemyList = EnemyRegionLists.EnemiesForestDark;
                    break;
                case RegionType.ForestSnowLight:
                    enemyList = EnemyRegionLists.EnemiesForestSnowLight;
                    break;
                case RegionType.ForestSnowDark:
                    enemyList = EnemyRegionLists.EnemiesForestSnowDark;
                    break;
                case RegionType.WoodsLight:
                    enemyList = EnemyRegionLists.EnemiesWoodsLight;
                    break;
                case RegionType.WoodsDark:
                    enemyList = EnemyRegionLists.EnemiesWoodsDark;
                    break;
                case RegionType.WoodsSnowLight:
                    enemyList = EnemyRegionLists.EnemiesWoodsSnowLight;
                    break;
                case RegionType.WoodsSnowDark:
                    enemyList = EnemyRegionLists.EnemiesWoodsSnowDark;
                    break;
                case RegionType.Tropic:
                    enemyList = EnemyRegionLists.EnemiesTropic;
                    break;
                case RegionType.JungleLight:
                    enemyList = EnemyRegionLists.EnemiesJungleLight;
                    break;
                case RegionType.JungleDark:
                    enemyList = EnemyRegionLists.EnemiesJungleDark;
                    break;
                case RegionType.Oasis:
                    enemyList = EnemyRegionLists.EnemiesOasis;
                    break;
                case RegionType.DesertLight:
                    enemyList = EnemyRegionLists.EnemiesDesertLight;
                    break;
                case RegionType.DesertMedium:
                    enemyList = EnemyRegionLists.EnemiesDesertMedium;
                    break;
                case RegionType.DesertDark:
                    enemyList = EnemyRegionLists.EnemiesDesertDark;
                    break;
                // Indoor regions
                case RegionType.DungeonGrey:
                    enemyList = EnemyRegionLists.EnemiesDungeonGrey;
                    break;
                case RegionType.DungeonBlue:
                    enemyList = EnemyRegionLists.EnemiesDungeonBlue;
                    break;
                case RegionType.DungeonDark:
                    enemyList = EnemyRegionLists.EnemiesDungeonDark;
                    break;
                case RegionType.DungeonSnowLight:
                    enemyList = EnemyRegionLists.EnemiesDungeonSnowLight;
                    break;
                case RegionType.DungeonSnowDark:
                    enemyList = EnemyRegionLists.EnemiesDungeonSnowDark;
                    break;
                case RegionType.DungeonMetalOrange:
                    enemyList = EnemyRegionLists.EnemiesDungeonMetalOrange;
                    break;
                case RegionType.DungeonMetalDark:
                    enemyList = EnemyRegionLists.EnemiesDungeonMetalDark;
                    break;
                case RegionType.CavesLight:
                    enemyList = EnemyRegionLists.EnemiesCavesLight;
                    break;
                case RegionType.CavesMedium:
                    enemyList = EnemyRegionLists.EnemiesCavesMedium;
                    break;
                case RegionType.CavesDark:
                    enemyList = EnemyRegionLists.EnemiesCavesDark;
                    break;
                case RegionType.CavesIce:
                    enemyList = EnemyRegionLists.EnemiesCavesIce;
                    break;
                case RegionType.IndustrialLight:
                    enemyList = EnemyRegionLists.EnemiesIndustrialLight;
                    break;
                case RegionType.IndustrialDark:
                    enemyList = EnemyRegionLists.EnemiesIndustrialDark;
                    break;
                case RegionType.IceTunnelsLight:
                    enemyList = EnemyRegionLists.EnemiesIceTunnelsLight;
                    break;
                case RegionType.IceTunnelsDark:
                    enemyList = EnemyRegionLists.EnemiesIceTunnelsDark;
                    break;
            }

            return enemyList;
        }
    }
}