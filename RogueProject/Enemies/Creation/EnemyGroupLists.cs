﻿/********************************************
* Lists containing enemy groups in areas    *
*********************************************/
using System.Collections.Generic;

namespace RogueProject.Enemies
{
    static class EnemyGroupLists
    {
        public static List<List<Enemy>> Groups;

        public static List<Enemy> AddNewGroup()
        {
            List<Enemy> group = new List<Enemy>();
            Groups.Add(group);
            return group;
        }

        public static void ResetGroups()
        {
            if (Groups != null)
            {
                Groups.Clear();
            }
            Groups = new List<List<Enemy>>();
        }
    }
}