﻿/****************************************************************
* Handle setup of enemies for area through:                     *
* 1. AddEnemiesToArea() which calls other mehthods:            *
*   a. SelectNumberEnemies() returns how many enemies are to be *
*       created for the area                                    *
*   b. SelectEnemies() returns a list collection of which       *
*       EnemyTypes are to be added  to the area                 *
*****************************************************************/
using System;
using System.Collections.Generic;
using RogueProject.Terrain;

namespace RogueProject.Enemies
{
    public class EnemyMaker
    {
        private readonly AreaMaker areaMaker;
        private Game1 game;
        private readonly Random random;


        public EnemyMaker(Game1 game, AreaMaker areaMaker)
        {
            this.game = game;
            this.areaMaker = areaMaker;
            random = new Random();
        }


        public void AddEnemiesToArea(Game1 game)
        {
            EnemyGroupLists.ResetGroups();

            this.game = game;

            List<EnemyType> enemiesOptions = EnemyHelperMaker.AppropriateEnemyList(game.Area.RegionType);
            int numberEnemyTypes = random.Next(EnemyVariables.MIN_ENEMY_TYPES,
                EnemyVariables.MAX_ENEMY_TYPES);
            int numberEnemyCategories = random.Next(EnemyVariables.MIN_ENEMY_CATEGORIES,
                EnemyHelperMaker.NumberEnemyCategoriesInList(enemiesOptions));
            List<EnemyCategory> enemyCategories = EnemyHelperMaker.SelectEnemyCategories(enemiesOptions, numberEnemyCategories);
            List<EnemyType> enemiesSelected = SelectEnemies(enemyCategories, enemiesOptions, numberEnemyTypes);

            while (enemiesSelected.Count < numberEnemyTypes)
            {
                enemyCategories = EnemyHelperMaker.ExpandCategories(enemiesOptions, enemyCategories);
                enemiesSelected = SelectEnemies(enemyCategories, enemiesOptions, numberEnemyTypes);
            }

            game.Area.EnemyTypes = enemiesSelected;
            game.Area.Enemies = CreateEnemies(enemiesSelected);
        }

        private int SelectNumberEnemies(int numberTilesArea, Area area)
        {
            int numberTiles = numberTilesArea;
            int numberEnemiesMultiplier = 0;
            if (area.AreaType == AreaType.Indoor)
            {
                numberEnemiesMultiplier = random.Next(EnemyVariables.MIN_PORTION_TILES_ENEMIES_INDOOR,
                    EnemyVariables.MAX_PORTION_TILES_ENEMIES_INDOOR + 1);
            }
            else
            {
                numberEnemiesMultiplier = random.Next(EnemyVariables.MIN_PORTION_TILES_ENEMIES_OUTDOOR,
                    EnemyVariables.MAX_PORTION_TILES_ENEMIES_OUTDOOR + 1);
            }

            int numberEnemies = (int)((numberTilesArea * numberEnemiesMultiplier) * 0.01f);

            return numberEnemies;
        }   

        private List<EnemyType> SelectEnemies(List<EnemyCategory> enemyCategories, List<EnemyType> enemyList,
            int numberEnemyTypes)
        {
            List<EnemyCategory> specifiedCategories = enemyCategories;
            List<EnemyType> enemiesOptions = enemyList;
            int numberTypes = numberEnemyTypes;

            List<EnemyType> enemiesChosen = new List<EnemyType>();

            int selectionOld = enemiesOptions.Count + 1;
            while (numberTypes > 0 )
            {
                int selection = random.Next(0, enemiesOptions.Count);
                EnemyType enemy = enemiesOptions[selection];
                EnemyCategory category = EnemyHelperMaker.EnemyCategory(enemy);

                if (!enemiesChosen.Contains(enemy) && specifiedCategories.Contains(category))
                {
                    enemiesChosen.Add(enemy);
                }
                if (selection != selectionOld)
                {
                    selectionOld = selection;
                    numberTypes--;
                }
            }

            return enemiesChosen;
        }

        public List<Enemy> CreateEnemies(List<EnemyType> enemiesSelected)
        {
            int enemiesToCreate = SelectNumberEnemies((int)(game.Area.SizeArea.X * game.Area.SizeArea.Y), game.Area);
            List<Enemy> enemies = new List<Enemy>();

            while (enemiesToCreate > 0)
            {
                EnemyType enemyType = enemiesSelected[random.Next(0, enemiesSelected.Count)];
                enemies.Add(new Enemy(game, enemyType, EnemyHelperMaker.EnemyCategory(enemyType)));
                enemiesToCreate--;
            }

            return enemies;
        }

        public List<Enemy> LoadEnemies(List<EnemyType> enemiesSelected)
        {
            int enemiesToCreate = SelectNumberEnemies((int)(areaMaker.Area.SizeArea.X * areaMaker.Area.SizeArea.Y), areaMaker.Area);
            List<Enemy> enemies = new List<Enemy>();
            while (enemiesToCreate > 0)
            {
                EnemyType enemyType = enemiesSelected[random.Next(0, enemiesSelected.Count)];

                enemies.Add(new Enemy(game, enemyType, EnemyHelperMaker.EnemyCategory(enemyType)));
                enemiesToCreate--;
            }

            return enemies;
        }
    }
}