﻿/********************************************************************
* Lists containing different enemy types for different region types *
*********************************************************************/
using System.Collections.Generic;

namespace RogueProject.Enemies
{
    static class EnemyRegionLists
    {
        // Outdoor regions
        public static List<EnemyType> EnemiesForestDark = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormBrown,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Mosquito,
            EnemyType.MosquitoLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntBlack,

            EnemyType.GrasshopperSmall,
            EnemyType.GrasshopperLarge,

            EnemyType.LadyBugBrown,
        };
        public static List<EnemyType> EnemiesForestLight = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormTan,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Bee,     
            EnemyType.BeeLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntTan,

            EnemyType.LocustSmall,
            EnemyType.LocustLarge,


            EnemyType.LadyBugRed,
        };
        public static List<EnemyType> EnemiesForestSnowDark = new List<EnemyType>()
        {
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.KiteSnowTanLarge,
            EnemyType.KiteSnowTanSmall,
            EnemyType.KiteSnowTanSmall,
            EnemyType.KiteSnowSmall,

        };        
        public static List<EnemyType> EnemiesForestSnowLight = new List<EnemyType>()
        {
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.ChickenSnowSmall,
            EnemyType.ChickenSnowLarge,
            EnemyType.BirdFluffSnow,
        };
        
        public static List<EnemyType> EnemiesWoodsLight = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormTan,        

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntTan,

            EnemyType.LocustSmall,
            EnemyType.LocustLarge,

            EnemyType.LadyBugRed,
        };
        public static List<EnemyType> EnemiesWoodsDark = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormBrown,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Mosquito,
            EnemyType.MosquitoLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntBlack,

            EnemyType.GrasshopperSmall,
            EnemyType.GrasshopperLarge,

            EnemyType.LadyBugBrown,
        };
        public static List<EnemyType> EnemiesWoodsSnowDark = new List<EnemyType>()
        {
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.KiteSnowLarge,
            EnemyType.KiteSnowSmall,
            EnemyType.KiteSnowTanSmall,
        };
        public static List<EnemyType> EnemiesWoodsSnowLight = new List<EnemyType>()
        {
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.ChickenSnowSmall,
            EnemyType.ChickenSnowLarge,
            EnemyType.EagleSnowSmall,
            EnemyType.EagleSnowLarge,
        };

        public static List<EnemyType> EnemiesTropic = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormTan,
            EnemyType.WormBrown,

            EnemyType.Mosquito,
            EnemyType.MosquitoLarva,
            EnemyType.BugGlowWings,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntRed,

            EnemyType.LeechTan,
            EnemyType.LeechTanSmall,
            EnemyType.SnailSmall,
            EnemyType.SnailLarge,

            EnemyType.LadyBugRed,
        };
        public static List<EnemyType> EnemiesJungleLight = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormTan,


            EnemyType.Mosquito,
            EnemyType.MosquitoLarva,
            EnemyType.BugGlowWings,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntTan,
            EnemyType.AntRed,

            EnemyType.LeechTan,
            EnemyType.LeechTanSmall,
            EnemyType.SnailSmall,
            EnemyType.SnailLarge,

            EnemyType.LadyBugRed,
        };
        public static List<EnemyType> EnemiesJungleDark = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBlack,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,
            EnemyType.WormBrown,

            EnemyType.Mosquito,
            EnemyType.MosquitoLarva,
            EnemyType.BugGlowWings,
            EnemyType.Fly,
            EnemyType.FlyLarva,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.SpiderLarge,

            EnemyType.AntTan,
            EnemyType.AntRed,

            EnemyType.LeechTan,
            EnemyType.LeechTanSmall,
            EnemyType.SnailSmall,
            EnemyType.SnailLarge,

            EnemyType.LadyBugBrown,
        };
        public static List<EnemyType> EnemiesOasis = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.BeetleLargeBrown,
            EnemyType.BeetleLong,

            EnemyType.Fly,
            EnemyType.FlyLarva,

            EnemyType.ScorpionSmall,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,

            EnemyType.LocustSmall,
            EnemyType.LocustLarge,

            EnemyType.AntTan,

            EnemyType.LeechTan,
            EnemyType.LeechTanSmall,
        };

        public static List<EnemyType> EnemiesDesertLight = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.WormTan,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.ScorpionSmall,
            EnemyType.ScorpionMedium,
            EnemyType.ScorpionLarge,

            EnemyType.SpiderSmall,

            EnemyType.LocustSmall,
            EnemyType.LocustLarge,

            EnemyType.SlugSmall,

            EnemyType.AntTan,
        };
        public static List<EnemyType> EnemiesDesertMedium = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.WormBrown,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.ScorpionSmall,
            EnemyType.ScorpionMedium,
            EnemyType.ScorpionLarge,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,

            EnemyType.LocustSmall,

            EnemyType.SlugSmall,

            EnemyType.AntTan,
            EnemyType.AntRed,

            EnemyType.LadyBugBrown,
        };
        public static List<EnemyType> EnemiesDesertDark = new List<EnemyType>()
        {
            EnemyType.BeetleSmall,
            EnemyType.BeetleMedium,
            EnemyType.WormBrown,

            EnemyType.Fly,
            EnemyType.FlyLarva,
            EnemyType.Bee,
            EnemyType.BeeLarva,

            EnemyType.ScorpionSmall,
            EnemyType.ScorpionMedium,
            EnemyType.ScorpionLarge,

            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,

            EnemyType.LocustSmall,
            EnemyType.LocustLarge,

            EnemyType.SlugSmall,

            EnemyType.AntTan,
            EnemyType.AntRed,
        };

        // Indoor regions
        public static List<EnemyType> EnemiesDungeonGrey = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.LeechTanSmall,
            EnemyType.LeechTan,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatBrownLarge,
        };
        public static List<EnemyType> EnemiesDungeonBlue = new List<EnemyType>()
        {
            EnemyType.BatBlackLarge,
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.LeechTanSmall,
            EnemyType.LeechTan,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatBrownLarge,
        };
        public static List<EnemyType> EnemiesDungeonDark = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.LeechRedSmall,
            EnemyType.LeechRed,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreyLarge,
        };
        public static List<EnemyType> EnemiesDungeonSnowLight = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BirdFluffSnow,
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
        };
        public static List<EnemyType> EnemiesDungeonSnowDark = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BirdFluffSnow,
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreyLarge,
            EnemyType.RatLeech,
        };
        public static List<EnemyType> EnemiesDungeonMetalOrange = new List<EnemyType>()
        {
            EnemyType.BatBlackLarge,
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.LeechRedSmall,
            EnemyType.LeechRed,
            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatBrownLarge,
            EnemyType.RatGreen,
       };
        public static List<EnemyType> EnemiesDungeonMetalDark = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.LeechTan,
            EnemyType.LeechTanSmall,
            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreyLarge,
             EnemyType.RatGreen,
       };

        public static List<EnemyType> EnemiesCavesLight = new List<EnemyType>()
        {
            EnemyType.AntTan,
            EnemyType.BatBlackLarge,
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.LeechTanSmall,
            EnemyType.LeechTan,
            EnemyType.WormBrown,
            EnemyType.WormTan,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatBrownLarge,
        };
        public static List<EnemyType> EnemiesCavesMedium = new List<EnemyType>()
        {
            EnemyType.AntRed,
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.LeechRedSmall,
            EnemyType.LeechRed,
            EnemyType.WormBrown,
            EnemyType.WormTan,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatBrownLarge,
            EnemyType.RatGreyLarge,
        };
        public static List<EnemyType> EnemiesCavesDark = new List<EnemyType>()
        {
            EnemyType.AntBlack,
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.LeechRedSmall,
            EnemyType.LeechRed,
            EnemyType.WormBrown,
            EnemyType.WormTan,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreyLarge,
            EnemyType.RatGreen,
        };
        public static List<EnemyType> EnemiesCavesIce = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BirdFluffSnow,
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatLeech,
        };

        public static List<EnemyType> EnemiesIndustrialLight = new List<EnemyType>()
        {
            EnemyType.BatTanSmall,
            EnemyType.BatTanLarge,
            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumBrown,
            EnemyType.BatTanLarge,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreen,
            EnemyType.RatTan,
            EnemyType.RatGreen,
        };
        public static List<EnemyType> EnemiesIndustrialDark = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.SpiderSmall,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatBrownSmall,
            EnemyType.RatBrownMedium,
            EnemyType.RatGreen,
            EnemyType.RatGreyLarge,
            EnemyType.RatTan,
            EnemyType.RatGreen,
        };
        public static List<EnemyType> EnemiesIceTunnelsLight = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BirdFluffSnow,
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatLeech,
        };
        public static List<EnemyType> EnemiesIceTunnelsDark = new List<EnemyType>()
        {
            EnemyType.BatBrownSmall,
            EnemyType.BatBrownLarge,
            EnemyType.BatBlackLarge,
            EnemyType.BirdFluffSnow,
            EnemyType.AntBlue,
            EnemyType.SpiderMediumGrey,
            EnemyType.RatLeech,
        };
    }
}