﻿using Microsoft.Xna.Framework;

namespace RogueProject.Enemies
{
    class EnemyDamage
    {
        public static Vector2 Damage(Enemy enemy)
        {
            Vector2 damage = new Vector2(0, 1);
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    damage = AvianDamage(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    damage = PestDamage(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    damage = RodentDamage(enemy.EnemyType);
                    break;
            }

            return damage;
        }

        private static Vector2 AvianDamage(EnemyType enemyType)
        {
            Vector2 damage = new Vector2(0, 1);
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.BatBrownLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BatBlackLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BatTanSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.BatTanLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BirdFluffSnow:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.ChickenSnowLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.ChickenSnowSmall:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.EagleSnowLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.EagleSnowSmall:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.KiteSnowSmall:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.KiteSnowLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.KiteSnowTanSmall:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.KiteSnowTanLarge:
                    damage = new Vector2(1, 8);
                    break;
            }

            return damage;
        }

        private static Vector2 PestDamage(EnemyType enemyType)
        {
            Vector2 damage = new Vector2(0, 1);
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.AntBlack:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.AntRed:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.AntBlue:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.BeetleSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.BeetleMedium:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.BeetleLargeBlack:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BeetleLargeBrown:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BeetleLong:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.Bee:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.BeeLarva:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.Fly:
                    damage = new Vector2(1, 7);
                    break;
                case EnemyType.FlyLarva:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.Mosquito:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.MosquitoLarva:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.BugGlowWings:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.BugGlowSparkle:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.BugSparkleSmall:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.BugSparkleLarge:
                    damage = new Vector2(1, 7);
                    break;
                case EnemyType.GrasshopperSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.GrasshopperLarge:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.LocustSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.LocustLarge:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.LadyBugBrown:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.LadyBugRed:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.LeechTanSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.LeechTan:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.LeechRedSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.LeechRed:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.SlugSmall:
                    damage = new Vector2(1, 3);
                    break;
                case EnemyType.SlugLarge:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.SnailSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.SnailLarge:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.ScorpionSmall:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.ScorpionMedium:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.ScorpionLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.SpiderSmall:
                    damage = new Vector2(1, 5);
                    break;
                case EnemyType.SpiderMediumBrown:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.SpiderMediumGrey:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.SpiderLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.WormBrown:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.WormTan:
                    damage = new Vector2(1, 4);
                    break;
            }
            return damage;
        }

        private static Vector2 RodentDamage(EnemyType enemyType)
        {
            Vector2 damage = new Vector2(0, 1);
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    damage = new Vector2(1, 4);
                    break;
                case EnemyType.RatBrownMedium:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.RatBrownLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.RatGreyLarge:
                    damage = new Vector2(1, 8);
                    break;
                case EnemyType.RatGreen:
                    damage = new Vector2(1, 6);
                    break;
                case EnemyType.RatTan:
                    damage = new Vector2(1, 7);
                    break;
                case EnemyType.RatLeech:
                    damage = new Vector2(1, 9);
                    break;
            }
            return damage;
        }
    }
}
