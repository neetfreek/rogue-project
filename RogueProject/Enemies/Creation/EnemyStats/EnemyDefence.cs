﻿namespace RogueProject.Enemies
{
    class EnemyDefence
    {            
        public static int RatingDefence(Enemy enemy)
        {
            int ratingDefence = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    ratingDefence = AvianDefence(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    ratingDefence = PestDefence(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    ratingDefence = RodentDefence(enemy.EnemyType);
                    break;
            }

            return ratingDefence;
        }

        private static int AvianDefence(EnemyType enemyType)
        {
            int ratingDefence = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    ratingDefence = 10;
                    break;
                case EnemyType.BatBrownLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.BatBlackLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.BatTanSmall:
                    ratingDefence = 10;
                    break;
                case EnemyType.BatTanLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.BirdFluffSnow:
                    ratingDefence = 5;
                    break;
                case EnemyType.ChickenSnowLarge:
                    ratingDefence = 12;
                    break;
                case EnemyType.ChickenSnowSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.EagleSnowLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.EagleSnowSmall:
                    ratingDefence = 10;
                    break;
                case EnemyType.KiteSnowSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.KiteSnowLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    ratingDefence = 15;
                    break;
            }

            return ratingDefence;
        }

        private static int PestDefence(EnemyType enemyType)
        {
            int ratingDefence = 0;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    ratingDefence = 10;
                    break;
                case EnemyType.AntBlack:
                    ratingDefence = 10;
                    break;
                case EnemyType.AntRed:
                    ratingDefence = 10;
                    break;
                case EnemyType.AntBlue:
                    ratingDefence = 10;
                    break;
                case EnemyType.BeetleSmall:
                    ratingDefence = 15;
                    break;
                case EnemyType.BeetleMedium:
                    ratingDefence = 15;
                    break;
                case EnemyType.BeetleLargeBlack:
                    ratingDefence = 20;
                    break;
                case EnemyType.BeetleLargeBrown:
                    ratingDefence = 20;
                    break;
                case EnemyType.BeetleLong:
                    ratingDefence = 5;
                    break;
                case EnemyType.Bee:
                    ratingDefence = 10;
                    break;
                case EnemyType.BeeLarva:
                    ratingDefence = 5;
                    break;
                case EnemyType.Fly:
                    ratingDefence = 10;
                    break;
                case EnemyType.FlyLarva:
                    ratingDefence = 5;
                    break;
                case EnemyType.Mosquito:
                    ratingDefence = 10;
                    break;
                case EnemyType.MosquitoLarva:
                    ratingDefence = 5;
                    break;
                case EnemyType.BugGlowWings:
                    ratingDefence = 10;
                    break;
                case EnemyType.BugGlowSparkle:
                    ratingDefence = 5;
                    break;
                case EnemyType.BugSparkleSmall:
                    ratingDefence = 12;
                    break;
                case EnemyType.BugSparkleLarge:
                    ratingDefence = 18;
                    break;
                case EnemyType.GrasshopperSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.GrasshopperLarge:
                    ratingDefence = 10;
                    break;
                case EnemyType.LocustSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.LocustLarge:
                    ratingDefence = 10;
                    break;
                case EnemyType.LadyBugBrown:
                    ratingDefence = 15;
                    break;
                case EnemyType.LadyBugRed:
                    ratingDefence = 15;
                    break;
                case EnemyType.LeechTanSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.LeechTan:
                    ratingDefence = 15;
                    break;
                case EnemyType.LeechRedSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.LeechRed:
                    ratingDefence = 15;
                    break;
                case EnemyType.SlugSmall:
                    ratingDefence = 5;
                    break;
                case EnemyType.SlugLarge:
                    ratingDefence = 10;
                    break;
                case EnemyType.SnailSmall:
                    ratingDefence = 8;
                    break;
                case EnemyType.SnailLarge:
                    ratingDefence = 18;
                    break;
                case EnemyType.ScorpionSmall:
                    ratingDefence = 8;
                    break;
                case EnemyType.ScorpionMedium:
                    ratingDefence = 12;
                    break;
                case EnemyType.ScorpionLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.SpiderSmall:
                    ratingDefence = 8;
                    break;
                case EnemyType.SpiderMediumBrown:
                    ratingDefence = 12;
                    break;
                case EnemyType.SpiderMediumGrey:
                    ratingDefence = 12;
                    break;
                case EnemyType.SpiderLarge:
                    ratingDefence = 15;
                    break;
                case EnemyType.WormBrown:
                    ratingDefence = 5;
                    break;
                case EnemyType.WormTan:
                    ratingDefence = 5;
                    break;
            }
            return ratingDefence;
        }

        private static int RodentDefence(EnemyType enemyType)
        {
            int ratingDefence = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    ratingDefence = 10;
                    break;
                case EnemyType.RatBrownMedium:
                    ratingDefence = 13;
                    break;
                case EnemyType.RatBrownLarge:
                    ratingDefence = 18;
                    break;
                case EnemyType.RatGreyLarge:
                    ratingDefence = 18;
                    break;
                case EnemyType.RatGreen:
                    ratingDefence = 20;
                    break;
                case EnemyType.RatTan:
                    ratingDefence = 12;
                    break;
                case EnemyType.RatLeech:
                    ratingDefence = 20;
                    break;
            }
            return ratingDefence;
        }
    }
}
