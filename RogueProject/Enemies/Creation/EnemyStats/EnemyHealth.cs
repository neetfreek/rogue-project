﻿namespace RogueProject.Enemies
{
    class EnemyHealth
    {
        public static int Health(Enemy enemy)
        {
            int health = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    health = AvianHealth(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    health = PestDefence(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    health = RodentHealth(enemy.EnemyType);
                    break;
            }

            return health;
        }

        private static int AvianHealth(EnemyType enemyType)
        {
            int health = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    health = 8;
                    break;
                case EnemyType.BatBrownLarge:
                    health = 13;
                    break;
                case EnemyType.BatBlackLarge:
                    health = 13;
                    break;
                case EnemyType.BatTanSmall:
                    health = 8;
                    break;
                case EnemyType.BatTanLarge:
                    health = 13;
                    break;
                case EnemyType.BirdFluffSnow:
                    health = 8;
                    break;
                case EnemyType.ChickenSnowLarge:
                    health = 13;
                    break;
                case EnemyType.ChickenSnowSmall:
                    health = 8;
                    break;
                case EnemyType.EagleSnowLarge:
                    health = 13;
                    break;
                case EnemyType.EagleSnowSmall:
                    health = 8;
                    break;
                case EnemyType.KiteSnowSmall:
                    health = 13;
                    break;
                case EnemyType.KiteSnowLarge:
                    health = 13;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    health = 8;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    health = 13;
                    break;
            }

            return health;
        }

        private static int PestDefence(EnemyType enemyType)
        {
            int health = 0;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    health = 10;
                    break;
                case EnemyType.AntBlack:
                    health = 10;
                    break;
                case EnemyType.AntRed:
                    health = 10;
                    break;
                case EnemyType.AntBlue:
                    health = 10;
                    break;
                case EnemyType.BeetleSmall:
                    health = 6;
                    break;
                case EnemyType.BeetleMedium:
                    health = 12;
                    break;
                case EnemyType.BeetleLargeBlack:
                    health = 16;
                    break;
                case EnemyType.BeetleLargeBrown:
                    health = 16;
                    break;
                case EnemyType.BeetleLong:
                    health = 10;
                    break;
                case EnemyType.Bee:
                    health = 12;
                    break;
                case EnemyType.BeeLarva:
                    health = 6;
                    break;
                case EnemyType.Fly:
                    health = 12;
                    break;
                case EnemyType.FlyLarva:
                    health = 6;
                    break;
                case EnemyType.Mosquito:
                    health = 12;
                    break;
                case EnemyType.MosquitoLarva:
                    health = 6;
                    break;
                case EnemyType.BugGlowWings:
                    health = 11;
                    break;
                case EnemyType.BugGlowSparkle:
                    health = 13;
                    break;
                case EnemyType.BugSparkleSmall:
                    health = 7;
                    break;
                case EnemyType.BugSparkleLarge:
                    health = 17;
                    break;
                case EnemyType.GrasshopperSmall:
                    health = 8;
                    break;
                case EnemyType.GrasshopperLarge:
                    health = 12;
                    break;
                case EnemyType.LocustSmall:
                    health = 8;
                    break;
                case EnemyType.LocustLarge:
                    health = 12;
                    break;
                case EnemyType.LadyBugBrown:
                    health = 18;
                    break;
                case EnemyType.LadyBugRed:
                    health = 18;
                    break;
                case EnemyType.LeechTanSmall:
                    health = 6;
                    break;
                case EnemyType.LeechTan:
                    health = 14;
                    break;
                case EnemyType.LeechRedSmall:
                    health = 6;
                    break;
                case EnemyType.LeechRed:
                    health = 14;
                    break;
                case EnemyType.SlugSmall:
                    health = 5;
                    break;
                case EnemyType.SlugLarge:
                    health = 15;
                    break;
                case EnemyType.SnailSmall:
                    health = 7;
                    break;
                case EnemyType.SnailLarge:
                    health = 17;
                    break;
                case EnemyType.ScorpionSmall:
                    health = 7;
                    break;
                case EnemyType.ScorpionMedium:
                    health = 12;
                    break;
                case EnemyType.ScorpionLarge:
                    health = 18;
                    break;
                case EnemyType.SpiderSmall:
                    health = 7;
                    break;
                case EnemyType.SpiderMediumBrown:
                    health = 12;
                    break;
                case EnemyType.SpiderMediumGrey:
                    health = 12;
                    break;
                case EnemyType.SpiderLarge:
                    health = 16;
                    break;
                case EnemyType.WormBrown:
                    health = 5;
                    break;
                case EnemyType.WormTan:
                    health = 5;
                    break;
            }
            return health;
        }

        private static int RodentHealth(EnemyType enemyType)
        {
            int health = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    health = 8;
                    break;
                case EnemyType.RatBrownMedium:
                    health = 13;
                    break;
                case EnemyType.RatBrownLarge:
                    health = 16;
                    break;
                case EnemyType.RatGreyLarge:
                    health = 16;
                    break;
                case EnemyType.RatGreen:
                    health = 16;
                    break;
                case EnemyType.RatTan:
                    health = 13;
                    break;
                case EnemyType.RatLeech:
                    health = 16;
                    break;
            }
            return health;
        }
    }
}