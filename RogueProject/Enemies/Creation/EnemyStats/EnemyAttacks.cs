﻿namespace RogueProject.Enemies
{
    class EnemyAttacks
    {
        public static int Attacks(Enemy enemy)
        {
            int attacks = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    attacks = AvianAttacks(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    attacks = PetAttacks(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    attacks = RodentAttacks(enemy.EnemyType);
                    break;
            }

            return attacks;
        }

        private static int AvianAttacks(EnemyType enemyType)
        {
            int attacks = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    attacks = 1;
                    break;
                case EnemyType.BatBrownLarge:
                    attacks = 1;
                    break;
                case EnemyType.BatBlackLarge:
                    attacks = 1;
                    break;
                case EnemyType.BatTanSmall:
                    attacks = 1;
                    break;
                case EnemyType.BatTanLarge:
                    attacks = 1;
                    break;
                case EnemyType.BirdFluffSnow:
                    attacks = 1;
                    break;
                case EnemyType.ChickenSnowLarge:
                    attacks = 1;
                    break;
                case EnemyType.ChickenSnowSmall:
                    attacks = 1;
                    break;
                case EnemyType.EagleSnowLarge:
                    attacks = 1;
                    break;
                case EnemyType.EagleSnowSmall:
                    attacks = 1;
                    break;
                case EnemyType.KiteSnowSmall:
                    attacks = 1;
                    break;
                case EnemyType.KiteSnowLarge:
                    attacks = 1;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    attacks = 1;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    attacks = 1;
                    break;
            }

            return attacks;
        }

        private static int PetAttacks(EnemyType enemyType)
        {
            int attacks = 1;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    attacks = 1;
                    break;
                case EnemyType.AntBlack:
                    attacks = 1;
                    break;
                case EnemyType.AntRed:
                    attacks = 1;
                    break;
                case EnemyType.AntBlue:
                    attacks = 1;
                    break;
                case EnemyType.BeetleSmall:
                    attacks = 1;
                    break;
                case EnemyType.BeetleMedium:
                    attacks = 1;
                    break;
                case EnemyType.BeetleLargeBlack:
                    attacks = 1;
                    break;
                case EnemyType.BeetleLargeBrown:
                    attacks = 1;
                    break;
                case EnemyType.BeetleLong:
                    attacks = 1;
                    break;
                case EnemyType.Bee:
                    attacks = 1;
                    break;
                case EnemyType.BeeLarva:
                    attacks = 1;
                    break;
                case EnemyType.Fly:
                    attacks = 1;
                    break;
                case EnemyType.FlyLarva:
                    attacks = 1;
                    break;
                case EnemyType.Mosquito:
                    attacks = 1;
                    break;
                case EnemyType.MosquitoLarva:
                    attacks = 1;
                    break;
                case EnemyType.BugGlowWings:
                    attacks = 1;
                    break;
                case EnemyType.BugGlowSparkle:
                    attacks = 1;
                    break;
                case EnemyType.BugSparkleSmall:
                    attacks = 1;
                    break;
                case EnemyType.BugSparkleLarge:
                    attacks = 1;
                    break;
                case EnemyType.GrasshopperSmall:
                    attacks = 1;
                    break;
                case EnemyType.GrasshopperLarge:
                    attacks = 1;
                    break;
                case EnemyType.LocustSmall:
                    attacks = 1;
                    break;
                case EnemyType.LocustLarge:
                    attacks = 1;
                    break;
                case EnemyType.LadyBugBrown:
                    attacks = 1;
                    break;
                case EnemyType.LadyBugRed:
                    attacks = 1;
                    break;
                case EnemyType.LeechTanSmall:
                    attacks = 1;
                    break;
                case EnemyType.LeechTan:
                    attacks = 1;
                    break;
                case EnemyType.LeechRedSmall:
                    attacks = 1;
                    break;
                case EnemyType.LeechRed:
                    attacks = 1;
                    break;
                case EnemyType.SlugSmall:
                    attacks = 1;
                    break;
                case EnemyType.SlugLarge:
                    attacks = 1;
                    break;
                case EnemyType.SnailSmall:
                    attacks = 1;
                    break;
                case EnemyType.SnailLarge:
                    attacks = 1;
                    break;
                case EnemyType.ScorpionSmall:
                    attacks = 1;
                    break;
                case EnemyType.ScorpionMedium:
                    attacks = 1;
                    break;
                case EnemyType.ScorpionLarge:
                    attacks = 1;
                    break;
                case EnemyType.SpiderSmall:
                    attacks = 1;
                    break;
                case EnemyType.SpiderMediumBrown:
                    attacks = 1;
                    break;
                case EnemyType.SpiderMediumGrey:
                    attacks = 1;
                    break;
                case EnemyType.SpiderLarge:
                    attacks = 1;
                    break;
                case EnemyType.WormBrown:
                    attacks = 1;
                    break;
                case EnemyType.WormTan:
                    attacks = 1;
                    break;
            }
            return attacks;
        }

        private static int RodentAttacks(EnemyType enemyType)
        {
            int attacks = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    attacks = 1;
                    break;
                case EnemyType.RatBrownMedium:
                    attacks = 1;
                    break;
                case EnemyType.RatBrownLarge:
                    attacks = 1;
                    break;
                case EnemyType.RatGreyLarge:
                    attacks = 1;
                    break;
                case EnemyType.RatGreen:
                    attacks = 1;
                    break;
                case EnemyType.RatTan:
                    attacks = 1;
                    break;
                case EnemyType.RatLeech:
                    attacks = 1;
                    break;
            }
            return attacks;
        }
    }
}