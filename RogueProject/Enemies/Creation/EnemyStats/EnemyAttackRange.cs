﻿namespace RogueProject.Enemies
{
    public static class EnemyAttackRange
    {
        public static int AttackRange(Enemy enemy)
        {
            int attackRange = 1;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    attackRange = AvianAttackRange(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    attackRange = PestAttackRange(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    attackRange = RodentAttackRange(enemy.EnemyType);
                    break;
            }

            return attackRange;
        }

        private static int AvianAttackRange(EnemyType enemyType)
        {
            int attackRange = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    attackRange = 1;
                    break;
                case EnemyType.BatBrownLarge:
                    attackRange = 1;
                    break;
                case EnemyType.BatBlackLarge:
                    attackRange = 1;
                    break;
                case EnemyType.BatTanSmall:
                    attackRange = 1;
                    break;
                case EnemyType.BatTanLarge:
                    attackRange = 1;
                    break;
                case EnemyType.BirdFluffSnow:
                    attackRange = 1;
                    break;
                case EnemyType.ChickenSnowLarge:
                    attackRange = 1;
                    break;
                case EnemyType.ChickenSnowSmall:
                    attackRange = 1;
                    break;
                case EnemyType.EagleSnowLarge:
                    attackRange = 2;
                    break;
                case EnemyType.EagleSnowSmall:
                    attackRange = 1;
                    break;
                case EnemyType.KiteSnowSmall:
                    attackRange = 1;
                    break;
                case EnemyType.KiteSnowLarge:
                    attackRange = 1;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    attackRange = 1;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    attackRange = 1;
                    break;
            }

            return attackRange;
        }

        private static int PestAttackRange(EnemyType enemyType)
        {
            int attackRange = 0;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    attackRange = 1;
                    break;
                case EnemyType.AntBlack:
                    attackRange = 1;
                    break;
                case EnemyType.AntRed:
                    attackRange = 1;
                    break;
                case EnemyType.AntBlue:
                    attackRange = 1;
                    break;
                case EnemyType.BeetleSmall:
                    attackRange = 1;
                    break;
                case EnemyType.BeetleMedium:
                    attackRange = 1;
                    break;
                case EnemyType.BeetleLargeBlack:
                    attackRange = 1;
                    break;
                case EnemyType.BeetleLargeBrown:
                    attackRange = 1;
                    break;
                case EnemyType.BeetleLong:
                    attackRange = 3;
                    break;
                case EnemyType.Bee:
                    attackRange = 1;
                    break;
                case EnemyType.BeeLarva:
                    attackRange = 1;
                    break;
                case EnemyType.Fly:
                    attackRange = 1;
                    break;
                case EnemyType.FlyLarva:
                    attackRange = 1;
                    break;
                case EnemyType.Mosquito:
                    attackRange = 3;
                    break;
                case EnemyType.MosquitoLarva:
                    attackRange = 1;
                    break;
                case EnemyType.BugGlowWings:
                    attackRange = 1;
                    break;
                case EnemyType.BugGlowSparkle:
                    attackRange = 2;
                    break;
                case EnemyType.BugSparkleSmall:
                    attackRange = 3;
                    break;
                case EnemyType.BugSparkleLarge:
                    attackRange = 3;
                    break;
                case EnemyType.GrasshopperSmall:
                    attackRange = 1;
                    break;
                case EnemyType.GrasshopperLarge:
                    attackRange = 1;
                    break;
                case EnemyType.LocustSmall:
                    attackRange = 1;
                    break;
                case EnemyType.LocustLarge:
                    attackRange = 1;
                    break;
                case EnemyType.LadyBugBrown:
                    attackRange = 1;
                    break;
                case EnemyType.LadyBugRed:
                    attackRange = 1;
                    break;
                case EnemyType.LeechTanSmall:
                    attackRange = 1;
                    break;
                case EnemyType.LeechTan:
                    attackRange = 1;
                    break;
                case EnemyType.LeechRedSmall:
                    attackRange = 1;
                    break;
                case EnemyType.LeechRed:
                    attackRange = 1;
                    break;
                case EnemyType.SlugSmall:
                    attackRange = 1;
                    break;
                case EnemyType.SlugLarge:
                    attackRange = 1;
                    break;
                case EnemyType.SnailSmall:
                    attackRange = 1;
                    break;
                case EnemyType.SnailLarge:
                    attackRange = 1;
                    break;
                case EnemyType.ScorpionSmall:
                    attackRange = 1;
                    break;
                case EnemyType.ScorpionMedium:
                    attackRange = 1;
                    break;
                case EnemyType.ScorpionLarge:
                    attackRange = 1;
                    break;
                case EnemyType.SpiderSmall:
                    attackRange = 1;
                    break;
                case EnemyType.SpiderMediumBrown:
                    attackRange = 1;
                    break;
                case EnemyType.SpiderMediumGrey:
                    attackRange = 1;
                    break;
                case EnemyType.SpiderLarge:
                    attackRange = 1;
                    break;
                case EnemyType.WormBrown:
                    attackRange = 1;
                    break;
                case EnemyType.WormTan:
                    attackRange = 1;
                    break;
            }
            return attackRange;
        }

        private static int RodentAttackRange(EnemyType enemyType)
        {
            int attackRange = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    attackRange = 1;
                    break;
                case EnemyType.RatBrownMedium:
                    attackRange = 1;
                    break;
                case EnemyType.RatBrownLarge:
                    attackRange = 1;
                    break;
                case EnemyType.RatGreyLarge:
                    attackRange = 1;
                    break;
                case EnemyType.RatGreen:
                    attackRange = 3;
                    break;
                case EnemyType.RatTan:
                    attackRange = 1;
                    break;
                case EnemyType.RatLeech:
                    attackRange = 1;
                    break;
            }
            return attackRange;
        }
    }
}