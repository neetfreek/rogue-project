﻿namespace RogueProject.Enemies
{
    class EnemyExperience
    {
        public static int Experience(Enemy enemy)
        {
            int experience = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    experience = AvianExperience(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    experience = PestExperience(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    experience = RodentExperience(enemy.EnemyType);
                    break;
            }

            return experience;
        }

        private static int AvianExperience(EnemyType enemyType)
        {
            int experience = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    experience = 1;
                    break;
                case EnemyType.BatBrownLarge:
                    experience = 2;
                    break;
                case EnemyType.BatBlackLarge:
                    experience = 3;
                    break;
                case EnemyType.BatTanSmall:
                    experience = 1;
                    break;
                case EnemyType.BatTanLarge:
                    experience = 3;
                    break;
                case EnemyType.BirdFluffSnow:
                    experience = 1;
                    break;
                case EnemyType.ChickenSnowLarge:
                    experience = 3;
                    break;
                case EnemyType.ChickenSnowSmall:
                    experience = 1;
                    break;
                case EnemyType.EagleSnowLarge:
                    experience = 3;
                    break;
                case EnemyType.EagleSnowSmall:
                    experience = 1;
                    break;
                case EnemyType.KiteSnowSmall:
                    experience = 3;
                    break;
                case EnemyType.KiteSnowLarge:
                    experience = 3;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    experience = 1;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    experience = 3;
                    break;
            }

            return experience;
        }

        private static int PestExperience(EnemyType enemyType)
        {
            int experience = 1;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    experience = 1;
                    break;
                case EnemyType.AntBlack:
                    experience = 1;
                    break;
                case EnemyType.AntRed:
                    experience = 1;
                    break;
                case EnemyType.AntBlue:
                    experience = 1;
                    break;
                case EnemyType.BeetleSmall:
                    experience = 1;
                    break;
                case EnemyType.BeetleMedium:
                    experience = 2;
                    break;
                case EnemyType.BeetleLargeBlack:
                    experience = 3;
                    break;
                case EnemyType.BeetleLargeBrown:
                    experience = 3;
                    break;
                case EnemyType.BeetleLong:
                    experience = 1;
                    break;
                case EnemyType.Bee:
                    experience = 1;
                    break;
                case EnemyType.BeeLarva:
                    experience = 1;
                    break;
                case EnemyType.Fly:
                    experience = 1;
                    break;
                case EnemyType.FlyLarva:
                    experience = 1;
                    break;
                case EnemyType.Mosquito:
                    experience = 1;
                    break;
                case EnemyType.MosquitoLarva:
                    experience = 1;
                    break;
                case EnemyType.BugGlowWings:
                    experience = 1;
                    break;
                case EnemyType.BugGlowSparkle:
                    experience = 1;
                    break;
                case EnemyType.BugSparkleSmall:
                    experience = 1;
                    break;
                case EnemyType.BugSparkleLarge:
                    experience = 3;
                    break;
                case EnemyType.GrasshopperSmall:
                    experience = 1;
                    break;
                case EnemyType.GrasshopperLarge:
                    experience = 2;
                    break;
                case EnemyType.LocustSmall:
                    experience = 1;
                    break;
                case EnemyType.LocustLarge:
                    experience = 2;
                    break;
                case EnemyType.LadyBugBrown:
                    experience = 2;
                    break;
                case EnemyType.LadyBugRed:
                    experience = 2;
                    break;
                case EnemyType.LeechTanSmall:
                    experience = 1;
                    break;
                case EnemyType.LeechTan:
                    experience = 2;
                    break;
                case EnemyType.LeechRedSmall:
                    experience = 1;
                    break;
                case EnemyType.LeechRed:
                    experience = 2;
                    break;
                case EnemyType.SlugSmall:
                    experience = 1;
                    break;
                case EnemyType.SlugLarge:
                    experience = 2;
                    break;
                case EnemyType.SnailSmall:
                    experience = 1;
                    break;
                case EnemyType.SnailLarge:
                    experience = 2;
                    break;
                case EnemyType.ScorpionSmall:
                    experience = 1;
                    break;
                case EnemyType.ScorpionMedium:
                    experience = 2;
                    break;
                case EnemyType.ScorpionLarge:
                    experience = 3;
                    break;
                case EnemyType.SpiderSmall:
                    experience = 1;
                    break;
                case EnemyType.SpiderMediumBrown:
                    experience = 2;
                    break;
                case EnemyType.SpiderMediumGrey:
                    experience = 2;
                    break;
                case EnemyType.SpiderLarge:
                    experience = 3;
                    break;
                case EnemyType.WormBrown:
                    experience = 1;
                    break;
                case EnemyType.WormTan:
                    experience = 1;
                    break;
            }
            return experience;
        }

        private static int RodentExperience(EnemyType enemyType)
        {
            int experience = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    experience = 1;
                    break;
                case EnemyType.RatBrownMedium:
                    experience = 2;
                    break;
                case EnemyType.RatBrownLarge:
                    experience = 2;
                    break;
                case EnemyType.RatGreyLarge:
                    experience = 2;
                    break;
                case EnemyType.RatGreen:
                    experience = 3;
                    break;
                case EnemyType.RatTan:
                    experience = 2;
                    break;
                case EnemyType.RatLeech:
                    experience = 3;
                    break;
            }
            return experience;
        }
    }
}
