﻿namespace RogueProject.Enemies
{
    class EnemyHitRating
    {
        public static int Hit(Enemy enemy)
        {
            int hit = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    hit = AvianHit(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    hit = PestHit(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    hit = RodentHit(enemy.EnemyType);
                    break;
            }

            return hit;
        }

        private static int AvianHit(EnemyType enemyType)
        {
            int hit = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    hit = 0;
                    break;
                case EnemyType.BatBrownLarge:
                    hit = 2;
                    break;
                case EnemyType.BatBlackLarge:
                    hit = 2;
                    break;
                case EnemyType.BatTanSmall:
                    hit = 0;
                    break;
                case EnemyType.BatTanLarge:
                    hit = 2;
                    break;
                case EnemyType.BirdFluffSnow:
                    hit = 0;
                    break;
                case EnemyType.ChickenSnowLarge:
                    hit = 1;
                    break;
                case EnemyType.ChickenSnowSmall:
                    hit = 0;
                    break;
                case EnemyType.EagleSnowLarge:
                    hit = 2;
                    break;
                case EnemyType.EagleSnowSmall:
                    hit = 1;
                    break;
                case EnemyType.KiteSnowSmall:
                    hit = 1;
                    break;
                case EnemyType.KiteSnowLarge:
                    hit = 2;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    hit = 1;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    hit = 2;
                    break;
            }

            return hit;
        }

        private static int PestHit(EnemyType enemyType)
        {
            int hit = 0;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    hit = 0;
                    break;
                case EnemyType.AntBlack:
                    hit = 0;
                    break;
                case EnemyType.AntRed:
                    hit = 0;
                    break;
                case EnemyType.AntBlue:
                    hit = 0;
                    break;
                case EnemyType.BeetleSmall:
                    hit = 1;
                    break;
                case EnemyType.BeetleMedium:
                    hit = 1;
                    break;
                case EnemyType.BeetleLargeBlack:
                    hit = 2;
                    break;
                case EnemyType.BeetleLargeBrown:
                    hit = 2;
                    break;
                case EnemyType.BeetleLong:
                    hit = 1;
                    break;
                case EnemyType.Bee:
                    hit = 1;
                    break;
                case EnemyType.BeeLarva:
                    hit = 0;
                    break;
                case EnemyType.Fly:
                    hit = 1;
                    break;
                case EnemyType.FlyLarva:
                    hit = 0;
                    break;
                case EnemyType.Mosquito:
                    hit = 1;
                    break;
                case EnemyType.MosquitoLarva:
                    hit = 0;
                    break;
                case EnemyType.BugGlowWings:
                    hit = 1;
                    break;
                case EnemyType.BugGlowSparkle:
                    hit = 1;
                    break;
                case EnemyType.BugSparkleSmall:
                    hit = 0;
                    break;
                case EnemyType.BugSparkleLarge:
                    hit = 1;
                    break;
                case EnemyType.GrasshopperSmall:
                    hit = 0;
                    break;
                case EnemyType.GrasshopperLarge:
                    hit = 1;
                    break;
                case EnemyType.LocustSmall:
                    hit = 0;
                    break;
                case EnemyType.LocustLarge:
                    hit = 1;
                    break;
                case EnemyType.LadyBugBrown:
                    hit = 1;
                    break;
                case EnemyType.LadyBugRed:
                    hit = 1;
                    break;
                case EnemyType.LeechTanSmall:
                    hit = 0;
                    break;
                case EnemyType.LeechTan:
                    hit = 1;
                    break;
                case EnemyType.LeechRedSmall:
                    hit = 0;
                    break;
                case EnemyType.LeechRed:
                    hit = 1;
                    break;
                case EnemyType.SlugSmall:
                    hit = 0;
                    break;
                case EnemyType.SlugLarge:
                    hit = 1;
                    break;
                case EnemyType.SnailSmall:
                    hit = 0;
                    break;
                case EnemyType.SnailLarge:
                    hit = 1;
                    break;
                case EnemyType.ScorpionSmall:
                    hit = 0;
                    break;
                case EnemyType.ScorpionMedium:
                    hit = 1;
                    break;
                case EnemyType.ScorpionLarge:
                    hit = 2;
                    break;
                case EnemyType.SpiderSmall:
                    hit = 0;
                    break;
                case EnemyType.SpiderMediumBrown:
                    hit = 1;
                    break;
                case EnemyType.SpiderMediumGrey:
                    hit = 1;
                    break;
                case EnemyType.SpiderLarge:
                    hit = 2;
                    break;
                case EnemyType.WormBrown:
                    hit = 0;
                    break;
                case EnemyType.WormTan:
                    hit = 0;
                    break;
            }
            return hit;
        }

        private static int RodentHit(EnemyType enemyType)
        {
            int hit = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    hit = 0;
                    break;
                case EnemyType.RatBrownMedium:
                    hit = 1;
                    break;
                case EnemyType.RatBrownLarge:
                    hit = 2;
                    break;
                case EnemyType.RatGreyLarge:
                    hit = 2;
                    break;
                case EnemyType.RatGreen:
                    hit = 2;
                    break;
                case EnemyType.RatTan:
                    hit = 1;
                    break;
                case EnemyType.RatLeech:
                    hit = 2;
                    break;
            }
            return hit;
        }
    }
}
