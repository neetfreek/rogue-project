﻿namespace RogueProject.Enemies
{
    class EnemyMoves
    {
        public static int Moves(Enemy enemy)
        {
            int moves = 0;
            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    moves = AvianMoves(enemy.EnemyType);
                    break;
                case EnemyCategory.Pest:
                    moves = PestMoves(enemy.EnemyType);
                    break;
                case EnemyCategory.Rodent:
                    moves = RodentMoves(enemy.EnemyType);
                    break;
            }

            return moves;
        }

        private static int AvianMoves(EnemyType enemyType)
        {
            int moves = 0;
            switch (enemyType)
            {
                case EnemyType.BatBrownSmall:
                    moves = 2;
                    break;
                case EnemyType.BatBrownLarge:
                    moves = 1;
                    break;
                case EnemyType.BatBlackLarge:
                    moves = 1;
                    break;
                case EnemyType.BatTanSmall:
                    moves = 2;
                    break;
                case EnemyType.BatTanLarge:
                    moves = 1;
                    break;
                case EnemyType.BirdFluffSnow:
                    moves = 2;
                    break;
                case EnemyType.ChickenSnowLarge:
                    moves = 1;
                    break;
                case EnemyType.ChickenSnowSmall:
                    moves = 1;
                    break;
                case EnemyType.EagleSnowLarge:
                    moves = 1;
                    break;
                case EnemyType.EagleSnowSmall:
                    moves = 1;
                    break;
                case EnemyType.KiteSnowSmall:
                    moves = 2;
                    break;
                case EnemyType.KiteSnowLarge:
                    moves = 1;
                    break;
                case EnemyType.KiteSnowTanSmall:
                    moves = 2;
                    break;
                case EnemyType.KiteSnowTanLarge:
                    moves = 1;
                    break;
            }

            return moves;
        }

        private static int PestMoves(EnemyType enemyType)
        {
            int moves = 0;
            switch (enemyType)
            {
                case EnemyType.AntTan:
                    moves = 1;
                    break;
                case EnemyType.AntBlack:
                    moves = 1;
                    break;
                case EnemyType.AntRed:
                    moves = 1;
                    break;
                case EnemyType.AntBlue:
                    moves = 1;
                    break;
                case EnemyType.BeetleSmall:
                    moves = 2;
                    break;
                case EnemyType.BeetleMedium:
                    moves = 1;
                    break;
                case EnemyType.BeetleLargeBlack:
                    moves = 1;
                    break;
                case EnemyType.BeetleLargeBrown:
                    moves = 1;
                    break;
                case EnemyType.BeetleLong:
                    moves = 1;
                    break;
                case EnemyType.Bee:
                    moves = 2;
                    break;
                case EnemyType.BeeLarva:
                    moves = 1;
                    break;
                case EnemyType.Fly:
                    moves = 2;
                    break;
                case EnemyType.FlyLarva:
                    moves = 1;
                    break;
                case EnemyType.Mosquito:
                    moves = 2;
                    break;
                case EnemyType.MosquitoLarva:
                    moves = 1;
                    break;
                case EnemyType.BugGlowWings:
                    moves = 2;
                    break;
                case EnemyType.BugGlowSparkle:
                    moves = 2;
                    break;
                case EnemyType.BugSparkleSmall:
                    moves = 1;
                    break;
                case EnemyType.BugSparkleLarge:
                    moves = 2;
                    break;
                case EnemyType.GrasshopperSmall:
                    moves = 1;
                    break;
                case EnemyType.GrasshopperLarge:
                    moves = 2;
                    break;
                case EnemyType.LocustSmall:
                    moves = 1;
                    break;
                case EnemyType.LocustLarge:
                    moves = 2;
                    break;
                case EnemyType.LadyBugBrown:
                    moves = 1;
                    break;
                case EnemyType.LadyBugRed:
                    moves = 1;
                    break;
                case EnemyType.LeechTanSmall:
                    moves = 1;
                    break;
                case EnemyType.LeechTan:
                    moves = 2;
                    break;
                case EnemyType.LeechRedSmall:
                    moves = 1;
                    break;
                case EnemyType.LeechRed:
                    moves = 2;
                    break;
                case EnemyType.SlugSmall:
                    moves = 1;
                    break;
                case EnemyType.SlugLarge:
                    moves = 1;
                    break;
                case EnemyType.SnailSmall:
                    moves = 1;
                    break;
                case EnemyType.SnailLarge:
                    moves = 1;
                    break;
                case EnemyType.ScorpionSmall:
                    moves = 1;
                    break;
                case EnemyType.ScorpionMedium:
                    moves = 2;
                    break;
                case EnemyType.ScorpionLarge:
                    moves = 2;
                    break;
                case EnemyType.SpiderSmall:
                    moves = 1;
                    break;
                case EnemyType.SpiderMediumBrown:
                    moves = 2;
                    break;
                case EnemyType.SpiderMediumGrey:
                    moves = 2;
                    break;
                case EnemyType.SpiderLarge:
                    moves = 2;
                    break;
                case EnemyType.WormBrown:
                    moves = 1;
                    break;
                case EnemyType.WormTan:
                    moves = 1;
                    break;
            }
            return moves;
        }

        private static int RodentMoves(EnemyType enemyType)
        {
            int moves = 1;
            switch (enemyType)
            {
                case EnemyType.RatBrownSmall:
                    moves = 1;
                    break;
                case EnemyType.RatBrownMedium:
                    moves = 1;
                    break;
                case EnemyType.RatBrownLarge:
                    moves = 2;
                    break;
                case EnemyType.RatGreyLarge:
                    moves = 2;
                    break;
                case EnemyType.RatGreen:
                    moves = 2;
                    break;
                case EnemyType.RatTan:
                    moves = 1;
                    break;
                case EnemyType.RatLeech:
                    moves = 2;
                    break;
            }
            return moves;
        }
    }
}
