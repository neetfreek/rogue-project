﻿/************************************************************
* Contains dictionary mapping EnemyType enum values to      *
*   corresponding enemy base names and public interface for *
*   returning base names.                                   *
*************************************************************/
using System.Collections.Generic;

namespace RogueProject.Enemies
{
    public static class EnemyDisplayName
    {
        public static string DisplayName(Enemy enemy)
        {
            EnemyType enemyType = enemy.EnemyType;
            EnemyDisplayNames.TryGetValue(enemyType, out string nameDisplay);

            return nameDisplay;
        }

        public static Dictionary<EnemyType, string> EnemyDisplayNames = new Dictionary<EnemyType, string>()
        {
            // Avian
            {EnemyType.BatBrownSmall,       "Flapper"},
            {EnemyType.BatBrownLarge,       "Flapper"},
            {EnemyType.BatBlackLarge,       "Flapper"},
            {EnemyType.BatTanLarge,         "Flapper"},
            {EnemyType.BatTanSmall,         "Flapper"},
                                              
            {EnemyType.BirdFluffSnow,       "Fuzzball"},
                                              
            {EnemyType.ChickenSnowLarge,    "Squaker"},
            {EnemyType.ChickenSnowSmall,    "Squaker"},
                                              
            {EnemyType.EagleSnowLarge,      "Imperius"},
            {EnemyType.EagleSnowSmall,      "Imperius"},
                                              
            {EnemyType.KiteSnowSmall,       "Shriker"},
            {EnemyType.KiteSnowLarge,       "Shriker"},
            {EnemyType.KiteSnowTanSmall,    "Shriker"},
            {EnemyType.KiteSnowTanLarge,    "Shriker"},
                                              
            // Pest                           
            {EnemyType.BeetleSmall,         "Buttle"},
            {EnemyType.BeetleLargeBlack,    "Buttle"},
            {EnemyType.BeetleMedium,        "Buttle"},
            {EnemyType.BeetleLargeBrown,    "Buttle"},
            {EnemyType.BeetleLong,          "Buttle"},
                                              
            {EnemyType.WormBrown,           "Wurm"},
            {EnemyType.WormTan,             "Wurm"},
                                              
            {EnemyType.FlyLarva,            "Rip Wing"},
            {EnemyType.Fly,                 "Flick Fly"},
            {EnemyType.BeeLarva,            "Stinkpede"},
            {EnemyType.Bee,                 "Waspish"},
            {EnemyType.MosquitoLarva,       "Pohl"},
            {EnemyType.Mosquito,            "Darter"},
                                               
            {EnemyType.SpiderSmall,         "Spinner"},
            {EnemyType.SpiderMediumBrown,   "Spinner"},
            {EnemyType.SpiderMediumGrey,    "Spinner"},
            {EnemyType.SpiderLarge,         "Spinner"},
            {EnemyType.ScorpionSmall,       "Skorply"},
            {EnemyType.ScorpionMedium,      "Skorply"},
            {EnemyType.ScorpionLarge,       "Skorply"},
                                               
            {EnemyType.LeechTanSmall,       "Leecher"},
            {EnemyType.LeechTan,            "Leecher"},
            {EnemyType.LeechRedSmall,       "Leecher"},
            {EnemyType.LeechRed,            "Leecher"},
                                               
            {EnemyType.AntTan,              "Crawly"},
            {EnemyType.AntBlack,            "Crawly"},
            {EnemyType.AntRed,              "Crawly"},
            {EnemyType.AntBlue,             "Crawly"},
                                               
            {EnemyType.GrasshopperSmall,    "Hopper"},
            {EnemyType.GrasshopperLarge,    "Hopper"},
            {EnemyType.LocustSmall,         "Scout Bug"},
            {EnemyType.LocustLarge,         "Scout Bug"},
                                               
            {EnemyType.BugGlowWings,        "Light Bug"},
            {EnemyType.BugGlowSparkle,      "Ice Bug"},
            {EnemyType.BugSparkleSmall,     "Stinger"},
            {EnemyType.BugSparkleLarge,     "Stinger"},
                                               
            {EnemyType.SlugSmall,           "Slopper"},
            {EnemyType.SlugLarge,           "Slopper"},
            {EnemyType.SnailSmall,          "Slime Shell"},
            {EnemyType.SnailLarge,          "Slime Shell"},
                                               
            {EnemyType.LadyBugRed,          "Gargant"},
            {EnemyType.LadyBugBrown,        "Gargant"},
                                            
            // Rodent                          
            {EnemyType.RatBrownLarge,       "Scuffler"},
            {EnemyType.RatBrownMedium,      "Scuffler"},
            {EnemyType.RatBrownSmall,       "Scuffler"},
            {EnemyType.RatGreen,            "Plague Bourne"},
            {EnemyType.RatGreyLarge,        "Scuffler"},
            {EnemyType.RatLeech,            "Death Mole"},
            {EnemyType.RatTan,              "Scuffler"},
        };
    }
}