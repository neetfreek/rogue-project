﻿using Microsoft.Xna.Framework;

namespace RogueProject.Enemies
{
    public class EnemySetup
    {
        readonly Game1 game;

        public EnemySetup(Game1 game)
        {
            this.game = game;
        }

        public void SetupEnemy(Enemy enemy)
        {
            // Get base states
            enemy.Attacks = EnemyAttacks.Attacks(enemy);
            enemy.DisplayName = EnemyDisplayName.DisplayName(enemy);
            enemy.CombatRange = EnemyAttackRange.AttackRange(enemy);
            enemy.Damage = EnemyDamage.Damage(enemy);
            enemy.Experience = EnemyExperience.Experience(enemy);
            enemy.HealthTotal = EnemyHealth.Health(enemy);
            enemy.HealthCurrent= enemy.HealthTotal;
            enemy.Moves = EnemyMoves.Moves(enemy);
            enemy.RatingHit = 0;
            enemy.RatingDefence = EnemyDefence.RatingDefence(enemy) + Combat.CombatVariables.BASE_DEFENCE;

            // Modify base stats according to area index number, player level
        }
    }
}