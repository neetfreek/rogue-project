﻿/****************************************************************
* Handles enemies' turn.                                        *
* Adds all enemies within aggro range to enemiesActive list.    *
* For each enemie in enemiesActive list, calls its              *
*   StartTurn() and then removes it from enemiesActive list,    *
*   delays thread task for                                      *
*   EnemyVariables.PAUSE_BETWEEN_TURN_ENEMIES, until no         *
*   enemies remain in enemiesActive list.                       *
* If enemies defeated in their turn, their                      *
*   TakeDamageFromSelf()adds them to ToDestroy list. After      *
*   all enemies in enemiesActive list handled, enemies'         *
*   Destroy() is called for each enemy in ToDestroy()           *
* Player in-game keyboard, mouse disabled during handle         *
*   enemies' turns (while HandlingActiveEnemies == true)        *
* Call Player.Action.NewTurn() at end of EndEnemiesTurn()       *
*****************************************************************/
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RogueProject.Enemies
{
    public class EnemyAction
    {
        private readonly Game1 game;

        public bool HandlingActiveEnemies { get => handlingActiveEnemies; }

        private bool handlingActiveEnemies;
        private int activeEnemyCurrent;
        private int activeEnemiesThisTurn;

        public List<Enemy> ToDestroy = new List<Enemy>();
        private List<Enemy> enemiesActive = new List<Enemy>();

        public EnemyAction(Game1 game)
        {
            this.game = game;
        }


        public void UpdateEnemies()
        {
            StartEnemiesTurn();
            HandleEnemiesTurn();
        }

        // Add all enemies in aggro range of player to enemiesActive list
        private void StartEnemiesTurn()
        {
            handlingActiveEnemies = true;

            foreach (Enemy enemy in game.Area.Enemies)
            {
                if (enemy.InRangeAggroPlayer(game.Player.Tile))
                {
                    enemiesActive.Add(enemy);
                }

                activeEnemyCurrent = 0;
                activeEnemiesThisTurn = enemiesActive.Count;
            }
        }
        private void HandleEnemiesTurn()
        {
            if (enemiesActive.Count > 0)
            {
                HandleEnemyActiveTurn();
            }
            else
            {
                EndEnemiesTurn();
            }
        }
        private void HandleEnemyActiveTurn()
        {
            Enemy enemyActiveCurrent = enemiesActive[0];

            activeEnemyCurrent++;
            game.TextBoxes.TextBoxStatus.UpdateStatusEnemyTurns(activeEnemyCurrent, activeEnemiesThisTurn);
            enemyActiveCurrent.StartTurn(game.Player.Tile);
            enemiesActive.Remove(enemyActiveCurrent);

            if (enemiesActive.Count > 0)
            {
                PauseBetweenEnemies();
            }
            else
            {
                EndEnemiesTurn();
            }
        }

        private void EndEnemiesTurn()
        {
            HandleDestroyEnemies();
            handlingActiveEnemies = false;
            game.PlayerAction.NewTurn();
        }

        private void PauseBetweenEnemies()
        {
            Task.Delay(EnemyVariables.PAUSE_BETWEEN_TURN_ENEMIES).ContinueWith(t => HandleEnemiesTurn());
        }
        private void HandleDestroyEnemies()
        {
            foreach (Enemy enemyToDestroy in ToDestroy)
            {
                enemyToDestroy.DestroyEnemy();
            }
        }
    }
}