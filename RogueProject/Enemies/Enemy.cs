﻿/************************
* Enemy game object.    *
*************************/
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Terrain;
using RogueProject.Combat;
using System;
using RogueProject.GUI;
using System.Threading.Tasks;
using RogueProject.Items;

namespace RogueProject.Enemies
{
    public class Enemy
    {
        private readonly Game1 game;

        // State
        public EnemyType EnemyType { get => enemyType; }
        public EnemyCategory EnemyCategory { get => enemyCategory; }
        public string DisplayName;
        // CombatMechanics
        public int RatingDefence { get => ratingDefence; set => ratingDefence = value; }
        public int RatingHit { get => ratingHit; set => ratingHit = value; }
        public float HealthTotal { get => healthTotal; set => healthTotal = value; }
        public float HealthCurrent { get => healthCurrent; set => healthCurrent = value; }
        public float HealthPercent { get => (float)(Math.Round((decimal)(HealthCurrent / HealthTotal), 3, MidpointRounding.AwayFromZero)); }
        public Vector2 Damage { get => damage; set => damage = value; }
        // Navigation
        public Direction DirectionUnBlock{ get => directionUnBlock; set => directionUnBlock = value; }
        public Direction DirectionToPlayer{ get => directionToPlayer; }
        public Tile Tile { get => tile; set => tile = value; }
        public Vector2 DistanceToPlayer { get => distanceToPlayer; }
        public int AggroRangeAdjustment { get => aggroRangeAdjustment; set => aggroRangeAdjustment = value; }
        public int CombatRange {get => combatRange; set => combatRange = value; }
        public int Moves { get => moves; set => moves = value; }
        public int Attacks { get => attacks; set => attacks = value; }
        public int Experience { get => experience; set => experience = value; }


        // Turn-specific stats
        private int movesRemaining;
        private int attacksRemaining;
        private Vector2 distanceToPlayer;
        private Direction directionToPlayer = Direction.None;

        private readonly EnemyType enemyType;
        private readonly EnemyCategory enemyCategory;
        private Tile tile;
        private Direction directionUnBlock = Direction.None;
        private int aggroRangeAdjustment;
        private int combatRange;
        private int attacks;
        private int moves;
        private int ratingDefence;
        private int ratingHit;
        private float healthTotal;
        private float healthCurrent;
        private Vector2 damage;
        private int experience;


        public Enemy(Game1 game, EnemyType enemyType, EnemyCategory enemyCategory)
        {
            this.game = game;
            this.enemyCategory = enemyCategory;
            this.enemyType = enemyType;

            game.EnemySetup.SetupEnemy(this);

        }

        public void DestroyEnemy()
        {
            if (!Game1.DestroyingEnemiesForLoad)
            {
                // Award experience
                game.PlayerStats.GainExperience(Experience);
                game.TextBoxes.TextBoxMessages.UpdateFoeDefeated(DisplayName, Experience);
                // Drop item
                if (ItemDropper.RollDrop(EnemyVariables.CHANCE_DROP_ITEM_BASE))
                {
                    string nameItem = ItemDropper.DropNewItem(Tile);
                    game.TextBoxes.TextBoxMessages.UpdateItemDropped(DisplayName, nameItem);
                }

            }

            Tile.character = Characters.None;
            game.Area.Enemies.Remove(this);
            tile.Enemy = null;
        }


        /************************
        * Manage turn, actions  *
        *************************/
        public void StartTurn(Tile tilePlayer)
        {
            movesRemaining = Moves;
            attacksRemaining = Attacks;

            while (movesRemaining > 0 | attacksRemaining > 0)
            {
                if (NeedPauseBetweenActions(tilePlayer))
                {
                    PauseBetweenActions(tilePlayer);
                }
                else
                {
                    DetermineAction(tilePlayer);
                }
            }
            EndTurn();
        }
        private void EndTurn()
        {
            movesRemaining = 0;
            attacksRemaining = 0;
        }
        private void DetermineAction(Tile tilePlayer)
        {
            UpdateDistanceDirection(tilePlayer);

            // If in attack range and not blocked
            if (InRangeAttackPlayer(tilePlayer) && attacksRemaining > 0 && !CombatMechanics.AttackObstructed(Tile, tilePlayer, DirectionToPlayer))
            {
                AttackPlayer(tilePlayer);
                attacksRemaining--;
            }
            else if (InRangeAggroPlayer(tilePlayer) && movesRemaining > 0)
            {
                game.EnemyMove.ApproachPlayer(this, tilePlayer);
                movesRemaining--;

                if (movesRemaining == 0 && !InRangeAttackPlayer(tilePlayer))
                {
                    EndTurn();
                }
            }
            else
            {
                EndTurn();
            }
        }


        /************
        * Combat    *
        *************/
        public void AttackPlayer(Tile tilePlayer)
        {
            HitRollType hitRoll = CombatMechanics.RollToHit(RatingHit, game.PlayerStats.RatingDefence);
            if (hitRoll == HitRollType.Miss)
            {
                game.TextBoxes.TextBoxMessages.UpdateAttackerMiss(DisplayName, game.PlayerStats.Name);
            }
            else if (game.PlayerStats.HealthCurrent > 0)
            {
                int damage = CombatMechanics.ProcessHit(hitRoll, Damage);

                if (damage < 0)
                {
                    game.TextBoxes.TextBoxMessages.UpdateCriticalMiss(DisplayName, game.PlayerStats.Name, damage);
                    TakeDamageFromSelf(-damage);
                }
                else if (damage > 0)
                {
                    game.PlayerStats.TakeDamage(damage);
                    if (!game.PlayerStats.GameOver)
                    {
                        game.TextBoxes.TextBoxMessages.UpdateAttackerHit(DisplayName, game.PlayerStats.Name, damage, game.PlayerStats.HealthPercent);
                    }
                }
            }
        }
        public void TakeDamageFromPlayer(int damage)
        {
            HealthCurrent -= damage;
            if (HealthCurrent <= 0)
            {
                DestroyEnemy();
            }
        }
        private void TakeDamageFromSelf(int damage)
        {
            HealthCurrent -= damage;

            if (HealthCurrent <= 0)
            {
                game.EnemyAction.ToDestroy.Add(this);
            }
        }


        /************
        * Helpers   *
        *************/
        private void UpdateDistanceDirection(Tile tilePlayer)
        {
            directionToPlayer = Helper.DirectionBetween(tilePlayer, Tile);
            distanceToPlayer = EnemyHelper.DistanceFromPlayer(tilePlayer, Tile, AggroRangeAdjustment);
        }

        public bool InRangeAggroPlayer(Tile tilePlayer)
        {
            if (EnemyHelper.InRangePlayer(tilePlayer, Tile, AggroRangeAdjustment))
            {
                return true;
            }

            return false;
        }
        public bool InRangeAttackPlayer(Tile tilePlayer)
        {
            Vector2 distanceFromPlayer = EnemyHelper.DistanceFromPlayer(tilePlayer, Tile, 0);
            if (distanceFromPlayer.X <= combatRange && distanceFromPlayer.Y <= combatRange)
            {
                return true;
            }

            return false;
        }

        private void PauseBetweenActions(Tile tilePlayer)
        {
            Task.Delay(EnemyVariables.PAUSE_BETWEEN_ACTION_ENEMIES).ContinueWith(t => DetermineAction(tilePlayer));
        }

        private bool NeedPauseBetweenActions(Tile tilePlayer)
        {
            if (Helper.DirectionBetween(tilePlayer, Tile) == Direction.Up || Helper.DirectionBetween(tilePlayer, Tile) == Direction.Down)
            {
                if (Helper.DistanceBetween(tilePlayer, Tile).Y < 9)
                {
                    return true;
                }
            }
            if (Helper.DirectionBetween(tilePlayer, Tile) == Direction.Right || Helper.DirectionBetween(tilePlayer, Tile) == Direction.Right)
            {
                if (Helper.DistanceBetween(tilePlayer, Tile).X < 15)
                {
                    return true;
                }
            }
            else
            {
                if (Helper.DistanceBetween(tilePlayer, Tile).X < 15 | Helper.DistanceBetween(tilePlayer, Tile).Y < 9)
                {
                    return true;
                }
            }

            return false;
        }
    }
}