﻿/****************************************************
* Handle enemy game object movement functionality.  *
*****************************************************/
using RogueProject.Common;
using RogueProject.Terrain;

namespace RogueProject.Enemies
{
    public class EnemyMove
    {
        private readonly Game1 game;

        public EnemyMove(Game1 game)
        {
            this.game = game;
        }

        public void HandleMove(Enemy enemy, Tile tilePlayer, Direction directionToPlayer)
        {
            MoveFromTile(enemy.Tile);
            MoveToTile(enemy, enemy.Tile.Neighbour(directionToPlayer, 1));
        }
        public void MoveToTile(Enemy enemy, Tile tile)
        {
            enemy.Tile = tile;
            enemy.Tile.character = Characters.Enemy;
            enemy.Tile.Enemy = enemy;
        }
        public void MoveFromTile(Tile tile)
        {
            tile.character = Characters.None;
            tile.Enemy = null;
        }

        public void ApproachPlayer(Enemy enemy, Tile tilePlayer)
        {
            //Direction directionToPlayer = Helper.DirectionBetween(tilePlayer, enemy.Tile);
            // Path clear
            if (enemy.Tile.Neighbour(enemy.DirectionToPlayer, 1).Walkable)
            {
                HandleMove(enemy, tilePlayer, enemy.DirectionToPlayer);
                enemy.DirectionUnBlock = Direction.None;
            }
            // Path blocked
            else
            {
                // If new path along side of obstruction blocked go opposite of directionToPlayer
                if (enemy.DirectionUnBlock != Direction.None && !enemy.Tile.Neighbour(enemy.DirectionUnBlock, 1).Walkable)
                {
                    if (enemy.Tile.Neighbour(Helper.DirectionOpposite(enemy.DirectionToPlayer), 1).Walkable)
                    {
                        HandleMove(enemy, tilePlayer, Helper.DirectionOpposite(enemy.DirectionToPlayer));
                        enemy.DirectionUnBlock = Helper.DirectionOpposite(enemy.DirectionToPlayer);
                    }
                }
                // Follow new path along side of obtruction
                else if (enemy.DirectionUnBlock != Direction.None && enemy.Tile.Neighbour(enemy.DirectionUnBlock, 1).Walkable)
                {
                    MoveFromTile(enemy.Tile);
                    MoveToTile(enemy, enemy.Tile.Neighbour(enemy.DirectionUnBlock, 1));
                }
                // Start new path along side of obstruction
                else
                {
                    enemy.DirectionUnBlock = Helper.DirectionBetween(EnemyHelper.CircumventObstacle(tilePlayer, enemy.Tile, enemy.DirectionToPlayer),
                         enemy.Tile);

                    MoveFromTile(enemy.Tile);
                    MoveToTile(enemy, enemy.Tile.Neighbour(enemy.DirectionUnBlock, 1));
                }
            }
        }
    }
}