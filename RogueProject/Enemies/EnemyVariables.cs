﻿/************************************************
* Contain all references to enemy variables     * 
*************************************************/

namespace RogueProject.Enemies
{
    public static class EnemyVariables
    {
        // Turn timers 
        public const int PAUSE_BETWEEN_TURN_ENEMIES = 200;
        public const int PAUSE_BETWEEN_ACTION_ENEMIES = 50;

        // Enemy numbers in areas
        public const int MIN_ENEMY_TYPES = 2;
        public const int MAX_ENEMY_TYPES = 5;
        public const int MIN_ENEMY_CATEGORIES = 1;
        public const int MIN_PORTION_TILES_ENEMIES_OUTDOOR = 1;
        public const int MAX_PORTION_TILES_ENEMIES_OUTDOOR = 1;
        public const int MIN_PORTION_TILES_ENEMIES_INDOOR = 4;
        public const int MAX_PORTION_TILES_ENEMIES_INDOOR = 4;

        // Item drop
        public const int CHANCE_DROP_ITEM_BASE = 35;

        // Group sizes
        public const int MIN_SIZE_GROUP = 1;
        public const int MAX_SIZE_GROUP = 8;
        public const int SIZE_PLACEMENT_GRID = 3;

        // Aggro range
        public const int AGGRO_RANGE_Y = 8;
        public const int AGGRO_RANGE_X = 15;
    }
}