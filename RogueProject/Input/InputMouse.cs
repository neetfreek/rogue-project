﻿/************************
* Handles mouse input   * 
*************************/
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RogueProject.Common;
using RogueProject.GUI;
using RogueProject.PlayerCharacter;
using RogueProject.Terrain;

namespace RogueProject.Input
{
    public class InputMouse
    {
        public static Tile MousedOverTile;
        public static GUIRectangle MousedOverGUIComponent;
        public static GUIRectangle MousedOverGUISubComponent;
        public static Direction MousedOverDirection;

        public Vector2 Position {get => position; }
        public Vector2 PositionOld {get => positionOld; }
        public Tile TileClicked { get => tileClicked; }
        public Vector2 TileClickedDistance { get => tileClickedDistance; }


        private readonly Game1 game;
        private GUIAction guiAction;
        private PlayerAction playerAction;
        private Player player;

        private Vector2 position;
        private Vector2 positionOld;
        private Tile tileClicked;
        private Vector2 tileClickedDistance;
        private Vector2 tilePositionAdjust = new Vector2(-GlobalVariables.WidthScreenActual * 0.5f,
            -GlobalVariables.HeightScreenActual * 0.5f);
        private Vector2 tilePosition;
        private int scrollValue;
        private bool clicked;
        private bool cooldownRunning;


        public InputMouse(Game1 game)
        {
            this.game = game;
            guiAction = game.GUIAction;
            player = game.Player;
            playerAction = game.PlayerAction;
        }

        public void Update()
        {
            MouseState mouseState = Mouse.GetState();
            position.X = mouseState.X;
            position.Y = mouseState.Y;
            game.draw.MousePos = position;

                // Update mouse input in game
                if (Game1.InGame)
                {
                    if (MousedOverGUIComponent == null && !game.EnemyAction.HandlingActiveEnemies)
                    {
                        UpdateGame(mouseState);
                    }
                    UpdateGameGUI(mouseState);
                }



            // Update mouse input in main menu
            if (Game1.InMainMenu)
            {
                position = ScaleMousePositionGUIMenuMain(position);
                UpdateGUIMenu(mouseState);
            }

            positionOld = position;
        }


        /****************************
        * In-menu GUI functionality * 
        *****************************/
        private void UpdateGUIMenu(MouseState mouseState)
        {
            if (guiAction.CurrentComponent.TextHeader != GUIVariables.HEADING_PANEL_NEWGAME)
            {
                if (position != positionOld)
                {
                    HandleMouseOverGUIMenuMain();
                }
            }

            if (clicked == false)
            {
                HandleClickGUIMenuMain(mouseState);
            }

            if (clicked == true && cooldownRunning == false)
            {
                StartCooldownClick();
            }
        }

        private void HandleMouseOverGUIMenuMain()
        {
            foreach (GUIRectangle subComponent in guiAction.CurrentComponent.Subcomponents)
            {
                if (subComponent.MouseOverComponent(position))
                {
                    MousedOverGUIComponent = subComponent;
                    guiAction.SetCurrentSubComponentMouse(MousedOverGUIComponent);
                }
            }
        }

        private void HandleClickGUIMenuMain(MouseState mouseState)
        {
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                // Required for New Game panel to reset 
                if (guiAction.CurrentComponent.TextHeader == GUIVariables.HEADING_PANEL_NEWGAME)
                {
                    HandleMouseOverGUIGame();
                }

                if (MousedOverGUIComponent != null)
                {
                    guiAction.InteractSubComponent();
                }

                clicked = true;
                MousedOverGUIComponent = null;
                guiAction.UnsetCurrentSubComponent();
            }
        }

        private Vector2 ScaleMousePositionGUIMenuMain(Vector2 positionUnscaled)
        {
            return new Vector2(positionUnscaled.X / (GlobalVariables.ScaledSprite * (GlobalVariables.WidthScreenActual / (float)GlobalVariables.WIDTH_SCREEN_VIRTUAL)),
                positionUnscaled.Y / (GlobalVariables.ScaledSprite * (GlobalVariables.HeightScreenActual / (float)GlobalVariables.HEIGHT_SCREEN_VIRTUAL)));
        }


        /****************************
        * In-game GUI functionality * 
        *****************************/
        private void UpdateGameGUI(MouseState mouseState)
        {
            if (positionOld != position)
            {
                HandleMouseOverGUIGame();
            }

            if (clicked == false)
            {
                HandleClickGUIGame(mouseState);
            }

            if (clicked == true && cooldownRunning == false)
            {
                StartCooldownClick();
            }

            HandleScrollGUIGame(mouseState);
        }

        private void HandleMouseOverGUIGame()
        {
            bool overComponent = false;
            bool overSubComponent = false;

            foreach (GUIRectangle component in guiAction.CurrentComponentsList)
            {
                if (component.MouseOverComponent(position))
                {
                    overComponent = true;
                    MousedOverGUIComponent = component;
                    if (guiAction.CurrentComponent != component)
                    {
                        guiAction.SetCurrentComponentGame(component);
                    }

                    foreach (GUIRectangle subComponent in component.Subcomponents)
                    {
                        if (MouseOverGUISubComponentGame(subComponent) == true)
                        {
                            overSubComponent = true;
                        }
                    }
                }
            }
            HandleMouseOffGuiComponentGame(overComponent, overSubComponent);
        }
        // Returns true if mousing over subcomponent, assigns as guiAction.CurrentSubComponent
        private bool MouseOverGUISubComponentGame(GUIRectangle subComponent)
        {
            if (subComponent.MouseOverComponent(position))
            {
                if (guiAction.CurrentSubComponent != subComponent)
                {
                    guiAction.UnsetCurrentSubComponent();
                    guiAction.SetCurrentSubComponentGame(subComponent);
                    MousedOverGUISubComponent = subComponent;
                }
                return true;
            }

            return false;
        }
        
        private void HandleMouseOffGuiComponentGame(bool overComponent, bool overSubComponent)
        {
            if (overComponent == false)
            {
                guiAction.UnsetCurrentSubComponent();
                guiAction.UnsetCurrentComponentGame();
                MousedOverGUISubComponent = null;
                MousedOverGUIComponent = null;
            }
            if (overSubComponent == false)
            {
                guiAction.UnsetCurrentSubComponent();
                MousedOverGUISubComponent = null;
            }
        }

        private void HandleClickGUIGame(MouseState mouseState)
        {
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (game.GUIAction.ClickedStatusBox)
                {
                    game.GUIAction.CheckResetStatusTextBox();
                }
                else
                {
                    if (MousedOverGUISubComponent != null)
                    {
                        guiAction.InteractSubComponent();
                    }
                    else if (MousedOverGUIComponent != null)
                    {
                        guiAction.InteractComponent();
                    }
                }
                clicked = true;
            }
        }

        private void HandleScrollGUIGame(MouseState mouseState)
        {
            if (mouseState.ScrollWheelValue > scrollValue)
            {
                guiAction.InteractScroll(Direction.Up);
            }
            if (mouseState.ScrollWheelValue < scrollValue)
            {
                guiAction.InteractScroll(Direction.Down);
            }

            scrollValue = mouseState.ScrollWheelValue;
        }


        /********************************
        * In-game tile functionality    * 
        *********************************/
        private void UpdateGame(MouseState mouseState)
        {
            if (positionOld != position)
            {
                MouseOverTile();
            }

            if (clicked == false)
            {
                HandleClickGame(mouseState);
            }

            if (clicked == true && cooldownRunning == false)
            {
                StartCooldownClick();
            }
        }

        private void MouseOverTile()
        {
            tilePosition = Position + player.Tile.Position + tilePositionAdjust;

            foreach (Tile tile in game.Area.Grid)
            {
                if (tile.MouseOverTile(tilePosition))
                {
                    MousedOverTile = tile;
                    MousedOverDirection = Helper.DirectionBetween(tile, player.Tile);
                    playerAction.SetMouseHighlight(MousedOverTile);
                }
            }
        }

        private void HandleClickGame(MouseState mouseState)
        {
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (game.GUIAction.ClickedStatusBox)
                {
                    game.GUIAction.CheckResetStatusTextBox();
                }
                else
                {
                    if (MousedOverTile != null)
                    {
                        LeftClickTile(MousedOverTile);
                    }
                }
                clicked = true;
            }
        }

        private void LeftClickTile(Tile tile)
        {
            guiAction.ClearDraggedItem();
            playerAction.SetLeftClickSelect(tile);
            playerAction.KeyboardMove = false;
            tileClickedDistance = tile.PositionGrid - player.Tile.PositionGrid;
            tileClicked = tile;

            // Vertical
            if (tileClickedDistance.X == 0)
            {
                if (tileClickedDistance.Y < 0)
                {
                    playerAction.Action(Direction.Up);
                }
                if (tileClickedDistance.Y > 0)
                {
                    playerAction.Action(Direction.Down);
                }
            }
            //Horizontal
            if (tileClickedDistance.Y == 0)
            {
                if (tileClickedDistance.X < 0)
                {
                    playerAction.Action(Direction.Left);
                }
                if (tileClickedDistance.X > 0)
                {
                    playerAction.Action(Direction.Right);
                }
            }
            // Diagonal
            if (tileClickedDistance.X != 0 && tileClickedDistance.Y != 0)
            {
                if (tileClickedDistance.X > 0 && tileClickedDistance.Y < 0)
                {
                    playerAction.Action(Direction.UpRight);
                }
                if (tileClickedDistance.X > 0 && tileClickedDistance.Y > 0)
                {
                    playerAction.Action(Direction.DownRight);
                }
                if (tileClickedDistance.X < 0 && tileClickedDistance.Y > 0)
                {
                    playerAction.Action(Direction.DownLeft);
                }
                if (tileClickedDistance.X < 0 && tileClickedDistance.Y < 0)
                {
                    playerAction.Action(Direction.UpLeft);
                }
            }
        }


        /************************************
        * Cooldown, prevents re-clicking    * 
        *************************************/
        private void StartCooldownClick()
        {
            cooldownRunning = true;
            Task.Delay(GlobalVariables.COOLDOWN_INPUT_MOUSE).ContinueWith(t => CooldownClickEnd());
        }
        private void CooldownClickEnd()
        {
            clicked = false;
            cooldownRunning = false;
        }
    }
}