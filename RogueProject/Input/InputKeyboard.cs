﻿/***************************
* Handles keyboard input   * 
****************************/
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using RogueProject.Common;
using RogueProject.GUI;
using RogueProject.PlayerCharacter;

namespace RogueProject.Input
{
    public class InputKeyboard
    {
        private Game1 game;
        private PlayerAction playerAction;
        private GUIAction guiAction;
        private readonly Player player;

        public bool UserIsTyping;

        private bool cooldownGameRunning;
        private bool cooldownGUIRunning;
        private bool cooldownInputRunning;
        private bool pressedInput;
        private bool pressedGame;
        private bool pressedGUI;

        public InputKeyboard(Game1 game)
        {
            this.game = game;
            guiAction = game.GUIAction;
            player = game.Player;
            playerAction = game.PlayerAction;
        }

        public void Update()
        {
            if (Game1.InGame && !game.EnemyAction.HandlingActiveEnemies)
            {
                UpdateGame();

                if (pressedGame == true && cooldownGameRunning == false)
                {
                    CooldownPressGameStart();
                }
            }

            if (Game1.InMainMenu)
            {
                if (UserIsTyping && pressedInput == false)
                {
                    if (game.GUIAction.NewGameMenu.SetName(Keyboard.GetState()) == true)
                    {
                        pressedInput = true;
                    }
                }
                else
                {
                    UpdateMainMenu();
                }

                if (pressedGUI == true && cooldownGUIRunning == false)
                {
                    CooldownPressGUIStart();
                }

                if (pressedInput == true && cooldownInputRunning== false)
                {
                    CooldownInputStart();
                }
            }
        }


        /************************
        * In-menu functionality * 
        *************************/
        private void UpdateMainMenu()
        {
            if (pressedGUI == false)
            {
                HandlePressMainMenu();
            }
        }

        private void HandlePressMainMenu()
        {
            KeyboardState keyboardPressed = Keyboard.GetState();

            if (keyboardPressed.IsKeyDown(Keys.Escape))
            {
                if (guiAction.CurrentComponent.TextHeader == GUIVariables.HEADING_MENU_SAVE)
                {
                    guiAction.ToggleSave(false);
                    guiAction.ToggleMainMenu(true);
                }
                else if (guiAction.CurrentComponent.TextHeader == GUIVariables.HEADING_MENU_LOAD)
                {
                    guiAction.ToggleLoad(false);
                    guiAction.ToggleMainMenu(true);
                }
                else if (guiAction.CurrentComponent.TextHeader == GUIVariables.HEADING_PANEL_NEWGAME)
                {
                    guiAction.ToggleNewGame(false);
                    guiAction.ToggleMainMenu(true);
                }
                else if (Game1.GameStarted && guiAction.CurrentComponent.TextHeader == GUIVariables.HEADING_MENU_MAIN)
                {
                    guiAction.MenuMainToGame();
                }
                pressedGame = true;
                pressedGUI = true;
            }


            /****************
            * Interaction   * 
            *****************/
            if (keyboardPressed.IsKeyDown(Keys.Space) || keyboardPressed.IsKeyDown(Keys.Enter))
            {
                guiAction.InteractSubComponent();
                pressedGUI = true;
                pressedGame = true;
            }


            /****************
            * Navigation    * 
            *****************/
            if (keyboardPressed.IsKeyDown(Keys.Up) || keyboardPressed.IsKeyDown(Keys.NumPad8))
            {
                guiAction.SetCurrentSubComponentKeyboard(Direction.Up);
                pressedGUI = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Right) || keyboardPressed.IsKeyDown(Keys.NumPad6))
            {
                guiAction.SetCurrentSubComponentKeyboard(Direction.Right);
                pressedGUI = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Down) || keyboardPressed.IsKeyDown(Keys.NumPad2))
            {
                guiAction.SetCurrentSubComponentKeyboard(Direction.Down);
                pressedGUI = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Left) || keyboardPressed.IsKeyDown(Keys.NumPad4))
            {
                guiAction.SetCurrentSubComponentKeyboard(Direction.Left);
                pressedGUI = true;
            }
        }


        /************************
        * In-game functionality * 
        *************************/
        private void UpdateGame()
        {
            if (pressedGame == false)
            {
                if (Keyboard.GetState().GetPressedKeys().Count() > 0)
                {
                    if (game.GUIAction.ClickedStatusBox)
                    {
                        game.GUIAction.CheckResetStatusTextBox();
                    }
                    else
                    {
                        HandlePressGame();
                    }
                }
            }

            if (pressedGame == true && cooldownGameRunning == false)
            {
                CooldownPressGameStart();
            }
        }

        private void HandlePressGame()
        {
            KeyboardState keyboardPressed = Keyboard.GetState();

            /********
            * Menu  * 
            *********/
            if (keyboardPressed.IsKeyDown(Keys.F5))
            {
                game.Save(GUIVariables.TEXT_SAVE_SLOT_4);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.F9))
            {
                game.Load(GUIVariables.TEXT_SAVE_SLOT_4);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Escape))
            {
                guiAction.ToggleEscape();
                pressedGame = true;
                pressedGUI = true;
            }


            /********************
            * Interaction HUD   * 
            *********************/
            if (keyboardPressed.IsKeyDown(Keys.I))
            {
                guiAction.ToggleInventory();
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.C))
            {
                guiAction.ToggleCharacterSheet();
                pressedGame = true;
            }


            /********************
            * Interaction input * 
            *********************/
            if (keyboardPressed.IsKeyDown(Keys.Space))
            {
                if (guiAction.inventoryOpen)
                {
                    guiAction.KeyboardHUDComponentInteract();
                    pressedGame = true;
                }
                else if (playerAction.InteractableInRange())
                {
                    playerAction.SetLeftClickSelect(player.Direction);
                    playerAction.Action(player.Direction);
                    pressedGame = true;
                }
            }
            if (keyboardPressed.IsKeyDown(Keys.Enter))
            {
                playerAction.EndTurn();
                pressedGame = true;
            }


            /********************
            * Movement input    * 
            *********************/
            if (keyboardPressed.IsKeyDown(Keys.Up) || keyboardPressed.IsKeyDown(Keys.NumPad8))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.Up);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Right) || keyboardPressed.IsKeyDown(Keys.NumPad6))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.Right);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Down) || keyboardPressed.IsKeyDown(Keys.NumPad2))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.Down);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.Left) || keyboardPressed.IsKeyDown(Keys.NumPad4))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.Left);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.NumPad9))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.UpRight);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.NumPad3))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.DownRight);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.NumPad1))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.DownLeft);
                pressedGame = true;
            }
            if (keyboardPressed.IsKeyDown(Keys.NumPad7))
            {
                playerAction.KeyboardMove = true;
                playerAction.Action(Direction.UpLeft);
                pressedGame = true;
            }
        }


        /************************************
        * Cooldown, prevents re-pressing    * 
        *************************************/
        private void CooldownPressGameStart()
        {
            cooldownGameRunning = true;
            Task.Delay(GlobalVariables.COOLDOWN_INPUT_GAME).ContinueWith(t => CooldownPressGameEnd());
        }
        private void CooldownPressGameEnd()
        {
            pressedGame = false;
            cooldownGameRunning = false;
        }

        private void CooldownPressGUIStart()
        {
            cooldownGUIRunning = true;
            Task.Delay(GlobalVariables.COOLDOWN_INPUT_GUI).ContinueWith(t => CooldownPressGUIEnd());
        }
        private void CooldownPressGUIEnd()
        {
            pressedGUI = false;
            cooldownGUIRunning = false;
        }

        private void CooldownInputStart()
        {
            cooldownInputRunning = true;
            Task.Delay(GlobalVariables.COOLDOWN_INPUT).ContinueWith(t => CooldownInputEnd());
        }
        private void CooldownInputEnd()
        {
            pressedInput = false;
            cooldownInputRunning= false;
        }
    }
}