﻿/****************************************************
* Door game object. Provides access in/out rooms.   *
* Interaction causes door to open (DoorState goes   *
*   from .Closed to .Open), allowing player to walk *
*   through and changing sprite to opened version   * 
*****************************************************/
using RogueProject.Common;
using RogueProject.Terrain;
using RogueProject.Textures;
using System.Collections.Generic;

namespace RogueProject.Objects
{
    public class Door
    {
        public readonly Tile Tile;
        public List<Room> Rooms = new List<Room>();
        public DoorSpriteOrientation DoorSpriteOrientation;
        public DoorSpriteCategory DoorSpriteCategory;
        public DoorSprite DoorSprite;
        public DoorState DoorState = DoorState.Closed;

        public FloorSpriteCategory FloorCategoryNeighbour;
        public Direction NeighbourDirection;

        public bool Locked { get => locked; set => locked = value; }
        public bool Opened { get => open; }

        private bool locked;
        private bool open;

        public Door(Tile tile, Direction directionNeighbourTile)
        {
            Tile = tile;
            tile.TileType = TileType.Door;
            tile.FloorCategory = tile.Room.CategoryFloor;
            tile.Door = this;

            NeighbourDirection = directionNeighbourTile;
            if (NeighbourDirection == Direction.Left)
            {
                FloorCategoryNeighbour = Tile.Neighbour(Direction.Left, 1).FloorCategory;
            }
            else if (NeighbourDirection == Direction.Right)
            {
                FloorCategoryNeighbour = Tile.Neighbour(Direction.Right, 1).FloorCategory;
            }
            else
            {
                FloorCategoryNeighbour = Tile.FloorCategory;
            }
        }

        public void Open()
        {
            // Change sprite to opened
            DoorState = DoorState.Open;
            open = true;
        }
    }
}
