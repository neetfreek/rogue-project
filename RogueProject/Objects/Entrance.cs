﻿/****************************************************
* Entrance game object. Provides access to/from     *
*   areas.                                          *
*****************************************************/
using RogueProject.Common;
using RogueProject.Terrain;

namespace RogueProject.Objects
{
    public class Entrance
    {
        public Tile Tile;
        public EntranceType EntranceType;

        public int AreaIndex { get => areaIndex;}
        public int AreaToLoadIndex { get => areaToLoadIndex;}

        private readonly int areaIndex;
        private readonly int areaToLoadIndex;

        public Entrance(int areaIndex, int nextAreaIndex)
        {
            this.areaIndex = areaIndex;
            areaToLoadIndex = nextAreaIndex;
        }
    }
}
