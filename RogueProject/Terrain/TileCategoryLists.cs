﻿/***********************************************************
* Contains lists for region floor, room floor, tree, wall  *
*   sprites for areas. Used by RegionMaker.cs to set       *
*   region variables for areas on being set up             *
************************************************************/
using System.Collections.Generic;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public class TileCategoryLists
    {
        public TileCategoryLists()
        {
            PopulateIndoorFloorLists();
            PopulateIndoorFloorRoomsLists();
            PopulateIndoorWallsLists();

            PopulateOutdoorFloorLists();
            PopulateOutdoorFloorRoomsLists();
            PopulateOutdoorTreeLists();
            PopulateOutdoorWallLists();        
        }

        private void PopulateIndoorFloorLists()
        {
            FloorsDungeonBlue.Add(FloorSpriteCategory.BrickBlue);
            FloorsDungeonBlue.Add(FloorSpriteCategory.BrickGrey);
            FloorsDungeonBlue.Add(FloorSpriteCategory.BrickGreyLight);

            FloorsDungeonGrey.Add(FloorSpriteCategory.BrickGrey);
            FloorsDungeonBlue.Add(FloorSpriteCategory.BrickGreyLight);
            FloorsDungeonGrey.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsDungeonGrey.Add(FloorSpriteCategory.BrickGreyBrown);

            FloorsDungeonDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsDungeonDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsDungeonDark.Add(FloorSpriteCategory.BrickGreyBrown);

            FloorsDungeonSnowLight.Add(FloorSpriteCategory.SnowTan);
            FloorsDungeonSnowLight.Add(FloorSpriteCategory.SnowOrange);

            FloorsDungeonSnowDark.Add(FloorSpriteCategory.SnowBrown);
            FloorsDungeonSnowDark.Add(FloorSpriteCategory.SnowDark);

            FloorsDungeonMetalOrange.Add(FloorSpriteCategory.BrickTan);
            FloorsDungeonMetalOrange.Add(FloorSpriteCategory.BrickOrange);

            FloorsDungeonMetalDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsDungeonMetalDark.Add(FloorSpriteCategory.BrickBrown);
            FloorsDungeonMetalDark.Add(FloorSpriteCategory.BrickGrey);

            FloorsCavesLight.Add(FloorSpriteCategory.ClayTan);
            FloorsCavesLight.Add(FloorSpriteCategory.SandTan);
            FloorsCavesLight.Add(FloorSpriteCategory.SandBrown);

            FloorsCavesMedium.Add(FloorSpriteCategory.ClayOrange);
            FloorsCavesMedium.Add(FloorSpriteCategory.SandOrange);
            FloorsCavesMedium.Add(FloorSpriteCategory.SandBrown);

            FloorsCavesDark.Add(FloorSpriteCategory.SandBrown);
            FloorsCavesDark.Add(FloorSpriteCategory.SandDark);
            FloorsCavesDark.Add(FloorSpriteCategory.ClayDark);
            FloorsCavesDark.Add(FloorSpriteCategory.ClayBrown);

            FloorsCavesIce.Add(FloorSpriteCategory.SandDark);
            FloorsCavesIce.Add(FloorSpriteCategory.ClayBrown);
            FloorsCavesIce.Add(FloorSpriteCategory.ClayDark);
            FloorsCavesIce.Add(FloorSpriteCategory.SandBrown);

            FloorsIndustrialLight.Add(FloorSpriteCategory.ClayOrange);
            FloorsIndustrialLight.Add(FloorSpriteCategory.BrickGreyLight);
            FloorsIndustrialLight.Add(FloorSpriteCategory.BrickBrown);

            FloorsIndustrialDark.Add(FloorSpriteCategory.BrickBrown);
            FloorsIndustrialDark.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsIndustrialDark.Add(FloorSpriteCategory.BrickGreyDark);

            FloorsIceTunnelsLight.Add(FloorSpriteCategory.SnowTan);
            FloorsIceTunnelsLight.Add(FloorSpriteCategory.SnowOrange);

            FloorsIceTunnelsDark.Add(FloorSpriteCategory.SnowBrown);
            FloorsIceTunnelsDark.Add(FloorSpriteCategory.SnowDark);
        }
        private void PopulateIndoorFloorRoomsLists()
        {
            FloorsRoomsDungeonBlue.Add(FloorSpriteCategory.BrickBlue);
            FloorsRoomsDungeonBlue.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsDungeonBlue.Add(FloorSpriteCategory.BrickGreyLight);
                  
            FloorsRoomsDungeonGrey.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsDungeonGrey.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsDungeonGrey.Add(FloorSpriteCategory.BrickGreyBrown);
                  
            FloorsRoomsDungeonDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsDungeonDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsDungeonDark.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsDungeonDark.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsDungeonSnowLight.Add(FloorSpriteCategory.SnowTan);
            FloorsRoomsDungeonSnowLight.Add(FloorSpriteCategory.SnowOrange);
            FloorsRoomsDungeonSnowLight.Add(FloorSpriteCategory.BrickBlue);
            FloorsRoomsDungeonSnowLight.Add(FloorSpriteCategory.BrickGreyLight);
                  
            FloorsRoomsDungeonSnowDark.Add(FloorSpriteCategory.SnowBrown);
            FloorsRoomsDungeonSnowDark.Add(FloorSpriteCategory.SnowDark);
            FloorsRoomsDungeonSnowDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsDungeonSnowDark.Add(FloorSpriteCategory.BrickGreyDark);
                  
            FloorsRoomsDungeonMetalOrange.Add(FloorSpriteCategory.BrickTan);
            FloorsRoomsDungeonMetalOrange.Add(FloorSpriteCategory.BrickOrange);
            FloorsRoomsDungeonMetalOrange.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsDungeonMetalOrange.Add(FloorSpriteCategory.SandBrown);
                  
            FloorsRoomsDungeonMetalDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsDungeonMetalDark.Add(FloorSpriteCategory.BrickBrown);
            FloorsRoomsDungeonMetalDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsDungeonMetalDark.Add(FloorSpriteCategory.SandDark);
                  
            FloorsRoomsCavesLight.Add(FloorSpriteCategory.BrickGreyLight);
            FloorsRoomsCavesLight.Add(FloorSpriteCategory.SandTan);
            FloorsRoomsCavesLight.Add(FloorSpriteCategory.SandBrown);
            FloorsRoomsCavesLight.Add(FloorSpriteCategory.BrickOrange);
                  
            FloorsRoomsCavesMedium.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsCavesMedium.Add(FloorSpriteCategory.SandOrange);

            FloorsRoomsCavesDark.Add(FloorSpriteCategory.SandBrown);
            FloorsRoomsCavesDark.Add(FloorSpriteCategory.BrickGreyDark);
                  
            FloorsRoomsCavesIce.Add(FloorSpriteCategory.SandDark);
            FloorsRoomsCavesIce.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsCavesIce.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsCavesIce.Add(FloorSpriteCategory.SandBrown);
                  
            FloorsRoomsIndustrialLight.Add(FloorSpriteCategory.BrickGreyLight);
            FloorsRoomsIndustrialLight.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsIndustrialLight.Add(FloorSpriteCategory.BrickBrown);
            FloorsRoomsIndustrialLight.Add(FloorSpriteCategory.SandBrown);
                  
            FloorsRoomsIndustrialDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsIndustrialDark.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsIndustrialDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsIndustrialDark.Add(FloorSpriteCategory.SandDark);
                  
            FloorsRoomsIceTunnelsLight.Add(FloorSpriteCategory.BrickBlue);
            FloorsRoomsIceTunnelsLight.Add(FloorSpriteCategory.BrickGreyLight);
            FloorsRoomsIceTunnelsLight.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsIceTunnelsDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsIceTunnelsDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsIceTunnelsDark.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsIceTunnelsDark.Add(FloorSpriteCategory.SandDark);
        }
        private void PopulateIndoorWallsLists()
        {
            WallsDungeonBlue.Add(WallSpriteCategory.BrickSilver);
            WallsDungeonBlue.Add(WallSpriteCategory.BrickGrey);
            WallsDungeonBlue.Add(WallSpriteCategory.BrickBlue);
            
            WallsDungeonGrey.Add(WallSpriteCategory.BrickGrey);
            WallsDungeonGrey.Add(WallSpriteCategory.BrickDark);
            
            WallsDungeonDark.Add(WallSpriteCategory.BrickDark);
            WallsDungeonDark.Add(WallSpriteCategory.MetalDark);
            
            WallsDungeonSnowLight.Add(WallSpriteCategory.IceBlueLight);
            WallsDungeonSnowLight.Add(WallSpriteCategory.IceWhite);

            WallsDungeonSnowDark.Add(WallSpriteCategory.IceBlue);
            WallsDungeonSnowDark.Add(WallSpriteCategory.IceBlueDark);
            
            WallsDungeonMetalOrange.Add(WallSpriteCategory.PlainTan);
            WallsDungeonMetalOrange.Add(WallSpriteCategory.PlainTanDark);
            WallsDungeonMetalOrange.Add(WallSpriteCategory.ShinyTan);
            WallsDungeonMetalOrange.Add(WallSpriteCategory.ShinyTanDark);
            WallsDungeonMetalOrange.Add(WallSpriteCategory.ShinyTanLight);
            
            WallsDungeonMetalDark.Add(WallSpriteCategory.MetalBrown);
            WallsDungeonMetalDark.Add(WallSpriteCategory.MetalBrownDark);
            WallsDungeonMetalDark.Add(WallSpriteCategory.MetalTan);
            WallsDungeonMetalDark.Add(WallSpriteCategory.MetalTan);
            
            WallsCavesLight.Add(WallSpriteCategory.ClayTan);
            WallsCavesLight.Add(WallSpriteCategory.ClayTanBright);
            WallsCavesLight.Add(WallSpriteCategory.ClayOrange);
            WallsCavesLight.Add(WallSpriteCategory.ClaySilver);
            WallsCavesLight.Add(WallSpriteCategory.WoodBrown);
            
            WallsCavesMedium.Add(WallSpriteCategory.ClayGrey);
            WallsCavesMedium.Add(WallSpriteCategory.ClayBlue);
            WallsCavesMedium.Add(WallSpriteCategory.ClayRed);
            WallsCavesMedium.Add(WallSpriteCategory.ClayRedBright);
            WallsCavesMedium.Add(WallSpriteCategory.WoodBrownDark);

            WallsCavesDark.Add(WallSpriteCategory.ClayBrown);
            WallsCavesDark.Add(WallSpriteCategory.ClayBrownDark);
            WallsCavesDark.Add(WallSpriteCategory.ClayBrownGrey);
            WallsCavesDark.Add(WallSpriteCategory.ClayDark);
            WallsCavesDark.Add(WallSpriteCategory.WoodDark);

            WallsCavesIce.Add(WallSpriteCategory.ShinyWhite);
            WallsCavesIce.Add(WallSpriteCategory.ShinyBlue);
            WallsCavesIce.Add(WallSpriteCategory.ShinyBlueLight);
            WallsCavesIce.Add(WallSpriteCategory.ShinyBlueDark);
            
            WallsIndustrialLight.Add(WallSpriteCategory.ShinyTanDark);
            WallsIndustrialLight.Add(WallSpriteCategory.PlainTanDark);
            WallsIndustrialLight.Add(WallSpriteCategory.MetalTan);
            WallsIndustrialLight.Add(WallSpriteCategory.MetalBrown);
            WallsIndustrialLight.Add(WallSpriteCategory.PlainGreyDark);
            
            WallsIndustrialDark.Add(WallSpriteCategory.MetalBrown);
            WallsIndustrialDark.Add(WallSpriteCategory.MetalBrownDark);
            WallsIndustrialDark.Add(WallSpriteCategory.MetalDark);
            WallsIndustrialDark.Add(WallSpriteCategory.PlainDark);
            WallsIndustrialDark.Add(WallSpriteCategory.PlainGreyDark);
            
            WallsIceTunnelsLight.Add(WallSpriteCategory.PlainBlueLight);
            WallsIceTunnelsLight.Add(WallSpriteCategory.PlainBlue);
            WallsIceTunnelsLight.Add(WallSpriteCategory.ShinyBlue);
            
            WallsIceTunnelsDark.Add(WallSpriteCategory.PlainGrey);
            WallsIceTunnelsDark.Add(WallSpriteCategory.PlainGreyDark);
            WallsIceTunnelsDark.Add(WallSpriteCategory.ShinyBlueDark);
        }

        private void PopulateOutdoorFloorLists()
        {
            FloorsForestLight.Add(FloorSpriteCategory.GrassTan);
            FloorsForestLight.Add(FloorSpriteCategory.GrassOrange);

            FloorsForestDark.Add(FloorSpriteCategory.GrassBrown);
            FloorsForestDark.Add(FloorSpriteCategory.GrassDark);

            FloorsForestSnowLight.Add(FloorSpriteCategory.SnowOrange);
            FloorsForestSnowLight.Add(FloorSpriteCategory.SnowTan);

            FloorsForestSnowDark.Add(FloorSpriteCategory.SnowDark);
            FloorsForestSnowDark.Add(FloorSpriteCategory.SnowBrown);

            FloorsWoodsLight.Add(FloorSpriteCategory.GrassTan);
            FloorsWoodsLight.Add(FloorSpriteCategory.GrassOrange);

            FloorsWoodsDark.Add(FloorSpriteCategory.GrassBrown);
            FloorsWoodsDark.Add(FloorSpriteCategory.GrassDark);

            FloorsWoodsSnowLight.Add(FloorSpriteCategory.SnowTan);
            FloorsWoodsSnowLight.Add(FloorSpriteCategory.SnowOrange);

            FloorsWoodsSnowDark.Add(FloorSpriteCategory.SnowBrown);
            FloorsWoodsSnowDark.Add(FloorSpriteCategory.SnowDark);

            FloorsTropic.Add(FloorSpriteCategory.GrassTan);
            FloorsTropic.Add(FloorSpriteCategory.GrassBrown);

            FloorsJungleLight.Add(FloorSpriteCategory.GrassTan);
            FloorsJungleLight.Add(FloorSpriteCategory.GrassOrange);

            FloorsJungleDark.Add(FloorSpriteCategory.GrassDark);
            FloorsJungleDark.Add(FloorSpriteCategory.GrassBrown);

            FloorsOasis.Add(FloorSpriteCategory.SandTan);
            FloorsOasis.Add(FloorSpriteCategory.SandOrange);

            FloorsDesertLight.Add(FloorSpriteCategory.SandTan);
            FloorsDesertLight.Add(FloorSpriteCategory.SandOrange);
            FloorsDesertLight.Add(FloorSpriteCategory.ClayTan);
            FloorsDesertLight.Add(FloorSpriteCategory.ClayOrange);

            FloorsDesertMedium.Add(FloorSpriteCategory.SandBrown);
            FloorsDesertMedium.Add(FloorSpriteCategory.ClayOrange);

            FloorsDesertDark.Add(FloorSpriteCategory.SandBrown);
            FloorsDesertDark.Add(FloorSpriteCategory.SandDark);
            FloorsDesertDark.Add(FloorSpriteCategory.ClayBrown);
            FloorsDesertDark.Add(FloorSpriteCategory.ClayDark);
        }
        private void PopulateOutdoorFloorRoomsLists()
        {
            FloorsRoomsForestLight.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsForestLight.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsForestDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsForestDark.Add(FloorSpriteCategory.BrickGreyBrown);
                  
            FloorsRoomsForestSnowLight.Add(FloorSpriteCategory.BrickBlue);
            FloorsRoomsForestSnowLight.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsForestSnowDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsForestSnowDark.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsWoodsLight.Add(FloorSpriteCategory.BrickBrown);
            FloorsRoomsWoodsLight.Add(FloorSpriteCategory.BrickTan);
            FloorsRoomsWoodsLight.Add(FloorSpriteCategory.BrickOrange);
                  
            FloorsRoomsWoodsDark.Add(FloorSpriteCategory.BrickGreyBrown);
            FloorsRoomsWoodsDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsWoodsDark.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsWoodsSnowLight.Add(FloorSpriteCategory.BrickBlue);
            FloorsRoomsWoodsSnowLight.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsWoodsSnowDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsWoodsSnowDark.Add(FloorSpriteCategory.BrickGrey);
                  
            FloorsRoomsTropic.Add(FloorSpriteCategory.BrickTan);
            FloorsRoomsTropic.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsJungleLight.Add(FloorSpriteCategory.BrickBrown);
            FloorsRoomsJungleLight.Add(FloorSpriteCategory.BrickTan);
            FloorsRoomsJungleLight.Add(FloorSpriteCategory.BrickOrange);
                  
            FloorsRoomsJungleDark.Add(FloorSpriteCategory.BrickGrey);
            FloorsRoomsJungleDark.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsOasis.Add(FloorSpriteCategory.BrickOrange);
            FloorsRoomsOasis.Add(FloorSpriteCategory.BrickTan);
                  
            FloorsRoomsDesertLight.Add(FloorSpriteCategory.BrickTan);
            FloorsRoomsDesertLight.Add(FloorSpriteCategory.BrickOrange);
            FloorsRoomsDesertLight.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsDesertMedium.Add(FloorSpriteCategory.BrickOrange);
            FloorsRoomsDesertMedium.Add(FloorSpriteCategory.BrickBrown);
                  
            FloorsRoomsDesertDark.Add(FloorSpriteCategory.BrickGreyDark);
            FloorsRoomsDesertDark.Add(FloorSpriteCategory.BrickBrown);
            FloorsRoomsDesertDark.Add(FloorSpriteCategory.BrickGreyBrown);
        }
        private void PopulateOutdoorTreeLists()
        {
            TreesForestLight.Add(TreeSpriteCategory.PlainGreenLight);

            TreesForestDark.Add(TreeSpriteCategory.PlainGreenDark);
            TreesForestDark.Add(TreeSpriteCategory.PlainGreenDarkDead);

            TreesForestSnowLight.Add(TreeSpriteCategory.PlainSnowWhite);
            TreesForestSnowLight.Add(TreeSpriteCategory.PlainSnowWhiteDead);

            TreesForestSnowDark.Add(TreeSpriteCategory.PlainSnowBlue);
            TreesForestSnowDark.Add(TreeSpriteCategory.PlainSnowBlueDead);
            
            TreesWoodsLight.Add(TreeSpriteCategory.PineGreenLight);

            TreesWoodsDark.Add(TreeSpriteCategory.PineGreenDark);
            TreesWoodsDark.Add(TreeSpriteCategory.PineGreenDarkDead);

            TreesWoodsSnowLight.Add(TreeSpriteCategory.PineSnowWhite);
            TreesWoodsSnowLight.Add(TreeSpriteCategory.PineSnowWhiteDead);

            TreesWoodsSnowDark.Add(TreeSpriteCategory.PineSnowBlue);
            TreesWoodsSnowDark.Add(TreeSpriteCategory.PineSnowBlueDead);
            
            TreesTropic.Add(TreeSpriteCategory.Palm);
            
            TreesJungleLight.Add(TreeSpriteCategory.WillowLight);
            TreesJungleLight.Add(TreeSpriteCategory.MushroomLight);
            TreesJungleLight.Add(TreeSpriteCategory.Palm);

            TreesJungleDark.Add(TreeSpriteCategory.WillowDark);
            TreesJungleDark.Add(TreeSpriteCategory.MushroomDark);
            
            TreesOasis.Add(TreeSpriteCategory.Palm);
            
            TreesDesertLight.Add(TreeSpriteCategory.PineGreenLightDead);
            TreesDesertLight.Add(TreeSpriteCategory.MushroomLight);

            TreesDesertMedium.Add(TreeSpriteCategory.PlainGreenLightDead);
            TreesDesertMedium.Add(TreeSpriteCategory.MushroomDark);

            TreesDesertDark.Add(TreeSpriteCategory.PineGreenDarkDead);
            TreesDesertDark.Add(TreeSpriteCategory.MushroomDark);
        }
        private void PopulateOutdoorWallLists()
        {
            WallsForestLight.Add(WallSpriteCategory.WoodTan);
            WallsForestLight.Add(WallSpriteCategory.WoodBrown);
            WallsForestLight.Add(WallSpriteCategory.BrickGrey);

            WallsForestDark.Add(WallSpriteCategory.WoodBrownDark);
            WallsForestDark.Add(WallSpriteCategory.WoodDark);
            WallsForestDark.Add(WallSpriteCategory.BrickBlue);
            WallsForestDark.Add(WallSpriteCategory.BrickDark);

            WallsForestSnowLight.Add(WallSpriteCategory.BrickSilver);
            WallsForestSnowLight.Add(WallSpriteCategory.IceWhite);
            WallsForestSnowLight.Add(WallSpriteCategory.IceBlueLight);

            WallsForestSnowDark.Add(WallSpriteCategory.BrickBlue);
            WallsForestSnowDark.Add(WallSpriteCategory.IceBlue);
            WallsForestSnowDark.Add(WallSpriteCategory.IceBlueDark);
            
            WallsWoodsLight.Add(WallSpriteCategory.WoodTan);
            WallsWoodsLight.Add(WallSpriteCategory.WoodBrown);
            WallsWoodsLight.Add(WallSpriteCategory.BrickGrey);

            WallsWoodsDark.Add(WallSpriteCategory.WoodBrownDark);
            WallsWoodsDark.Add(WallSpriteCategory.WoodDark);
            WallsWoodsDark.Add(WallSpriteCategory.BrickBlue);
            WallsWoodsDark.Add(WallSpriteCategory.BrickDark);

            WallsWoodsSnowLight.Add(WallSpriteCategory.BrickSilver);
            WallsWoodsSnowLight.Add(WallSpriteCategory.IceWhite);
            WallsWoodsSnowLight.Add(WallSpriteCategory.IceBlueLight);

            WallsWoodsSnowDark.Add(WallSpriteCategory.BrickBlue);
            WallsWoodsSnowDark.Add(WallSpriteCategory.IceBlue);
            WallsWoodsSnowDark.Add(WallSpriteCategory.IceBlueDark);
            
            WallsTropic.Add(WallSpriteCategory.WoodTan);
            WallsTropic.Add(WallSpriteCategory.BrickDark);
            WallsTropic.Add(WallSpriteCategory.WoodBrownDark);
            
            WallsJungleLight.Add(WallSpriteCategory.WoodTan);
            WallsJungleLight.Add(WallSpriteCategory.WoodBrown);

            WallsJungleDark.Add(WallSpriteCategory.WoodBrown);
            WallsJungleDark.Add(WallSpriteCategory.WoodBrownDark);
            
            WallsOasis.Add(WallSpriteCategory.ClayTan);
            WallsOasis.Add(WallSpriteCategory.ClayOrange);
            
            WallsDesertLight.Add(WallSpriteCategory.ClayOrange);
            WallsDesertLight.Add(WallSpriteCategory.ClayTan);

            WallsDesertMedium.Add(WallSpriteCategory.ClayBrown);
            WallsDesertMedium.Add(WallSpriteCategory.ClayTan);
            WallsDesertMedium.Add(WallSpriteCategory.ClayRed);

            WallsDesertDark.Add(WallSpriteCategory.ClayBrownDark);
            WallsDesertDark.Add(WallSpriteCategory.ClayGrey);
            WallsDesertDark.Add(WallSpriteCategory.ClayBrown);
        }


        // Indoor region floor sprite sets
        public static List<FloorSpriteCategory> FloorsDungeonGrey =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDungeonBlue =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDungeonDark =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDungeonSnowLight =    new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDungeonSnowDark =     new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsCavesLight =          new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsCavesMedium =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsCavesDark =           new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsCavesIce =            new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsIndustrialLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsIndustrialDark =      new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsIceTunnelsLight =       new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsIceTunnelsDark =        new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsDungeonMetalOrange =  new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDungeonMetalDark =    new List<FloorSpriteCategory>();
        // Outdoor region floor sprite sets
        public static List<FloorSpriteCategory> FloorsForestLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsForestDark =      new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsForestSnowLight = new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsForestSnowDark =  new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsWoodsLight =      new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsWoodsDark =       new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsWoodsSnowLight =  new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsWoodsSnowDark =   new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsTropic =          new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsJungleLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsJungleDark =      new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsOasis =           new List<FloorSpriteCategory>();

        public static List<FloorSpriteCategory> FloorsDesertLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDesertMedium =    new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsDesertDark =      new List<FloorSpriteCategory>();
        
        // Indoor region room floor sprite sets
        public static List<FloorSpriteCategory> FloorsRoomsDungeonGrey =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDungeonBlue =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDungeonDark =         new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsDungeonSnowLight =    new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDungeonSnowDark =     new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsCavesLight =          new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsCavesMedium =         new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsCavesDark =           new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsCavesIce =            new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsIndustrialLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsIndustrialDark =      new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsIceTunnelsLight =       new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsIceTunnelsDark =        new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsDungeonMetalOrange =  new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDungeonMetalDark =    new List<FloorSpriteCategory>();
        // Outdoor region room floor sprite sets
        public static List<FloorSpriteCategory> FloorsRoomsForestLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsForestDark =      new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsForestSnowLight = new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsForestSnowDark =  new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsWoodsLight =      new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsWoodsDark =       new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsWoodsSnowLight =  new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsWoodsSnowDark =   new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsTropic =          new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsJungleLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsJungleDark =      new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsOasis =           new List<FloorSpriteCategory>();
                                                      
        public static List<FloorSpriteCategory> FloorsRoomsDesertLight =     new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDesertMedium =    new List<FloorSpriteCategory>();
        public static List<FloorSpriteCategory> FloorsRoomsDesertDark =      new List<FloorSpriteCategory>();

        // Outdoor region tree sprite sets
        public static List<TreeSpriteCategory> TreesForestLight =      new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesForestDark =       new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesForestSnowLight =  new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesForestSnowDark =   new List<TreeSpriteCategory>();
                                                                                 
        public static List<TreeSpriteCategory> TreesWoodsLight =       new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesWoodsDark =        new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesWoodsSnowLight =   new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesWoodsSnowDark =    new List<TreeSpriteCategory>();
                                                                            
        public static List<TreeSpriteCategory> TreesTropic =           new List<TreeSpriteCategory>();
                                                                            
        public static List<TreeSpriteCategory> TreesJungleLight =      new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesJungleDark =       new List<TreeSpriteCategory>();
                                                                            
        public static List<TreeSpriteCategory> TreesOasis =            new List<TreeSpriteCategory>();
                                                                            
        public static List<TreeSpriteCategory> TreesDesertLight =      new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesDesertMedium =     new List<TreeSpriteCategory>();
        public static List<TreeSpriteCategory> TreesDesertDark =       new List<TreeSpriteCategory>();

        // Indoor region wall sprite sets
        public static List<WallSpriteCategory> WallsDungeonGrey =         new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDungeonBlue =         new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDungeonDark =         new List<WallSpriteCategory>();
                                                 
        public static List<WallSpriteCategory> WallsDungeonSnowLight =    new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDungeonSnowDark =     new List<WallSpriteCategory>();
                                                 
        public static List<WallSpriteCategory> WallsCavesLight =          new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsCavesMedium =         new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsCavesDark =           new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsCavesIce =            new List<WallSpriteCategory>();
                                                 
        public static List<WallSpriteCategory> WallsIndustrialLight =     new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsIndustrialDark =      new List<WallSpriteCategory>();
                                                 
        public static List<WallSpriteCategory> WallsIceTunnelsLight =       new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsIceTunnelsDark =        new List<WallSpriteCategory>();
                                                 
        public static List<WallSpriteCategory> WallsDungeonMetalOrange =  new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDungeonMetalDark =    new List<WallSpriteCategory>();

        // Outdoor region wall sprite sets
        public static List<WallSpriteCategory> WallsForestLight =      new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsForestDark =       new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsForestSnowLight =  new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsForestSnowDark =   new List<WallSpriteCategory>();
                                                                       
        public static List<WallSpriteCategory> WallsWoodsLight =       new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsWoodsDark =        new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsWoodsSnowLight =   new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsWoodsSnowDark =    new List<WallSpriteCategory>();
                                                                       
        public static List<WallSpriteCategory> WallsTropic =           new List<WallSpriteCategory>();
                                                                       
        public static List<WallSpriteCategory> WallsJungleLight =      new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsJungleDark =       new List<WallSpriteCategory>();
                                                                       
        public static List<WallSpriteCategory> WallsOasis =            new List<WallSpriteCategory>();
                                                                       
        public static List<WallSpriteCategory> WallsDesertLight =      new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDesertMedium =     new List<WallSpriteCategory>();
        public static List<WallSpriteCategory> WallsDesertDark =       new List<WallSpriteCategory>();
    }
}