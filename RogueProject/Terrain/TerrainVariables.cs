﻿/************************************************
* Contains all references to terrain variables  * 
*************************************************/
using RogueProject.Common;
using System;

namespace RogueProject.Terrain
{
    public static class TerrainVariables
    {
        // Entrances
        public const int MAX_ENTRANCES = 3; // Used in random.Next, so 1 higher than actual count

        // Regions
        public const int CHANCE_NEXT_AREA_REGION_SAME = 100;
        public const int CHANCE_NEXT_AREA_DECREMENTOR = 20;

        // Minimum, maximum dimensions of area
        public static int MIN_X_AREA = (int)(GlobalVariables.WIDTH_HEIGHT_SPRITE * Math.Round(GlobalVariables.AspectVirtual));
        public static int MIN_Y_AREA = (int)((GlobalVariables.WIDTH_HEIGHT_SPRITE * Math.Round(GlobalVariables.AspectVirtual)) *
            GlobalVariables.HEIGHT_SCREEN_VIRTUAL / GlobalVariables.WIDTH_SCREEN_VIRTUAL);
        public const int MAX_Y_AREA = 100;
        public const int MAX_X_AREA = 100;
        //public const int MIN_X_AREA = 20;
        //public const int MIN_Y_AREA = 20;
        //public const int MAX_X_AREA = 20;
        //public const int MAX_Y_AREA = 20;

        // Rooms
        public const int MIN_X_ROOM = 4;
        public const int MIN_Y_ROOM = 4;
        public const int MAX_X_ROOM = 12;
        public const int MAX_Y_ROOM = 12;
        public const int NUMBER_ROOM_FLOOR_CATEGORIES = 2;
        // Minimum number of tiles to constitute a room
        public const int TILES_ROOM_MIN = MIN_X_ROOM * MIN_Y_ROOM;

        // Minimum, maximum portion of total area that can be room tiles
        public const int PORTION_ROOM_INDOOR_MIN = 60;
        public const int PORTION_ROOM_INDOOR_MAX = 100;
        public const int PORTION_ROOM_OUTDOOR_MIN = 8;
        public const int PORTION_ROOM_OUTDOOR_MAX = 15;

        // Minimum, maximum portion of non-room tiles that can be obstacles
        public const int PORTION_FLOOR_OUTDOOR_OBSTACLES_MIN = 5;
        public const int PORTION_FLOOR_OUTDOOR_OBSTACLES_MAX = 15;
        public const int OBSTACLE_SIZE_MIN = 8;
        public const int OBSTACLE_SIZE_MAX = 8;

        // Decorations
        public const int NUMBER_DECORATIONS_CATEGORIES = 2;
        public const int PORTION_FLOOR_OUTDOOR_DECORATIONS_MIN = 3;
        public const int PORTION_FLOOR_OUTDOOR_DECORATIONS_MAX = 7;
        public const int PORTION_FLOOR_INDOOR_DECORATIONS_MIN = 2;
        public const int PORTION_FLOOR_INDOOR_DECORATIONS_MAX = 4;
    }
}