﻿/***********************************************************************
* Makes rooms for areas and places them into gameArea Rooms list       *
* Passed gameArea object, getting public references for gameArea       *
* 1. HandleMakeRooms() handles creating all area rooms:                *
*   a. Determine how many tiles in area are for rooms (tilesForRooms)  * 
*   b. Create new rooms by calling:                                    *
*       i. SetRoomSize()                                               *
*       ii. SetRoomPosition()                                          *
*   c. Instantiate new room object with above methods' return values   *
*       and this.category floor as parameters                          *
* 2. Add rooms to gameArea's Rooms list                                *
************************************************************************/
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public class RoomMaker
    {
        private Area area;
        private DoorMaker doorMaker;

        // Created here to prevent rapid re-initialisation, similar numbers generated
        private Random random = new Random();
        private Vector2 sizeArea;
        private AreaType typeArea;
        private  FloorSpriteCategory CategoryFloorArea;
        private bool placeRoomFail = false;

        // Current room variables
        private Vector2 sizeRoomCurrent;
        private Vector2 positionRoomCurrent;


        /********
        * Setup *
        *********/
        private void UpdateReferences(Area area)
        {
            this.area = area;
            doorMaker = new DoorMaker();
            sizeArea = area.SizeArea;
            typeArea = area.AreaType;

            if (area.areaState.Initialised)
            {
                ReinitialiseAreaTilesetRoomFloors();
            }
            else
            {
                InitialiseAreaTilesetRoomFloors();
            }
            CategoryFloorArea = area.FloorCategory;
        }

        private void InitialiseAreaTilesetRoomFloors()
        {
            area.TilesetRoomFloorActual.Clear();
            if (!area.areaState.Initialised)
            {
                for (int counter = 0; counter < TerrainVariables.NUMBER_ROOM_FLOOR_CATEGORIES; counter++)
                {
                    int numberSelection = random.Next(0, area.TilesetRoomFloorOptions.Count);
                    FloorSpriteCategory categoryToAdd = area.TilesetRoomFloorOptions[numberSelection];

                    while (area.TilesetRoomFloorActual.Contains(categoryToAdd) | categoryToAdd == area.FloorCategory)
                    {
                        numberSelection = random.Next(0, area.TilesetRoomFloorOptions.Count);
                        categoryToAdd = area.TilesetRoomFloorOptions[numberSelection];
                    }
                    area.TilesetRoomFloorActual.Add(categoryToAdd);
                }
            }
        }

        private void ReinitialiseAreaTilesetRoomFloors()
        {
            area.TilesetRoomFloorActual = area.areaState.TilesetRoomFloorActual;
        }


        /****************
        * Make rooms    *
        ****************/
        // Room creation routine
        public void HandleMakeRooms(AreaMaker areaMaker)
        {
            UpdateReferences(areaMaker.Area);
            MakeRooms();
            areaMaker.gridMaker.AddRoomsToGrid();
            MakeRoomOverlapLists();
            doorMaker.MakeDoors(area);
            
        }
        // Assign rooms' sizes, positions, textures, add to Area.Rooms list
        private void MakeRooms()
        {
            // How many tiles in area should be room tiles
            int tilesForRooms = (int)((sizeArea.X * sizeArea.Y) * PortionAreaRooms());

            // Keep generating rooms until run out of tilesForRooms
            while (tilesForRooms > TerrainVariables.TILES_ROOM_MIN)
            {
                Vector2 sizeRoom = SetRoomSize();
                sizeRoomCurrent = sizeRoom;
                Vector2 positionRoom = SetRoomPosition();

                if (!placeRoomFail)
                {
                    Room room = new Room(area, positionRoom, sizeRoomCurrent, RetrieveAreaFloorCategory());
                    area.Rooms.Add(room);
                }
                else
                {
                    placeRoomFail = false;
                }

                if (typeArea == AreaType.Indoor)
                {
                    tilesForRooms -= (int)((sizeRoomCurrent.X * sizeRoomCurrent.Y) * 0.1f);
                }
                else
                {
                    tilesForRooms -= (int)(sizeRoomCurrent.X * sizeRoomCurrent.Y);
                }
            }
        }
        // Populate each room's overlap tiles lists
        private void MakeRoomOverlapLists()
        {
            foreach (Tile tile in area.Grid)
            {
                if (tile.RoomNeighbour != null)
                {
                    tile.Room.OverlapTiles.Add(tile);
                }
            }

            foreach (Room room in area.Rooms)
            {
                foreach (Tile tile in room.OverlapTiles)
                {
                    if (!room.OverlapRooms.Contains(tile.RoomNeighbour))
                    {
                        room.OverlapRooms.Add(tile.RoomNeighbour);
                    }
                }
            }

            int counter = 1;
            foreach (Room room in area.Rooms)
            {
                foreach (Room roomInList in room.OverlapRooms)
                {
                    counter++;
                }
            }
        }   
      

        /********************
        * Make room helpers *
        *********************/
        // Return what percentage of total area occupied by rooms
        private float PortionAreaRooms()
        {
            float portion = 0f;

            switch (typeArea)
            {
                case AreaType.Indoor:
                    portion = random.Next(TerrainVariables.PORTION_ROOM_INDOOR_MIN, TerrainVariables.PORTION_ROOM_INDOOR_MAX);
                    break;
                case AreaType.Outdoor:
                    portion = random.Next(TerrainVariables.PORTION_ROOM_OUTDOOR_MIN, TerrainVariables.PORTION_ROOM_OUTDOOR_MAX);
                    break;
            }
            return portion * 0.01f;
        }

        private Vector2 SetRoomPosition()
        {
            int borderStartSide1;
            int borderStartSide2;
            // Indoor rooms can spawn on area borders (borders are 1 wide)
            if (area.AreaType == AreaType.Indoor)
            {
                borderStartSide1 = 1;
                borderStartSide2 = 2;
            }
            // Outdoor rooms spawn one tile away from borders (borders are 3 wide)
            else
            {
                borderStartSide1 = 4;
                borderStartSide2 = 5;
            }

            int min = borderStartSide1;
            int maxX = (int)(sizeArea.X - (borderStartSide2 + sizeRoomCurrent.X));
            int maxY = (int)(sizeArea.Y - (borderStartSide2 + sizeRoomCurrent.Y));

            int counterNoFit = 0;

            while (true)
            {
                if (min <= maxX && min <= maxY)
                {
                    // Select position at random
                    positionRoomCurrent = new Vector2
                    {
                        X = random.Next(borderStartSide1, (int)(sizeArea.X - (borderStartSide2 + sizeRoomCurrent.X))),
                        Y = random.Next(borderStartSide1, (int)(sizeArea.Y - (borderStartSide2 + sizeRoomCurrent.Y)))
                    };
                }
                else
                {
                    placeRoomFail = true;
                    break;
                }

                // Accept position if not within 3 cells of other rooms and within area bounds
                Rectangle rectangleCurrentRoom = new Rectangle((int)positionRoomCurrent.X, (int)positionRoomCurrent.Y, (int)sizeRoomCurrent.X, (int)sizeRoomCurrent.Y);

                if (!IntersectsOtherRooms(rectangleCurrentRoom, area.Rooms) &&
                    positionRoomCurrent.X + sizeRoomCurrent.X < sizeArea.X &&
                    positionRoomCurrent.Y + sizeRoomCurrent.Y < sizeArea.Y)
                {
                    break;
                }
                // Otherwise re-position
                else
                {
                    positionRoomCurrent = new Vector2
                    {
                        X = random.Next(borderStartSide1, (int)(sizeArea.X - borderStartSide2 - sizeRoomCurrent.X)),
                        Y = random.Next(borderStartSide1, (int)(sizeArea.Y - borderStartSide2 - sizeRoomCurrent.Y))
                    };
                    counterNoFit++;

                    // If no fit 100 times, reduce either X, Y
                    if (counterNoFit % 100 == 0)
                    {
                        if (random.Next(0, 1) == 0 && sizeRoomCurrent.X > TerrainVariables.MIN_X_ROOM)
                        {
                            sizeRoomCurrent.X -= 1;
                        }
                        else if (sizeRoomCurrent.Y > TerrainVariables.MIN_Y_ROOM)
                        {
                            sizeRoomCurrent.Y -= 1;
                        }

                        positionRoomCurrent = new Vector2
                        {
                            X = random.Next(0, (int)(sizeArea.X - sizeRoomCurrent.X)),
                            Y = random.Next(0, (int)(sizeArea.Y - sizeRoomCurrent.Y))
                        };

                        counterNoFit = 0;
                    }

                    // If fail 1000 times give up on room
                    if (counterNoFit % 1000 == 0)
                    {
                        placeRoomFail = true;
                        break;
                    }
                }
            }
            return positionRoomCurrent;
        }

        private Vector2 SetRoomSize()
        {
            Vector2 sizeRoom = new Vector2(0f, 0f);

            while (sizeRoom.X == 0 && sizeRoom.Y == 0 ||
                sizeRoom.X >= sizeArea.X | sizeRoom.Y >= sizeArea.Y)
            {
                sizeRoom.X = random.Next(TerrainVariables.MIN_X_ROOM, TerrainVariables.MAX_X_ROOM);
                sizeRoom.Y = random.Next(TerrainVariables.MIN_Y_ROOM, TerrainVariables.MAX_X_ROOM);
            }

            return sizeRoom;
        }

        // Check if room within 1 cell (in RectangleBorder) of other rooms
        private bool IntersectsOtherRooms(Rectangle roomCurrent, List<Room> listRoom)
        {
            foreach (Room room in listRoom)
            {
                if (room.RectangleBorder.Intersects(roomCurrent))
                {
                    return true;
                }
            }

            return false;
        }

        private FloorSpriteCategory RetrieveAreaFloorCategory()
        {
            int r = random.Next(0, area.TilesetRoomFloorActual.Count);
            return area.TilesetRoomFloorActual[r];
        }
    }
}