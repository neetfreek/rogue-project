﻿/************************************************************
* Responsible for setting up tile grid for areas            *
* 1. Populate area.Grid with tiles through SetupNewGrid(),  *
*   which handles creating tiles, setting borders and       *
*   floors.                                                 *
* 2. Populate area with rooms in area through               *
*       AddRoomsToGrid() by setting necessary tiles' types, *
*       sprites to floors, walls.                           *                
* 3. Populate area with objects through                     *
*       AddEntrancesToGrid(), AddEnemiesToGrid()            *
* 4. Add enemies to grid with AddEnemiesToGrid()            *
* 5. Add obstacles to grid with AddObstaclesToGrid()        *
* 6. Add decorations to grid tiles with                     *
*       Handle AddDecorationsToGrid()*                      *
*************************************************************/ 
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Enemies;
using RogueProject.Objects;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public class GridMaker
    {
        private Game1 game;
        private Area area;
        private Random random = new Random();


        /************************
        * Grid setup routine    * 
        *************************/
        public void SetupNewGrid(Game1 game, Area area)
        {
            this.game = game;
            this.area = area;

            area.Grid = new Tile[(int)area.SizeArea.X, (int)area.SizeArea.Y];

            for (int counterRows = 0; counterRows < area.Grid.GetLength(1); counterRows++)
            {
                for (int counterCols = 0; counterCols < area.Grid.GetLength(0); counterCols++)
                {
                    Tile tile = MakeTile(counterCols, counterRows);
                    // If area border tile, make TileType Wall tile
                    if (!CheckMakeBorderIndoor(counterCols, counterRows, tile) &&
                        !CheckMakeBorderOutdoor(counterCols, counterRows, tile))
                    // Else inside area border, make TileType floor tile
                    {
                        MakeFloorTile(tile);
                    }
                }
            }
        }
        // Create new tile in grid position
        private Tile MakeTile(int counterCols, int counterRows)
        {
            area.Grid[counterCols, counterRows] = new Tile(area);
            Tile tile = area.Grid[counterCols, counterRows];
            tile.Position = new Vector2(counterCols * GlobalVariables.ScaledSprite,
                counterRows * GlobalVariables.ScaledSprite);
            tile.PositionGrid = new Vector2(counterCols, counterRows);
            tile.Size = new Vector2(GlobalVariables.ScaledSprite,
                GlobalVariables.ScaledSprite);

            return tile;
        }
        // If area border tile, make TileType Wall tile, return true, else false
        private bool CheckMakeBorderIndoor(int counterCols, int counterRows, Tile tile)
        {
            if (area.AreaType == AreaType.Indoor)
            {
                if (counterRows == 0 || counterRows == area.Grid.GetLength(1) - 1 ||
                    counterCols == 0 || counterCols == area.Grid.GetLength(0) - 1)
                {
                    tile.TileType = TileType.Wall;
                    tile.WallCategory = area.WallCategory;
                    tile.WallType = GridSpriteTypes.WallTypeByPosition(counterCols, counterRows, new Vector2(area.Grid.GetLength(0), area.Grid.GetLength(1)));
                    tile.Layer = 0.2f;
                    return true;
                }
            }

            return false;
        }
        private bool CheckMakeBorderOutdoor(int counterCols, int counterRows, Tile tile)
        {
            if (area.AreaType == AreaType.Outdoor)
            {
                // Outermost section (1/3)
                if (counterRows == 0 || counterRows == area.Grid.GetLength(1) - 1 ||
                    counterCols == 0 || counterCols == area.Grid.GetLength(0) - 1)
                {
                    tile.TileType = TileType.Tree;
                    tile.TreeCategory = area.TreeCategory;
                    tile.TreeType = GridSpriteTypes.OuterBorderTypesByPosition(counterCols, counterRows, new Vector2(area.Grid.GetLength(0), area.Grid.GetLength(1)));
                    tile.Layer = 0.2f;
                    tile.FloorCategory = area.FloorCategory;
                    tile.FloorType = GridSpriteTypes.OuterBorderFloorTypeByPosition(counterCols, counterRows, new Vector2(area.Grid.GetLength(0), area.Grid.GetLength(1)));
                    return true;
                }
                // Middle section (2/3)
                if (counterRows == 1 || counterRows == area.Grid.GetLength(1) - 2 ||
                    counterCols == 1 || counterCols == area.Grid.GetLength(0) - 2)
                {
                    tile.TileType = TileType.Tree;
                    tile.TreeCategory = area.TreeCategory;
                    tile.TreeType = TreeSpriteType.MidM;
                    tile.Layer = 0.2f;
                    tile.FloorCategory = area.FloorCategory;
                    tile.FloorType = FloorSpriteType.MidM;
                    return true;
                }
                // Inner-most section (/3)
                if (counterRows == 2 || counterRows == area.Grid.GetLength(1) - 3 ||
                    counterCols == 2 || counterCols == area.Grid.GetLength(0) - 3)
                {
                    tile.TileType = TileType.Tree;
                    tile.TreeCategory = area.TreeCategory;
                    tile.TreeType = GridSpriteTypes.InnerBorderTreeTypeByPosition(counterCols, counterRows, new Vector2(area.Grid.GetLength(0), area.Grid.GetLength(1)));
                    tile.Layer = 0.2f;
                    tile.FloorCategory = area.FloorCategory;
                    tile.FloorType = FloorSpriteType.MidM;
                    return true;
                }
            }

            return false;
        }

        private void MakeFloorTile(Tile tile)
        {
            {
                tile.TileType = TileType.Floor;
                tile.FloorCategory = area.FloorCategory;
                tile.FloorType = FloorSpriteType.MidM;
                tile.Layer = 0f;
            }
        }


        /********************
        * Add grid sections * 
        *********************/
        public void AddRoomsToGrid()
        {
            foreach (Room room in area.Rooms)
            {
                for (int counterRows = 0; counterRows < room.SizeRoom.Y; counterRows++)
                {
                    for (int counterCols = 0; counterCols < room.SizeRoom.X; counterCols++)
                    {
                        Tile tile = area.Grid[(int)room.PositionRoom.X + counterCols, (int)room.PositionRoom.Y + counterRows];

                        // Iterate room borders
                        if (counterRows == 0 || counterRows == room.SizeRoom.Y - 1 ||
                            counterCols == 0 || counterCols == room.SizeRoom.X - 1)
                        {
                            tile.TileType = TileType.Wall;
                            tile.WallCategory = area.WallCategory;

                            if (tile.WallType == WallSpriteType.None)
                            {
                                tile.WallType = GridSpriteTypes.WallTypeByPosition(counterCols, counterRows, room.SizeRoom);
                                tile.Room = room;
                            }
                            else
                            {
                                tile.WallType = GridSpriteTypes.WallTypeOverlap(tile, GridSpriteTypes.WallTypeByPosition(counterCols, counterRows, room.SizeRoom));
                                tile.RoomNeighbour = tile.Room;
                                tile.Room = room;
                            }

                            tile.Layer = 0.2f;
                        }
                        else
                        {
                            tile.FloorCategory = room.CategoryFloor;
                            tile.TileType = TileType.Floor;
                            tile.Layer = 0f;
                        }
                    }
                }
                SetRoomFloorType(room);
            }
        }
        private void SetRoomFloorType(Room room)
        {
            for (int counterRows = 1; counterRows < room.SizeRoom.Y - 1; counterRows++)
            {
                for (int counterCols = 1; counterCols < room.SizeRoom.X - 1; counterCols++)
                {
                    area.Grid[(int)room.PositionRoom.X + counterCols, (int)room.PositionRoom.Y + counterRows].FloorType = GridSpriteTypes.OuterBorderFloorTypeByPosition(counterCols - 1, counterRows - 1, room.SizeFloor);
                }
            }
        }


        /****************
        * Add entrances * 
        *****************/
        public void AddEntrancesToGrid(EntranceMaker entranceMaker)
        {
            foreach (Entrance entrance in area.Entrances)
            {
                Vector2 positionEntrance = entranceMaker.PositionEntrance(area, entrance);
                Tile tile = area.Grid[(int)positionEntrance.X, (int)positionEntrance.Y];

                tile = area.Grid[(int)positionEntrance.X, (int)positionEntrance.Y];
                tile.Entrance = entrance;
                entrance.Tile = tile;

                if (entrance.EntranceType == EntranceType.Outdoor)
                {
                    SetOutdoorEntranceGrid(tile);
                }
                SetEntranceSprite(tile);
            }
            entranceMaker.ResetPlacementBools();
        }
        private void SetEntranceSprite(Tile tile)
        {
            if (tile.TileType == TileType.Floor)
            {
                int roll = random.Next(0, 3);
                if (roll == 0)
                {
                    tile.EntranceSprite = EntranceSprite.Arch;
                }
                if (roll == 1)
                {
                    tile.EntranceSprite = EntranceSprite.Stairs;
                }
                if (roll == 2)
                {
                    tile.EntranceSprite = EntranceSprite.Ladder;
                }

            }
            if (area.Entrances.IndexOf(tile.Entrance) == 0 && area.AreaType == AreaType.Outdoor)
            {
                tile.EntranceSprite = EntranceSprite.None;
            }
        }
        // Setup entrance path, neighbouring tree tiles
        private void SetOutdoorEntranceGrid(Tile tile)
        {
            if (tile.PositionGrid.Y == 2)
            {
                EntranceTrunkGrid.TopOutdoorEntranceGrid(tile);
            }

            if (tile.PositionGrid.X == area.SizeArea.X - 3)
            {
                EntranceTrunkGrid.RightOutdoorEntranceGrid(tile);
            }

            if (tile.PositionGrid.Y == area.SizeArea.Y - 3)
            {
                EntranceTrunkGrid.BottomOutdoorEntranceGrid(tile);
            }

            if (tile.PositionGrid.X == 2)
            {
                EntranceTrunkGrid.LeftOutdoorEntranceGrid(tile);
            }
        }


        /****************
        * Add obstacles * 
        *****************/
        public void AddObstaclesToGrid(ObstacleType obstacleType)
        {
            int numberObstacles = (int)((random.Next(TerrainVariables.PORTION_FLOOR_OUTDOOR_OBSTACLES_MIN, TerrainVariables.PORTION_FLOOR_OUTDOOR_OBSTACLES_MAX + 1) * 0.01) * CountExteriorTilesFloor());

            int sizeObstacle = 0;
            Vector2 placementGridStart = new Vector2(0f, 0f);
            Vector2 placementGridStartOld = placementGridStart;
            Tile tileCurrent = area.Grid[0, 0];
            Tile tilePrevious = tileCurrent;

            while (numberObstacles > 0)
            {
                sizeObstacle = random.Next(TerrainVariables.OBSTACLE_SIZE_MIN, TerrainVariables.OBSTACLE_SIZE_MAX + 1);
                // Get suitable obstacle patch start position
                while (placementGridStart == placementGridStartOld | !TileAppropriateForObstacle(tileCurrent))
                {
                    placementGridStart = area.Grid[random.Next(0, area.Grid.GetLength(0) - 1),
                        random.Next(random.Next(0, area.Grid.GetLength(1) - 1))].PositionGrid;

                    tileCurrent = area.Grid[(int)(placementGridStart.X), (int)(placementGridStart.Y)];
                }
                // Iterate obstacle start patch area
                for (int counterRows = 0; counterRows < random.Next(0, (int)((sizeObstacle) + 1)); counterRows++)
                {
                    for (int counterCols = 0; counterCols < (int)((sizeObstacle) + 1); counterCols++)
                    {
                        if (tileCurrent.PositionGrid.X + counterCols < area.Grid.GetLength(0) &&
                            tileCurrent.PositionGrid.Y + counterRows < area.Grid.GetLength(1))
                        {
                            tileCurrent = area.Grid[(int)(tileCurrent.PositionGrid.X + counterCols), (int)(tileCurrent.PositionGrid.Y + counterRows)];
                        }
                        // Set tile as obstacle if appropriate tile
                        if (tileCurrent.Walkable && tileCurrent.Room == null)
                        {
                            PlaceObstacle(tileCurrent, obstacleType);
                            numberObstacles--;
                        }
                    }
                }

                placementGridStartOld = placementGridStart;
                tilePrevious = tileCurrent;
            }
        }
        private bool TileAppropriateForObstacle(Tile tile)
        {
            if (tile.Walkable && tile.Room == null
                && tile.Neighbour(Direction.Up, 1).Walkable && tile.Neighbour(Direction.Down, 1).Walkable
                && tile.Neighbour(Direction.Right, 1).Walkable && tile.Neighbour(Direction.Left, 1).Walkable)
            {
                return true;
            }

            return false;
        }
        private void PlaceObstacle(Tile tile, ObstacleType obstacleType)
        {
            switch (obstacleType)
            {
                case ObstacleType.Tree:
                    tile.TileType = TileType.Tree;
                    tile.TreeCategory = area.TreeCategory;
                    tile.TreeType = TreeSpriteType.Single;
                    tile.Layer = 0.2f;
                    break;
            }
        }


        /********************
        * Add decorations   * 
        *********************/
        public void HandleAddDecorationsToGrid()
        {
            List<DecorationGroundSpriteCategory> decorationCagetoryList = new List<DecorationGroundSpriteCategory>();
            int numberDecorationsToAdd;

            switch (area.AreaType)
            {
                case AreaType.Indoor:
                    decorationCagetoryList = DecorationListIndoor();
                    numberDecorationsToAdd = (int)(((random.Next(TerrainVariables.PORTION_FLOOR_INDOOR_DECORATIONS_MIN, TerrainVariables.PORTION_FLOOR_INDOOR_DECORATIONS_MAX)) * 0.01f) * area.Grid.GetLength(0) * area.Grid.GetLength(1));
                    AddDecorationsToGrid(decorationCagetoryList, numberDecorationsToAdd);
                    break;
                case AreaType.Outdoor:
                    decorationCagetoryList = DecorationListIndoor();
                    numberDecorationsToAdd = (int)(((random.Next(TerrainVariables.PORTION_FLOOR_OUTDOOR_DECORATIONS_MIN, TerrainVariables.PORTION_FLOOR_OUTDOOR_DECORATIONS_MAX)) * 0.01f) * area.Grid.GetLength(0) * area.Grid.GetLength(1));
                    decorationCagetoryList = DecorationListOutdoor();
                    AddDecorationsToGrid(decorationCagetoryList, numberDecorationsToAdd);
                    break;
            }
        }
        private List<DecorationGroundSpriteCategory> DecorationListIndoor()
        {
            List<DecorationGroundSpriteCategory> decorationCagetoryList = new List<DecorationGroundSpriteCategory>();

            // Snow regions
            if (area.RegionType == RegionType.DungeonSnowLight || area.RegionType == RegionType.DungeonSnowDark ||
                area.RegionType == RegionType.CavesIce || area.RegionType == RegionType.IceTunnelsLight ||
                area.RegionType == RegionType.IceTunnelsDark)
            {
                decorationCagetoryList.Add(DecorationGroundSpriteCategory.StonesBlueLarge);
                decorationCagetoryList.Add(DecorationGroundSpriteCategory.StonesBlueSmall);
            }
            // Other regions
            else
            {
                decorationCagetoryList.Add(DecorationGroundSpriteCategory.StonesRedLarge);
                decorationCagetoryList.Add(DecorationGroundSpriteCategory.StonesRedSmall);
            }
            return decorationCagetoryList;
        }
        private List<DecorationGroundSpriteCategory> DecorationListOutdoor()
        {
            List<DecorationGroundSpriteCategory> decorationCagetoryList = new List<DecorationGroundSpriteCategory>();
            int decorationCategoriesToAdd = TerrainVariables.NUMBER_DECORATIONS_CATEGORIES;
            DecorationGroundSpriteCategory categoryToAdd = DecorationGroundSpriteCategory.None;

            while (decorationCategoriesToAdd > 0)
            {
                // Snow regions
                if (area.RegionType == RegionType.ForestSnowDark || area.RegionType == RegionType.ForestSnowLight ||
                    area.RegionType == RegionType.WoodsSnowDark || area.RegionType == RegionType.WoodsSnowLight)
                {
                    categoryToAdd = DecorationCategory(9, 12);
                }
                // Other regions
                else
                {
                    categoryToAdd = DecorationCategory(1, 12);
                }

                if (!decorationCagetoryList.Contains(categoryToAdd))
                {
                    decorationCagetoryList.Add(categoryToAdd);
                    decorationCategoriesToAdd--;
                }
            }
            return decorationCagetoryList;
        }
        private DecorationGroundSpriteCategory DecorationCategory(int indexStart, int indexStop)
        {
            int numberSelection = random.Next(indexStart, indexStop + 1); // Default doors are DoorWood, DoorMetal
            DecorationGroundSpriteCategory categoryDecoration = (DecorationGroundSpriteCategory)Enum.ToObject(typeof(DecorationGroundSpriteCategory), numberSelection);

            return categoryDecoration;
        }
        private void AddDecorationsToGrid(List<DecorationGroundSpriteCategory> decorationsCategory, int numberDecorationsToPlace)
        {
            List<DecorationGroundSpriteCategory> decorationsCategoryListLocal = decorationsCategory;
            Tile tileTarget;
            while (numberDecorationsToPlace > 0)
            {
                tileTarget = area.Grid[random.Next(0, (area.Grid.GetLength(0) - 1)), random.Next(0, (area.Grid.GetLength(1)))];
                if (tileTarget.Entrance == null && tileTarget.Room == null && tileTarget.TileType == TileType.Floor)
                {
                    tileTarget.DecorationGroundCategory = decorationsCategoryListLocal[random.Next(0, decorationsCategoryListLocal.Count)];
                    int typeRoll = random.Next(1, 3);
                    tileTarget.DecorationGroundType = (DecorationGroundSpriteType)Enum.ToObject(typeof(DecorationGroundSpriteType), typeRoll);

                    numberDecorationsToPlace--;
                }
            }
        }


        /************************
        * Add enemies to grid   * 
        *************************/
        // Handle place enemies in groups on Walkable tiles
        public void AddEnemiesToGrid()
        {
            // Roll number enemies to place around same position
            int enemiesInGroupToPlace = random.Next(EnemyVariables.MIN_SIZE_GROUP, EnemyVariables.MAX_SIZE_GROUP + 1);
            List<Enemy> group = EnemyGroupLists.AddNewGroup();
            // Get position for group
            Vector2 positionEnemies = PlacementGridCenter(EnemyVariables.SIZE_PLACEMENT_GRID);
            // Iterate all enemies to place
            for (int enemyToPlace = 0; enemyToPlace < area.Enemies.Count; enemyToPlace++)
            {
                PlaceEnemyAtPosition(area.Enemies[enemyToPlace], EnemyPositionInPlacementGrid(positionEnemies));
                group.Add(area.Enemies[enemyToPlace]);
                // If placed all enemies around same position make new group, position
                if (enemiesInGroupToPlace == 0 && enemyToPlace < area.Enemies.Count)
                {
                    enemiesInGroupToPlace = random.Next(EnemyVariables.MIN_SIZE_GROUP, EnemyVariables.MAX_SIZE_GROUP + 1);
                    group = EnemyGroupLists.AddNewGroup();
                    positionEnemies = PlacementGridCenter(EnemyVariables.SIZE_PLACEMENT_GRID);
                }
                // Otherwise decrement number enemies in group to still place in current position
                else if (enemyToPlace < area.Enemies.Count)
                {
                    enemiesInGroupToPlace--;
                }
            }
        }
        // Return enemy placement position within enemy group's placement grid
        private Vector2 EnemyPositionInPlacementGrid(Vector2 gridCenter)
        {
            Vector2 position = gridCenter;
            Tile tile = area.Grid[(int)position.X, (int)position.Y];
            int changeX = 0;
            int changeY = 0;

            while (!tile.Walkable)
            {
                changeX = random.Next(-EnemyVariables.SIZE_PLACEMENT_GRID, EnemyVariables.SIZE_PLACEMENT_GRID);
                changeY = random.Next(-EnemyVariables.SIZE_PLACEMENT_GRID, EnemyVariables.SIZE_PLACEMENT_GRID);
                position = new Vector2(gridCenter.X + changeX, gridCenter.Y + changeY);
                tile = area.Grid[(int)position.X, (int)position.Y];
            }

            return position;
        }
        private void PlaceEnemyAtPosition(Enemy enemy, Vector2 positionEnemy)
        {
            Tile tile = area.Grid[(int)positionEnemy.X, (int)positionEnemy.Y];
            game.EnemyMove.MoveToTile(enemy, tile);
        }


        /************
        * Helpers   * 
        *************/
        private int CountExteriorTilesFloor()
        {
            int countTilesFloor = 0;
            foreach (Tile tile in area.Grid)
            {
                if (tile.TileType == TileType.Floor && tile.Room == null)
                {
                    countTilesFloor++;
                }
            }

            return countTilesFloor;
        }
        // Return center point of placement grid
        private Vector2 PlacementGridCenter(int widthLengthGrid)
        {
            Vector2 positionCenter = new Vector2(random.Next(widthLengthGrid * 2,
                area.Grid.GetLength(0) - (widthLengthGrid * 2)),
                random.Next(widthLengthGrid * 2,
                area.Grid.GetLength(1) - (widthLengthGrid * 2)));
            Tile tile = area.Grid[(int)positionCenter.X, (int)positionCenter.Y];

            while (!tile.Walkable)
            {
                positionCenter = new Vector2(random.Next(widthLengthGrid * 2,
                    area.Grid.GetLength(0) - (widthLengthGrid * 2)),
                    random.Next(widthLengthGrid * 2,
                    area.Grid.GetLength(1) - (widthLengthGrid * 2)));
                tile = area.Grid[(int)positionCenter.X, (int)positionCenter.Y];
            }

            return positionCenter;
        }
    }
}