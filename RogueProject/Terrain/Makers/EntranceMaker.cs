﻿/****************************************************
* Creates and places Entrance objects in area for   *
*   moving between areas.                             *
*****************************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Objects;

namespace RogueProject.Terrain
{
    public class EntranceMaker
    {
        private Random random = new Random();
        private bool alreadyPlacedLeft; // Used to ensure entrances don't get made on same dimension
        private bool alreadyPlacedRight;
        private bool alreadyPlacedTop;
        private bool alreadyPlacedBottom;

        public void AddEntrancesToArea(Area area)
        {
            int numberEntrances = random.Next(1, TerrainVariables.MAX_ENTRANCES);

            if (area.AreaType == AreaType.Indoor)
            {
                // Indoor areas 0-1 entrances; one to next level
                numberEntrances = random.Next(0, TerrainVariables.MAX_ENTRANCES - 1);
            }

            while (numberEntrances > 0)
            {
                Game1.HighestAreaPlanned++;
                Entrance entrance = new Entrance(area.AreaIndex, Game1.HighestAreaPlanned);
                area.Entrances.Add(entrance);
                numberEntrances--;
            }
            // Add entrance to previous area
            if (area.AreaIndex != 0)
            {
                MakeEntranceToPreviousArea(area);
            }

            foreach (Entrance entrance in area.Entrances)
            {
                Helper.SetEntranceType(area, entrance);
            }
        }

        private void MakeEntranceToPreviousArea(Area area)
        {
            Entrance entrance = new Entrance(area.AreaIndex, Game1.PreviousArea);
            area.Entrances.Add(entrance);
        }

        public Vector2 PositionEntrance(Area area, Entrance entrance)
        {
            Vector2 positionEntrance = new Vector2();
            // If outdoor entrance to next area
            if (entrance.EntranceType == EntranceType.Outdoor)
            {
                positionEntrance = PlaceOutdoorEntrance(area);
            }
            else
            {
                positionEntrance = PlaceIndoorEntrance(area);
            }

            return positionEntrance;
        }

        private Vector2 PlaceOutdoorEntrance(Area area)
        {
            bool placeAppropriate = false;
            Vector2 positionEntrance = new Vector2();
            while (!placeAppropriate)
            {
                int placeDimension1 = random.Next(0, 2); // place along column or row
                int placeDimension2 = random.Next(0, 2); // place along one or other side

                if (placeDimension1 == 0) // Place on column
                {
                    positionEntrance.X = random.Next(4, area.Grid.GetLength(0) - 4); // Along left, right
                    if (placeDimension2 == 0)
                    {
                        if (alreadyPlacedTop)
                        {
                            positionEntrance.Y = area.Grid.GetLength(1) - 3; // Place bottom
                        }
                        else
                        {
                            positionEntrance.Y = 2; // Place Top
                            alreadyPlacedTop = true;
                        }
                    }
                    else
                    {
                        if (alreadyPlacedBottom)
                        {
                            positionEntrance.Y = 2; // Place Top
                        }
                        else
                        {
                            positionEntrance.Y = area.Grid.GetLength(1) - 3; // Place bottom
                            alreadyPlacedBottom = true;
                        }
                    }
                }
                else // Place on row
                {
                    positionEntrance.Y = random.Next(4, area.Grid.GetLength(1) - 4); // Along top, bottom
                    if (placeDimension2 == 0)
                    {
                        if (alreadyPlacedLeft)
                        {
                            positionEntrance.X = area.Grid.GetLength(0) - 3; // Place right
                        }
                        else
                        {
                            positionEntrance.X = 2; // Place left
                            alreadyPlacedLeft = true;
                        }
                    }
                    else
                    {
                        if (alreadyPlacedRight)
                        {
                            positionEntrance.X = 2; // Place left
                        }
                        else
                        {
                            positionEntrance.X = area.Grid.GetLength(0) - 3; // Place right
                            alreadyPlacedRight = true;
                        }
                    }
                }             

                Tile tilePosition = area.Grid[(int)positionEntrance.X, (int)positionEntrance.Y];
                placeAppropriate = true;
            }
            return positionEntrance;
        } // Entrances linking outdoor areas together, placed on borders

        private bool EntranceFarEnoughFromRoom(Area area, Tile tileEntrance)
        {
            foreach (Room room in area.Rooms)
            {
                Tile tileRoom = area.Grid[(int)room.PositionRoom.X, (int)room.PositionRoom.Y];
                int largestSideRoom = 0;
                if (room.SizeRoom.X > room.SizeRoom.Y)
                {
                    largestSideRoom = (int)room.SizeRoom.X;
                }
                else
                {
                    largestSideRoom = (int)room.SizeRoom.Y;
                }

                if (Helper.InRange(tileRoom, tileEntrance, largestSideRoom))
                {
                    return false;
                }
            }
            return true;
        }

        public void ResetPlacementBools()
        {
            alreadyPlacedLeft  = false;
            alreadyPlacedRight = false;
            alreadyPlacedTop  = false;
            alreadyPlacedBottom = false;
        }   

        private Vector2 PlaceIndoorEntrance(Area area)
        {
            bool positionAppropriate = false;
            Tile tile = new Tile(area);
            while (!positionAppropriate)
            {
                int roomRandom = random.Next(0, area.Rooms.Count);
                int tileRandom = random.Next(0, area.Rooms[roomRandom].FloorTiles.Count);
                tile = area.Rooms[roomRandom].FloorTiles[tileRandom];

                if (tile.Neighbour(Direction.Up, 1).TileType != TileType.Door &&
                    tile.Neighbour(Direction.Right, 1).TileType != TileType.Door &&
                    tile.Neighbour(Direction.Down, 1).TileType != TileType.Door &&
                    tile.Neighbour(Direction.Left, 1).TileType != TileType.Door)
                {
                    positionAppropriate = true;
                }
            }

            return tile.PositionGrid;
        } // Indoor entrances, can't be placed on interactables
    }
}