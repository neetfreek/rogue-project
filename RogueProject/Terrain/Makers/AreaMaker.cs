﻿/****************************************************
* Handles creation of new and already-visited areas *
*****************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Enemies;
using RogueProject.Objects;
using RogueProject.Textures;
using System;

namespace RogueProject.Terrain
{
    public class AreaMaker
    {

        private Game1 game;
        private Random random = new Random();

        public Area Area;
        private readonly EnemyMaker enemyMaker;
        private readonly EntranceMaker entranceMaker;
        public readonly GridMaker gridMaker;
        private readonly RoomMaker roomMaker;

        private AreaState areaState;

        public AreaMaker(Game1 game)
        {
            this.game = game;
            enemyMaker = new EnemyMaker(game, this);
            entranceMaker = new EntranceMaker();
            gridMaker = new GridMaker();
            roomMaker = new RoomMaker();
        }


        /********************
        * Make area routine *
        *********************/
        public void MakeArea(AreaState areaState)
        {
            this.areaState = areaState;

            SetupArea();
            CreateArea();
            ReadyArea();
        }

        // Setup area variables like grid size, textures to use
        private void SetupArea()
        {
            if (Area != null)
            {
                ClearArea();
            }
            Area = new Area()
            {
                areaState = this.areaState
            };
            areaState.Area = Area;

            if (Game1.LoadingGame)
            {
                Area.AreaIndex = Game1.CurrentAreaIndex;
            }
            else
            {
                Area.AreaIndex = Game1.PlannedArea;
            }

            game.Area = Area;
            if (areaState.Initialised)
            {
                SetupInitialisedArea();
            }
            else
            {
                SetupUninitialisedArea();
            }
        }

        private void SetupUninitialisedArea()
        {
            Area.SetupAreaType(DetermineAreaType());

            RegionMaker.HandleSetAreaRegion(Area);
            Area.SetupAreaSize(RandomAreaSize());
            Area.SetupRoomWidth(RandomRoomsWidth());
            Area.SetupRoomHeight(RandomRoomsHeight());
            entranceMaker.AddEntrancesToArea(Area);
            enemyMaker.AddEnemiesToArea(game);
        }
        private void SetupInitialisedArea()
        {
            Area.SetupAreaType(areaState.AreaType);

            Area.SetupDoorCategory(areaState.DoorSpriteCategory);
            Area.SetupFloorCategory(areaState.FloorSpriteCategory);
            Area.SetupTreeCategory(areaState.TreeSpriteCategory);
            Area.SetupWallCategory(areaState.WallSpriteCategory);

            Area.TilesetRoomFloorActual.Clear();
            Area.TilesetRoomFloorActual = areaState.TilesetRoomFloorActual;
            Area.Entrances = areaState.Entrances;

            Area.SetupAreaSize(areaState.Size);
            Area.SetupRoomWidth(areaState.RoomsWidth);
            Area.SetupRoomHeight(areaState.RoomsHeight);

            Area.EnemyTypes = areaState.EnemyTypes;
            Area.Enemies = enemyMaker.LoadEnemies(Area.EnemyTypes);
        } // Load area data from current session
        public void SetupAreaFromLoad(int areaNumber)
        {
            areaState = AreaStatesList.AreaStates[areaNumber];
            areaState.Initialised = true;
            if (Area != null)
            {
                ClearArea();
            }

            Area = new Area()
            {
                areaState = areaState
            };
            Area.SetupAreaType(areaState.AreaType);
            Area.AreaIndex = areaState.AreaIndex;

            Area.SetupDoorCategory(areaState.DoorSpriteCategory);
            Area.SetupFloorCategory(areaState.FloorSpriteCategory);
            Area.SetupTreeCategory(areaState.TreeSpriteCategory);
            Area.SetupWallCategory(areaState.WallSpriteCategory);

            Area.TilesetRoomFloorActual = areaState.TilesetRoomFloorActual;
            Area.TilesetRoomFloorOptions = areaState.TilesetRoomFloorActual;
            Area.SetupAreaSize(areaState.Size);
            Area.SetupRoomWidth(RandomRoomsWidth());
            Area.SetupRoomHeight(RandomRoomsHeight());
            Area.Entrances = areaState.Entrances;
            foreach (Entrance entrance in Area.Entrances)
            {
                Helper.SetEntranceType(Area, entrance);
            }
            EnemyGroupLists.ResetGroups();
            Area.EnemyTypes = areaState.EnemyTypes;

            Area.Enemies = enemyMaker.LoadEnemies(Area.EnemyTypes);

            CreateArea();
            ReadyArea();
        }

        // Create area grid, rooms, area objects
        private void CreateArea()
        {
            game.Area = Area;
            gridMaker.SetupNewGrid(game, Area);
            roomMaker.HandleMakeRooms(this);
            gridMaker.AddEntrancesToGrid(entranceMaker);
            gridMaker.AddEnemiesToGrid();

            if (Area.AreaType == AreaType.Outdoor)
            {
                gridMaker.AddObstaclesToGrid(ObstacleType.Tree);
            }
            gridMaker.HandleAddDecorationsToGrid();

            if (!areaState.Initialised)
            {
                Area.SaveAreaState(areaState);
            }
        }

        public void ClearArea()
        {
            Area.Rooms.Clear();
            Area.Grid = new Tile[0, 0];
        }
               
        public void ReadyArea()
        {
            game.Camera.SetupCamera(Area.SizeArea);
            game.draw.Area = Area;
        }

        private AreaType DetermineAreaType()
        {
            // Default area indoor
            AreaType type = AreaType.Outdoor;

            // Use type of entrance player clicked to determine area type
            if (game.Player.ClickedNextOutdoorEntrance || game.Player.ClickedPreviousOutdoorEntrance)
            {
                type = AreaType.Outdoor;
            }

            if (game.Player.ClickedNextIndoorEntrance || game.Player.ClickedpreviousIndoorEntrance)
            {
                type = AreaType.Indoor;
            }

            return type;
        }


        /****************************
        * Random area routine parts *
        *****************************/
        private DoorSpriteCategory RandomDoorCategory()
        {
            int numberSelection = random.Next(1, 3); // Default doors are DoorWood, DoorMetal
            DoorSpriteCategory categoryDoor = (DoorSpriteCategory)Enum.ToObject(typeof(DoorSpriteCategory), numberSelection);

            return categoryDoor;
        }
        private Vector2 RandomAreaSize()
        {
            int heightArea = random.Next(TerrainVariables.MIN_Y_AREA, TerrainVariables.MAX_Y_AREA);
            int widthArea = random.Next(TerrainVariables.MIN_X_AREA, TerrainVariables.MAX_X_AREA);

            return new Vector2(widthArea, heightArea);
        }
        private Vector2 RandomRoomsWidth()
        {
            float multiplierMinWidth = 1f + (random.Next(0,6) * 0.1f);

            int widthRoomMin = (int)(TerrainVariables.MIN_X_ROOM * multiplierMinWidth);
            int widthRoomMax = random.Next(widthRoomMin + (int)(widthRoomMin * 0.5f), TerrainVariables.MAX_X_ROOM);
            if (widthRoomMax > Area.SizeArea.X)
            {
                widthRoomMax = (int)Area.SizeArea.X;
            }

            return new Vector2(widthRoomMin, widthRoomMax);
        }
        private Vector2 RandomRoomsHeight()
        {
            float multiplierMinHeight = 1f + (random.Next(0, 6) * 0.1f);

            int heightRoomMin = (int)(TerrainVariables.MIN_Y_ROOM * multiplierMinHeight);
            int heightRoomMax = random.Next(heightRoomMin + (int)(heightRoomMin * 0.5f), TerrainVariables.MAX_Y_ROOM);
            if (heightRoomMax > Area.SizeArea.Y)
            {
                heightRoomMax = (int)Area.SizeArea.Y;
            }

            return new Vector2(heightRoomMin, heightRoomMax);
        }
    }       
}