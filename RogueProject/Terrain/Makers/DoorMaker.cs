﻿/***********************************************************************
* Make doors for rooms                                                 *
* Make overlapping rooms' doors: make doors along overlapping          *
* (intersecting) rooms' walls to connect them to one another.          *
* Make isolated rooms' doors: make doors on walls for rooms which don't*
*   overlap any other rooms'.                                          *
************************************************************************/
using System;
using System.Collections.Generic;
using RogueProject.Common;
using RogueProject.Objects;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    class DoorMaker
    {
        private Area area;
        private Random random = new Random();

        /************************
        * Make doors routine    *
        *************************/
        public void MakeDoors(Area area)
        {
            this.area = area;
            MakeOverlapRoomDoors();
            MakeIsolatedRoomDoors();

            foreach (Room room in area.Rooms)
            {
                if (room.Doors.Count > 0)
                {
                    foreach (Door door in room.Doors)
                    {
                        door.DoorSpriteCategory = area.DoorCategory;
                    }
                }
            }
        }

        /********************************************
        * Make overlapping rooms' door sub-routine  *
        *********************************************/
        private void MakeOverlapRoomDoors()
        {
            foreach (Room room in area.Rooms)
            {
                List<Room> roomsToDo = new List<Room>();
                foreach (Room roomOverlap in room.OverlapRooms)
                {
                    roomsToDo.Add(roomOverlap);
                }

                // Doors for overlapping rooms
                foreach (Tile tile in room.OverlapTiles)
                {
                    Tile tileRandom = room.OverlapTiles[random.Next(0, room.OverlapTiles.Count)];

                    if (tileRandom.Room.Doors.Count < DoorsRequired(tileRandom.Room) && roomsToDo.Contains(tileRandom.RoomNeighbour) &&
                        tileRandom.WallType == WallSpriteType.MidV | tileRandom.WallType == WallSpriteType.MidH)
                    {
                        //Direction of neighbouring tile
                        Direction directionNeighbourTile = Direction.Left;
                        if (tileRandom.Room.PositionRoom.X < tileRandom.RoomNeighbour.PositionRoom.X)
                        {
                            directionNeighbourTile = Direction.Right;
                        }

                        if (tileRandom.WallType == WallSpriteType.MidH)
                        {
                            AddDoorToRoom(room, directionNeighbourTile, tileRandom, DoorSpriteOrientation.Horizontal);
                        }
                        if (tileRandom.WallType == WallSpriteType.MidV)
                        {
                            AddDoorToRoom(room, directionNeighbourTile, tileRandom, DoorSpriteOrientation.Vertical);
                        }
                        roomsToDo.Remove(tileRandom.RoomNeighbour);
                    }
                }
            }
        }
        // Return number of doors required for overlapping room
        private int DoorsRequired(Room room)
        {
            List<Room> roomsCounted = new List<Room>
            {
                room
            };
            int doorsRequired = 0;
            foreach (Tile tile in room.OverlapTiles)
            {
                if (!roomsCounted.Contains(tile.RoomNeighbour) && tile.WallType == WallSpriteType.MidV | tile.WallType == WallSpriteType.MidH)
                {
                    roomsCounted.Add(tile.RoomNeighbour);
                    doorsRequired++;
                }
            }
            return doorsRequired;
        }

        /****************************************
        * Make isolated rooms' door sub-routine *
        *****************************************/
        private void MakeIsolatedRoomDoors()
        {
            foreach (Room room in area.Rooms)
            {
                List<Tile> tilesToConsider = room.WallTiles();
                bool doorPlaced = false;

                while (doorPlaced == false)
                {
                    if (tilesToConsider.Count == 0)
                    {
                        Console.WriteLine($"Abandonend place door for room at {room.PositionFloor}, no more wall  tiles left!");
                        break;
                    }
                    else
                    {
                        Tile tileForDoor = tilesToConsider[random.Next(0, tilesToConsider.Count)];

                        if (tileForDoor.WallType == WallSpriteType.MidV)
                        {
                            if (Helper.ValidWallPositionForInteractable(area, tileForDoor))
                            {
                                Direction directionNeighbourTile = Direction.Right;
                                if (tileForDoor.FloorCategory != tileForDoor.Neighbour(Direction.Right, 1).FloorCategory)
                                {
                                    directionNeighbourTile = Direction.Left;
                                }
                                AddDoorToRoom(tileForDoor.Room, directionNeighbourTile, tileForDoor, DoorSpriteOrientation.Vertical);
                                doorPlaced = true;
                            }
                        }
                        if (tileForDoor.WallType == WallSpriteType.MidH)
                        {
                            if(Helper.ValidWallPositionForInteractable(area, tileForDoor))
                            {
                                Direction directionNeighbourTile = Direction.Up;
                                if (tileForDoor.FloorCategory == tileForDoor.Neighbour(Direction.Up, 1).FloorCategory)
                                {
                                    directionNeighbourTile = Direction.Down;
                                }
                                AddDoorToRoom(tileForDoor.Room, directionNeighbourTile, tileForDoor, DoorSpriteOrientation.Horizontal);
                                doorPlaced = true;
                            }
                        }
                        tilesToConsider.Remove(tileForDoor);
                    }
                }
            }
        }
        // Return whether tile overlaps any room in area
        private bool TileOverlapRooms(Tile tile)
        {
            foreach (Room room in area.Rooms)
            {
                if (room.OverlapTiles.Contains(tile))
                {
                    return true;
                }
            }
            return false;
        }

        //Return if Neighbouring tile in Direction exists(e.g.not off-area)
        private bool NeighbourTileValid(Tile tile, Direction direction)
        {
            bool isValid = false;
            switch (direction)
            {
                case Direction.Up:
                    if (tile.PositionGrid.Y > 1)
                    {
                        isValid = true;
                    }
                    break;
                case Direction.Right:
                    if (tile.PositionGrid.X < area.Grid.GetLength(0) - 1)
                    {
                        isValid = true;
                    }
                    break;
                case Direction.Down:
                    if (tile.PositionGrid.Y < area.Grid.GetLength(1) - 1)
                    {
                        isValid = true;
                    }
                    break;
                case Direction.Left:
                    if (tile.PositionGrid.X > 1)
                    {
                        isValid = true;
                    }
                    break;
            }
            return isValid;
        }

        private void AddDoorToRoom(Room room, Direction directionNeighbourTile, Tile tile, DoorSpriteOrientation spriteType)
        {
            Door doorNew = new Door(tile, directionNeighbourTile)
            {
                DoorSpriteOrientation = spriteType
            };
            // Set door floor tile to room's
            doorNew.Tile.FloorCategory = room.CategoryFloor;

            // Add door to room
            doorNew.Rooms.Add(room);
            room.Doors.Add(doorNew);
            // Add door to overlapping room
            if (tile.RoomNeighbour != null)
            {
                doorNew.Rooms.Add(tile.RoomNeighbour);
                tile.RoomNeighbour.Doors.Add(doorNew);
            }
        }
    }
}