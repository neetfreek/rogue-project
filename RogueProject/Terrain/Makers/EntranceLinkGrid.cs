﻿/********************************************************
* Sets tiles' sprite types around outdoor outdoor       *
*   entrances to create pathway through trees heading   *
*   off map for outdoor connected areas                 *
*********************************************************/
using RogueProject.Textures;
using RogueProject.Common;

namespace RogueProject.Terrain
{
    public static class EntranceTrunkGrid
    {
        public static void TopOutdoorEntranceGrid(Tile tile)
        {
            Tile[,] entranceGrid = new Tile[3, 3];
            Tile origin = tile.Neighbour(Direction.Up, 2).Neighbour(Direction.Left, 1);
            // Top row
            origin.FloorType = FloorSpriteType.TopR;
            origin.TreeType = TreeSpriteType.TopR;
            origin.Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorVM;
            origin.Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.TopL;
            origin.Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.TopL;
            // Middle row
            origin.Neighbour(Direction.Down, 1).FloorType = FloorSpriteType.MidR;
            origin.Neighbour(Direction.Down, 1).TreeType = TreeSpriteType.MidR;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorVM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidL;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.MidL;
            // Bottom row
            origin.Neighbour(Direction.Down, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 2).TreeType = TreeSpriteType.BotR;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.BotL;
        }
        public static void RightOutdoorEntranceGrid(Tile tile)
        {
            Tile[,] entranceGrid = new Tile[3, 3];
            Tile origin = tile.Neighbour(Direction.Up, 1);
            // Top row
            origin.FloorType = FloorSpriteType.MidM;
            origin.TreeType = TreeSpriteType.BotL;
            origin.Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.BotM;
            origin.Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.BotM;
            origin.Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.BotR;
            origin.Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.BotR;
            // Middle row
            origin.Neighbour(Direction.Down, 1).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorHM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.CorHM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.None;
            // Bottom row
            origin.Neighbour(Direction.Down, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 2).TreeType = TreeSpriteType.TopL;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.TopM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.TopM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.TopR;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.TopR;
        }
        public static void BottomOutdoorEntranceGrid(Tile tile)
        {
            Tile[,] entranceGrid = new Tile[3, 3];
            Tile origin = tile.Neighbour(Direction.Left, 1);
            // Top row
            origin.FloorType = FloorSpriteType.MidM;
            origin.TreeType = TreeSpriteType.TopR;
            origin.Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.TopL;
            // Middle row
            origin.Neighbour(Direction.Down, 1).FloorType = FloorSpriteType.MidR;
            origin.Neighbour(Direction.Down, 1).TreeType = TreeSpriteType.MidR;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorVM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidL;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.MidL;
            // Bottom row
            origin.Neighbour(Direction.Down, 2).FloorType = FloorSpriteType.BotR;
            origin.Neighbour(Direction.Down, 2).TreeType = TreeSpriteType.BotR;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorVM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.BotL;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.BotL;
        }
        public static void LeftOutdoorEntranceGrid(Tile tile)
        {
            Tile[,] entranceGrid = new Tile[3, 3];
            Tile origin = tile.Neighbour(Direction.Up, 1).Neighbour(Direction.Left, 2);
            // Top row
            origin.FloorType = FloorSpriteType.BotL;
            origin.TreeType = TreeSpriteType.BotL;
            origin.Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.BotM;
            origin.Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.BotM;
            origin.Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.BotR;
            // Middle row
            origin.Neighbour(Direction.Down, 1).FloorType = FloorSpriteType.CorHM;
            origin.Neighbour(Direction.Down, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.CorHM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.None;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.CorHM;
            origin.Neighbour(Direction.Down, 1).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.None;
            // Bottom row
            origin.Neighbour(Direction.Down, 2).FloorType = FloorSpriteType.TopL;
            origin.Neighbour(Direction.Down, 2).TreeType = TreeSpriteType.TopL;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).FloorType = FloorSpriteType.TopM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 1).TreeType = TreeSpriteType.TopM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).FloorType = FloorSpriteType.MidM;
            origin.Neighbour(Direction.Down, 2).Neighbour(Direction.Right, 2).TreeType = TreeSpriteType.TopR;
        }
    }
}