﻿/************************************************************************
* Contains prefabericated sets of tree, floor, floor rooms, and wall    *
*   sprite combinations for making regions.                             *    
* HandleSetAreaRegion() handles entire routine, calling                 *
*   SetIndoor/OutdoorRegion and then PointToRegion()                    *             
* SetIndoorRegion() looks for previous Indoor area, if find, call       *
*   SetRegionAsPrior(), else call RandomIndoorRegion()                  *
* SetOutdoorRegion() finds previous Outdoor areas with lower            *
*   AreaIndex public field then calls SetOutdoorRegion(). If first      *
*   area, calls RandomOutdoorRegion()                                   *
* SetRegionAsPrior() rolls against previous area's                      *
*   ChanceNextAreaSameRegion; if within range assign same region,       *
*   otherwise call RandomIndoor/OutdoorRegion()                         *
* PointToRegion() determines which Set[RegionType]  method to call for  *
*   the area                                                            *
* Set[RegionType] methods get region variables and send as parameters   *
*   to area.area.SetOutdoorAreaRegion(). Sends random door type using   *
*   RandomDoorCategory()                                                *
*************************************************************************/
using System;
using System.Collections.Generic;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public static class RegionMaker
    {
        private static Random random = new Random();


        /****************************************************
        * Methods determine which region to assign to area, *
        *   assign RegionType to area                       *
        *****************************************************/
        public static void HandleSetAreaRegion(Area area)
        {
            switch (area.AreaType)
            {
                case AreaType.Indoor:
                    SetIndoorRegion(area);
                    PointToRegion(area);
                    break;
                case AreaType.Outdoor:
                    SetOutdoorRegion(area);
                    PointToRegion(area);
                    break;
            }
        }

        private static void SetIndoorRegion(Area area)
        {
            int areaStateIndex = AreaStatesList.AreaStates.IndexOf(area.areaState); 
            int chanceUsePriorRegion = 0;

            // Increment back through areas
            while (areaStateIndex > 0)
            {
                areaStateIndex--;
                AreaState previousAreaState = AreaStatesList.AreaStates[areaStateIndex];
                // If proior area indoor, call SetRegionAsPrior to see if apply same region
                if (previousAreaState.AreaType == AreaType.Indoor &&
                    previousAreaState.AreaIndex < area.AreaIndex)
                {
                    RegionType previousRegion = previousAreaState.RegionType;
                    chanceUsePriorRegion = previousAreaState.ChanceNextAreaSameRegion;
                    SetRegionAsPrior(area, previousRegion, chanceUsePriorRegion);
                    break;
                }
                // If prior area outdoor (first region area) call RandomIndoorRegion to set random region
                else
                {
                    area.RegionType = RandomIndoorRegion();
                    area.ChanceNextAreaSameRegion = TerrainVariables.CHANCE_NEXT_AREA_REGION_SAME;
                    break;
                }                
            }
        }

        private static void SetOutdoorRegion(Area area)
        {
            int areaStateIndex = AreaStatesList.AreaStates.IndexOf(area.areaState);
            int chanceUsePriorRegion = 0;

            // Random region for first area
            if (areaStateIndex == 0)
            {
                area.RegionType = RandomOutdoorRegion();
                area.ChanceNextAreaSameRegion = TerrainVariables.CHANCE_NEXT_AREA_REGION_SAME;
            }
            // Else find closest-index previous outdoor area
            else
            {
                while (areaStateIndex > 0)
                {
                    areaStateIndex--;
                    AreaState previousAreaState = AreaStatesList.AreaStates[areaStateIndex];

                    if (AreaStatesList.AreaStates[areaStateIndex].AreaType == AreaType.Outdoor)
                    {                    
                        RegionType previousRegion = previousAreaState.RegionType;
                        chanceUsePriorRegion = previousAreaState.ChanceNextAreaSameRegion;

                        SetRegionAsPrior(area, previousRegion, chanceUsePriorRegion);
                        break;
                    }
                }
            }
        }

        private static void SetRegionAsPrior(Area area, RegionType previousRegion, int chanceUsePriorRegion)
        {
            int chanceSameRegion = chanceUsePriorRegion;
            int sameRegionRoll = random.Next(0, TerrainVariables.CHANCE_NEXT_AREA_REGION_SAME);
            // If roll within chanceSameRegion, set same region, decrement chance for next area
            if (sameRegionRoll <= chanceSameRegion)
            {
                area.RegionType = previousRegion;
                area.ChanceNextAreaSameRegion = chanceSameRegion - TerrainVariables.CHANCE_NEXT_AREA_DECREMENTOR;
            }
            // Else assign new region, assign chance for next as defualt
            else
            {
                if (area.AreaType == AreaType.Indoor)
                {
                    area.RegionType = RandomIndoorRegion();
                }
                else if (area.AreaType == AreaType.Outdoor)
                {
                    area.RegionType = RandomOutdoorRegion();
                }
                area.ChanceNextAreaSameRegion = TerrainVariables.CHANCE_NEXT_AREA_REGION_SAME;
            }
        }


        /************************************************
        * Methods assign set relevant region-specific   *
        *   variables, use as paramenters in call to    *
        *   area.SetIndoor/OutdoorRegion()              *
        *************************************************/
        // Call Set[RegionType] method corresponding to area.RegionType
        private static void PointToRegion(Area area)
        {
            switch (area.RegionType)
            {
                // Indoor regions
                case RegionType.DungeonGrey:
                    SetDungeonGrey(area);
                    break;
                case RegionType.DungeonBlue:
                    SetDungeonBlue(area);
                    break;
                case RegionType.DungeonDark:
                    SetDungeonDark(area);
                    break;
                case RegionType.DungeonSnowLight:
                    SetDungeonSnowLight(area);
                    break;
                case RegionType.DungeonSnowDark:
                    SetDungeonSnowDark(area);
                    break;
                case RegionType.DungeonMetalOrange:
                    SetDungeonMetalOrange(area);
                    break;
                case RegionType.DungeonMetalDark:
                    SetDungeonMetalDark(area);
                    break;

                case RegionType.CavesLight:
                    SetCavesLight(area);
                    break;
                case RegionType.CavesMedium:
                    SetCavesMedium(area);
                    break;
                case RegionType.CavesDark:
                    SetCavesDark(area);
                    break;
                case RegionType.CavesIce:
                    SetCavesIce(area);
                    break;

                case RegionType.IndustrialLight:
                    SetIndustrialLight(area);
                    break;
                case RegionType.IndustrialDark:
                    SetIndustrialDark(area);
                    break;

                case RegionType.IceTunnelsLight:
                    SetIceTunnelsLight(area);
                    break;
                case RegionType.IceTunnelsDark:
                    SetIceTunnelsDark(area);
                    break;

                // Outdoor regions
                case RegionType.ForestDark:
                    SetForestDark(area);
                    break;
                case RegionType.ForestLight:
                    SetForestLight(area);
                    break;
                case RegionType.ForestSnowDark:
                    SetForestSnowDark(area);
                    break;
                case RegionType.ForestSnowLight:
                    SetForestSnowLight(area);
                    break;

                case RegionType.WoodsLight:
                    SetWoodsDark(area);
                    break;
                case RegionType.WoodsDark:
                    SetWoodsDark(area);
                    break;
                case RegionType.WoodsSnowDark:
                    SetWoodsSnowDark(area);
                    break;
                case RegionType.WoodsSnowLight:
                    SetWoodsSnowLight(area);
                    break;

                case RegionType.Tropic:
                    SetTropic(area);
                    break;

                case RegionType.JungleLight:
                    SetJungleLight(area);
                    break;
                case RegionType.JungleDark:
                    SetJungleDark(area);
                    break;

                case RegionType.Oasis:
                    SetOasis(area);
                    break;

                case RegionType.DesertLight:
                    SetDesertLight(area);
                    break;
                case RegionType.DesertMedium:
                    SetDesertMedium(area);
                    break;
                case RegionType.DesertDark:
                    SetDesertDark(area);
                    break;
            }
        }

        // Get required region varaibles, send to area.SetOutdoorAreaRegion to setup region variables
        private static void SetDungeonGrey(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonGrey.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonGrey[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonGrey;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonGrey.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonGrey[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonBlue(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonBlue.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonBlue[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonBlue;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonBlue.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonBlue[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonSnowLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonSnowLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonSnowLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonSnowLight;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonSnowLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonSnowLight[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonSnowDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonSnowDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonSnowDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonSnowDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonSnowDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonSnowDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonMetalOrange(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonMetalOrange.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonMetalOrange[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonMetalOrange;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonMetalOrange.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonMetalOrange[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetDungeonMetalDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDungeonMetalDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDungeonMetalDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDungeonMetalDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsDungeonMetalDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDungeonMetalDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }

        private static void SetCavesLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsCavesLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsCavesLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsCavesLight;
            numberSelection = random.Next(0, TileCategoryLists.WallsCavesLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsCavesLight[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetCavesMedium(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsCavesMedium.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsCavesMedium[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsCavesMedium;
            numberSelection = random.Next(0, TileCategoryLists.WallsCavesMedium.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsCavesMedium[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetCavesDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsCavesDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsCavesDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsCavesDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsCavesDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsCavesDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetCavesIce(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsCavesIce.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsCavesIce[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsCavesIce;
            numberSelection = random.Next(0, TileCategoryLists.WallsCavesIce.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsCavesIce[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }

        private static void SetIndustrialLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsIndustrialLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsIndustrialLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsIndustrialLight;
            numberSelection = random.Next(0, TileCategoryLists.WallsIndustrialLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsIndustrialLight[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetIndustrialDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsIndustrialDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsIndustrialDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsIndustrialDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsIndustrialDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsIndustrialDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }

        private static void SetIceTunnelsLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsIceTunnelsLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsIceTunnelsLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsIceTunnelsLight;
            numberSelection = random.Next(0, TileCategoryLists.WallsIceTunnelsLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsIceTunnelsLight[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }
        private static void SetIceTunnelsDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsIceTunnelsDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsIceTunnelsDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsIceTunnelsDark;
            numberSelection = random.Next(0, TileCategoryLists.WallsIceTunnelsDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsIceTunnelsDark[numberSelection];
            area.SetIndoorAreaRegion(categoryFloor, categoryFloorRooms, categoryWalls, RandomDoorCategory());
        }

        private static void SetForestDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsForestDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsForestDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsForestDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesForestDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesForestDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsForestDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsForestDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetForestLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsForestLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsForestLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsForestLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesForestLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesForestLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsForestLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsForestLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetForestSnowDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsForestSnowDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsForestSnowDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsForestSnowDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesForestSnowDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesForestSnowDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsForestSnowDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsForestSnowDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetForestSnowLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsForestSnowLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsForestSnowLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsForestSnowLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesForestSnowLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesForestSnowLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsForestSnowLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsForestSnowLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }

        private static void SetWoodsDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsWoodsDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsWoodsDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsWoodsDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesWoodsDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesWoodsDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsWoodsDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsWoodsDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetWoodsLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsWoodsLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsWoodsLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsWoodsLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesWoodsLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesWoodsLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsWoodsLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsWoodsLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetWoodsSnowDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsWoodsSnowDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsWoodsSnowDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsWoodsSnowDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesWoodsSnowDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesWoodsSnowDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsWoodsSnowDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsWoodsSnowDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetWoodsSnowLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsWoodsSnowLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsWoodsSnowLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsWoodsSnowLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesWoodsSnowLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesWoodsSnowLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsWoodsSnowLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsWoodsSnowLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }

        private static void SetTropic(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsTropic.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsTropic[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsTropic;
            numberSelection = random.Next(0, TileCategoryLists.TreesTropic.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesTropic[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsTropic.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsTropic[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }

        private static void SetJungleLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsJungleLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsJungleLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsJungleLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesJungleLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesJungleLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsJungleLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsJungleLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetJungleDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsJungleDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsJungleDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsJungleDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesJungleDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesJungleDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsJungleDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsJungleDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }

        private static void SetOasis(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsOasis.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsOasis[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsOasis;
            numberSelection = random.Next(0, TileCategoryLists.TreesOasis.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesOasis[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsOasis.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsOasis[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }

        private static void SetDesertLight(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDesertLight.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDesertLight[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDesertLight;
            numberSelection = random.Next(0, TileCategoryLists.TreesDesertLight.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesDesertLight[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsDesertLight.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDesertLight[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetDesertMedium(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDesertMedium.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDesertMedium[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDesertMedium;
            numberSelection = random.Next(0, TileCategoryLists.TreesDesertMedium.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesDesertMedium[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsDesertMedium.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDesertMedium[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }
        private static void SetDesertDark(Area area)
        {
            int numberSelection = random.Next(0, TileCategoryLists.FloorsDesertDark.Count);
            FloorSpriteCategory categoryFloor = TileCategoryLists.FloorsDesertDark[numberSelection];
            List<FloorSpriteCategory> categoryFloorRooms = TileCategoryLists.FloorsRoomsDesertDark;
            numberSelection = random.Next(0, TileCategoryLists.TreesDesertDark.Count);
            TreeSpriteCategory categoryTrees = TileCategoryLists.TreesDesertDark[numberSelection];
            numberSelection = random.Next(0, TileCategoryLists.WallsDesertDark.Count);
            WallSpriteCategory categoryWalls = TileCategoryLists.WallsDesertDark[numberSelection];
            area.SetOutdoorAreaRegion(categoryFloor, categoryFloorRooms, categoryTrees, categoryWalls, RandomDoorCategory());
        }


        // Return randoms
        private static RegionType RandomIndoorRegion()
        {
            Random random = new Random();

            int enumLength = Enum.GetNames(typeof(RegionType)).Length;
            int numberSelection = random.Next(16, 31); // Include all indoor RegionTypes
            RegionType typeArea = (RegionType)Enum.ToObject(typeof(RegionType), numberSelection);

            return typeArea;
        }
        private static RegionType RandomOutdoorRegion()
        {
            Random random = new Random();

            int enumLength = Enum.GetNames(typeof(RegionType)).Length;
            int numberSelection = random.Next(1, 16); // Include all outdoor RegionTypes
            RegionType typeArea = (RegionType)Enum.ToObject(typeof(RegionType), numberSelection);

            return typeArea;
        }
        private static DoorSpriteCategory RandomDoorCategory()
        {
            int numberSelection = random.Next(1, 3); // Default doors are DoorWood, DoorMetal
            DoorSpriteCategory categoryDoor = (DoorSpriteCategory)Enum.ToObject(typeof(DoorSpriteCategory), numberSelection);

            return categoryDoor;
        }
    }
}