﻿/********************************************************
* Return sprites for tiles as needed in GridMaker.cs    * 
*********************************************************/
using Microsoft.Xna.Framework;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public static class GridSpriteTypes
    {

        // Set sprite type for outer border tile groups like area border outer edge, room floor outer edge
        public static FloorSpriteType OuterBorderFloorTypeByPosition(int counterCols, int counterRows, Vector2 sizeGrid)
        {
            FloorSpriteType spriteType = FloorSpriteType.MidM;

            // Vertical corridor or single tile
            if (sizeGrid.X == 1)
            {
                // Single tile
                if (sizeGrid.Y == 1)
                {
                    spriteType = FloorSpriteType.Single;
                }
                else
                {
                    if (counterRows == 0)
                    {
                        spriteType = FloorSpriteType.CorVT;
                    }
                    else if (counterRows == sizeGrid.Y - 1)
                    {
                        spriteType = FloorSpriteType.CorVB;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.CorVM;
                    }
                }
            }
            // Horizontal corridor
            else if (sizeGrid.Y == 1)
            {
                if (counterCols == 0)
                {
                    spriteType = FloorSpriteType.CorHL;
                }
                else if (counterCols == sizeGrid.X - 1)
                {
                    spriteType = FloorSpriteType.CorHR;
                }
                else
                {
                    spriteType = FloorSpriteType.CorHM;
                }
            }
            // Room
            else
            {
                // Top row and corners
                if (counterRows == 0)
                {
                    if (counterCols == 0)
                    {
                        spriteType = FloorSpriteType.TopL;
                    }
                    else if (counterCols == sizeGrid.X - 1)
                    {
                        spriteType = FloorSpriteType.TopR;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.TopM;
                    }
                }
                // Bottom row and corners
                else if (counterRows == sizeGrid.Y - 1)
                {
                    if (counterCols == 0)
                    {
                        spriteType = FloorSpriteType.BotL;
                    }
                    else if (counterCols == sizeGrid.X - 1)
                    {
                        spriteType = FloorSpriteType.BotR;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.BotM;
                    }
                }
                // Left column
                else if (counterCols == 0)
                {
                    spriteType = FloorSpriteType.MidL;
                }
                // Right column
                else if (counterCols == sizeGrid.X - 1)
                {
                    spriteType = FloorSpriteType.MidR;
                }
            }

            return spriteType;
        }
        // Set sprite type for inner border tile groups like area border inner edge
        public static FloorSpriteType InnerBorderFloorTypeByPosition(int counterCols, int counterRows, Vector2 sizeGrid, int sizeBorder)
        {
            int sizeBorderAdjust = sizeBorder - 1; // adjusted for 0-index
            FloorSpriteType spriteType = FloorSpriteType.MidM;

            // Vertical corridor or single tile
            if (sizeGrid.X == 1)
            {
                // Single tile
                if (sizeGrid.Y == 1)
                {
                    spriteType = FloorSpriteType.Single;
                }
                else
                {
                    if (counterRows == 0)
                    {
                        spriteType = FloorSpriteType.CorVT;
                    }
                    else if (counterRows == sizeGrid.Y - 1)
                    {
                        spriteType = FloorSpriteType.CorVB;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.CorVM;
                    }
                }
            }
            // Horizontal corridor
            else if (sizeGrid.Y == 1)
            {
                if (counterCols == 0)
                {
                    spriteType = FloorSpriteType.CorHL;
                }
                else if (counterCols == sizeGrid.X - 1)
                {
                    spriteType = FloorSpriteType.CorHR;
                }
                else
                {
                    spriteType = FloorSpriteType.CorHM;
                }
            }
            // Room
            else
            {
                // Top row and corners
                if (counterRows == sizeBorderAdjust)
                {
                    if (counterCols == sizeBorderAdjust)
                    {
                        spriteType = FloorSpriteType.BotM;
                    }
                    else if (counterCols == sizeGrid.X - (sizeBorderAdjust + 1))
                    {
                        spriteType = FloorSpriteType.MidM;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.MidM;
                    }
                }
                // Bottom row and corners
                else if (counterRows == sizeGrid.Y - (sizeBorderAdjust + 1))
                {
                    if (counterCols == 0)
                    {
                        spriteType = FloorSpriteType.BotL;
                    }
                    else if (counterCols == sizeGrid.X - (sizeBorderAdjust + 1))
                    {
                        spriteType = FloorSpriteType.BotR;
                    }
                    else
                    {
                        spriteType = FloorSpriteType.BotM;
                    }
                }
                // Left column
                else if (counterCols == 0)
                {
                    spriteType = FloorSpriteType.MidL;
                }
                // Right column
                else if (counterCols == sizeGrid.X - (sizeBorderAdjust + 1))
                {
                    spriteType = FloorSpriteType.MidR;
                }
            }

            return spriteType;
        }

        public static TreeSpriteType InnerBorderTreeTypeByPosition(int counterCols, int counterRows, Vector2 sizeContainer)
        {
            TreeSpriteType spriteType = TreeSpriteType.None;

            if (counterCols == 2) // Left column, corners
            {
                if (counterRows == 2) // Top corner
                {
                    spriteType = TreeSpriteType.InnerBR; // innerBR
                }
                else if (counterRows == sizeContainer.Y - 3) // Bottom corner
                {
                    spriteType = TreeSpriteType.InnerTR; // innerTR
                }
                else
                {
                    spriteType = TreeSpriteType.MidR; // Outer edge
                }
            }
            else if (counterCols == sizeContainer.X - 3) // Right column, corners
            {
                if (counterRows == 2) // Top corner
                {
                    spriteType = TreeSpriteType.InnerBL; // innerBL
                }
                else if (counterRows == sizeContainer.Y - 3) // Bottom corner
                {
                    spriteType = TreeSpriteType.InnerTL; //innerTL
                }
                else
                {
                    spriteType = TreeSpriteType.MidL;
                }
            }
            else if (counterRows == 2 && counterCols != 2 && counterCols != sizeContainer.X - 3) // Top row
            {
                spriteType = TreeSpriteType.BotM;
            }
            else if (counterRows == sizeContainer.Y - 3 && counterCols != 2 && counterCols != sizeContainer.X - 3) // Bottom row
            {
                spriteType = TreeSpriteType.TopM;
            }

            return spriteType;
        }
        public static TreeSpriteType OuterBorderTypesByPosition(int counterCols, int counterRows, Vector2 sizeContainer)
        {
            TreeSpriteType spriteType = TreeSpriteType.None;

            // Top-most row and corners
            if (counterRows == 0)
            {
                if (counterCols == 0)
                {
                    spriteType = TreeSpriteType.TopL;
                }
                else if (counterCols == sizeContainer.X - 1)
                {
                    spriteType = TreeSpriteType.TopR;

                }
                else
                {
                    spriteType = TreeSpriteType.TopM;
                }
            }
            // Bottom-most row and corners
            else if (counterRows == sizeContainer.Y - 1)
            {
                if (counterCols == 0)
                {
                    spriteType = TreeSpriteType.BotL;
                }
                else if (counterCols == sizeContainer.X - 1)
                {
                    spriteType = TreeSpriteType.BotR;
                }
                else
                {
                    spriteType = TreeSpriteType.BotM;
                }
            }
            // Left-most column
            else if (counterCols == 0)
            {
                spriteType = TreeSpriteType.MidL;
            }
            // Right-most column
            else if (counterCols == sizeContainer.X - 1)
            {
                spriteType = TreeSpriteType.MidR;
            }

            return spriteType;
        }

        public static WallSpriteType WallTypeByPosition(int counterCols, int counterRows, Vector2 sizeContainer)
        {
            WallSpriteType spriteType = WallSpriteType.None;

            // Top row and corners
            if (counterRows == 0)
            {
                if (counterCols == 0)
                {
                    spriteType = WallSpriteType.TopL;
                }
                else if (counterCols == sizeContainer.X - 1)
                {
                    spriteType = WallSpriteType.TopR;

                }
                else
                {
                    spriteType = WallSpriteType.MidH;
                }
            }
            // Bottom row and corners
            else if (counterRows == sizeContainer.Y - 1)
            {
                if (counterCols == 0)
                {
                    spriteType = WallSpriteType.BotL;
                }
                else if (counterCols == sizeContainer.X - 1)
                {
                    spriteType = WallSpriteType.BotR;
                }
                else
                {
                    spriteType = WallSpriteType.MidH;
                }
            }
            // Left column
            else if (counterCols == 0)
            {
                spriteType = WallSpriteType.MidV;
            }
            // Right column
            else if (counterCols == sizeContainer.X - 1)
            {
                spriteType = WallSpriteType.MidV;
            }

            return spriteType;
        }
        public static WallSpriteType WallTypeOverlap(Tile tile, WallSpriteType wallType)
        {
            WallSpriteType typeNew = wallType;

            switch (tile.WallType)
            {
                case WallSpriteType.MidH:
                    if (typeNew == WallSpriteType.TopL || typeNew == WallSpriteType.TopR)
                    {
                        typeNew = WallSpriteType.OverlapTop;
                    }
                    if (typeNew == WallSpriteType.BotL || typeNew == WallSpriteType.BotR)
                    {
                        typeNew = WallSpriteType.OverlapBot;
                    }
                    break;
                case WallSpriteType.MidV:
                    if (typeNew == WallSpriteType.TopL || typeNew == WallSpriteType.BotL)
                    {
                        typeNew = WallSpriteType.OverlapLeft;
                    }
                    if (typeNew == WallSpriteType.TopR || typeNew == WallSpriteType.BotR)
                    {
                        typeNew = WallSpriteType.OverlapRight;
                    }
                    break;
                case WallSpriteType.TopL:
                    if (typeNew == WallSpriteType.TopR || typeNew == WallSpriteType.MidH)
                    {
                        typeNew = WallSpriteType.OverlapTop;
                    }
                    if (typeNew == WallSpriteType.BotL || typeNew == WallSpriteType.BotR ||
                        typeNew == WallSpriteType.MidV)
                    {
                        typeNew = WallSpriteType.OverlapLeft;
                    }
                    break;
                case WallSpriteType.TopR:
                    if (typeNew == WallSpriteType.TopL || typeNew == WallSpriteType.MidH)
                    {
                        typeNew = WallSpriteType.OverlapTop;
                    }
                    if (typeNew == WallSpriteType.BotL || typeNew == WallSpriteType.BotR ||
                        typeNew == WallSpriteType.MidV)
                    {
                        typeNew = WallSpriteType.OverlapRight;
                    }
                    break;
                case WallSpriteType.BotL:
                    if (typeNew == WallSpriteType.BotR || typeNew == WallSpriteType.MidH)
                    {
                        typeNew = WallSpriteType.OverlapBot;
                    }
                    if (typeNew == WallSpriteType.TopL || typeNew == WallSpriteType.TopR ||
                        typeNew == WallSpriteType.MidV)
                    {
                        typeNew = WallSpriteType.OverlapLeft;
                    }
                    break;
                case WallSpriteType.BotR:
                    if (typeNew == WallSpriteType.BotL || typeNew == WallSpriteType.MidH)
                    {
                        typeNew = WallSpriteType.OverlapBot;
                    }
                    if (typeNew == WallSpriteType.TopL || typeNew == WallSpriteType.TopR ||
                        typeNew == WallSpriteType.MidV)
                    {
                        typeNew = WallSpriteType.OverlapRight;
                    }
                    break;
            }

            return typeNew;
        }
    }
}