﻿/************************************************************
* Contains list of AreaState objects, each area instantiates *
*   its own object.                                         *
* Save and Load calls use the index value of this list to   *
*   access the correct area.                                *
*************************************************************/
using System.Collections.Generic;

namespace RogueProject
{
    public class AreaStatesList
    {
        public static List<AreaState> AreaStates = new List<AreaState>();
    }
}