﻿/************************************************************
* Contains properties of Area for use in saving area data   *
*   to save file, and other operations related to setting or*
*   getting area properties                                 *
*************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Enemies;
using RogueProject.Objects;
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject
{
    public class AreaState
    {
        public int AreaIndex;
        public int ChanceNextAreaSameRegion;
        public Area Area;
        public List<Entrance> Entrances = new List<Entrance>();
        public List<EnemyType> EnemyTypes = new List<EnemyType>();
        public Vector2 Size;
        public Vector2 RoomsWidth;
        public Vector2 RoomsHeight;
        public AreaType AreaType;
        public RegionType RegionType;
        public TreeSpriteCategory TreeSpriteCategory;
        public WallSpriteCategory WallSpriteCategory;
        public FloorSpriteCategory FloorSpriteCategory;
        public List<FloorSpriteCategory> TilesetRoomFloorActual = new List<FloorSpriteCategory>();
        public DoorSpriteCategory DoorSpriteCategory;
        public bool Initialised;

        private readonly Game1 game;

        public AreaState(Game1 game)
        {
            this.game = game;
        }

        // Area object states
        private List<Door> openedDoors = new List<Door>(); // Tracks doors opened while in area

        public Tile savedPlayerTile;
       
        // Object states, called by PlayerAction.HandleTileInteract(); tracks states until new area
        public void UpdateObjectState(Door door)
        {
            openedDoors.Add(door);
        }
    }
}