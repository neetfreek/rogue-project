﻿using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Enemies;
using RogueProject.Items;
using RogueProject.Objects;
using RogueProject.Textures;

namespace RogueProject.Terrain
{
    public class Tile
    {
        public TileType TileType;
        public Characters character;
        public EntranceSprite EntranceSprite;
        public FloorSpriteCategory FloorCategory;
        public FloorSpriteType FloorType;
        public TreeSpriteCategory TreeCategory;
        public TreeSpriteType TreeType;
        public WallSpriteCategory WallCategory;
        public WallSpriteType WallType;
        public DecorationGroundSpriteType DecorationGroundType;
        public DecorationGroundSpriteCategory DecorationGroundCategory;

        public Item Item { get => item; set => item = value; }

        public Rectangle Rectangle { get => new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y);}
        public Vector2 Position;
        public Vector2 PositionGrid;
        public Vector2 Size;

        private Area area;
        public Room Room;
        public Room RoomNeighbour;

        public Door Door = null;
        public Enemy Enemy = null;
        public Entrance Entrance = null;
        private Item item;

        public float Layer;
        public bool Walkable { get => IsWalkable();}


        public Tile(Area area)
        {
            this.area = area;
        }


        /************************
        * Player interactions   * 
        *************************/
        public bool ContainsInteractable()
        {
            if (Door != null)
            {
                if (!Door.Opened)
                {
                    return true;
                }
            }
            if (Entrance != null)
            {
                return true;
            }

            if (Enemy != null)
            {
                return true;
            }

            if (Item != null)
            {
                return true;
            }
            return false;
        }
        public bool MouseOverTile(Vector2 positionMouse)
        {
            if (Rectangle.Contains(positionMouse.X, positionMouse.Y))
            {
                return true;
            }

            return false;
        }


        /****************************
        * Return neighbouring tiles * 
        *****************************/
        // Return Direction to neighbours
        public Tile Neighbour(Direction directionNeighbour, int amount)
        {
            Tile tile = this;

            switch (directionNeighbour)
            {
                case Direction.Up:
                    tile = NeighbourUp(amount);
                    break;
                case Direction.Right:
                    tile = NeighbourRight(amount);
                    break;
                case Direction.Down:
                    tile = NeighbourDown(amount);
                    break;
                case Direction.Left:
                    tile = NeighbourLeft(amount);
                    break;
                case Direction.UpRight:
                    tile = NeighbourUp(amount).NeighbourRight(amount);
                    break;
                case Direction.DownRight:
                    tile = NeighbourDown(amount).NeighbourRight(amount);
                    break;
                case Direction.DownLeft:
                    tile = NeighbourDown(amount).NeighbourLeft(amount);
                    break;
                case Direction.UpLeft:
                    tile = NeighbourUp(amount).NeighbourLeft(amount);
                    break;
            }
            return tile;
        }
        // Return neighbouring tile (distance) away; helpers for Neighbour()
        private Tile NeighbourUp(int distance)
        {
            return area.Grid[(int)PositionGrid.X, (int)PositionGrid.Y - distance];
        }
        private Tile NeighbourRight(int distance)
        {
            return area.Grid[(int)PositionGrid.X + distance, (int)PositionGrid.Y];
        }
        private Tile NeighbourDown(int distance)
        {
            return area.Grid[(int)PositionGrid.X, (int)PositionGrid.Y + distance];
        }
        private Tile NeighbourLeft(int distance)
        {
            return area.Grid[(int)PositionGrid.X - distance, (int)PositionGrid.Y];
        }
        

        /************
        * Helpers   * 
        *************/
        // Return if tile occupied (e.g. to prevent moving to tile)
        private bool IsWalkable()
        {
            if (TileType== TileType.Wall | TileType == TileType.Tree | character != Characters.None | Entrance != null)
            {
                return false;
            }
            if (TileType == TileType.Door)
            {
                if (!Door.Opened)
                {
                    return false;
                }
            }
            return true;
        }
    }
}