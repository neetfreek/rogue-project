﻿/************************************************************
* Contains information about current gameArea, including:   *
* List of rooms                                             *
************************************************************/
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using RogueProject.Textures;
using System;
using RogueProject.Objects;
using RogueProject.Enemies;

namespace RogueProject.Terrain
{
    public class Area
    {
        private readonly Random random = new Random();
        public AreaState areaState;
        public int AreaIndex;
        public int ChanceNextAreaSameRegion; // Between 0-100

        // Contain reference to all rooms
        public List<Room> Rooms = new List<Room>();
        public List<Entrance> Entrances = new List<Entrance>();
        public List<Enemy> Enemies = new List<Enemy>();
        public List<EnemyType> EnemyTypes = new List<EnemyType>();
        public List<FloorSpriteCategory> TilesetRoomFloorOptions = new List<FloorSpriteCategory>();
        public List<FloorSpriteCategory> TilesetRoomFloorActual = new List<FloorSpriteCategory>();
        public Tile[,] Grid;

        public Vector2 PositionArea = new Vector2(0f, 0f);
        public Vector2 SizeArea { get => sizeArea; }

        public RegionType RegionType;
        public AreaType AreaType { get => typeArea; }
        public FloorSpriteCategory FloorCategory { get => categoryFloorArea; }
        public WallSpriteCategory WallCategory { get => categoryWalls; }
        public TreeSpriteCategory TreeCategory { get => categoryTrees; }
        public DoorSpriteCategory DoorCategory { get => categoryDoors; }

        public int WidthRoomMax { get => widthRoomMax; }
        public int WidthRoomMin { get => widthRoomMin; }
        public int HeightRoomMin { get => heightRoomMin; }
        public int HeightRoomMax { get => heightRoomMax; }

        private Vector2 sizeArea;
        private AreaType typeArea;
        private DoorSpriteCategory categoryDoors;
        private FloorSpriteCategory categoryFloorArea;
        private TreeSpriteCategory categoryTrees;
        private WallSpriteCategory categoryWalls;
        private int widthRoomMax;
        private int widthRoomMin;
        private int heightRoomMin;
        private int heightRoomMax;


        /************************
        * Area setup methods    * 
        ************************/
        public void SetupAreaType(AreaType typeArea)
        {
            this.typeArea = typeArea;
        }

        // Initial setup of area region
        public void SetIndoorAreaRegion(FloorSpriteCategory areaFloor, List<FloorSpriteCategory> areaFloorRooms,
            WallSpriteCategory areaWalls, DoorSpriteCategory areaDoors)
        {
            categoryFloorArea = areaFloor;
            categoryDoors = areaDoors;
            categoryWalls = areaWalls;

            TilesetRoomFloorOptions = new List<FloorSpriteCategory>();
            foreach (FloorSpriteCategory floorSpriteCategory in areaFloorRooms)
            {
                TilesetRoomFloorOptions.Add(floorSpriteCategory);
            }
        }
        public void SetOutdoorAreaRegion(FloorSpriteCategory areaFloor, List<FloorSpriteCategory> areaFloorRooms,
            TreeSpriteCategory areaTrees, WallSpriteCategory areaWalls, DoorSpriteCategory areaDoors)
        {
            categoryFloorArea = areaFloor;
            categoryDoors = areaDoors;
            categoryTrees = areaTrees;
            categoryWalls = areaWalls;

            TilesetRoomFloorOptions = new List<FloorSpriteCategory>();
            foreach (FloorSpriteCategory floorSpriteCategory in areaFloorRooms)
            {
                TilesetRoomFloorOptions.Add(floorSpriteCategory);
            }
        }

        // Reloading of area region
        public void SetupDoorCategory(DoorSpriteCategory areaDoors)
        {
            categoryDoors = areaDoors;
        }
        public void SetupFloorCategory(FloorSpriteCategory floorSpriteCategory)
        {
            categoryFloorArea = floorSpriteCategory;
        }
        public void SetupTreeCategory(TreeSpriteCategory treeSpriteCategory)
        {
            categoryTrees = treeSpriteCategory;
        }
        public void SetupWallCategory(WallSpriteCategory areaWalls)
        {
            categoryWalls = areaWalls;
        }

        // Size setup
        public void SetupAreaSize(Vector2 sizeArea)
        {
            this.sizeArea.X = sizeArea.X;
            this.sizeArea.Y = sizeArea.Y;
        }
        public void SetupRoomWidth(Vector2 widthRoom)
        {
            widthRoomMin = (int)widthRoom.X;
            widthRoomMax = (int)widthRoom.Y;
        }
        public void SetupRoomHeight(Vector2 heightRoom)
        {
            heightRoomMin = (int)heightRoom.X;
            heightRoomMax = (int)heightRoom.Y;
        }

        // Save area to areaState
        public void SaveAreaState(AreaState areaStateToSave)
        {
            AreaState areaState = areaStateToSave;
            areaState.ChanceNextAreaSameRegion = ChanceNextAreaSameRegion;
            areaState.AreaIndex = AreaIndex;
            areaState.AreaType = typeArea;
            areaState.RegionType = RegionType;
            areaState.DoorSpriteCategory = categoryDoors;
            areaState.FloorSpriteCategory = categoryFloorArea;
            areaState.TreeSpriteCategory = categoryTrees;
            areaState.WallSpriteCategory = WallCategory;
            areaState.TilesetRoomFloorActual.Clear();
            areaState.TilesetRoomFloorActual = TilesetRoomFloorActual;
            areaState.Size = SizeArea;
            areaState.RoomsWidth = new Vector2(widthRoomMin, widthRoomMax);
            areaState.RoomsHeight = new Vector2(heightRoomMin, heightRoomMax);
            areaState.Entrances = Entrances;
            areaState.EnemyTypes = EnemyTypes;
            areaState.Initialised = true;
        }
    }
}