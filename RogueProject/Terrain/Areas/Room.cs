﻿/************************************************
* Room game object                              *
* References to room objects kept in Area.cs    * 
*************************************************/

using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using RogueProject.Textures;
using RogueProject.Objects;

namespace RogueProject.Terrain
{
    public class Room
    {
        private Area area;

        public FloorSpriteCategory CategoryFloor { get => categoryFloor;}

        public List<Door> Doors = new List<Door>();
        public List<Tile> OverlapTiles = new List<Tile>();
        public List<Room> OverlapRooms = new List<Room>();
        public List<Tile> FloorTiles = new List<Tile>();
        public Vector2 MidPoint { get => midPoint; }
        public Vector2 MidPointArea { get => PositionRoom + MidPoint; }
        public Vector2 PositionRoom { get => positionRoom; set => positionRoom = value; }
        public Vector2 PositionFloor { get => positionFloor; set => positionFloor = value; }
        public Rectangle RectangleBorder { get => RandomRectangleBorder(); }
        public Rectangle RectangleRoom { get => rectangleRoom; }
        public Vector2 SizeRoom { get => sizeRoom;}
        public Vector2 SizeFloor { get => sizeFloor;}

        public int MidPointSort { get => (int)(MidPointArea.X + MidPointArea.Y); }

        private readonly FloorSpriteCategory categoryFloor;
        private Vector2 midPoint;

        private Rectangle rectangleRoom;
        private Vector2 positionRoom;
        private Vector2 positionFloor;
        private Vector2 sizeRoom;
        private Vector2 sizeFloor;

        private Random random = new Random();

        public Room(Area area, Vector2 position, Vector2 size, FloorSpriteCategory categoryFloor)
        {
            this.area = area;
            this.categoryFloor = categoryFloor;
            positionRoom = position;
            PositionFloor = new Vector2(PositionRoom.X + 1, PositionRoom.Y + 1);
            sizeRoom = size;
            sizeFloor = new Vector2(SizeRoom.X - 2, SizeRoom.Y - 2);
            midPoint = new Vector2((int)SizeRoom.X / 2, (int)SizeRoom.Y / 2);
            rectangleRoom = new Rectangle((int)PositionRoom.X, (int)PositionRoom.Y, (int)SizeRoom.X, (int)SizeRoom.Y);
            FloorTiles = SetFloorTiles();
        }

        // Rectangle border determines how much space between this and other
        //  rooms when creating rooms
        private Rectangle RandomRectangleBorder()
        {
            int roomGap = random.Next(-1,4);
            if (roomGap == 0)
            {
                roomGap = 1;
            }

            Rectangle rectangleBorder = new Rectangle((int)PositionRoom.X - roomGap, (int)PositionRoom.Y - roomGap, (int)SizeRoom.X + roomGap * 2, (int)SizeRoom.Y + roomGap * 2);

            return rectangleBorder;
        }

        public List<Tile> WallTiles()
        {
            List<Tile> listTiles = new List<Tile>();

            for (int counterRows = 0; counterRows < SizeRoom.Y; counterRows++)
            {
                for (int counterCols = 0; counterCols < SizeRoom.X; counterCols++)
                {
                    if (counterRows == 0 | counterRows == SizeRoom.Y - 1 |
                        counterCols == 0 | counterCols == SizeRoom.X - 1)
                    {
                        listTiles.Add(area.Grid[(int)PositionRoom.X + counterCols, (int)PositionRoom.Y + counterRows]);
                    }
                }
            }

            return listTiles;
        }

        public List<Tile> SetFloorTiles()
        {
            List<Tile> listTiles = new List<Tile>();

            for (int counterRows = 0; counterRows < SizeRoom.Y; counterRows++)
            {
                for (int counterCols = 0; counterCols < SizeRoom.X; counterCols++)
                {
                    if (counterRows > 0 && counterRows < SizeRoom.Y - 1 &&
                        counterCols > 0 && counterCols < SizeRoom.X - 1)
                    {
                        Tile tile = area.Grid[(int)PositionRoom.X + counterCols, (int)PositionRoom.Y + counterRows];
                        tile.Room = this;
                        listTiles.Add(tile);
                    }
                }
            }

            return listTiles;
        }
    }
}