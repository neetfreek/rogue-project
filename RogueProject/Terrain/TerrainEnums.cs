﻿namespace RogueProject.Terrain
{
    public enum AreaType
    {
        Indoor = 0,
        Outdoor = 1,
    }

    public enum TileType
    {
        Door,
        Floor,
        Tree,
        Wall,
    }

    public enum EntranceType
    {
        Outdoor = 0,
        Indoor = 1,
    }

    public enum RegionType
    {
        None = 0,
        // Outdoor region types
        ForestLight = 1, 
        ForestDark = 2,
        ForestSnowLight = 3,
        ForestSnowDark = 4,
        WoodsLight = 5,
        WoodsDark = 6,
        WoodsSnowLight = 7,
        WoodsSnowDark = 8,
        Tropic = 9,
        JungleLight = 10,
        JungleDark = 11, 
        Oasis = 12,
        DesertLight = 13,
        DesertMedium = 14,
        DesertDark = 15,
        // Indoor region types
        DungeonGrey = 16,
        DungeonBlue = 17,
        DungeonDark = 18,
        DungeonSnowLight = 19,
        DungeonSnowDark = 20,
        DungeonMetalOrange = 21,
        DungeonMetalDark = 22,
        CavesLight = 23,
        CavesMedium = 24,
        CavesDark = 25,
        CavesIce = 26,
        IndustrialLight = 27,
        IndustrialDark = 28,
        IceTunnelsLight = 29,
        IceTunnelsDark = 30,
    }

    public enum ObstacleType
    {
        None = 0,
        Tree = 1,
    }
}