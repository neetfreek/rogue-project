﻿/********************************************
* Contain all references to global enums    * 
********************************************/

namespace RogueProject.Common
{
    public enum Direction
    {
        None, 

        Up,
        Right,
        Down,
        Left,
        UpRight,
        DownRight,
        DownLeft,
        UpLeft,        
    }

    public enum Characters
    {
        None,
        Enemy,
        Player,
    }
}