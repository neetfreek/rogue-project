﻿/************************************************
* Contain all references to global variables    * 
*************************************************/

namespace RogueProject.Common
{
    public static class GlobalVariables
    {
        // Screen dimensions
        public const int WIDTH_SCREEN_VIRTUAL = 1920;
        public const int HEIGHT_SCREEN_VIRTUAL = 1080;
        public static int WidthScreenActual;
        public static int HeightScreenActual;
        // Screen aspect ratios
        public static float AspectActual
        {
            get
            {
                return WidthScreenActual / (float)HeightScreenActual;
            }
        }
        public static float AspectVirtual
        {
            get
            {
                return (WIDTH_SCREEN_VIRTUAL / (float)HEIGHT_SCREEN_VIRTUAL);
            }
        }
        // Sprite dimensions, scale
        public const int WIDTH_HEIGHT_SPRITE = 16;
        public const float SCALE_SPRITE = 4f;
        public static int ScaledSprite
        {
            get
            {
                return (int)(WIDTH_HEIGHT_SPRITE * SCALE_SPRITE);
            }
        }

        // Tiles
        public const float TILES_SCREEN_X = 30f;
        public const float TILES_SCREEN_Y = 17f;

        // Input
        public const int COOLDOWN_INPUT_GAME = 300;
        public const int COOLDOWN_INPUT_MOUSE = 300;
        public const int COOLDOWN_INPUT_GUI = 200;
        public const int COOLDOWN_INPUT = 150;
        public const int COOLDOWN_MOVE = 300;

        // Draw layers
        public const float LAYER_DRAW_DOOR = 0.2f;
        public const float LAYER_DRAW_DECORATIONS = 0.15f;
        public const float LAYER_DRAW_TREE = 0.25f;
        public const float LAYER_DRAW_CHARACTER = 0.35f;
        public const float LAYER_DRAW_ITEM = 0.3f;
        public const float LAYER_DRAW_FLOOR = 0.1f;
        public const float LAYER_DRAW_WALL = 0.15f;
        public const float LAYER_DRAW_CURSOR = 1f;
    }
}