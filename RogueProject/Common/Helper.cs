﻿/****************************************************************
* Helper functionality for common tasks for various game object *
*   tasks.                                                      *
*****************************************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Objects;
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject.Common
{
    public static class Helper
    {
        // Return AreaState from AreaStateList corresponding to given areaIndex
        public static AreaState AreaStateNumber(int areaIndex, Game1 game)
        {
            AreaState areaStateToLoad = new AreaState(game);
            foreach (AreaState areaState in AreaStatesList.AreaStates)
            {
                if (areaState.AreaIndex == areaIndex)
                {
                    areaStateToLoad = areaState;
                    break;
                }
            }
            return areaStateToLoad;
        }

        // Return if specified tile within distanceAllowed away
        public static bool InRange(Tile tileDestination, Tile tileOrigin, int distanceAllowed)
        {
            Vector2 distance = tileDestination.PositionGrid - tileOrigin.PositionGrid;
            Direction direction = DirectionBetween(tileDestination, tileOrigin);

            if (distance.X == 0 && distance.Y == 0)
            {
                return true;
            }

            switch (direction)
            {
                case Direction.Up:
                    if (distance.Y >= -distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.UpRight:
                    if (distance.Y >= -distanceAllowed && distance.X <= distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.Right:
                    if (distance.X <= distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.DownRight:
                    if (distance.Y <= distanceAllowed && distance.X <= distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.Down:
                    if (distance.Y <= distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.DownLeft:
                    if (distance.Y <= distanceAllowed && distance.X >= -distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.Left:
                    if (distance.X >= -distanceAllowed)
                    {
                        return true;
                    }
                    break;
                case Direction.UpLeft:
                    if (distance.Y >= -distanceAllowed && distance.X >= -distanceAllowed)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        // Return direction from tileOrigin to tileDestination
        public static Direction DirectionBetween(Tile tileDestination, Tile tileOrigin)
        {
            Vector2 vectorBetween = tileDestination.PositionGrid - tileOrigin.PositionGrid;
            Direction direction = Direction.None;

            // Vertical
            if (vectorBetween.X == 0)
            {
                if (vectorBetween.Y < 0)
                {
                    direction = Direction.Up;
                }
                if (vectorBetween.Y > 0)
                {
                    direction = Direction.Down;
                }
            }
            //Horizontal
            if (vectorBetween.Y == 0)
            {
                if (vectorBetween.X < 0)
                {
                    direction = Direction.Left;
                }
                if (vectorBetween.X > 0)
                {
                    direction = Direction.Right;
                }
            }
            // Diagonal
            if (vectorBetween.X != 0 && vectorBetween.Y != 0)
            {
                if (vectorBetween.X > 0 && vectorBetween.Y < 0)
                {
                    direction = Direction.UpRight;
                }
                if (vectorBetween.X > 0 && vectorBetween.Y > 0)
                {
                    direction = Direction.DownRight;
                }
                if (vectorBetween.X < 0 && vectorBetween.Y > 0)
                {
                    direction = Direction.DownLeft;
                }
                if (vectorBetween.X < 0 && vectorBetween.Y < 0)
                {
                    direction = Direction.UpLeft;
                }
            }

            return direction;
        }

        // Return opposite Direction enum value to argument
        public static Direction DirectionOpposite(Direction direction)
        {
            Direction directionOpposide = Direction.None;
            switch (direction)
            {
                case Direction.Up:
                    directionOpposide = Direction.Down;
                    break;
                case Direction.UpRight:
                    directionOpposide = Direction.DownLeft;
                    break;
                case Direction.Right:
                    directionOpposide = Direction.Left;
                    break;
                case Direction.DownRight:
                    directionOpposide = Direction.UpLeft;
                    break;
                case Direction.Down:
                    directionOpposide = Direction.Up;
                    break;
                case Direction.DownLeft:
                    directionOpposide = Direction.UpRight;
                    break;
                case Direction.Left:
                    directionOpposide = Direction.Right;
                    break;
                case Direction.UpLeft:
                    directionOpposide = Direction.DownRight;
                    break;
            }


            return directionOpposide;
        }

        // Return Vector2 distance between two given Tile objects
        public static Vector2 DistanceBetween(Tile tileDestination, Tile tileOrigin)
        {
            Vector2 distance = tileDestination.PositionGrid - tileOrigin.PositionGrid;
            distance.X = Math.Abs(distance.X);
            distance.Y = Math.Abs(distance.Y);
            return distance;
        }

        // Assign entrance object's entrance type depending on in/outdoors
        public static void SetEntranceType(Area area, Entrance entrance)
        {
            // If outdoor entrance to next area
            if (area.Entrances.IndexOf(entrance) == 0 && area.AreaType == AreaType.Outdoor)
            {
                entrance.EntranceType = EntranceType.Outdoor;
            }
            // If outdoor entrance to previous area
            else if (area.Entrances.IndexOf(entrance) == area.Entrances.Count - 1 &&
                     area.AreaType == AreaType.Outdoor && AreaStatesList.AreaStates.IndexOf(area.areaState) != 0)
            {
                entrance.EntranceType = EntranceType.Outdoor;
            }
            // If outdoor entrance  to indoor area
            else
            {
                entrance.EntranceType = EntranceType.Indoor;
            }
        }

        // Return Vector2 value converted to string
        public static string Vector2RangeToString(Vector2 rangeVector2)
        {
            string rangeString = rangeVector2.ToString();

            int indexX = rangeString.IndexOf("X:") + 2;
            int indexY = rangeString.IndexOf("Y:") + 2;
            string rangeMin = rangeString.Substring(indexX, (rangeString.IndexOf("Y") -1) - indexX);
            string rangeMax = rangeString.Substring(indexY, (rangeString.Length - 1) - indexY);
            string formatted = $"{rangeMin}-{rangeMax}";

            return formatted;
        }


        // Return whether interactable position on wall is valid for e.g. doors, entrances
        public static bool ValidWallPositionForInteractable(Area area, Tile tile)
        {
            bool isValid = false;

            if (tile.WallType == WallSpriteType.MidV)
            {
                isValid = ValidPositionInteractableVertical(area, tile);
            }
            else if (tile.WallType == WallSpriteType.MidH)
            {
                isValid = ValidPositionInteractableHorizontal(area, tile);
            }
            return isValid;
        }
        private static bool ValidPositionInteractableHorizontal(Area area, Tile tile)
        {
            // Horizontal
            if (tile.WallType == WallSpriteType.MidH &&
                NeighbourAreaBorder(area, tile, Direction.Up) == false && NeighbourAreaBorder(area, tile, Direction.Down) == false)
            {
                // Not adjacent to trees, walls
                if (tile.Neighbour(Direction.Up, 1).WallCategory == WallSpriteCategory.None &&
                    tile.Neighbour(Direction.Up, 1).TreeCategory == TreeSpriteCategory.None &&
                    tile.Neighbour(Direction.Down, 1).WallCategory == WallSpriteCategory.None &&
                    tile.Neighbour(Direction.Down, 1).TreeCategory == TreeSpriteCategory.None &&
                    // Not placed on interactable
                    tile.ContainsInteractable() == false
                    )
                {
                    return true;
                }
            }
            return false;
        }
        private static bool ValidPositionInteractableVertical(Area area, Tile tile)
        {
            // Vertical
            if (tile.WallType == WallSpriteType.MidV &&
                NeighbourAreaBorder(area, tile, Direction.Left) == false && NeighbourAreaBorder(area, tile, Direction.Right) == false)
            {
                // Not adjacent to trees, walls
                if (tile.Neighbour(Direction.Right, 1).WallCategory == WallSpriteCategory.None &&
                tile.Neighbour(Direction.Right, 1).TreeCategory == TreeSpriteCategory.None &&
                tile.Neighbour(Direction.Left, 1).WallCategory == WallSpriteCategory.None &&
                tile.Neighbour(Direction.Left, 1).TreeCategory == TreeSpriteCategory.None &&
                // Not placed on interactable
                tile.ContainsInteractable() == false
                )
                {
                    return true;
                }
            }
            return false;
        }
        private static bool NeighbourAreaBorder(Area area, Tile tile, Direction direction)
        {
            bool neighbourBorder = true;
            switch (direction)
            {
                case Direction.Up:
                    if (tile.PositionGrid.Y > 1)
                    {
                        neighbourBorder = false;
                    }
                    break;
                case Direction.Right:
                    if (tile.PositionGrid.X < area.Grid.GetLength(0) - 1)
                    {
                        neighbourBorder = false;
                    }
                    break;
                case Direction.Down:
                    if (tile.PositionGrid.Y < area.Grid.GetLength(1) - 1)
                    {
                        neighbourBorder = false;
                    }
                    break;
                case Direction.Left:
                    if (tile.PositionGrid.X > 1)
                    {
                        neighbourBorder = false;
                    }
                    break;
            }
            return neighbourBorder;
        }
    }
}