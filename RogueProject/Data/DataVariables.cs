﻿/****************************************
* Contain all CONST data variable names * 
*****************************************/

namespace RogueProject.Data
{
    public static class DataVariables
    {
        public const string saveSubDirectoryName = "\\SaveData\\";
        public const string saveFileName1 = "SaveData1.xml";
        public const string saveFileName2 = "SaveData2.xml";
        public const string saveFileName3 = "SaveData3.xml";
        public const string saveFileName4 = "QuickSave.xml";
        public const string rootName = "SaveData";
        public const string headingArea = "area";
        public const string headingCurrentAreaNumber = "currentArea";
        public const string headingEntrances = "entrances";
        public const string headingFloorCat = "floorTileSets";
        public const string headingEnemyTypes = "enemyTypes";
        public const string nameCatDoor = "doorCat";
        public const string nameCatEnemyType = "enemyType";
        public const string nameCatFloor = "floorCat";
        public const string nameCatTree = "treeCat";
        public const string nameCatWall = "wallCat";
        public const string nameChanceNextAreaRegionSame = "chanceNextAreaSameRegion";
        public const string nameEntrance = "entrance";
        public const string nameRegionType = "regionType";
        public const string nameSize = "size";
        public const string nameType = "type";

        public const string nameCurrentAreaNumber = "currentArea";
    }
}
