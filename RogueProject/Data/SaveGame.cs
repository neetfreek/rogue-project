﻿/***********************************************************************
* Saves game status data to external XML file in buildfolder/Saves.    *
* Create/Overwrite save folder which contains save data xml file.      *
* Xml file elements represent data to save:                            *
*   1. areaState object properties are written as child elements of    *
*       elements named area[numberArea]                                *
*   2. CurrentArea (area player on when saved) written as element      *
************************************************************************/
using System.IO;
using System.Xml;
using System.Text;
using RogueProject.Textures;
using RogueProject.Objects;
using RogueProject.GUI;
using RogueProject.Enemies;

namespace RogueProject.Data
{
    public class SaveGame
    {
        // Public interface to call save functionality
        public void SaveGameData(string nameSaveFile)
        {
            SaveAreaStates(nameSaveFile);
        }

        // Save state of all encountered game areas
        private void SaveAreaStates(string nameSaveFile)
        {
            XmlWriterSettings writerSettings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "      ",
                ConformanceLevel = ConformanceLevel.Fragment,
                Encoding = Encoding.UTF8
            };

            // Save directory
            string saveDirString = $"{Directory.GetCurrentDirectory().ToString()}{DataVariables.saveSubDirectoryName}";

            Directory.CreateDirectory(saveDirString);
            using (XmlWriter writer = XmlWriter.Create(SaveDataPath(nameSaveFile), writerSettings))
            {
                writer.WriteStartElement(DataVariables.rootName);

                foreach (AreaState areaState in AreaStatesList.AreaStates)
                {
                    writer.WriteStartElement($"{DataVariables.headingArea}{areaState.AreaIndex}");
                    writer.WriteElementString($"{DataVariables.nameType}", areaState.AreaType.ToString());
                    writer.WriteElementString($"{DataVariables.nameRegionType}", areaState.RegionType.ToString());
                    writer.WriteElementString($"{DataVariables.nameChanceNextAreaRegionSame}", areaState.ChanceNextAreaSameRegion.ToString());
                    writer.WriteElementString($"{DataVariables.nameCatDoor}", areaState.DoorSpriteCategory.ToString());
                    writer.WriteElementString($"{DataVariables.nameCatFloor}", areaState.FloorSpriteCategory.ToString());
                    writer.WriteStartElement($"{DataVariables.headingFloorCat}");
                    foreach (FloorSpriteCategory floorCategory in areaState.TilesetRoomFloorActual)
                    {
                        writer.WriteElementString($"{DataVariables.nameCatFloor}", floorCategory.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement($"{DataVariables.headingEnemyTypes}");
                    foreach (EnemyType enemyType in areaState.EnemyTypes)
                    {
                        writer.WriteElementString($"{DataVariables.nameCatEnemyType}", enemyType.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement($"{DataVariables.headingEntrances}");
                    foreach (Entrance entrance in areaState.Entrances)
                    {
                        writer.WriteElementString($"{DataVariables.nameEntrance}", $"{entrance.AreaIndex} {entrance.AreaToLoadIndex}");
                    }
                    writer.WriteEndElement();
                    writer.WriteElementString($"{DataVariables.nameCatTree}", areaState.TreeSpriteCategory.ToString());
                    writer.WriteElementString($"{DataVariables.nameCatWall}", areaState.WallSpriteCategory.ToString());
                    writer.WriteElementString($"{DataVariables.nameSize}", areaState.Size.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteElementString($"{DataVariables.nameCurrentAreaNumber}", Game1.CurrentAreaIndex.ToString());

                writer.WriteEndElement();
                writer.Flush();
            }
        }

        // Return appropriate data path for given save file
        public static string SaveDataPath(string nameSaveFile)
        {
            string nameFile = "";
            switch (nameSaveFile)
            {
                case GUIVariables.TEXT_SAVE_SLOT_1:
                    nameFile = DataVariables.saveFileName1;
                    break;
                case GUIVariables.TEXT_SAVE_SLOT_2:
                    nameFile = DataVariables.saveFileName2;
                    break;
                case GUIVariables.TEXT_SAVE_SLOT_3:
                    nameFile = DataVariables.saveFileName3;
                    break;
                case GUIVariables.TEXT_SAVE_SLOT_4:
                    nameFile = DataVariables.saveFileName4;
                    break;
            }

            string saveDirString = $"{Directory.GetCurrentDirectory().ToString()}{DataVariables.saveSubDirectoryName}";
            string path = $"{saveDirString}{nameFile}";

            return path;
        }
    }
}