﻿/***********************************************************************
* Load game state from previous session saved in xml data file.        *
* Area states represented by areaState objects. These are re-created   *
*   and their variables set on load to matching elements in xml doc.   *
* areaState objects are stored in a temporary list, which overwrites   *
*   AreaStatesList list after xml gone through.                        *
* The number of areas (CountAreas) is also over-written using the      *
*   count of the written-over AreaStatesList list.                     *
* The current area (level player was on when saving) is also saved,    *
*   used for placing player back into that area on load.               *
************************************************************************/
using System;
using System.Xml.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RogueProject.Textures;
using RogueProject.Objects;
using RogueProject.Common;
using RogueProject.Enemies;

namespace RogueProject.Data
{
    class LoadGame
    {
        private readonly SaveGame saveGame;
        private readonly Game1 game;

        int areaIndexToLoad = 0;

        // temporary area states list to overwrite AreaStatesList.AreaStates
        private static List<AreaState> areaStates;

        public LoadGame(Game1 game)
        {
            this.game = game;
            saveGame = game.SaveGame;
        }

        public void LoadGameData(string nameSaveFile)
        {
            Game1.LoadingGame = true;
            // Populate temporary area state list
            areaStates = RetriveAreaStates(nameSaveFile);
            Game1.HighestAreaPlanned = RetriveHighestAreaPlanned(areaStates);

            // Create area for each area state in temporary list
            foreach (AreaState areaState in areaStates)
            {
                AreaStatesList.AreaStates.Add(areaState);
                game.AreaMaker.SetupAreaFromLoad(areaStates.IndexOf(areaState));
            }
            game.AreaMaker.SetupAreaFromLoad(areaIndexToLoad);
            Game1.CurrentAreaIndex = areaIndexToLoad;

            Game1.LoadingGame = false;
        }

        private List<AreaState> RetriveAreaStates(string nameSaveFile)
        {
            // Xml document to load from
            XDocument xDocument = XDocument.Load(SaveGame.SaveDataPath(nameSaveFile));
            // Temporary area states list to overwrite AreaStatesList.AreaStates with
            List<AreaState> areaStatesTemporary = new List<AreaState>();

            foreach (XElement element in xDocument.Descendants())
            {
                // Set current area number/ area player was in when saved
                if (element.Name.ToString() == DataVariables.nameCurrentAreaNumber)
                {
                    areaIndexToLoad = int.Parse(element.Value.ToString());
                }

                if (element.Name.ToString().Contains(DataVariables.headingArea))
                {
                    // Temporary area state
                    AreaState areaState = new AreaState(game)
                    {
                        AreaIndex = AreaIndexFromName(element.Name.ToString())
                    };
                    IEnumerable<XElement> children = element.Elements();
                    foreach (var child in children)
                    {
                        // Add area properties to temporary area state
                        if (child.Name == DataVariables.nameType)
                        {
                            Enum.TryParse(child.Value, out areaState.AreaType);
                        }
                        if (child.Name == DataVariables.nameRegionType)
                        {
                            Enum.TryParse(child.Value, out areaState.RegionType);
                        }
                        if (child.Name == DataVariables.nameChanceNextAreaRegionSame)
                        {
                            areaState.ChanceNextAreaSameRegion = int.Parse(child.Value.ToString());
                        }
                        if (child.Name == DataVariables.nameCatDoor)
                        {
                            Enum.TryParse(child.Value, out areaState.DoorSpriteCategory);
                        }
                        if (child.Name == DataVariables.nameCatFloor)
                        {
                            Enum.TryParse(child.Value, out areaState.FloorSpriteCategory);
                        }
                        if (child.Name == DataVariables.headingFloorCat)
                        {
                            IEnumerable<XElement> subChildren = child.Elements();
                            foreach (var subChild in subChildren)
                            {
                                Enum.TryParse(subChild.Value, out FloorSpriteCategory floorCat);
                                areaState.TilesetRoomFloorActual.Add(floorCat);
                            }
                        }
                        if (child.Name == DataVariables.headingEnemyTypes)
                        {
                            IEnumerable<XElement> subChildren = child.Elements();
                            foreach (var subChild in subChildren)
                            {
                                Enum.TryParse(subChild.Value, out EnemyType enemyType);
                                areaState.EnemyTypes.Add(enemyType);
                            }
                        }
                        if (child.Name == DataVariables.headingEntrances)
                        {
                            IEnumerable<XElement> subChildren = child.Elements();
                            foreach (var subChild in subChildren)
                            {
                                areaState.Entrances.Add(EntranceFromData(subChild.Value));
                            }
                        }
                        if (child.Name == DataVariables.nameCatTree)
                        {
                            Enum.TryParse(child.Value, out areaState.TreeSpriteCategory);
                        }
                        if (child.Name == DataVariables.nameCatWall)
                        {
                            Enum.TryParse(child.Value, out areaState.WallSpriteCategory);
                        }
                        if (child.Name == DataVariables.nameSize)
                        {
                            areaState.Size = Vector2FromData(child.Value);

                            // Add area state to temporary list
                            areaStatesTemporary.Add(areaState);
                        }
                    }
                }
            }

            return areaStatesTemporary;
        }

        private int AreaIndexFromName(string areaName)
        {
            string name = areaName;
            string nameFixed = name.Substring(DataVariables.headingArea.Length);
            return int.Parse(nameFixed);
        }

        private Vector2 Vector2FromData(string v2String)
        {
            int indexX = v2String.IndexOf("X:") + 2;
            int indexY = v2String.IndexOf("Y:") + 2;
            float posX = float.Parse(v2String.Substring(indexX, v2String.IndexOf("Y") - indexX));
            float posY = float.Parse(v2String.Substring(indexY, v2String.IndexOf("}") - indexY));

            return new Vector2(posX, posY);
        }

        private Entrance EntranceFromData(string entranceString)
        {
            int indexArea = 0;
            int indexNextArea = entranceString.IndexOf(" ") + 1;
            int indexEnd = entranceString.Length;
            int area = int.Parse(entranceString.Substring(indexArea, indexNextArea - indexArea));
            int areaNext = int.Parse(entranceString.Substring(indexNextArea, indexEnd - indexNextArea));
            return new Entrance(area, areaNext);
        }

        private int RetriveHighestAreaPlanned(List<AreaState> areaStates)
        {
            int highestArea = 0;

            foreach (AreaState areaState in areaStates)
            {
                foreach (Entrance entrance in areaState.Entrances)
                {
                    if (entrance.AreaToLoadIndex > highestArea)
                    {
                        highestArea = entrance.AreaToLoadIndex;
                    }
                }
            }
            return highestArea;
        }
    }
}