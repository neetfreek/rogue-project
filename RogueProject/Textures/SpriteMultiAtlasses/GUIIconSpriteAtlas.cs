﻿/********************************************************************************
* Return specified sprite source rectang from relevant sprite atlas texture     *
* using:                                                                        *
*   Handler argument object name which decides what SpriteType to get e.g.      *
*       character, scroll sprite. This in turn calls relevant routine handling  *
*       return of sprite source rectangle from relevant sprite dictionary,      *
*       atlas, texture.                                                         *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       [RelevantSpriteDictionaryName].[RelevantSpriteTypeName]                 *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   A. Entry handler determing type of sprite to get source rectangle for       *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding sprite  *
*       type enum as parameter                                                  *
*   3. Sprite() calls SpritePosition() with relevant sprite type enum as        *
*       parameter                                                               *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       relevant sprite type enum as key, returns positionStart to Sprite()     *               
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.GUI;
using RogueProject.PlayerCharacter;

namespace RogueProject.Textures
{
    public class GUIIconSpriteAtlas
    {
        private readonly TextureLoader textures;

        public GUIIconSpriteAtlas(TextureLoader textures)
        {
            this.textures = textures;
        }

        public Texture2D TextureSprite(string nameObject)
        {
            Texture2D textureSprite = textures.TextureScroll;
            switch (nameObject)
            {
                case GUIVariables.TEXT_ICON_INVENTORY:
                    textureSprite = textures.TextureContainer;
                    break;
                case GUIVariables.TEXT_ICON_MAIN_MENU:
                    textureSprite = textures.TextureScroll;
                    break;
                case GUIVariables.TEXT_ICON_FEMALE:
                    textureSprite = textures.TexturePlayer;
                    break;
                case GUIVariables.TEXT_ICON_MALE:
                    textureSprite = textures.TexturePlayer;
                    break;
                case GUIVariables.TEXT_ICON_CHARACTER_SHEET:
                    textureSprite = textures.TexturePlayer;
                    break;
            }
            if (nameObject.Contains("Plus") || nameObject.Contains("Minus"))
            {
                textureSprite = textures.TextureGUI;
            }

            return textureSprite;
        }

        public Rectangle SourceRectangleSprite(string nameObject)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (nameObject)
            {
                case GUIVariables.TEXT_ICON_INVENTORY:
                    sourceRectangle = textures.ContainerAtlas.Sprite(ContainerSprite.SackBrown);
                    break;
                case GUIVariables.TEXT_ICON_MAIN_MENU:
                    sourceRectangle = textures.ScrollAtlas.SourceRectangleSprite(ScrollSprite.ScrollWhite);
                    break;
                case GUIVariables.TEXT_ICON_FEMALE:
                    sourceRectangle = textures.PlayerAtlas.SourceRectangleSprite(PlayerSprite.NoneFemale);
                    break;
                case GUIVariables.TEXT_ICON_MALE:
                    sourceRectangle = textures.PlayerAtlas.SourceRectangleSprite(PlayerSprite.NoneMale);
                    break;
                case GUIVariables.TEXT_ICON_CHARACTER_SHEET:
                    sourceRectangle = textures.PlayerAtlas.SourceRectangleSprite(PlayerSprite.PortraitFemale);
                    break;
            }

            if (nameObject.Contains("Plus"))
            {
                sourceRectangle = textures.GUIAtlas.SourceRectangleSprite(GUISpriteCategory.Arrow, GUISpriteType.ArrowUp);
            }
            if (nameObject.Contains("Minus"))
            {
                sourceRectangle = textures.GUIAtlas.SourceRectangleSprite(GUISpriteCategory.Arrow, GUISpriteType.ArrowDown);
            }

            return sourceRectangle;
        }

        public Rectangle SourceRectangleCharacterSheet(Gender gender)
        {
            Rectangle sourceRectangle = new Rectangle();

            if (gender == Gender.Female)
            {
                sourceRectangle = textures.PlayerAtlas.SourceRectangleSprite(PlayerSprite.PortraitFemale);
            }
            if (gender == Gender.Male)
            {
                sourceRectangle = textures.PlayerAtlas.SourceRectangleSprite(PlayerSprite.PortraitMale);
            }

            return sourceRectangle;
        }
    }
}