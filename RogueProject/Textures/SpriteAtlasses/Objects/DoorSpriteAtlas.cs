﻿/********************************************************************************
* Return specified sprite source rectang from Door0, Door1 sprite atlas         *
* textures using either                                                         *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName] SpriteOrientation                                           *
*   (which have both horizontal, vertical) or                                   *
*   1. DoorSprite for Arch, Portal1 doors (which only have horizontal)           *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       FloorSpriteDictionary.SpritesFloor                                      *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding Door    *
*       enum as parameter                                                       *
*   3. Sprite() calls SpritePosition() with Floor enum as parameter             *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Door enum as key, returns positionStart to Sprite()                     *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class DoorSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public DoorSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }
        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(DoorSpriteCategory categoryDoor, DoorSpriteOrientation orientationDoor, DoorSprite spriteDoor, DoorState state)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (categoryDoor)
            {
                case DoorSpriteCategory.Arch:
                    sourceRectangle = Arch(spriteDoor);
                    break;

                case DoorSpriteCategory.BarsGold:
                    sourceRectangle = BarsGold(orientationDoor, state);
                    break;
                case DoorSpriteCategory.BarsGoldBroken:
                    sourceRectangle = BarsGoldBroken(orientationDoor);
                    break;
                case DoorSpriteCategory.BarsGoldChain:
                    sourceRectangle = BarsGoldChain(orientationDoor);
                    break;
                case DoorSpriteCategory.BarsGoldChainGold:
                    sourceRectangle = BarsGoldChainGold(orientationDoor);
                    break;

                case DoorSpriteCategory.BarsIron:
                    sourceRectangle = BarsIron(orientationDoor, state);
                    break;
                case DoorSpriteCategory.BarsIronBroken:
                    sourceRectangle = BarsIronBroken(orientationDoor);
                    break;
                case DoorSpriteCategory.BarsIronChain:
                    sourceRectangle = BarsIronChain(orientationDoor);
                    break;
                case DoorSpriteCategory.BarsIronChainGold:
                    sourceRectangle = BarsIronChainGold(orientationDoor);
                    break;


                case DoorSpriteCategory.Chains:
                    sourceRectangle = Chains(orientationDoor);
                    break;
                case DoorSpriteCategory.ChainsGold:
                    sourceRectangle = ChainsGold(orientationDoor);
                    break;

                case DoorSpriteCategory.DoorMetal:
                    sourceRectangle = DoorMetal(orientationDoor, state);
                    break;
                case DoorSpriteCategory.DoorMetalBroken:
                    sourceRectangle = DoorMetalBroken(orientationDoor);
                    break;
                case DoorSpriteCategory.DoorMetalChain:
                    sourceRectangle = DoorMetalChain(orientationDoor);
                    break;
                case DoorSpriteCategory.DoorMetalChainGold:
                    sourceRectangle = DoorMetalChainGold(orientationDoor);
                    break;

                case DoorSpriteCategory.DoorWood:
                    sourceRectangle = DoorWood(orientationDoor, state);
                    break;
                case DoorSpriteCategory.DoorWoodBroken:
                    sourceRectangle = DoorWoodBroken(orientationDoor);
                    break;
                case DoorSpriteCategory.DoorWoodChain:
                    sourceRectangle = DoorWoodChain(orientationDoor);
                    break;
                case DoorSpriteCategory.DoorWoodChainGold:
                    sourceRectangle = DoorWoodChainGold(orientationDoor);
                    break;

                case DoorSpriteCategory.Portal:
                    sourceRectangle = Portal(spriteDoor);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different floor categories, calls Sprite()
        private Rectangle Arch(DoorSprite spriteDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (spriteDoor)
            {
                case DoorSprite.Arch:
                    sourceRectangle = Sprite(DoorSprite.Arch);
                    break;
                case DoorSprite.ArchDark:
                    sourceRectangle = Sprite(DoorSprite.ArchDark);
                    break;
                case DoorSprite.ArchGold:
                    sourceRectangle = Sprite(DoorSprite.ArchGold);
                    break;
                case DoorSprite.ArchSilver:
                    sourceRectangle = Sprite(DoorSprite.ArchSilver);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle BarsGoldBroken(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsGoldHBroken);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsGold(DoorSpriteOrientation orientationDoor, DoorState state)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsGoldH);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsGoldHOpen);
                    }
                    break;
                case DoorSpriteOrientation.Vertical:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsGoldV);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.Blank);
                    }
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsGoldChain(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsGoldChainH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.BarsGoldChainV);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsGoldChainGold(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsGoldChainGoldH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.BarsGoldChainGoldV);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle BarsIronBroken(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsIronHBroken);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsIron(DoorSpriteOrientation orientationDoor, DoorState state)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsIronH);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsIronHOpen);
                    }
                    break;
                case DoorSpriteOrientation.Vertical:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.BarsIronV);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.Blank);
                    }
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsIronChain(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsIronChainH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.BarsIronChainV);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarsIronChainGold(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.BarsIronChainGoldH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.BarsIronChainGoldV);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle Chains(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.ChainsH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.ChainsV);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ChainsGold(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.ChainsGoldH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.ChainsGoldV);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle DoorMetal(DoorSpriteOrientation orientationDoor, DoorState state)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorMetalH);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorMetalHOpen);
                    }
                    break;
                case DoorSpriteOrientation.Vertical:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorMetalV);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorMetalVOpen);
                    }
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorMetalBroken(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorMetalHBroken);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorMetalChain(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorMetalChainH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.DoorMetalChainV);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorMetalChainGold(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorMetalChainGoldH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.DoorMetalChainGoldV);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle DoorWood(DoorSpriteOrientation orientationDoor, DoorState state)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorWoodH);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorWoodHOpen);
                    }
                    break;
                case DoorSpriteOrientation.Vertical:
                    if (state == DoorState.Closed)
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorWoodV);
                    }
                    else
                    {
                        sourceRectangle = Sprite(DoorSprite.DoorWoodVOpen1);
                    }
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorWoodBroken(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorWoodHBroken);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorWoodChain(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorWoodChainH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.DoorWoodChainV);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle DoorWoodChainGold(DoorSpriteOrientation orientationDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (orientationDoor)
            {
                case DoorSpriteOrientation.Horizontal:
                    sourceRectangle = Sprite(DoorSprite.DoorWoodChainGoldH);
                    break;
                case DoorSpriteOrientation.Vertical:
                    sourceRectangle = Sprite(DoorSprite.DoorWoodChainGoldV);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle Portal(DoorSprite spriteDoor)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (spriteDoor)
            {
                case DoorSprite.Portal1:
                    sourceRectangle = Sprite(DoorSprite.Portal1);
                    break;
                case DoorSprite.PortalGold1:
                    sourceRectangle = Sprite(DoorSprite.PortalGold1);
                    break;
                case DoorSprite.PortalSilver1:
                    sourceRectangle = Sprite(DoorSprite.PortalSilver1);
                    break;
                case DoorSprite.PortalDark1:
                    sourceRectangle = Sprite(DoorSprite.PortalDark1);
                    break;
                case DoorSprite.Portal2:
                    sourceRectangle = Sprite(DoorSprite.Portal2);
                    break;
                case DoorSprite.PortalGold2:
                    sourceRectangle = Sprite(DoorSprite.PortalGold2);
                    break;
                case DoorSprite.PortalSilver2:
                    sourceRectangle = Sprite(DoorSprite.PortalSilver2);
                    break;
                case DoorSprite.PortalDark2:
                    sourceRectangle = Sprite(DoorSprite.PortalDark2);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(DoorSprite spriteDoor)
        {
            Vector2 spritePosition = SpritePosition(spriteDoor);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(DoorSprite spriteDoor)
        {
            DoorSpriteDictionary.SpritesDoor.TryGetValue(spriteDoor, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}