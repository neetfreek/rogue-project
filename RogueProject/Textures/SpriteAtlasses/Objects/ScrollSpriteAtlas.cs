﻿/********************************************************************************
* Return specified sprite source rectangle from scroll sprite atlas texture     *
* using:                                                                        *
*   1. ScrollSprite enum                                                        *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       ScrollSpriteDictionary.SpritesScroll                                    *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() calls SpritePosition() with ScrollSprite enum as *
*       parameter                                                               *
*   2. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       scroll enum as key, returns positionStart to Sprite()                   *    
*   3. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   4. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class ScrollSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public ScrollSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        public Rectangle SourceRectangleSprite(ScrollSprite sprite)
        {
            return Sprite(sprite);
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(ScrollSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(ScrollSprite sprite)
        {
            ScrollSpriteDictionary.SpritesScroll.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}