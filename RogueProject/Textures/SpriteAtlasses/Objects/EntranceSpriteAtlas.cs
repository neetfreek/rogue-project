﻿/********************************************************************************
* Return specified sprite source rectang from Entrance sprite atlas texture     *
* using:                                                                        *
*   1. [Sprite] enum; SourceRectangleSprite calls Sprite() with enum argument   *
*   3. Sprite() calls SpritePosition() with Entrance enum as parameter          *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Entrance enum as key, returns positionStart to Sprite()                 *
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class EntranceSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public EntranceSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(EntranceSprite entranceSprite)
        {
            return Sprite(entranceSprite);
        }


        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(EntranceSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(EntranceSprite sprite)
        {
            EntranceSpriteDictionary.SpritesEntrance.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}
