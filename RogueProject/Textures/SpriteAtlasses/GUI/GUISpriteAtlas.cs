﻿/********************************************************************************
* Return specified sprite source rectang from GUI sprite atlas texture          *
* using:                                                                        *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       FloorSpriteDictionary.SpritesFloor                                      *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding GUI     *
*       enum as parameter                                                       *
*   3. Sprite() calls SpritePosition() with GUI enum as parameter               *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       GUI enum as key, returns positionStart to Sprite()                      *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class GUISpriteAtlas
    {        
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public GUISpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(GUISpriteCategory categoryGUI, GUISpriteType typeGUI)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (categoryGUI)
            {
                // Arrows
                case GUISpriteCategory.Arrow:
                    sourceRectangle = Arrow(typeGUI);
                    break;
                // Bars
                case GUISpriteCategory.BarBorder:
                    sourceRectangle = BarBorder(typeGUI);
                    break;
                case GUISpriteCategory.BarRed:
                    sourceRectangle = BarRed(typeGUI);
                    break;
                case GUISpriteCategory.BarBlue:
                    sourceRectangle = BarBlue(typeGUI);
                    break;
                case GUISpriteCategory.BarYellow:
                    sourceRectangle = BarYellow(typeGUI);
                    break;
                // Cursor
                case GUISpriteCategory.Cursor:
                    sourceRectangle = Sprite(GUISprite.Cursor);
                    break;
                // None
                case GUISpriteCategory.None:
                    sourceRectangle = Sprite(GUISprite.None);
                    break;
                // Rectangles
                case GUISpriteCategory.RectBlackGrey:
                    sourceRectangle = RectBlackGrey(typeGUI);
                    break;
                case GUISpriteCategory.RectGrey:
                    sourceRectangle = RectGrey(typeGUI);
                    break;
                case GUISpriteCategory.RectGreyDark:
                    sourceRectangle = RectGreyDark(typeGUI);
                    break;
                case GUISpriteCategory.RectGreyLight:
                    sourceRectangle = RectGreyLight(typeGUI);
                    break;
                // Squares
                case GUISpriteCategory.Square:
                    sourceRectangle = Square(typeGUI);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different GUI categories, calls Sprite()
        private Rectangle Arrow(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.ArrowDown:
                    sourceRectangle = Sprite(GUISprite.ArrowDown);
                    break;
                case GUISpriteType.ArrowUp:
                    sourceRectangle = Sprite(GUISprite.ArrowUp);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle BarBorder(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.BarBorderL:
                    sourceRectangle = Sprite(GUISprite.BarBorderL);
                    break;
                case GUISpriteType.BarBorderM:
                    sourceRectangle = Sprite(GUISprite.BarBorderM);
                    break;
                case GUISpriteType.BarBorderR:
                    sourceRectangle = Sprite(GUISprite.BarBorderR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarBlue(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case GUISpriteType.Bar100End:
                    sourceRectangle = Sprite(GUISprite.BarBlue100End);
                    break;
                case GUISpriteType.Bar100:
                    sourceRectangle = Sprite(GUISprite.BarBlue100);
                    break;
                case GUISpriteType.Bar75:
                    sourceRectangle = Sprite(GUISprite.BarBlue75);
                    break;
                case GUISpriteType.Bar50:
                    sourceRectangle = Sprite(GUISprite.BarBlue50);
                    break;
                case GUISpriteType.Bar25:
                    sourceRectangle = Sprite(GUISprite.BarBlue25);
                    break;
                case GUISpriteType.Bar0:
                    sourceRectangle = Sprite(GUISprite.Bar0);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarRed(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case GUISpriteType.Bar100End:
                    sourceRectangle = Sprite(GUISprite.BarRed100End);
                    break;
                case GUISpriteType.Bar100:
                    sourceRectangle = Sprite(GUISprite.BarRed100);
                    break;
                case GUISpriteType.Bar75:
                    sourceRectangle = Sprite(GUISprite.BarRed75);
                    break;
                case GUISpriteType.Bar50:
                    sourceRectangle = Sprite(GUISprite.BarRed50);
                    break;
                case GUISpriteType.Bar25:
                    sourceRectangle = Sprite(GUISprite.BarRed25);
                    break;
                case GUISpriteType.Bar0:
                    sourceRectangle = Sprite(GUISprite.Bar0);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BarYellow(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case GUISpriteType.Bar100End:
                    sourceRectangle = Sprite(GUISprite.BarYellow100End);
                    break;
                case GUISpriteType.Bar100:
                    sourceRectangle = Sprite(GUISprite.BarYellow100);
                    break;
                case GUISpriteType.Bar75:
                    sourceRectangle = Sprite(GUISprite.BarYellow75);
                    break;
                case GUISpriteType.Bar50:
                    sourceRectangle = Sprite(GUISprite.BarYellow50);
                    break;
                case GUISpriteType.Bar25:
                    sourceRectangle = Sprite(GUISprite.BarYellow25);
                    break;
                case GUISpriteType.Bar0:
                    sourceRectangle = Sprite(GUISprite.Bar0);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle RectBlackGrey(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.TopL:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyTL);
                    break;
                case GUISpriteType.TopM:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyTM);
                    break;
                case GUISpriteType.TopR:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyTR);
                    break;
                case GUISpriteType.MidL:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyML);
                    break;
                case GUISpriteType.MidM:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyMM);
                    break;
                case GUISpriteType.MidR:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyMR);
                    break;
                case GUISpriteType.BotL:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyBL);
                    break;
                case GUISpriteType.BotM:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyBM);
                    break;
                case GUISpriteType.BotR:
                    sourceRectangle = Sprite(GUISprite.RectBlackGreyBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle RectGrey(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.TopL:
                    sourceRectangle = Sprite(GUISprite.RectGreyTL);
                    break;
                case GUISpriteType.TopM:
                    sourceRectangle = Sprite(GUISprite.RectGreyTM);
                    break;
                case GUISpriteType.TopR:
                    sourceRectangle = Sprite(GUISprite.RectGreyTR);
                    break;
                case GUISpriteType.MidL:
                    sourceRectangle = Sprite(GUISprite.RectGreyML);
                    break;
                case GUISpriteType.MidM:
                    sourceRectangle = Sprite(GUISprite.RectGreyMM);
                    break;
                case GUISpriteType.MidR:
                    sourceRectangle = Sprite(GUISprite.RectGreyMR);
                    break;
                case GUISpriteType.BotL:
                    sourceRectangle = Sprite(GUISprite.RectGreyBL);
                    break;
                case GUISpriteType.BotM:
                    sourceRectangle = Sprite(GUISprite.RectGreyBM);
                    break;
                case GUISpriteType.BotR:
                    sourceRectangle = Sprite(GUISprite.RectGreyBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle RectGreyDark(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.TopL:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkTL);
                    break;
                case GUISpriteType.TopM:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkTM);
                    break;
                case GUISpriteType.TopR:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkTR);
                    break;
                case GUISpriteType.MidL:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkML);
                    break;
                case GUISpriteType.MidM:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkMM);
                    break;
                case GUISpriteType.MidR:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkMR);
                    break;
                case GUISpriteType.BotL:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkBL);
                    break;
                case GUISpriteType.BotM:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkBM);
                    break;
                case GUISpriteType.BotR:
                    sourceRectangle = Sprite(GUISprite.RectGreyDarkBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle RectGreyLight(GUISpriteType type)

        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.TopL:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightTL);
                    break;
                case GUISpriteType.TopM:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightTM);
                    break;
                case GUISpriteType.TopR:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightTR);
                    break;
                case GUISpriteType.MidL:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightML);
                    break;
                case GUISpriteType.MidM:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightMM);
                    break;
                case GUISpriteType.MidR:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightMR);
                    break;
                case GUISpriteType.BotL:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightBL);
                    break;
                case GUISpriteType.BotM:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightBM);
                    break;
                case GUISpriteType.BotR:
                    sourceRectangle = Sprite(GUISprite.RectGreyLightBR);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle Square(GUISpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case GUISpriteType.SquareBlackGrey:
                    sourceRectangle = Sprite(GUISprite.SquareBlackGrey);
                    break;
                case GUISpriteType.SquareBlackGreyDark:
                    sourceRectangle = Sprite(GUISprite.SquareBlackGreyDark);
                    break;
                case GUISpriteType.SquareGrey:
                    sourceRectangle = Sprite(GUISprite.SquareGrey);
                    break;
                case GUISpriteType.SquareGreyDark:
                    sourceRectangle = Sprite(GUISprite.SquareGreyDark);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(GUISprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(GUISprite sprite)
        {
            GUISpriteDictionary.SpritesGUI.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}