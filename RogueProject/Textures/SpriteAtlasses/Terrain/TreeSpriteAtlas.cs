﻿/********************************************************************************
* Return specified sprite source rectang from Tree sprite atlas texture         *
* using:                                                                        *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       TreeSpriteDictionary.SpritesTree                                        *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding Tree    *
*       enum as parameter                                                       *
*   3. Sprite() calls SpritePosition() with Tree enum as parameter              *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Tree enum as key, returns positionStart to Sprite()                     *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class TreeSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public TreeSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(TreeSpriteCategory categoryTree, TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (categoryTree)
            {
                case TreeSpriteCategory.MushroomDark:
                    sourceRectangle = MushroomDark(type);
                    break;
                case TreeSpriteCategory.MushroomLight:
                    sourceRectangle = MushroomLight(type);
                    break;

                case TreeSpriteCategory.Palm:
                    sourceRectangle = Palm(type);
                    break;

                case TreeSpriteCategory.PineGreenDark:
                    sourceRectangle = PineGreenDark(type);
                    break;
                case TreeSpriteCategory.PineGreenDarkDead:
                    sourceRectangle = PineGreenDarkDead(type);
                    break;
                case TreeSpriteCategory.PineGreenLight:
                    sourceRectangle = PineGreenLight(type);
                    break;
                case TreeSpriteCategory.PineGreenLightDead:
                    sourceRectangle = PineGreenLightDead(type);
                    break;
                case TreeSpriteCategory.PineSnowBlue:
                    sourceRectangle = PineSnowBlue(type);
                    break;
                case TreeSpriteCategory.PineSnowBlueDead:
                    sourceRectangle = PineSnowBlueDead(type);
                    break;
                case TreeSpriteCategory.PineSnowWhite:
                    sourceRectangle = PineSnowWhite(type);
                    break;
                case TreeSpriteCategory.PineSnowWhiteDead:
                    sourceRectangle = PineSnowWhiteDead(type);
                    break;

                case TreeSpriteCategory.PlainGreenDark:
                    sourceRectangle = PlainGreenDark(type);
                    break;
                case TreeSpriteCategory.PlainGreenDarkDead:
                    sourceRectangle = PlainGreenDarkDead(type);
                    break;
                case TreeSpriteCategory.PlainGreenLight:
                    sourceRectangle = PlainGreenLight(type);
                    break;
                case TreeSpriteCategory.PlainGreenLightDead:
                    sourceRectangle = PlainGreenLightDead(type);
                    break;
                case TreeSpriteCategory.PlainSnowBlue:
                    sourceRectangle = PlainSnowBlue(type);
                    break;
                case TreeSpriteCategory.PlainSnowBlueDead:
                    sourceRectangle = PlainSnowBlueDead(type);
                    break;
                case TreeSpriteCategory.PlainSnowWhite:
                    sourceRectangle = PlainSnowWhite(type);
                    break;
                case TreeSpriteCategory.PlainSnowWhiteDead:
                    sourceRectangle = PlainSnowWhiteDead(type);
                    break;

                case TreeSpriteCategory.WillowDark:
                    sourceRectangle = WillowDark(type);
                    break;
                case TreeSpriteCategory.WillowLight:
                    sourceRectangle = WillowLight(type);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different tree categories, calls Sprite()
        private Rectangle MushroomDark(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.MushroomDarkInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle MushroomLight(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.MushroomLightInnerBR);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle Palm(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PalmTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PalmTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PalmTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PalmMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PalmMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PalmMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PalmBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PalmBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PalmBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PalmSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PalmInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PalmInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PalmInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PalmInnerBR);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle PlainGreenDark(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainGreenDarkDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenDarkDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainGreenLight(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainGreenLightDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainGreenLightDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainSnowBlue(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainSnowBlueDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowBlueDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainSnowWhite(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainSnowWhiteDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PlainSnowWhiteDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle PineGreenDark(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineGreenDarkDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenDarkDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineGreenLight(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineGreenLightDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineGreenLightDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineSnowBlue(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineSnowBlueDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowBlueDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineSnowWhite(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PineSnowWhiteDead(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.PineSnowWhiteDeadInnerBR);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle WillowDark(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.WillowDarkInnerBR);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle WillowLight(TreeSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();
            switch (type)
            {
                case TreeSpriteType.TopL:
                    sourceRectangle = Sprite(TreeSprite.WillowLightTopL);
                    break;
                case TreeSpriteType.TopM:
                    sourceRectangle = Sprite(TreeSprite.WillowLightTopM);
                    break;
                case TreeSpriteType.TopR:
                    sourceRectangle = Sprite(TreeSprite.WillowLightTopR);
                    break;
                case TreeSpriteType.MidL:
                    sourceRectangle = Sprite(TreeSprite.WillowLightMidL);
                    break;
                case TreeSpriteType.MidM:
                    sourceRectangle = Sprite(TreeSprite.WillowLightMidM);
                    break;
                case TreeSpriteType.MidR:
                    sourceRectangle = Sprite(TreeSprite.WillowLightMidR);
                    break;
                case TreeSpriteType.BotL:
                    sourceRectangle = Sprite(TreeSprite.WillowLightBotL);
                    break;
                case TreeSpriteType.BotM:
                    sourceRectangle = Sprite(TreeSprite.WillowLightBotM);
                    break;
                case TreeSpriteType.BotR:
                    sourceRectangle = Sprite(TreeSprite.WillowLightBotR);
                    break;
                case TreeSpriteType.Single:
                    sourceRectangle = Sprite(TreeSprite.WillowLightSingle);
                    break;
                case TreeSpriteType.InnerTL:
                    sourceRectangle = Sprite(TreeSprite.WillowLightInnerTL);
                    break;
                case TreeSpriteType.InnerTR:
                    sourceRectangle = Sprite(TreeSprite.WillowLightInnerTR);
                    break;
                case TreeSpriteType.InnerBL:
                    sourceRectangle = Sprite(TreeSprite.WillowLightInnerBL);
                    break;
                case TreeSpriteType.InnerBR:
                    sourceRectangle = Sprite(TreeSprite.WillowLightInnerBR);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(TreeSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(TreeSprite sprite)
        {
            TreeSpriteDictionary.SpritesTree.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}