﻿/********************************************************************************
* Return specified sprite source rectang from Wall sprite atlas texture         *
*   using:                                                                      *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       DecorationGroundSpriteDictionary.SpritesDecorationGround                *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding         *
*       decoration enum as parameter                                            *
*   3. Sprite() calls SpritePosition() with decoration enum as parameter        *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       decoration enum as key, returns positionStart to Sprite()               *
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class DecorationGroundSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public DecorationGroundSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(DecorationGroundSpriteCategory category, DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (category)
            {
                case DecorationGroundSpriteCategory.GrassGreen:
                    sourceRectangle = GrassGreen(type);
                    break;
                case DecorationGroundSpriteCategory.GrassOrange:
                    sourceRectangle = GrassOrange(type);
                    break;
                case DecorationGroundSpriteCategory.GrassPatchGreen:
                    sourceRectangle = GrassPatchGreen(type);
                    break;
                case DecorationGroundSpriteCategory.GrassPatchOrange:
                    sourceRectangle = GrassPatchOrange(type);
                    break;
                case DecorationGroundSpriteCategory.FlowerWhite:
                    sourceRectangle = FlowersWhite(type);
                    break;
                case DecorationGroundSpriteCategory.FlowerBlue:
                    sourceRectangle = FlowersBlue(type);
                    break;
                case DecorationGroundSpriteCategory.FlowerYellow:
                    sourceRectangle = FlowersYellow(type);
                    break;
                case DecorationGroundSpriteCategory.FlowerRed:
                    sourceRectangle = FlowersRed(type);
                    break;
                case DecorationGroundSpriteCategory.StonesBlueLarge:
                    sourceRectangle = StonesBlueLarge(type);
                    break;
                case DecorationGroundSpriteCategory.StonesBlueSmall:
                    sourceRectangle = StonesBlueSmall(type);
                    break;
                case DecorationGroundSpriteCategory.StonesRedLarge:
                    sourceRectangle = StonesRedLarge(type);
                    break;
                case DecorationGroundSpriteCategory.StonesRedSmall:
                    sourceRectangle = StonesRedSmall(type);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different ground decoration categories, calls Sprite()
        private Rectangle GrassGreen(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassGreenDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassGreenSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassOrange(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassOrangeDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassOrangeSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassPatchGreen(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassGreenPatchDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassGreenPatchSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassPatchOrange(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassOrangePatchDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.GrassOrangePatchSparse);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle FlowersWhite(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersWhiteDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersWhiteSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FlowersBlue(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersBlueDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersBlueSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FlowersYellow(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersYellowDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersYellowSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FlowersRed(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersRedDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.FlowersRedSparse);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle StonesBlueLarge(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesBlueLargeDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesBlueLargeSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle StonesBlueSmall(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesBlueSmallDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesBlueSmallSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle StonesRedLarge(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesRedLargeDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesRedLargeSparse);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle StonesRedSmall(DecorationGroundSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case DecorationGroundSpriteType.Dense:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesRedSmallDense);
                    break;
                case DecorationGroundSpriteType.Sparse:
                    sourceRectangle = Sprite(DecorationGroundSprite.StonesRedSmallSparse);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(DecorationGroundSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(DecorationGroundSprite sprite)
        {
            DecorationGroundSpriteDictionary.SpritesDecorationGround.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}