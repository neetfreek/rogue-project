﻿/********************************************************************************
* Return specified sprite source rectang from Wall sprite atlas texture         *
* using:                                                                        *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       WallSpriteDictionary.SpritesWall                                        *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding Wall    *
*       enum as parameter                                                       *
*   3. Sprite() calls SpritePosition() with Wall enum as parameter              *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Wall enum as key, returns positionStart to Sprite()                     *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class WallSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public WallSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(WallSpriteCategory categoryWall, WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (categoryWall)
            {
                case WallSpriteCategory.BrickBlue:
                    sourceRectangle = BrickBlue(type);
                    break;
                case WallSpriteCategory.BrickDark:
                    sourceRectangle = BrickDark(type);
                    break;
                case WallSpriteCategory.BrickGrey:
                    sourceRectangle = BrickGrey(type);
                    break;
                case WallSpriteCategory.BrickSilver:
                    sourceRectangle = BrickSilver(type);
                    break;

                case WallSpriteCategory.ClayBlue:
                    sourceRectangle = ClayBlue(type);
                    break;
                case WallSpriteCategory.ClayBrown:
                    sourceRectangle = ClayBrown(type);
                    break;
                case WallSpriteCategory.ClayBrownDark:
                    sourceRectangle = ClayBrownDark(type);
                    break;
                case WallSpriteCategory.ClayBrownGrey:
                    sourceRectangle = ClayBrownGrey(type);
                    break;
                case WallSpriteCategory.ClayDark:
                    sourceRectangle = ClayDark(type);
                    break;
                case WallSpriteCategory.ClayGrey:
                    sourceRectangle = ClayGrey(type);
                    break;
                case WallSpriteCategory.ClayOrange:
                    sourceRectangle = ClayOrange(type);
                    break;
                case WallSpriteCategory.ClayRed:
                    sourceRectangle = ClayRed(type);
                    break;
                case WallSpriteCategory.ClayRedBright:
                    sourceRectangle = ClayRedBright(type);
                    break;
                case WallSpriteCategory.ClaySilver:
                    sourceRectangle = ClaySilver(type);
                    break;
                case WallSpriteCategory.ClayTan:
                    sourceRectangle = ClayTan(type);
                    break;
                case WallSpriteCategory.ClayTanBright:
                    sourceRectangle = ClayTanBright(type);
                    break;

                case WallSpriteCategory.IceBlue:
                    sourceRectangle = IceBlue(type);
                    break;
                case WallSpriteCategory.IceBlueDark:
                    sourceRectangle = IceBlueDark(type);
                    break;
                case WallSpriteCategory.IceBlueLight:
                    sourceRectangle = IceBlueLight(type);
                    break;
                case WallSpriteCategory.IceWhite:
                    sourceRectangle = IceWhite(type);
                    break;

                case WallSpriteCategory.MetalBrown:
                    sourceRectangle = MetalBrown(type);
                    break;
                case WallSpriteCategory.MetalBrownDark:
                    sourceRectangle = MetalBrownDark(type);
                    break;
                case WallSpriteCategory.MetalDark:
                    sourceRectangle = MetalDark(type);
                    break;
                case WallSpriteCategory.MetalTan:
                    sourceRectangle = MetalTan(type);
                    break;

                case WallSpriteCategory.PlainBlue:
                    sourceRectangle = PlainBlue(type);
                    break;
                case WallSpriteCategory.PlainBlueLight:
                    sourceRectangle = PlainBlueLight(type);
                    break;
                case WallSpriteCategory.PlainBrown:
                    sourceRectangle = PlainBrown(type);
                    break;
                case WallSpriteCategory.PlainDark:
                    sourceRectangle = PlainDark(type);
                    break;
                case WallSpriteCategory.PlainGrey:
                    sourceRectangle = PlainGrey(type);
                    break;
                case WallSpriteCategory.PlainGreyDark:
                    sourceRectangle = PlainGreyDark(type);
                    break;
                case WallSpriteCategory.PlainTan:
                    sourceRectangle = PlainTan(type);
                    break;
                case WallSpriteCategory.PlainTanDark:
                    sourceRectangle = PlainTanDark(type);
                    break;

                case WallSpriteCategory.ShinyBlue:
                    sourceRectangle = ShinyBlue(type);
                    break;
                case WallSpriteCategory.ShinyBlueDark:
                    sourceRectangle = ShinyBlueDark(type);
                    break;
                case WallSpriteCategory.ShinyBlueLight:
                    sourceRectangle = ShinyBlueLight(type);
                    break;
                case WallSpriteCategory.ShinyBrown:
                    sourceRectangle = ShinyBrown(type);
                    break;
                case WallSpriteCategory.ShinyGreen:
                    sourceRectangle = ShinyGreen(type);
                    break;
                case WallSpriteCategory.ShinyGreenBright:
                    sourceRectangle = ShinyGreenBright(type);
                    break;
                case WallSpriteCategory.ShinyGreenDark:
                    sourceRectangle = ShinyGreenDark(type);
                    break;
                case WallSpriteCategory.ShinyGreenLight:
                    sourceRectangle = ShinyGreenLight(type);
                    break;
                case WallSpriteCategory.ShinyTanDark:
                    sourceRectangle = ShinyTanDark(type);
                    break;
                case WallSpriteCategory.ShinyTan:
                    sourceRectangle = ShinyTan(type);
                    break;
                case WallSpriteCategory.ShinyTanLight:
                    sourceRectangle = ShinyTanLight(type);
                    break;
                case WallSpriteCategory.ShinyWhite:
                    sourceRectangle = ShinyWhite(type);
                    break;

                case WallSpriteCategory.WoodBrown:
                    sourceRectangle = WoodBrown(type);
                    break;
                case WallSpriteCategory.WoodBrownDark:
                    sourceRectangle = WoodBrownDark(type);
                    break;
                case WallSpriteCategory.WoodDark:
                    sourceRectangle = WoodDark(type);
                    break;
                case WallSpriteCategory.WoodTan:
                    sourceRectangle = WoodTan(type);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different floor categories, calls Sprite()
        private Rectangle BrickBlue(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.BrickBlueTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.BrickBlueMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.BrickBlueTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.BrickBlueMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.BrickBlueBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.BrickBlueBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.BrickBlueOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.BrickBlueOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.BrickBlueOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.BrickBlueOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.BrickBlueOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.BrickDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.BrickDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.BrickDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.BrickDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.BrickDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.BrickDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.BrickDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.BrickDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.BrickDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.BrickDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.BrickDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickGrey(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.BrickGreyTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.BrickGreyMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.BrickGreyTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.BrickGreyMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.BrickGreyBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.BrickGreyBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.BrickGreyOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.BrickGreyOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.BrickGreyOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.BrickGreyOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.BrickGreyOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickSilver(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.BrickSilverTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.BrickSilverMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.BrickSilverTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.BrickSilverMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.BrickSilverBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.BrickSilverBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.BrickSilverOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.BrickSilverOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.BrickSilverOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.BrickSilverOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.BrickSilverOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle ClayBlue(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayBlueTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayBlueMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayBlueTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayBlueMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayBlueBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayBlueBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayBlueOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayBlueOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayBlueOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayBlueOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayBlueOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayBrown(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayBrownMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayBrownMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayBrownOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayBrownOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayBrownOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayBrownOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayBrownOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayBrownDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayBrownDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayBrownGrey(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayBrownGreyOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayGrey(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayGreyTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayGreyMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayGreyTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayGreyMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayGreyBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayGreyBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayGreyOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayGreyOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayGreyOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayGreyOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayGreyOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayOrange(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayOrangeOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayRed(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayRedTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayRedMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayRedTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayRedMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayRedBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayRedBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayRedOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayRedOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayRedOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayRedOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayRedOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayRedBright(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayRedBrightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClaySilver(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClaySilverTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClaySilverMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClaySilverTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClaySilverMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClaySilverBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClaySilverBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClaySilverOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClaySilverOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClaySilverOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClaySilverOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClaySilverOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayTan(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayTanTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayTanMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayTanTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayTanMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayTanBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayTanBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayTanOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayTanOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayTanOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayTanOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayTanOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayTanBright(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ClayTanBrightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle IceBlue(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.IceBlueTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.IceBlueMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.IceBlueTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.IceBlueMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.IceBlueBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.IceBlueBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.IceBlueOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.IceBlueOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.IceBlueOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.IceBlueOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.IceBlueOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle IceBlueDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.IceBlueDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle IceBlueLight(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.IceBlueLightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle IceWhite(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.IceWhiteTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.IceWhiteMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.IceWhiteTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.IceWhiteMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.IceWhiteBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.IceWhiteBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.IceWhiteOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.IceWhiteOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.IceWhiteOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.IceWhiteOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.IceWhiteOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle MetalBrown(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.MetalBrownTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.MetalBrownMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.MetalBrownTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.MetalBrownMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.MetalBrownBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.MetalBrownBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.MetalBrownOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.MetalBrownOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.MetalBrownOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.MetalBrownOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.MetalBrownOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle MetalBrownDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.MetalBrownDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle MetalDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.MetalDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.MetalDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.MetalDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.MetalDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.MetalDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.MetalDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.MetalDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.MetalDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.MetalDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.MetalDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.MetalDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle MetalTan(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.MetalTanTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.MetalTanMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.MetalTanTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.MetalTanMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.MetalTanBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.MetalTanBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.MetalTanOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.MetalTanOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.MetalTanOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.MetalTanOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.MetalTanOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle PlainBlue(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainBlueTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainBlueMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainBlueTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainBlueMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainBlueBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainBlueBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainBlueOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainBlueOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainBlueOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainBlueOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainBlueOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainBlueLight(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainBlueLightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainBrown(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainBrownTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainBrownMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainBrownTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainBrownMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainBrownBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainBrownBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainBrownOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainBrownOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainBrownOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainBrownOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainBrownOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainGrey(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainGreyTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainGreyMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainGreyTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainGreyMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainGreyBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainGreyBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainGreyOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainGreyOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainGreyOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainGreyOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainGreyOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainGreyDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainGreyDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainTan(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainTanTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainTanMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainTanTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainTanMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainTanBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainTanBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainTanOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainTanOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainTanOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainTanOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainTanOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle PlainTanDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.PlainTanDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle ShinyBlue(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyBlueDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyBlueLight(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyBlueLightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyBrown(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyBrownOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyGreen(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyGreenBright(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenBrightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyGreenDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyGreenLight(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyGreenLightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyTan(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyTanMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyTanMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyTanOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyTanOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyTanOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyTanOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyTanOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyTanDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyTanDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyTanLight(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyTanLightOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ShinyWhite(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.ShinyWhiteOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle WoodBrown(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.WoodBrownTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.WoodBrownMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.WoodBrownTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.WoodBrownMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.WoodBrownBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.WoodBrownBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.WoodBrownOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.WoodBrownOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.WoodBrownOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.WoodBrownOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.WoodBrownOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle WoodBrownDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.WoodBrownDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle WoodDark(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.WoodDarkTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.WoodDarkMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.WoodDarkTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.WoodDarkMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.WoodDarkBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.WoodDarkBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.WoodDarkOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.WoodDarkOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.WoodDarkOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.WoodDarkOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.WoodDarkOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle WoodTan(WallSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case WallSpriteType.TopL:
                    sourceRectangle = Sprite(WallSprite.WoodTanTopL);
                    break;
                case WallSpriteType.MidH:
                    sourceRectangle = Sprite(WallSprite.WoodTanMidH);
                    break;
                case WallSpriteType.TopR:
                    sourceRectangle = Sprite(WallSprite.WoodTanTopR);
                    break;
                case WallSpriteType.MidV:
                    sourceRectangle = Sprite(WallSprite.WoodTanMidV);
                    break;
                case WallSpriteType.BotL:
                    sourceRectangle = Sprite(WallSprite.WoodTanBotL);
                    break;
                case WallSpriteType.BotR:
                    sourceRectangle = Sprite(WallSprite.WoodTanBotR);
                    break;
                case WallSpriteType.OverlapTop:
                    sourceRectangle = Sprite(WallSprite.WoodTanOverlapTop);
                    break;
                case WallSpriteType.OverlapRight:
                    sourceRectangle = Sprite(WallSprite.WoodTanOverlapRight);
                    break;
                case WallSpriteType.OverlapBot:
                    sourceRectangle = Sprite(WallSprite.WoodTanOverlapBot);
                    break;
                case WallSpriteType.OverlapLeft:
                    sourceRectangle = Sprite(WallSprite.WoodTanOverlapLeft);
                    break;
                case WallSpriteType.Intersection:
                    sourceRectangle = Sprite(WallSprite.WoodTanOverlapIntersection);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(WallSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(WallSprite sprite)
        {
            SpriteWallDictionary.SpritesWall.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}
