﻿/********************************************************************************
* Return specified sprite source rectang from Floor sprite atlas texture        *
* using:                                                                        *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       FloorSpriteDictionary.SpritesFloor                                      *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding Floor   *
*       enum as parameter                                                       *
*   3. Sprite() calls SpritePosition() with Floor enum as parameter             *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Floor enum as key, returns positionStart to Sprite()                    *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class FloorSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public FloorSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(FloorSpriteCategory categoryFloor, FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (categoryFloor)
            {
                case FloorSpriteCategory.BrickBrown:
                    sourceRectangle = BrickBrown(type);
                    break;
                case FloorSpriteCategory.BrickBlue:
                    sourceRectangle = BrickBlue(type);
                    break;
                case FloorSpriteCategory.BrickGrey:
                    sourceRectangle = BrickGrey(type);
                    break;
                case FloorSpriteCategory.BrickGreyBrown:
                    sourceRectangle = BrickGreyBrown(type);
                    break;
                case FloorSpriteCategory.BrickGreyDark:
                    sourceRectangle = BrickGreyDark(type);
                    break;
                case FloorSpriteCategory.BrickGreyLight:
                    sourceRectangle = BrickGreyLight(type);
                    break;
                case FloorSpriteCategory.BrickOrange:
                    sourceRectangle = BrickOrange(type);
                    break;
                case FloorSpriteCategory.BrickTan:
                    sourceRectangle = BrickTan(type);
                    break;
                case FloorSpriteCategory.ClayDark:
                    sourceRectangle = ClayDark(type);
                    break;
                case FloorSpriteCategory.ClayBrown:
                    sourceRectangle = ClayBrown(type);
                    break;
                case FloorSpriteCategory.ClayOrange:
                    sourceRectangle = ClayOrange(type);
                    break;
                case FloorSpriteCategory.ClayTan:
                    sourceRectangle = ClayTan(type);
                    break;
                case FloorSpriteCategory.FieldBrown:
                    sourceRectangle = FieldBrown(type);
                    break;
                case FloorSpriteCategory.FieldDark:
                    sourceRectangle = FieldDark(type);
                    break;
                case FloorSpriteCategory.FieldOrange:
                    sourceRectangle = FieldOrange(type);
                    break;
                case FloorSpriteCategory.FieldTan:
                    sourceRectangle = FieldTan(type);
                    break;
                case FloorSpriteCategory.GrassTan:
                    sourceRectangle = GrassTan(type);
                    break;
                case FloorSpriteCategory.GrassDark:
                    sourceRectangle = GrassDark(type);
                    break;
                case FloorSpriteCategory.GrassBrown:
                    sourceRectangle = GrassBrown(type);
                    break;
                case FloorSpriteCategory.GrassOrange:
                    sourceRectangle = GrassOrange(type);
                    break;
                case FloorSpriteCategory.SandBrown:
                    sourceRectangle = SandBrown(type);
                    break;
                case FloorSpriteCategory.SandDark:
                    sourceRectangle = SandDark(type);
                    break;
                case FloorSpriteCategory.SandOrange:
                    sourceRectangle = SandOrange(type);
                    break;
                case FloorSpriteCategory.SandTan:
                    sourceRectangle = SandTan(type);
                    break;
                case FloorSpriteCategory.SnowBrown:
                    sourceRectangle = SnowBrown(type);
                    break;
                case FloorSpriteCategory.SnowDark:
                    sourceRectangle = SnowDark(type);
                    break;
                case FloorSpriteCategory.SnowOrange:
                    sourceRectangle = SnowOrange(type);
                    break;
                case FloorSpriteCategory.SnowTan:
                    sourceRectangle = SnowTan(type);
                    break;
            }
            return sourceRectangle;
        }

        // Handle return source for different floor categories, calls Sprite()
        private Rectangle BrickBrown(FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickBlue(FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickBlueSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickGrey(FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickGreySingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickGreyBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickGreyDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickGreyLight(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickGreyLightSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickOrange(FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle BrickTan(FloorSpriteType type)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.BrickTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.BrickTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.BrickTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.BrickTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.BrickTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.BrickTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.BrickTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.BrickTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.BrickTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.BrickTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.BrickTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle ClayBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.ClayBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.ClayDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayOrange(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.ClayOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle ClayTan(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.ClayTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.ClayTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.ClayTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.ClayTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.ClayTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.ClayTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.ClayTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.ClayTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.ClayTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.ClayTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.ClayTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle FieldBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.FieldBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FieldDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.FieldDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FieldOrange(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.FieldOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle FieldTan(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.FieldTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.FieldTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.FieldTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.FieldTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.FieldTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.FieldTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.FieldTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.FieldTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.FieldTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.FieldTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.FieldTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle GrassBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.GrassBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.GrassDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassOrange(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.GrassOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle GrassTan(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.GrassTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.GrassTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.GrassTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.GrassTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.GrassTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.GrassTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.GrassTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.GrassTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.GrassTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.GrassTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.GrassTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle SandBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SandBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SandBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SandBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SandBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SandBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SandBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SandBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SandBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SandBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SandBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SandBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SandDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SandDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SandDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SandDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SandDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SandDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SandDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SandDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SandDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SandDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SandDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SandDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SandOrange(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SandOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SandTan(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SandTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SandTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SandTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SandTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SandTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SandTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SandTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SandTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SandTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SandTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SandTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        private Rectangle SnowBrown(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SnowBrownSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SnowDark(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SnowDarkSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SnowOrange(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SnowOrangeSingle);
                    break;
            }
            return sourceRectangle;
        }
        private Rectangle SnowTan(FloorSpriteType type)
        {
            // Source (position, size of sprite) rectangle in sprite atlas texture
            Rectangle sourceRectangle = new Rectangle();

            switch (type)
            {
                case FloorSpriteType.TopL:
                    sourceRectangle = Sprite(FloorSprite.SnowTanTopL);
                    break;
                case FloorSpriteType.TopM:
                    sourceRectangle = Sprite(FloorSprite.SnowTanTopM);
                    break;
                case FloorSpriteType.TopR:
                    sourceRectangle = Sprite(FloorSprite.SnowTanTopR);
                    break;
                case FloorSpriteType.MidL:
                    sourceRectangle = Sprite(FloorSprite.SnowTanMidL);
                    break;
                case FloorSpriteType.MidM:
                    sourceRectangle = Sprite(FloorSprite.SnowTanMidM);
                    break;
                case FloorSpriteType.MidR:
                    sourceRectangle = Sprite(FloorSprite.SnowTanMidR);
                    break;
                case FloorSpriteType.BotL:
                    sourceRectangle = Sprite(FloorSprite.SnowTanBotL);
                    break;
                case FloorSpriteType.BotM:
                    sourceRectangle = Sprite(FloorSprite.SnowTanBotM);
                    break;
                case FloorSpriteType.BotR:
                    sourceRectangle = Sprite(FloorSprite.SnowTanBotR);
                    break;
                case FloorSpriteType.CorVT:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorVT);
                    break;
                case FloorSpriteType.CorVM:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorVM);
                    break;
                case FloorSpriteType.CorVB:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorVB);
                    break;
                case FloorSpriteType.CorHL:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorHL);
                    break;
                case FloorSpriteType.CorHM:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorHM);
                    break;
                case FloorSpriteType.CorHR:
                    sourceRectangle = Sprite(FloorSprite.SnowTanCorHR);
                    break;
                case FloorSpriteType.Single:
                    sourceRectangle = Sprite(FloorSprite.SnowTanSingle);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(FloorSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(FloorSprite sprite)
        {
            FloorSpriteDictionary.SpritesFloor.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}