﻿/********************************************************************************
* Return specified sprite source rectang from Rodent0 sprite atlas texture      *
*   using:                                                                      *
*   1. [EnemyType]Sprite enum                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       [EnemyType]SpriteDictionary.SpritesRodent                               *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. SourceRectangleSprite()  calls SpritePosition() with rodent enum as      *
*       parameter                                                               *
*   3. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Avian enum as key, returns positionStart to Sprite()                    *
*   4. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to SourceRectangleSprite()                              *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;
using RogueProject.Enemies;

namespace RogueProject.Textures
{
    public class EnemyRodentSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public EnemyRodentSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Handle return source for different pest sprites
        public Rectangle SourceRectangleSprite(EnemyType enemyType)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (enemyType)
            {
                case EnemyType.RatBrownLarge:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatBrownLarge);
                    break;
                case EnemyType.RatBrownMedium:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatBrownMedium);
                    break;
                case EnemyType.RatBrownSmall:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatBrownSmall);
                    break;
                case EnemyType.RatGreen:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatGreen);
                    break;
                case EnemyType.RatGreyLarge:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatGreyLarge);
                    break;
                case EnemyType.RatLeech:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatLeech);
                    break;
                case EnemyType.RatTan:
                    sourceRectangle = Sprite(EnemyRodentSprite.RatTan);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(EnemyRodentSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(EnemyRodentSprite sprite)
        {
            EnemyRodentSpriteDictionary.SpritesRodent.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}
