﻿/********************************************************************************
* Return specified sprite source rectang from Player0 sprite atlas texture      *
* using:                                                                        *
*   1. [SpriteName]Category enum                                                *
*   2. [SpriteName]SpriteType                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       PlayerSpriteDictionary.SpritesPlayer                                 *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. Type-finder method uses type to call Sprite() with corresponding         *
*       Character enum as parameter                                             *
*   3. Sprite() calls SpritePosition() with Floor enum as parameter             *
*   4. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Character enum as key, returns positionStart to Sprite()                *    
*   5. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to type-finding method                                  *
*   6. Type-finding method returns rectangle to SourceRectangleSprite()         *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;

namespace RogueProject.Textures
{
    public class PlayerSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public PlayerSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Source (position, size of sprite) rectangle in sprite atlas texture
        public Rectangle SourceRectangleSprite(PlayerSprite sprite)
        {
            return Sprite(sprite);
        }

        // Handle return source for different floor categories, calls Sprite()

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(PlayerSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(PlayerSprite sprite)
        {
            PlayerSpriteDictionary.SpritesPlayer.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}