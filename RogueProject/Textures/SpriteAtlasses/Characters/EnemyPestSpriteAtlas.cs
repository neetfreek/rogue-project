﻿/********************************************************************************
* Return specified sprite source rectang from Pest0 sprite atlas texture        *
*   using:                                                                      *
*   1. [EnemyType]Sprite enum                                                   *
* The source rectangle includes:                                                *
*   1. Sprite positionStart in atlas, stored in                                 *
*       [EnemyType]SpriteDictionary.SpritesPest                                 *
*   2. Sprite size, defined in GlobalVariables.WIDTH_HEIGHT_SPRITE              *
* Control flow:                                                                 *
*   1. SourceRectangleSprite() uses category to call appropriate type finder    *
*       method using type as parameter                                          *
*   2. SourceRectangleSprite()  calls SpritePosition() with Pest enum as        *
*       parameter                                                               *
*   3. SpritePosition() gets Vector2() positionStart of sprite in atlas using   *
*       Pest enum as key, returns positionStart to Sprite()                     *
*   4. Sprite uses returned positionStart, GlobalVariables.WIDTH_HEIGHT_SPRITE  *
*   to return rectangle to SourceRectangleSprite()                              *
*********************************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;
using RogueProject.Enemies;

namespace RogueProject.Textures
{
    public class EnemyPestSpriteAtlas
    {
        // Sprite atlas texture
        private readonly Texture2D texture;
        // Sprite atlas texture information
        private readonly int columns;
        private readonly int rows;
        private readonly int numberSprites;
        private readonly int heightSprite;
        private readonly int widthSprite;

        public EnemyPestSpriteAtlas(Texture2D texture, int columns, int rows)
        {
            this.texture = texture;
            this.columns = columns;
            this.rows = rows;

            heightSprite = texture.Height / rows;
            widthSprite = texture.Width / columns;
            numberSprites = rows * columns;
        }

        // Handle return source for different pest sprites
        public Rectangle SourceRectangleSprite(EnemyType enemyType)
        {
            Rectangle sourceRectangle = new Rectangle();

            switch (enemyType)
            {
                case EnemyType.AntBlack:
                    sourceRectangle = Sprite(EnemyPestSprite.AntBlack);
                    break;
                case EnemyType.AntBlue:
                    sourceRectangle = Sprite(EnemyPestSprite.AntBlue);
                    break;
                case EnemyType.AntRed:
                    sourceRectangle = Sprite(EnemyPestSprite.AntRed);
                    break;
                case EnemyType.AntTan:
                    sourceRectangle = Sprite(EnemyPestSprite.AntTan);
                    break;

                case EnemyType.BeetleSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.BeetleSmall);
                    break;
                case EnemyType.BeetleMedium:
                    sourceRectangle = Sprite(EnemyPestSprite.BeetleMedium);
                    break;
                case EnemyType.BeetleLargeBlack:
                    sourceRectangle = Sprite(EnemyPestSprite.BeetleLargeBlack);
                    break;
                case EnemyType.BeetleLargeBrown:
                    sourceRectangle = Sprite(EnemyPestSprite.BeetleLargeBrown);
                    break;
                case EnemyType.BeetleLong:
                    sourceRectangle = Sprite(EnemyPestSprite.BeetleLong);
                    break;

                case EnemyType.Bee:
                    sourceRectangle = Sprite(EnemyPestSprite.Bee);
                    break;
                case EnemyType.BeeLarva:
                    sourceRectangle = Sprite(EnemyPestSprite.BeeLarva);
                    break;
                case EnemyType.Fly:
                    sourceRectangle = Sprite(EnemyPestSprite.Fly);
                    break;
                case EnemyType.FlyLarva:
                    sourceRectangle = Sprite(EnemyPestSprite.FlyLarva);
                    break;
                case EnemyType.Mosquito:
                    sourceRectangle = Sprite(EnemyPestSprite.Mosquito);
                    break;
                case EnemyType.MosquitoLarva:
                    sourceRectangle = Sprite(EnemyPestSprite.MosquitoLarva);
                    break;

                case EnemyType.BugGlowWings:
                    sourceRectangle = Sprite(EnemyPestSprite.BugGlowWings);
                    break;
                case EnemyType.BugGlowSparkle:
                    sourceRectangle = Sprite(EnemyPestSprite.BugGlowSparkle);
                    break;
                case EnemyType.BugSparkleSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.BugSparkleSmall);
                    break;
                case EnemyType.BugSparkleLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.BugSparkleLarge);
                    break;

                case EnemyType.GrasshopperSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.GrasshopperSmall);
                    break;
                case EnemyType.GrasshopperLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.GrasshopperLarge);
                    break;
                case EnemyType.LocustSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.LocustSmall);
                    break;
                case EnemyType.LocustLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.LocustLarge);
                    break;

                case EnemyType.LadyBugBrown:
                    sourceRectangle = Sprite(EnemyPestSprite.LadyBugBrown);
                    break;
                case EnemyType.LadyBugRed:
                    sourceRectangle = Sprite(EnemyPestSprite.LadyBugRed);
                    break;

                case EnemyType.LeechTanSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.LeechTanSmall);
                    break;
                case EnemyType.LeechTan:
                    sourceRectangle = Sprite(EnemyPestSprite.LeechTan);
                    break;
                case EnemyType.LeechRedSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.LeechRedSmall);
                    break;
                case EnemyType.LeechRed:
                    sourceRectangle = Sprite(EnemyPestSprite.LeechRed);
                    break;

                case EnemyType.SlugSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.SlugSmall);
                    break;
                case EnemyType.SlugLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.SlugLarge);
                    break;
                case EnemyType.SnailSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.SnailSmall);
                    break;
                case EnemyType.SnailLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.SnailLarge);
                    break;

                case EnemyType.ScorpionSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.ScorpionSmall);
                    break;
                case EnemyType.ScorpionMedium:
                    sourceRectangle = Sprite(EnemyPestSprite.ScorpionMedium);
                    break;
                case EnemyType.ScorpionLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.ScorpionLarge);
                    break;

                case EnemyType.SpiderSmall:
                    sourceRectangle = Sprite(EnemyPestSprite.SpiderSmall);
                    break;
                case EnemyType.SpiderMediumBrown:
                    sourceRectangle = Sprite(EnemyPestSprite.SpiderMediumBrown);
                    break;
                case EnemyType.SpiderMediumGrey:
                    sourceRectangle = Sprite(EnemyPestSprite.SpiderMediumGrey);
                    break;
                case EnemyType.SpiderLarge:
                    sourceRectangle = Sprite(EnemyPestSprite.SpiderLarge);
                    break;

                case EnemyType.WormBrown:
                    sourceRectangle = Sprite(EnemyPestSprite.WormBrown);
                    break;
                case EnemyType.WormTan:
                    sourceRectangle = Sprite(EnemyPestSprite.WormTan);
                    break;
            }
            return sourceRectangle;
        }

        // Return source (position, size of sprite) rectangle in sprite atlas texture
        private Rectangle Sprite(EnemyPestSprite nameSprite)
        {
            Vector2 spritePosition = SpritePosition(nameSprite);

            return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, GlobalVariables.WIDTH_HEIGHT_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE);
        }

        // Return sprite positionStart in sprite atlas texture
        private Vector2 SpritePosition(EnemyPestSprite sprite)
        {
            EnemyPestSpriteDictionary.SpritesPest.TryGetValue(sprite, out Vector2 spritePosition);
            spritePosition.X *= GlobalVariables.WIDTH_HEIGHT_SPRITE;
            spritePosition.Y *= GlobalVariables.WIDTH_HEIGHT_SPRITE;

            return spritePosition;
        }
    }
}