﻿/********************************************
* Contain all references to GUI             *
* enums, used for organising and retriveing *
*   GUI sprites for drawing.                *
*********************************************/

namespace RogueProject.Textures
{
    public enum GUISprite
    {
        None,

        // Cursor
        Cursor,

        // Bars
        BarBorderL,
        BarBorderM,
        BarBorderR,
        BarBlue100End,
        BarBlue100,
        BarBlue75,
        BarBlue50,
        BarBlue25,
        BarRed100End,
        BarRed100,
        BarRed75,
        BarRed50,
        BarRed25,
        BarYellow100End,
        BarYellow100,
        BarYellow75,
        BarYellow50,
        BarYellow25,
        Bar0,

        // Arrows
        ArrowUp,
        ArrowDown,

        // Squares: used to represent grid positions
        SquareBlackGrey,
        SquareBlackGreyDark,
        SquareGrey,
        SquareGreyDark,

        // Rectangles: panel, button portions
        RectBlackGreyTL,
        RectBlackGreyTM,
        RectBlackGreyTR,
        RectBlackGreyML,
        RectBlackGreyMM,
        RectBlackGreyMR,
        RectBlackGreyBL,
        RectBlackGreyBM,
        RectBlackGreyBR,

        RectGreyTL,
        RectGreyTM,
        RectGreyTR,
        RectGreyML,
        RectGreyMM,
        RectGreyMR,
        RectGreyBL,
        RectGreyBM,
        RectGreyBR,

        RectGreyDarkTL,
        RectGreyDarkTM,
        RectGreyDarkTR,
        RectGreyDarkML,
        RectGreyDarkMM,
        RectGreyDarkMR,
        RectGreyDarkBL,
        RectGreyDarkBM,
        RectGreyDarkBR,

        RectGreyLightTL,
        RectGreyLightTM,
        RectGreyLightTR,
        RectGreyLightML,
        RectGreyLightMM,
        RectGreyLightMR,
        RectGreyLightBL,
        RectGreyLightBM,
        RectGreyLightBR,
    }

    public enum GUISpriteCategory
    {
        None,

        Cursor,

        RectBlackGrey,
        RectGrey,
        RectGreyLight,
        RectGreyDark,
        Square,

        // Bars
        BarBorder,
        BarBlue,
        BarRed,
        BarYellow,

        Arrow,
    }

    public enum GUISpriteType
    {
        Cursor,

        // Squares
        SquareBlackGrey,
        SquareBlackGreyDark,
        SquareGrey,
        SquareGreyDark,

        ArrowUp,
        ArrowDown,

        // Rectangles
        None,
        TopL,
        TopM,
        TopR,
        MidL,
        MidM,
        MidR,
        BotL,
        BotM,
        BotR,

        BarBorderL,
        BarBorderM,
        BarBorderR,
        Bar100End,
        Bar100,
        Bar75,
        Bar50,
        Bar25,
        Bar0

        // Hearts
        // SpeechBubbles
        // Icons
    }
}
