﻿/********************************************
* Contain all references to door            *
* enums, used for organising and retriveing *
*   door sprites for drawing.               *
*********************************************/

namespace RogueProject.Textures
{
    public enum DoorSprite
    {
        None,
        Blank,

        Arch,
        ArchDark,
        ArchGold,
        ArchSilver,

        BarsGoldH,
        BarsGoldHOpen,
        BarsGoldHBroken,
        BarsGoldV,
        BarsGoldChainH,
        BarsGoldChainV,
        BarsGoldChainGoldH,
        BarsGoldChainGoldV,

        BarsIronH,
        BarsIronHOpen,
        BarsIronHBroken,
        BarsIronV,
        BarsIronChainH,
        BarsIronChainV,
        BarsIronChainGoldH,
        BarsIronChainGoldV,

        ChainsH,
        ChainsV,
        ChainsGoldH,
        ChainsGoldV,

        DoorMetalH,
        DoorMetalHOpen,
        DoorMetalHBroken,
        DoorMetalV,
        DoorMetalVOpen,
        DoorMetalChainH,
        DoorMetalChainV,
        DoorMetalChainGoldH,
        DoorMetalChainGoldV,

        DoorWoodH,
        DoorWoodHOpen,
        DoorWoodHBroken,
        DoorWoodV,
        DoorWoodVOpen1,
        DoorWoodChainH,
        DoorWoodChainV,
        DoorWoodChainGoldH,
        DoorWoodChainGoldV,

        Portal1,
        PortalGold1,
        PortalSilver1,
        PortalDark1,
        Portal2,
        PortalGold2,
        PortalSilver2,
        PortalDark2,
    }

    public enum DoorSpriteCategory
    {
        None,
        DoorWood,
        DoorMetal,

        BarsGold,
        BarsGoldBroken,
        BarsGoldChain,
        BarsGoldChainGold,

        BarsIron,
        BarsIronBroken,        
        BarsIronChain,
        BarsIronChainGold,

        Chains,
        ChainsGold,

        DoorMetalBroken,
        DoorMetalChain,
        DoorMetalChainGold,

        DoorWoodBroken,
        DoorWoodChain,
        DoorWoodChainGold,

        Arch,
        Portal,
    }

    public enum DoorSpriteOrientation
    {
        None,
        Horizontal,
        Vertical,
    }

    public enum DoorState
    {
        Closed,
        Open,
    }
}