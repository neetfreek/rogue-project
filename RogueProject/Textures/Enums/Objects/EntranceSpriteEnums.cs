﻿/********************************************
* Contain all references to entrance        *
* enums, used for organising and retriveing *
*   entrance sprites for drawing.           *
*********************************************/

namespace RogueProject.Textures
{
    public enum EntranceSprite
    {
        None,
        Arch,
        Ladder,
        Stairs,
    }
}