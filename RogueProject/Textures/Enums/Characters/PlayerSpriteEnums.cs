﻿/********************************************
* Contain all references to character       *
* enums, used for organising and retriveing *
*   character sprites for drawing.          *
*********************************************/

namespace RogueProject.Textures
{
    public enum PlayerSprite
    {
        None,
        Leather,
        NoneFemale,
        NoneMale,
        Robe,
        RobeHooded,
        Chainmail,
        Plate,
        PortraitFemale,
        PortraitMale,
    }
}