﻿/********************************************
* Contain all references to rodent          *
* enums, used for organising and retriveing *
*   rodent sprites for drawing.             *
*********************************************/

namespace RogueProject.Textures
{
    public enum EnemyRodentSprite
    {
        RatBrownSmall,
        RatBrownMedium,
        RatBrownLarge,
        RatGreyLarge,
        RatGreen,
        RatLeech,
        RatTan,
    }
}