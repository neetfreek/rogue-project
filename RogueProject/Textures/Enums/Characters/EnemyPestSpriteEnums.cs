﻿/********************************************
* Contain all references to pest            *
* enums, used for organising and retriveing *
*   pest sprites for drawing.               *
*********************************************/

namespace RogueProject.Textures
{
    public enum EnemyPestSprite
    {
        AntTan,
        AntBlack,
        AntRed,
        AntBlue,

        BeetleSmall,
        BeetleMedium,
        BeetleLargeBlack,
        BeetleLargeBrown,
        BeetleLong,

        Bee,
        BeeLarva,
        Fly,
        FlyLarva,
        Mosquito,
        MosquitoLarva,

        BugGlowWings,
        BugGlowSparkle,
        BugSparkleSmall,
        BugSparkleLarge,

        GrasshopperSmall,
        GrasshopperLarge,
        LocustSmall,
        LocustLarge,

        LadyBugBrown,
        LadyBugRed,

        LeechTanSmall,
        LeechTan,
        LeechRedSmall,
        LeechRed,

        SlugSmall,
        SlugLarge,
        SnailSmall,
        SnailLarge,

        ScorpionSmall,
        ScorpionMedium,
        ScorpionLarge,

        SpiderSmall,
        SpiderMediumBrown,
        SpiderMediumGrey,
        SpiderLarge,

        WormBrown,
        WormTan,
    }
}