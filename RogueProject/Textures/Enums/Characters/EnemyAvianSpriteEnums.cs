﻿/********************************************
* Contain all references to pest            *
* enums, used for organising and retriveing *
*   pest sprites for drawing.               *
*********************************************/

namespace RogueProject.Textures
{
    public enum EnemyAvianSprite
    {
        BatBrownSmall,
        BatBrownLarge,
        BatBlackLarge,
        BatTanSmall,
        BatTanLarge,
        
        BirdFluffSnow,

        ChickenSnowLarge,
        ChickenSnowSmall,

        EagleSnowLarge,
        EagleSnowSmall,

        KiteSnowSmall,
        KiteSnowLarge,
        KiteSnowTanSmall,
        KiteSnowTanLarge,
    }
}