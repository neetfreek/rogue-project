﻿/********************************************
* Contain all references to weapon          *
* enums, used for organising and retriveing *
*   weapon sprites for drawing.             *
*********************************************/

namespace RogueProject.Textures
{
    public enum WeaponSprite
    {
        None,
        // Ranged
        Bow, 
        BowLongGreen, 
        BowLongWhite, 
        BowShort, 
        Crossbow, 
        Sling, 
        // Melee
        Axe, 
        AxeDouble, 
        Bardiche,
        Blade, 
        BladeCurved, 
        CutlassLight, 
        CutlassDark, 
        ClubShortThin, 
        ClubShortThick, 
        ClubLong, 
        DirkDark, 
        DirkLight, 
        DaggerLight, 
        DaggerDark, 
        DaggerCurved, 
        Flail, 
        Glaive,
        GreatSwordLight, 
        GreatSwordDark, 
        Halberd,
        KnifeBlue, 
        KnifeYellow, 
        LongSword, 
        Mallet, 
        Mace, 
        MaceFlanged, 
        PickaxeSmall, 
        PickaxeMedium, 
        PickaxeLarge, 
        Polearm,
        RodPlain, 
        RodSparkle, 
        Scythe,  
        ShortSwordLight,
        ShortSwordDark, 
        ShortSwordGold, 
        SpearLight,
        SpearDark, 
        SpearGold, 
        Stiletto, 
        WhipGreen, 
        WhipBrown, 
    }
}