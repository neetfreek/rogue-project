﻿/********************************************
* Contain all references to container       *
* enums, used for organising and retriveing *
*   container sprites for drawing.          *
*********************************************/

namespace RogueProject.Textures
{
    public enum ContainerSprite
    {
        None,
        ChestWood,
        ChestWoodTall,
        ChestWoodBroken,
        ChestMetal,
        ChestMetalTall,
        ChestMetalBroken,
        ChestShiny,
        ChestShinyBroken,
        BarrelBlue,
        BarrelBlueBroken,
        BarrelGold,
        BarrelGoldBroken,
        SackBrown,
        SackBrownGlow,
        SackRed,
        SackGreen,
    }
}