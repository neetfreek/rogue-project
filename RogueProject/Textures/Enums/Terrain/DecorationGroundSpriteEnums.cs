﻿/********************************************
* Contain all references to outdoor         *
*   decoration enums on ground, used for    *
*   organising and retriveing sprites for   *
*   drawing                                 *
*********************************************/

namespace RogueProject.Textures
{
    // Sprites in ground decoration sprite atlas texture
    public enum DecorationGroundSprite
    {
        FlowersWhiteDense,
        FlowersWhiteSparse,
        FlowersBlueDense,
        FlowersBlueSparse,
        FlowersYellowDense,
        FlowersYellowSparse,
        FlowersRedDense,
        FlowersRedSparse,

        GrassGreenDense,
        GrassGreenSparse,
        GrassGreenPatchDense,
        GrassGreenPatchSparse,

        GrassOrangeDense,
        GrassOrangeSparse,
        GrassOrangePatchDense,
        GrassOrangePatchSparse,

        StonesBlueSmallDense,
        StonesBlueLargeDense,
        StonesBlueSmallSparse,
        StonesBlueLargeSparse,
        StonesRedSmallDense,
        StonesRedLargeDense,
        StonesRedSmallSparse,
        StonesRedLargeSparse,
    }

    public enum DecorationGroundSpriteCategory
    {
        None,
        GrassGreen,
        GrassOrange,
        GrassPatchGreen,
        GrassPatchOrange,
        FlowerYellow,
        FlowerRed,
        StonesRedLarge,
        StonesRedSmall,
        FlowerWhite,
        FlowerBlue,
        StonesBlueLarge,
        StonesBlueSmall,
    }

    public enum DecorationGroundSpriteType
    {
        None,
        Dense,
        Sparse,
    }
}