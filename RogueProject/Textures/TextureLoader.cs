﻿/********************************************************
* Loads and contains all references to texture assets   * 
*********************************************************/
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RogueProject.Textures
{
    public class TextureLoader
    {
        // Fonts
        public SpriteFont Font;
        // Character textures
        public Texture2D TextureEnemyAvian;
        public Texture2D TextureEnemyPest;
        public Texture2D TextureEnemyRodent;
        public Texture2D TexturePlayer;
        // Environment textures
        public Texture2D TextureDecorationGround;
        public Texture2D TextureEntrance;
        public Texture2D TextureFloor;
        public Texture2D TextureTree;
        public Texture2D TextureWall;
        // Object textures
        public Texture2D TextureDoor;
        public Texture2D TextureScroll;
        // GUI Textures
        public Texture2D TextureGUI;
        // Item textures
        public Texture2D TextureArmour;
        public Texture2D TextureContainer;
        public Texture2D TextureWeapon;

        // Character sprite atlasses
        public EnemyAvianSpriteAtlas EnemyAvianAtlas;
        public EnemyPestSpriteAtlas EnemyPestAtlas;
        public EnemyRodentSpriteAtlas EnemyRodentAtlas;
        public PlayerSpriteAtlas PlayerAtlas;
        // Environment sprite atlasses
        public DecorationGroundSpriteAtlas DecorationGroundAtlas;
        public EntranceSpriteAtlas EntranceAtlas;
        public FloorSpriteAtlas FloorAtlas;
        public TreeSpriteAtlas TreeSpriteAtlas;
        public WallSpriteAtlas WallAtlas;
        // Object sprite atlasses
        public DoorSpriteAtlas DoorAtlas;
        public ScrollSpriteAtlas ScrollAtlas;
        // GUI sprite atlasses
        public GUISpriteAtlas GUIAtlas;
        // Item sprite atlasses
        public ArmourSpriteAtlas ArmourSpriteAtlas;
        public ContainerSpriteAtlas ContainerAtlas;
        public WeaponSpriteAtlas WeaponSpriteAtlas;

        // Multi-sprite atlasses
        public GUIIconSpriteAtlas GUIIconSpriteAtlas;


        public void LoadContent(ContentManager contentManager)
        {
            LoadFonts(contentManager);
            LoadTextures(contentManager);
            LoadAtlasses(contentManager);
        }

        private void LoadFonts(ContentManager contentManager)
        {
            Font = contentManager.Load<SpriteFont>("Fonts/SDS6x6");
        }

        private void LoadTextures(ContentManager contentManager)
        {        
            TextureArmour = contentManager.Load<Texture2D>("Textures/Items/Armour");
            TextureContainer = contentManager.Load<Texture2D>("Textures/Items/Container0");
            TextureDoor = contentManager.Load<Texture2D>("Textures/Objects/Door");
            TextureDecorationGround = contentManager.Load<Texture2D>("Textures/Terrain/Ground0");
            TextureEntrance = contentManager.Load<Texture2D>("Textures/Objects/Entrance");
            TextureFloor = contentManager.Load<Texture2D>("Textures/Terrain/Floor");
            TextureGUI = contentManager.Load<Texture2D>("Textures/GUI/GUI0");
            TextureEnemyAvian = contentManager.Load<Texture2D>("Textures/Characters/Avian0");
            TextureEnemyPest = contentManager.Load<Texture2D>("Textures/Characters/Pest0");
            TextureEnemyRodent = contentManager.Load<Texture2D>("Textures/Characters/Rodent0");
            TexturePlayer = contentManager.Load<Texture2D>("Textures/Characters/Player0");
            TextureScroll = contentManager.Load<Texture2D>("Textures/Objects/Scroll");
            TextureTree = contentManager.Load<Texture2D>("Textures/Terrain/Tree0");
            TextureWall = contentManager.Load<Texture2D>("Textures/Terrain/Wall");
            TextureWeapon = contentManager.Load<Texture2D>("Textures/Items/Weapon");
        }

        private void LoadAtlasses(ContentManager contentManager)
        {
            ContainerAtlas = new ContainerSpriteAtlas(TextureContainer, 8, 3);
            DoorAtlas = new DoorSpriteAtlas(TextureDoor, 8, 10);
            DecorationGroundAtlas = new DecorationGroundSpriteAtlas(TextureDoor, 7, 6);
            EnemyAvianAtlas = new EnemyAvianSpriteAtlas(TextureEnemyAvian, 8, 13);
            EnemyPestAtlas = new EnemyPestSpriteAtlas(TextureEnemyPest, 8, 11);
            EnemyRodentAtlas = new EnemyRodentSpriteAtlas(TextureEnemyRodent, 8, 4);
            EntranceAtlas = new EntranceSpriteAtlas(TextureEntrance, 3, 1);
            FloorAtlas = new FloorSpriteAtlas(TextureFloor, 21, 39);
            GUIAtlas = new GUISpriteAtlas(TextureGUI, 16, 19);
            PlayerAtlas = new PlayerSpriteAtlas(TexturePlayer, 8, 15);
            ScrollAtlas = new ScrollSpriteAtlas(TextureScroll, 8, 6);
            TreeSpriteAtlas = new TreeSpriteAtlas(TexturePlayer, 12, 36);
            WallAtlas = new WallSpriteAtlas(TextureWall, 20, 51);
            WeaponSpriteAtlas = new WeaponSpriteAtlas(TextureWeapon, 8, 15);
            ArmourSpriteAtlas = new ArmourSpriteAtlas(TextureArmour, 15, 13);

            GUIIconSpriteAtlas = new GUIIconSpriteAtlas(this);
        }
    }   
}