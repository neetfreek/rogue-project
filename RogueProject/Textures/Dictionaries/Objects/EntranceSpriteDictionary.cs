﻿/********************************************************************
* Contains dictionary with entrance sprites keys and associated     *
*   Vector2() positions (unscaled) in entrance sprite atlas texture *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class EntranceSpriteDictionary
    {
        // Map EntranceSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<EntranceSprite, Vector2> SpritesEntrance = new Dictionary<EntranceSprite, Vector2>()
        {
            {EntranceSprite.Arch,     new Vector2(2f, 0f)},
            {EntranceSprite.Ladder,   new Vector2(1f, 0f)},
            {EntranceSprite.Stairs,   new Vector2(0f, 0f)},
        };
    }
}