﻿/********************************************************************
* Contains dictionary with door sprites keys and associated         *
*   Vector2() positions (unscaled) in door sprite atlas texture     *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class DoorSpriteDictionary
    {
        // Map DoorSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<DoorSprite, Vector2> SpritesDoor = new Dictionary<DoorSprite, Vector2>()
        {
            {DoorSprite.Blank,                  new Vector2(0f, 4f)},

            {DoorSprite.Arch,                   new Vector2(0f, 5f)},
            {DoorSprite.ArchDark,               new Vector2(6f, 5f)},
            {DoorSprite.ArchGold,               new Vector2(2f, 5f)},
            {DoorSprite.ArchSilver,             new Vector2(4f, 5f)},

            {DoorSprite.BarsGoldH,              new Vector2(0f, 3f)},
            {DoorSprite.BarsGoldHOpen,          new Vector2(1f, 8f)},
            {DoorSprite.BarsGoldHBroken,        new Vector2(7f, 3f)},
            {DoorSprite.BarsGoldV,              new Vector2(1f, 3f)},
            {DoorSprite.BarsGoldChainH,         new Vector2(2f, 3f)},
            {DoorSprite.BarsGoldChainV,         new Vector2(3f, 3f)},
            {DoorSprite.BarsGoldChainGoldH,     new Vector2(4f, 3f)},
            {DoorSprite.BarsGoldChainGoldV,     new Vector2(5f, 3f)},

            {DoorSprite.BarsIronH,              new Vector2(0f, 2f)},
            {DoorSprite.BarsIronHOpen,          new Vector2(0f, 8f)},
            {DoorSprite.BarsIronHBroken,        new Vector2(7f, 2f)},
            {DoorSprite.BarsIronV,              new Vector2(1f, 2f)},
            {DoorSprite.BarsIronChainH,         new Vector2(2f, 2f)},
            {DoorSprite.BarsIronChainV,         new Vector2(3f, 2f)},
            {DoorSprite.BarsIronChainGoldH,     new Vector2(4f, 2f)},
            {DoorSprite.BarsIronChainGoldV,     new Vector2(5f, 2f)},

            {DoorSprite.ChainsH,                new Vector2(2f, 4f)},
            {DoorSprite.ChainsV,                new Vector2(3f, 4f)},
            {DoorSprite.ChainsGoldH,            new Vector2(4f, 4f)},
            {DoorSprite.ChainsGoldV,            new Vector2(5f, 4f)},

            {DoorSprite.DoorMetalH,             new Vector2(0f, 1f)},
            {DoorSprite.DoorMetalHOpen,         new Vector2(0f, 7f)},
            {DoorSprite.DoorMetalHBroken,       new Vector2(7f, 1f)},
            {DoorSprite.DoorMetalV,             new Vector2(1f, 1f)},
            {DoorSprite.DoorMetalVOpen,         new Vector2(1f, 7f)},
            {DoorSprite.DoorMetalChainH,        new Vector2(2f, 1f)},
            {DoorSprite.DoorMetalChainV,        new Vector2(3f, 1f)},
            {DoorSprite.DoorMetalChainGoldH,    new Vector2(4f, 1f)},
            {DoorSprite.DoorMetalChainGoldV,    new Vector2(5f, 1f)},

            {DoorSprite.DoorWoodH,              new Vector2(0f, 0f)},
            {DoorSprite.DoorWoodHOpen,          new Vector2(0f, 6f)},
            {DoorSprite.DoorWoodHBroken,        new Vector2(7f, 0f)},
            {DoorSprite.DoorWoodV,              new Vector2(1f, 0f)},
            {DoorSprite.DoorWoodVOpen1,         new Vector2(1f, 6f)},
            {DoorSprite.DoorWoodChainH,         new Vector2(2f, 0f)},
            {DoorSprite.DoorWoodChainV,         new Vector2(3f, 0f)},
            {DoorSprite.DoorWoodChainGoldH,     new Vector2(4f, 0f)},
            {DoorSprite.DoorWoodChainGoldV,     new Vector2(5f, 0f)},

            {DoorSprite.Portal1,                    new Vector2(1,5)},
            {DoorSprite.Portal2,                    new Vector2(0,9)},
            {DoorSprite.PortalGold1,                new Vector2(3,5)},
            {DoorSprite.PortalGold2,                new Vector2(1,9)},
            {DoorSprite.PortalSilver1,              new Vector2(5,5)},
            {DoorSprite.PortalSilver2,              new Vector2(2,9)},
            {DoorSprite.PortalDark1,                new Vector2(7,5)},
            {DoorSprite.PortalDark2,                new Vector2(3,9)},
        };
    }
}