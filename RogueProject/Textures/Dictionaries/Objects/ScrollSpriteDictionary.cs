﻿/********************************************************************
* Contains dictionary with scroll sprites keys and associated       *
*   Vector2() positions (unscaled) in scroll sprite atlas texture   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class ScrollSpriteDictionary
    {
        public static Dictionary<ScrollSprite, Vector2> SpritesScroll = new Dictionary<ScrollSprite, Vector2>()
        {
            {ScrollSprite.ScrollWhite,                  new Vector2(0f, 0f)},
        };
    }
}