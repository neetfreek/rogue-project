﻿/****************************************************************
* Contains dictionary with ground decoration sprite keys and    *
*   associated Vector2() positions (unscaled) in ground         *
*   decoration sprite atlas texture                             *
*****************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    class DecorationGroundSpriteDictionary
    {
        // Map DecorationGroundSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<DecorationGroundSprite, Vector2> SpritesDecorationGround =
            new Dictionary<DecorationGroundSprite, Vector2>()
            {
                {DecorationGroundSprite.FlowersWhiteDense,          new Vector2(0f, 2f)},
                {DecorationGroundSprite.FlowersWhiteSparse,         new Vector2(1f, 2f)},
                {DecorationGroundSprite.FlowersBlueDense,           new Vector2(2f, 2f)},
                {DecorationGroundSprite.FlowersBlueSparse,          new Vector2(3f, 2f)},
                {DecorationGroundSprite.FlowersYellowDense,         new Vector2(4f, 2f)},
                {DecorationGroundSprite.FlowersYellowSparse,        new Vector2(5f, 2f)},
                {DecorationGroundSprite.FlowersRedDense,            new Vector2(6f, 2)},
                {DecorationGroundSprite.FlowersRedSparse,           new Vector2(7f, 2f)},

                {DecorationGroundSprite.GrassGreenDense,            new Vector2(0f, 0f)},
                {DecorationGroundSprite.GrassGreenSparse,           new Vector2(1f, 0f)},
                {DecorationGroundSprite.GrassGreenPatchDense,       new Vector2(0f, 1f)},
                {DecorationGroundSprite.GrassGreenPatchSparse,      new Vector2(1f, 1f)},

                {DecorationGroundSprite.GrassOrangeDense,           new Vector2(2f, 0f)},
                {DecorationGroundSprite.GrassOrangeSparse,          new Vector2(3f, 0f)},
                {DecorationGroundSprite.GrassOrangePatchDense,      new Vector2(2f, 1f)},
                {DecorationGroundSprite.GrassOrangePatchSparse,     new Vector2(3f, 1f)},

                {DecorationGroundSprite.StonesBlueSmallDense,       new Vector2(4f, 0f)},
                {DecorationGroundSprite.StonesBlueLargeDense,       new Vector2(4f, 1f)},
                {DecorationGroundSprite.StonesBlueSmallSparse,      new Vector2(5f, 0f)},
                {DecorationGroundSprite.StonesBlueLargeSparse,      new Vector2(5f, 1f)},
                {DecorationGroundSprite.StonesRedSmallDense,        new Vector2(6f, 0f)},
                {DecorationGroundSprite.StonesRedLargeDense,        new Vector2(6f, 1f)},
                {DecorationGroundSprite.StonesRedSmallSparse,       new Vector2(7f, 0f)},
                {DecorationGroundSprite.StonesRedLargeSparse,       new Vector2(7f, 1f)},
            };
    }
}