﻿/********************************************************************
* Contains dictionary with tree sprites keys and associated         *
*   Vector2() positions (unscaled) in tree sprite atlas texture     *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    class TreeSpriteDictionary
    {
        // Map TreeSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<TreeSprite, Vector2> SpritesTree = new Dictionary<TreeSprite, Vector2>()
        {
            {TreeSprite.MushroomDarkTopL,         new Vector2(0f, 30f)},
            {TreeSprite.MushroomDarkTopM,         new Vector2(1f, 30f)},
            {TreeSprite.MushroomDarkTopR,         new Vector2(2f, 30f)},
            {TreeSprite.MushroomDarkMidL,         new Vector2(0f, 31f)},
            {TreeSprite.MushroomDarkMidM,         new Vector2(1f, 31f)},
            {TreeSprite.MushroomDarkMidR,         new Vector2(2f, 31f)},
            {TreeSprite.MushroomDarkBotL,         new Vector2(0f, 32f)},
            {TreeSprite.MushroomDarkBotM,         new Vector2(1f, 32f)},
            {TreeSprite.MushroomDarkBotR,         new Vector2(2f, 32f)},
            {TreeSprite.MushroomDarkSingle,       new Vector2(3f, 30f)},
            {TreeSprite.MushroomDarkInnerTR,      new Vector2(4f, 31f)},
            {TreeSprite.MushroomDarkInnerTL,      new Vector2(5f, 31f)},
            {TreeSprite.MushroomDarkInnerBR,      new Vector2(4f, 30f)},
            {TreeSprite.MushroomDarkInnerBL,      new Vector2(5f, 30f)},

            {TreeSprite.MushroomLightTopL,        new Vector2(6f, 30f)},
            {TreeSprite.MushroomLightTopM,        new Vector2(7f, 30f)},
            {TreeSprite.MushroomLightTopR,        new Vector2(8f, 30f)},
            {TreeSprite.MushroomLightMidL,        new Vector2(6f, 31f)},
            {TreeSprite.MushroomLightMidM,        new Vector2(7f, 31f)},
            {TreeSprite.MushroomLightMidR,        new Vector2(8f, 31f)},
            {TreeSprite.MushroomLightBotL,        new Vector2(6f, 32f)},
            {TreeSprite.MushroomLightBotM,        new Vector2(7f, 32f)},
            {TreeSprite.MushroomLightBotR,        new Vector2(8f, 32f)},
            {TreeSprite.MushroomLightSingle,      new Vector2(9f, 30f)},
            {TreeSprite.MushroomLightInnerTR,     new Vector2(10f, 31f)},
            {TreeSprite.MushroomLightInnerTL,     new Vector2(11f, 31f)},
            {TreeSprite.MushroomLightInnerBR,     new Vector2(10f, 30f)},
            {TreeSprite.MushroomLightInnerBL,     new Vector2(11f, 30f)},

            {TreeSprite.PalmTopL,        new Vector2(6f, 27f)},
            {TreeSprite.PalmTopM,        new Vector2(7f, 27f)},
            {TreeSprite.PalmTopR,        new Vector2(8f, 27f)},
            {TreeSprite.PalmMidL,        new Vector2(6f, 28f)},
            {TreeSprite.PalmMidM,        new Vector2(7f, 28f)},
            {TreeSprite.PalmMidR,        new Vector2(8f, 28f)},
            {TreeSprite.PalmBotL,        new Vector2(6f, 29f)},
            {TreeSprite.PalmBotM,        new Vector2(7f, 29f)},
            {TreeSprite.PalmBotR,        new Vector2(8f, 29f)},
            {TreeSprite.PalmSingle,      new Vector2(9f, 27f)},
            {TreeSprite.PalmInnerTR,     new Vector2(10f, 28f)},
            {TreeSprite.PalmInnerTL,     new Vector2(11f, 28f)},
            {TreeSprite.PalmInnerBR,     new Vector2(10f, 27f)},
            {TreeSprite.PalmInnerBL,     new Vector2(11f, 27f)},

            {TreeSprite.PineGreenDarkTopL,         new Vector2(0f, 18f)},
            {TreeSprite.PineGreenDarkTopM,         new Vector2(1f, 18f)},
            {TreeSprite.PineGreenDarkTopR,         new Vector2(2f, 18f)},
            {TreeSprite.PineGreenDarkMidL,         new Vector2(0f, 19f)},
            {TreeSprite.PineGreenDarkMidM,         new Vector2(1f, 19f)},
            {TreeSprite.PineGreenDarkMidR,         new Vector2(2f, 19f)},
            {TreeSprite.PineGreenDarkBotL,         new Vector2(0f, 20f)},
            {TreeSprite.PineGreenDarkBotM,         new Vector2(1f, 20f)},
            {TreeSprite.PineGreenDarkBotR,         new Vector2(2f, 20f)},
            {TreeSprite.PineGreenDarkSingle,       new Vector2(3f, 18f)},
            {TreeSprite.PineGreenDarkInnerTR,      new Vector2(4f, 19f)},
            {TreeSprite.PineGreenDarkInnerTL,      new Vector2(5f, 19f)},
            {TreeSprite.PineGreenDarkInnerBR,      new Vector2(4f, 18f)},
            {TreeSprite.PineGreenDarkInnerBL,      new Vector2(5f, 18f)},

            {TreeSprite.PineGreenDarkDeadTopL,        new Vector2(6f, 18f)},
            {TreeSprite.PineGreenDarkDeadTopM,        new Vector2(7f, 18f)},
            {TreeSprite.PineGreenDarkDeadTopR,        new Vector2(8f, 18f)},
            {TreeSprite.PineGreenDarkDeadMidL,        new Vector2(6f, 19f)},
            {TreeSprite.PineGreenDarkDeadMidM,        new Vector2(7f, 19f)},
            {TreeSprite.PineGreenDarkDeadMidR,        new Vector2(8f, 19f)},
            {TreeSprite.PineGreenDarkDeadBotL,        new Vector2(6f, 20f)},
            {TreeSprite.PineGreenDarkDeadBotM,        new Vector2(7f, 20f)},
            {TreeSprite.PineGreenDarkDeadBotR,        new Vector2(8f, 20f)},
            {TreeSprite.PineGreenDarkDeadSingle,      new Vector2(9f, 18f)},
            {TreeSprite.PineGreenDarkDeadInnerTR,     new Vector2(10f, 19f)},
            {TreeSprite.PineGreenDarkDeadInnerTL,     new Vector2(11f, 19f)},
            {TreeSprite.PineGreenDarkDeadInnerBR,     new Vector2(10f, 18f)},
            {TreeSprite.PineGreenDarkDeadInnerBL,     new Vector2(11f, 18f)},

            {TreeSprite.PineGreenLightTopL,         new Vector2(0f, 15f)},
            {TreeSprite.PineGreenLightTopM,         new Vector2(1f, 15f)},
            {TreeSprite.PineGreenLightTopR,         new Vector2(2f, 15f)},
            {TreeSprite.PineGreenLightMidL,         new Vector2(0f, 16f)},
            {TreeSprite.PineGreenLightMidM,         new Vector2(1f, 16f)},
            {TreeSprite.PineGreenLightMidR,         new Vector2(2f, 16f)},
            {TreeSprite.PineGreenLightBotL,         new Vector2(0f, 17f)},
            {TreeSprite.PineGreenLightBotM,         new Vector2(1f, 17f)},
            {TreeSprite.PineGreenLightBotR,         new Vector2(2f, 17f)},
            {TreeSprite.PineGreenLightSingle,       new Vector2(3f, 15f)},
            {TreeSprite.PineGreenLightInnerTR,      new Vector2(4f, 16f)},
            {TreeSprite.PineGreenLightInnerTL,      new Vector2(5f, 16f)},
            {TreeSprite.PineGreenLightInnerBR,      new Vector2(4f, 15f)},
            {TreeSprite.PineGreenLightInnerBL,      new Vector2(5f, 15f)},

            {TreeSprite.PineGreenLightDeadTopL,        new Vector2(6f, 15f)},
            {TreeSprite.PineGreenLightDeadTopM,        new Vector2(7f, 15f)},
            {TreeSprite.PineGreenLightDeadTopR,        new Vector2(8f, 15f)},
            {TreeSprite.PineGreenLightDeadMidL,        new Vector2(6f, 16f)},
            {TreeSprite.PineGreenLightDeadMidM,        new Vector2(7f, 16f)},
            {TreeSprite.PineGreenLightDeadMidR,        new Vector2(8f, 16f)},
            {TreeSprite.PineGreenLightDeadBotL,        new Vector2(6f, 17f)},
            {TreeSprite.PineGreenLightDeadBotM,        new Vector2(7f, 17f)},
            {TreeSprite.PineGreenLightDeadBotR,        new Vector2(8f, 17f)},
            {TreeSprite.PineGreenLightDeadSingle,      new Vector2(9f, 15f)},
            {TreeSprite.PineGreenLightDeadInnerTR,     new Vector2(10f, 16f)},
            {TreeSprite.PineGreenLightDeadInnerTL,     new Vector2(11f, 16f)},
            {TreeSprite.PineGreenLightDeadInnerBR,     new Vector2(10f, 15f)},
            {TreeSprite.PineGreenLightDeadInnerBL,     new Vector2(11f, 15f)},

            {TreeSprite.PineSnowBlueTopL,         new Vector2(0f, 24f)},
            {TreeSprite.PineSnowBlueTopM,         new Vector2(1f, 24f)},
            {TreeSprite.PineSnowBlueTopR,         new Vector2(2f, 24f)},
            {TreeSprite.PineSnowBlueMidL,         new Vector2(0f, 25f)},
            {TreeSprite.PineSnowBlueMidM,         new Vector2(1f, 25f)},
            {TreeSprite.PineSnowBlueMidR,         new Vector2(2f, 25f)},
            {TreeSprite.PineSnowBlueBotL,         new Vector2(0f, 26f)},
            {TreeSprite.PineSnowBlueBotM,         new Vector2(1f, 26f)},
            {TreeSprite.PineSnowBlueBotR,         new Vector2(2f, 26f)},
            {TreeSprite.PineSnowBlueSingle,       new Vector2(3f, 24f)},
            {TreeSprite.PineSnowBlueInnerTR,      new Vector2(4f, 25f)},
            {TreeSprite.PineSnowBlueInnerTL,      new Vector2(5f, 25f)},
            {TreeSprite.PineSnowBlueInnerBR,      new Vector2(4f, 24f)},
            {TreeSprite.PineSnowBlueInnerBL,      new Vector2(5f, 24f)},

            {TreeSprite.PineSnowBlueDeadTopL,        new Vector2(6f, 24f)},
            {TreeSprite.PineSnowBlueDeadTopM,        new Vector2(7f, 24f)},
            {TreeSprite.PineSnowBlueDeadTopR,        new Vector2(8f, 24f)},
            {TreeSprite.PineSnowBlueDeadMidL,        new Vector2(6f, 25f)},
            {TreeSprite.PineSnowBlueDeadMidM,        new Vector2(7f, 25f)},
            {TreeSprite.PineSnowBlueDeadMidR,        new Vector2(8f, 25f)},
            {TreeSprite.PineSnowBlueDeadBotL,        new Vector2(6f, 26f)},
            {TreeSprite.PineSnowBlueDeadBotM,        new Vector2(7f, 26f)},
            {TreeSprite.PineSnowBlueDeadBotR,        new Vector2(8f, 26f)},
            {TreeSprite.PineSnowBlueDeadSingle,      new Vector2(9f, 24f)},
            {TreeSprite.PineSnowBlueDeadInnerTR,     new Vector2(10f, 25f)},
            {TreeSprite.PineSnowBlueDeadInnerTL,     new Vector2(11f, 25f)},
            {TreeSprite.PineSnowBlueDeadInnerBR,     new Vector2(10f, 24f)},
            {TreeSprite.PineSnowBlueDeadInnerBL,     new Vector2(11f, 24f)},

            {TreeSprite.PineSnowWhiteTopL,         new Vector2(0f, 21f)},
            {TreeSprite.PineSnowWhiteTopM,         new Vector2(1f, 21f)},
            {TreeSprite.PineSnowWhiteTopR,         new Vector2(2f, 21f)},
            {TreeSprite.PineSnowWhiteMidL,         new Vector2(0f, 22f)},
            {TreeSprite.PineSnowWhiteMidM,         new Vector2(1f, 22f)},
            {TreeSprite.PineSnowWhiteMidR,         new Vector2(2f, 22f)},
            {TreeSprite.PineSnowWhiteBotL,         new Vector2(0f, 23f)},
            {TreeSprite.PineSnowWhiteBotM,         new Vector2(1f, 23f)},
            {TreeSprite.PineSnowWhiteBotR,         new Vector2(2f, 23f)},
            {TreeSprite.PineSnowWhiteSingle,       new Vector2(3f, 21f)},
            {TreeSprite.PineSnowWhiteInnerTR,      new Vector2(4f, 22f)},
            {TreeSprite.PineSnowWhiteInnerTL,      new Vector2(5f, 22f)},
            {TreeSprite.PineSnowWhiteInnerBR,      new Vector2(4f, 21f)},
            {TreeSprite.PineSnowWhiteInnerBL,      new Vector2(5f, 21f)},

            {TreeSprite.PineSnowWhiteDeadTopL,        new Vector2(6f, 21f)},
            {TreeSprite.PineSnowWhiteDeadTopM,        new Vector2(7f, 21f)},
            {TreeSprite.PineSnowWhiteDeadTopR,        new Vector2(8f, 21f)},
            {TreeSprite.PineSnowWhiteDeadMidL,        new Vector2(6f, 22f)},
            {TreeSprite.PineSnowWhiteDeadMidM,        new Vector2(7f, 22f)},
            {TreeSprite.PineSnowWhiteDeadMidR,        new Vector2(8f, 22f)},
            {TreeSprite.PineSnowWhiteDeadBotL,        new Vector2(6f, 23f)},
            {TreeSprite.PineSnowWhiteDeadBotM,        new Vector2(7f, 23f)},
            {TreeSprite.PineSnowWhiteDeadBotR,        new Vector2(8f, 23f)},
            {TreeSprite.PineSnowWhiteDeadSingle,      new Vector2(9f, 21f)},
            {TreeSprite.PineSnowWhiteDeadInnerTR,     new Vector2(10f, 22f)},
            {TreeSprite.PineSnowWhiteDeadInnerTL,     new Vector2(11f, 22f)},
            {TreeSprite.PineSnowWhiteDeadInnerBR,     new Vector2(10f, 21f)},
            {TreeSprite.PineSnowWhiteDeadInnerBL,     new Vector2(11f, 21f)},

            {TreeSprite.PlainGreenDarkTopL,         new Vector2(0f, 6f)},
            {TreeSprite.PlainGreenDarkTopM,         new Vector2(1f, 6f)},
            {TreeSprite.PlainGreenDarkTopR,         new Vector2(2f, 6f)},
            {TreeSprite.PlainGreenDarkMidL,         new Vector2(0f, 7f)},
            {TreeSprite.PlainGreenDarkMidM,         new Vector2(1f, 7f)},
            {TreeSprite.PlainGreenDarkMidR,         new Vector2(2f, 7f)},
            {TreeSprite.PlainGreenDarkBotL,         new Vector2(0f, 8f)},
            {TreeSprite.PlainGreenDarkBotM,         new Vector2(1f, 8f)},
            {TreeSprite.PlainGreenDarkBotR,         new Vector2(2f, 8f)},
            {TreeSprite.PlainGreenDarkSingle,       new Vector2(3f, 6f)},
            {TreeSprite.PlainGreenDarkInnerTR,      new Vector2(4f, 7f)},
            {TreeSprite.PlainGreenDarkInnerTL,      new Vector2(5f, 7f)},
            {TreeSprite.PlainGreenDarkInnerBR,      new Vector2(4f, 6f)},
            {TreeSprite.PlainGreenDarkInnerBL,      new Vector2(5f, 6f)},

            {TreeSprite.PlainGreenDarkDeadTopL,        new Vector2(6f, 6f)},
            {TreeSprite.PlainGreenDarkDeadTopM,        new Vector2(7f, 6f)},
            {TreeSprite.PlainGreenDarkDeadTopR,        new Vector2(8f, 6f)},
            {TreeSprite.PlainGreenDarkDeadMidL,        new Vector2(6f, 7f)},
            {TreeSprite.PlainGreenDarkDeadMidM,        new Vector2(7f, 7f)},
            {TreeSprite.PlainGreenDarkDeadMidR,        new Vector2(8f, 7f)},
            {TreeSprite.PlainGreenDarkDeadBotL,        new Vector2(6f, 8f)},
            {TreeSprite.PlainGreenDarkDeadBotM,        new Vector2(7f, 8f)},
            {TreeSprite.PlainGreenDarkDeadBotR,        new Vector2(8f, 8f)},
            {TreeSprite.PlainGreenDarkDeadSingle,      new Vector2(9f, 6f)},
            {TreeSprite.PlainGreenDarkDeadInnerTR,     new Vector2(10f, 7f)},
            {TreeSprite.PlainGreenDarkDeadInnerTL,     new Vector2(11f, 7f)},
            {TreeSprite.PlainGreenDarkDeadInnerBR,     new Vector2(10f, 6f)},
            {TreeSprite.PlainGreenDarkDeadInnerBL,     new Vector2(11f, 6f)},

            {TreeSprite.PlainGreenLightTopL,        new Vector2(0f, 3f)},
            {TreeSprite.PlainGreenLightTopM,        new Vector2(1f, 3f)},
            {TreeSprite.PlainGreenLightTopR,        new Vector2(2f, 3f)},
            {TreeSprite.PlainGreenLightMidL,        new Vector2(0f, 4f)},
            {TreeSprite.PlainGreenLightMidM,        new Vector2(1f, 4f)},
            {TreeSprite.PlainGreenLightMidR,        new Vector2(2f, 4f)},
            {TreeSprite.PlainGreenLightBotL,        new Vector2(0f, 5f)},
            {TreeSprite.PlainGreenLightBotM,        new Vector2(1f, 5f)},
            {TreeSprite.PlainGreenLightBotR,        new Vector2(2f, 5f)},
            {TreeSprite.PlainGreenLightSingle,      new Vector2(3f, 3f)},
            {TreeSprite.PlainGreenLightInnerTR,     new Vector2(4f, 4f)},
            {TreeSprite.PlainGreenLightInnerTL,     new Vector2(5f, 4f)},
            {TreeSprite.PlainGreenLightInnerBR,     new Vector2(4f, 3f)},
            {TreeSprite.PlainGreenLightInnerBL,     new Vector2(5f, 3f)},

            {TreeSprite.PlainGreenLightDeadTopL,        new Vector2(6f, 3f)},
            {TreeSprite.PlainGreenLightDeadTopM,        new Vector2(7f, 3f)},
            {TreeSprite.PlainGreenLightDeadTopR,        new Vector2(8f, 3f)},
            {TreeSprite.PlainGreenLightDeadMidL,        new Vector2(6f, 4f)},
            {TreeSprite.PlainGreenLightDeadMidM,        new Vector2(7f, 4f)},
            {TreeSprite.PlainGreenLightDeadMidR,        new Vector2(8f, 4f)},
            {TreeSprite.PlainGreenLightDeadBotL,        new Vector2(6f, 5f)},
            {TreeSprite.PlainGreenLightDeadBotM,        new Vector2(7f, 5f)},
            {TreeSprite.PlainGreenLightDeadBotR,        new Vector2(8f, 5f)},
            {TreeSprite.PlainGreenLightDeadSingle,      new Vector2(9f, 3f)},
            {TreeSprite.PlainGreenLightDeadInnerTR,     new Vector2(10f, 4f)},
            {TreeSprite.PlainGreenLightDeadInnerTL,     new Vector2(11f, 4f)},
            {TreeSprite.PlainGreenLightDeadInnerBR,     new Vector2(10f, 3f)},
            {TreeSprite.PlainGreenLightDeadInnerBL,     new Vector2(11f, 3f)},

            {TreeSprite.PlainSnowBlueTopL,         new Vector2(0f, 12f)},
            {TreeSprite.PlainSnowBlueTopM,         new Vector2(1f, 12f)},
            {TreeSprite.PlainSnowBlueTopR,         new Vector2(2f, 12f)},
            {TreeSprite.PlainSnowBlueMidL,         new Vector2(0f, 13f)},
            {TreeSprite.PlainSnowBlueMidM,         new Vector2(1f, 13f)},
            {TreeSprite.PlainSnowBlueMidR,         new Vector2(2f, 13f)},
            {TreeSprite.PlainSnowBlueBotL,         new Vector2(0f, 14f)},
            {TreeSprite.PlainSnowBlueBotM,         new Vector2(1f, 14f)},
            {TreeSprite.PlainSnowBlueBotR,         new Vector2(2f, 14f)},
            {TreeSprite.PlainSnowBlueSingle,       new Vector2(3f, 12f)},
            {TreeSprite.PlainSnowBlueInnerTR,      new Vector2(4f, 13f)},
            {TreeSprite.PlainSnowBlueInnerTL,      new Vector2(5f, 13f)},
            {TreeSprite.PlainSnowBlueInnerBR,      new Vector2(4f, 12f)},
            {TreeSprite.PlainSnowBlueInnerBL,      new Vector2(5f, 12f)},

            {TreeSprite.PlainSnowBlueDeadTopL,        new Vector2(6f, 12f)},
            {TreeSprite.PlainSnowBlueDeadTopM,        new Vector2(7f, 12f)},
            {TreeSprite.PlainSnowBlueDeadTopR,        new Vector2(8f, 12f)},
            {TreeSprite.PlainSnowBlueDeadMidL,        new Vector2(6f, 13f)},
            {TreeSprite.PlainSnowBlueDeadMidM,        new Vector2(7f, 13f)},
            {TreeSprite.PlainSnowBlueDeadMidR,        new Vector2(8f, 13f)},
            {TreeSprite.PlainSnowBlueDeadBotL,        new Vector2(6f, 14f)},
            {TreeSprite.PlainSnowBlueDeadBotM,        new Vector2(7f, 14f)},
            {TreeSprite.PlainSnowBlueDeadBotR,        new Vector2(8f, 14f)},
            {TreeSprite.PlainSnowBlueDeadSingle,      new Vector2(9f, 12f)},
            {TreeSprite.PlainSnowBlueDeadInnerTR,     new Vector2(10f, 13f)},
            {TreeSprite.PlainSnowBlueDeadInnerTL,     new Vector2(11f, 13f)},
            {TreeSprite.PlainSnowBlueDeadInnerBR,     new Vector2(10f, 12f)},
            {TreeSprite.PlainSnowBlueDeadInnerBL,     new Vector2(11f, 12f)},

            {TreeSprite.PlainSnowWhiteTopL,         new Vector2(0f, 9f)},
            {TreeSprite.PlainSnowWhiteTopM,         new Vector2(1f, 9f)},
            {TreeSprite.PlainSnowWhiteTopR,         new Vector2(2f, 9f)},
            {TreeSprite.PlainSnowWhiteMidL,         new Vector2(0f, 10f)},
            {TreeSprite.PlainSnowWhiteMidM,         new Vector2(1f, 10f)},
            {TreeSprite.PlainSnowWhiteMidR,         new Vector2(2f, 10f)},
            {TreeSprite.PlainSnowWhiteBotL,         new Vector2(0f, 11f)},
            {TreeSprite.PlainSnowWhiteBotM,         new Vector2(1f, 11f)},
            {TreeSprite.PlainSnowWhiteBotR,         new Vector2(2f, 11f)},
            {TreeSprite.PlainSnowWhiteSingle,       new Vector2(3f, 9f)},
            {TreeSprite.PlainSnowWhiteInnerTR,      new Vector2(4f, 10f)},
            {TreeSprite.PlainSnowWhiteInnerTL,      new Vector2(5f, 10f)},
            {TreeSprite.PlainSnowWhiteInnerBR,      new Vector2(4f, 9f)},
            {TreeSprite.PlainSnowWhiteInnerBL,      new Vector2(5f, 9f)},

            {TreeSprite.PlainSnowWhiteDeadTopL,        new Vector2(6f, 9f)},
            {TreeSprite.PlainSnowWhiteDeadTopM,        new Vector2(7f, 9f)},
            {TreeSprite.PlainSnowWhiteDeadTopR,        new Vector2(8f, 9f)},
            {TreeSprite.PlainSnowWhiteDeadMidL,        new Vector2(6f, 10f)},
            {TreeSprite.PlainSnowWhiteDeadMidM,        new Vector2(7f, 10f)},
            {TreeSprite.PlainSnowWhiteDeadMidR,        new Vector2(8f, 10f)},
            {TreeSprite.PlainSnowWhiteDeadBotL,        new Vector2(6f, 11f)},
            {TreeSprite.PlainSnowWhiteDeadBotM,        new Vector2(7f, 11f)},
            {TreeSprite.PlainSnowWhiteDeadBotR,        new Vector2(8f, 11f)},
            {TreeSprite.PlainSnowWhiteDeadSingle,      new Vector2(9f, 9f)},
            {TreeSprite.PlainSnowWhiteDeadInnerTR,     new Vector2(10f, 10f)},
            {TreeSprite.PlainSnowWhiteDeadInnerTL,     new Vector2(11f, 10f)},
            {TreeSprite.PlainSnowWhiteDeadInnerBR,     new Vector2(10f, 9f)},
            {TreeSprite.PlainSnowWhiteDeadInnerBL,     new Vector2(11f, 9f)},

            {TreeSprite.WillowDarkTopL,        new Vector2(6f, 33f)},
            {TreeSprite.WillowDarkTopM,        new Vector2(7f, 33f)},
            {TreeSprite.WillowDarkTopR,        new Vector2(8f, 33f)},
            {TreeSprite.WillowDarkMidL,        new Vector2(6f, 34f)},
            {TreeSprite.WillowDarkMidM,        new Vector2(7f, 34f)},
            {TreeSprite.WillowDarkMidR,        new Vector2(8f, 34f)},
            {TreeSprite.WillowDarkBotL,        new Vector2(6f, 35f)},
            {TreeSprite.WillowDarkBotM,        new Vector2(7f, 35f)},
            {TreeSprite.WillowDarkBotR,        new Vector2(8f, 35f)},
            {TreeSprite.WillowDarkSingle,      new Vector2(9f, 33f)},
            {TreeSprite.WillowDarkInnerTR,     new Vector2(10f, 34f)},
            {TreeSprite.WillowDarkInnerTL,     new Vector2(11f, 34f)},
            {TreeSprite.WillowDarkInnerBR,     new Vector2(10f, 33f)},
            {TreeSprite.WillowDarkInnerBL,     new Vector2(11f, 33f)},

            {TreeSprite.WillowLightTopL,         new Vector2(0f, 33f)},
            {TreeSprite.WillowLightTopM,         new Vector2(1f, 33f)},
            {TreeSprite.WillowLightTopR,         new Vector2(2f, 33f)},
            {TreeSprite.WillowLightMidL,         new Vector2(0f, 34f)},
            {TreeSprite.WillowLightMidM,         new Vector2(1f, 34f)},
            {TreeSprite.WillowLightMidR,         new Vector2(2f, 34f)},
            {TreeSprite.WillowLightBotL,         new Vector2(0f, 35f)},
            {TreeSprite.WillowLightBotM,         new Vector2(1f, 35f)},
            {TreeSprite.WillowLightBotR,         new Vector2(2f, 35f)},
            {TreeSprite.WillowLightSingle,       new Vector2(3f, 33f)},
            {TreeSprite.WillowLightInnerTR,      new Vector2(4f, 34f)},
            {TreeSprite.WillowLightInnerTL,      new Vector2(5f, 34f)},
            {TreeSprite.WillowLightInnerBR,      new Vector2(4f, 33f)},
            {TreeSprite.WillowLightInnerBL,      new Vector2(5f, 33f)},
        };
    }
}