﻿/********************************************************************
* Contains dictionary with GUI sprite part keys and associated      *
*   Vector2() positions (unscaled) in GUI0 sprite atlas texture     *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class GUISpriteDictionary
    {
        // Map GUISprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture 
        public static Dictionary<GUISprite, Vector2> SpritesGUI = new Dictionary<GUISprite, Vector2>()
        {
            {GUISprite.None,              new Vector2(0f, 18f)},

            // Cursor
            {GUISprite.Cursor,              new Vector2(15f, 0f)},

            // Arrows
            {GUISprite.ArrowDown,           new Vector2(0f, 6f)},
            {GUISprite.ArrowUp,             new Vector2(1f, 6f)},

            // Bars
            {GUISprite.BarBorderL,          new Vector2(6f, 0f)},
            {GUISprite.BarBorderM,          new Vector2(7f, 0f)},
            {GUISprite.BarBorderR,          new Vector2(8f, 0f)},

            {GUISprite.BarBlue100,          new Vector2(6f, 2f)},
            {GUISprite.BarBlue75,           new Vector2(7f, 2f)},
            {GUISprite.BarBlue50,           new Vector2(8f, 2f)},
            {GUISprite.BarBlue25,           new Vector2(9f, 2f)},
            {GUISprite.BarBlue100End,       new Vector2(11f, 1f)},

            {GUISprite.BarRed100,           new Vector2(6f, 1f)},
            {GUISprite.BarRed75,            new Vector2(7f, 1f)},
            {GUISprite.BarRed50,            new Vector2(8f, 1f)},
            {GUISprite.BarRed25,            new Vector2(9f, 1f)},
            {GUISprite.BarRed100End,        new Vector2(10f, 1f)},

            {GUISprite.BarYellow100,        new Vector2(6f, 4f)},
            {GUISprite.BarYellow75,         new Vector2(7f, 4f)},
            {GUISprite.BarYellow50,         new Vector2(8f, 4f)},
            {GUISprite.BarYellow25,         new Vector2(9f, 4f)},
            {GUISprite.BarYellow100End,     new Vector2(13f, 1f)},

            {GUISprite.Bar0,                new Vector2(10f, 0f)},

            // Squares
            {GUISprite.SquareBlackGrey,     new Vector2(8f, 13f)},
            {GUISprite.SquareBlackGreyDark, new Vector2(8f, 16f)},
            {GUISprite.SquareGrey,          new Vector2(12f, 13f)},
            {GUISprite.SquareGreyDark,      new Vector2(12f, 16f)},

            // Rectangles
            {GUISprite.RectBlackGreyTL,     new Vector2(9f, 13f)},
            {GUISprite.RectBlackGreyTM,     new Vector2(10f, 13f)},
            {GUISprite.RectBlackGreyTR,     new Vector2(11f, 13f)},
            {GUISprite.RectBlackGreyML,     new Vector2(9f, 14f)},
            {GUISprite.RectBlackGreyMM,     new Vector2(10f, 14f)},
            {GUISprite.RectBlackGreyMR,     new Vector2(11f, 14f)},
            {GUISprite.RectBlackGreyBL,     new Vector2(9f, 15f)},
            {GUISprite.RectBlackGreyBM,     new Vector2(10f, 15f)},
            {GUISprite.RectBlackGreyBR,     new Vector2(11f, 15f)},

            {GUISprite.RectGreyTL,          new Vector2(13f, 13f)},
            {GUISprite.RectGreyTM,          new Vector2(14f, 13f)},
            {GUISprite.RectGreyTR,          new Vector2(15f, 13f)},
            {GUISprite.RectGreyML,          new Vector2(13f, 14f)},
            {GUISprite.RectGreyMM,          new Vector2(14f, 14f)},
            {GUISprite.RectGreyMR,          new Vector2(15f, 14f)},
            {GUISprite.RectGreyBL,          new Vector2(13f, 15f)},
            {GUISprite.RectGreyBM,          new Vector2(14f, 15f)},
            {GUISprite.RectGreyBR,          new Vector2(15f, 15f)},

            {GUISprite.RectGreyDarkTL,      new Vector2(13f, 16f)},
            {GUISprite.RectGreyDarkTM,      new Vector2(14f, 16f)},
            {GUISprite.RectGreyDarkTR,      new Vector2(15f, 16f)},
            {GUISprite.RectGreyDarkML,      new Vector2(13f, 17f)},
            {GUISprite.RectGreyDarkMM,      new Vector2(14f, 17f)},
            {GUISprite.RectGreyDarkMR,      new Vector2(15f, 17f)},
            {GUISprite.RectGreyDarkBL,      new Vector2(13f, 18f)},
            {GUISprite.RectGreyDarkBM,      new Vector2(14f, 18f)},
            {GUISprite.RectGreyDarkBR,      new Vector2(15f, 18f)},

            {GUISprite.RectGreyLightTL,     new Vector2(13f, 10f)},
            {GUISprite.RectGreyLightTM,     new Vector2(14f, 10f)},
            {GUISprite.RectGreyLightTR,     new Vector2(15f, 10f)},
            {GUISprite.RectGreyLightML,     new Vector2(13f, 11f)},
            {GUISprite.RectGreyLightMM,     new Vector2(14f, 11f)},
            {GUISprite.RectGreyLightMR,     new Vector2(15f, 11f)},
            {GUISprite.RectGreyLightBL,     new Vector2(13f, 12f)},
            {GUISprite.RectGreyLightBM,     new Vector2(14f, 12f)},
            {GUISprite.RectGreyLightBR,     new Vector2(15f, 12f)},
        };
    }
}
