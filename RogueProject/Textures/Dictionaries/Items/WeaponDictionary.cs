﻿/********************************************************************
* Contains dictionary with weapon sprite part keys and associated   *
*   Vector2() positions (unscaled) in Weapon sprite atlas texture   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class WeaponDictionary
    {
        // Map WeaponSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture 
        public static Dictionary<WeaponSprite, Vector2> WeaponSprites = new Dictionary<WeaponSprite, Vector2>()
        {
            {WeaponSprite.Bow,              new Vector2(0f, 14f)},
            {WeaponSprite.BowLongGreen,     new Vector2(1f, 14f)},
            {WeaponSprite.BowLongWhite,     new Vector2(3f, 14f)},
            {WeaponSprite.BowShort,         new Vector2(2f, 14f)},
            {WeaponSprite.Sling,            new Vector2(4f, 14f)},
            {WeaponSprite.Crossbow,         new Vector2(5f, 14f)},

            {WeaponSprite.Axe,              new Vector2(0f, 6f)},
            {WeaponSprite.AxeDouble,        new Vector2(1f, 6f)},

            {WeaponSprite.Bardiche,         new Vector2(4f, 9f)},

            {WeaponSprite.Blade,            new Vector2(4f, 5f)},
            {WeaponSprite.BladeCurved,      new Vector2(5f, 5f)},

            {WeaponSprite.ClubShortThin,    new Vector2(0f, 2f)},
            {WeaponSprite.ClubShortThick,   new Vector2(1f, 2f)},
            {WeaponSprite.ClubLong,         new Vector2(2f, 11f)},

            {WeaponSprite.CutlassLight,     new Vector2(0f, 8f)},
            {WeaponSprite.CutlassDark,      new Vector2(1f, 8f)},

            {WeaponSprite.DaggerLight,      new Vector2(0f, 0f)},
            {WeaponSprite.DaggerDark,       new Vector2(1f, 0f)},
            {WeaponSprite.DaggerCurved,     new Vector2(1f, 1f)},

            {WeaponSprite.DirkLight,        new Vector2(0f, 1f)},
            {WeaponSprite.DirkDark,         new Vector2(4f, 0f)},

            {WeaponSprite.Glaive,           new Vector2(3f, 9f)},

            {WeaponSprite.Halberd,          new Vector2(4f, 10f)},

            {WeaponSprite.KnifeBlue,        new Vector2(5f, 0f)},
            {WeaponSprite.KnifeYellow,      new Vector2(6f, 0f)},

            {WeaponSprite.Mace,             new Vector2(0f, 4f)},
            {WeaponSprite.MaceFlanged,      new Vector2(6f, 7f)},
            {WeaponSprite.Flail,            new Vector2(1f, 4f)},

            {WeaponSprite.GreatSwordDark,   new Vector2(3f, 8f)},
            {WeaponSprite.GreatSwordLight,  new Vector2(5f, 8f)},

            {WeaponSprite.LongSword,        new Vector2(2f, 8f)},

            {WeaponSprite.Mallet,           new Vector2(0f, 13f)},

            {WeaponSprite.PickaxeSmall,     new Vector2(0f, 3f)},
            {WeaponSprite.PickaxeMedium,    new Vector2(1f, 3f)},
            {WeaponSprite.PickaxeLarge,     new Vector2(2f, 3f)},

            {WeaponSprite.Polearm,          new Vector2(3f, 10f)},

            {WeaponSprite.RodPlain,         new Vector2(4f, 7f)},
            {WeaponSprite.RodSparkle,       new Vector2(5f, 7f)},

            {WeaponSprite.Scythe,           new Vector2(3f, 11)},

            {WeaponSprite.ShortSwordDark,   new Vector2(1f, 5f)},
            {WeaponSprite.ShortSwordGold,   new Vector2(2f, 5f)},
            {WeaponSprite.ShortSwordLight,  new Vector2(0f, 5f)},

            {WeaponSprite.SpearLight,       new Vector2(0f, 7f)},
            {WeaponSprite.SpearDark,        new Vector2(1f, 7f)},
            {WeaponSprite.SpearGold,        new Vector2(2f, 7f)},

            {WeaponSprite.Stiletto,         new Vector2(7f, 0f)},

            {WeaponSprite.WhipGreen,        new Vector2(0f, 12f)},
            {WeaponSprite.WhipBrown,        new Vector2(1f, 12f)},
        };
    }
}