﻿/************************************************************
* Contains dictionary with container sprite part keys and   *  
*   associated Vector2() positions (unscaled) in Container  *
*   sprite atlas texture                                    *
*************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class ContainerDictionary
    {
        // Map ContainerSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture 
        public static Dictionary<ContainerSprite, Vector2> ContainerSprites = new Dictionary<ContainerSprite, Vector2>()
        {
            {ContainerSprite.ChestWood,             new Vector2(0f, 0f)},
            {ContainerSprite.ChestWoodTall,         new Vector2(1f, 0f)},
            {ContainerSprite.ChestWoodBroken,       new Vector2(2f, 0f)},
            {ContainerSprite.ChestMetal,            new Vector2(3f, 0f)},
            {ContainerSprite.ChestMetalTall,        new Vector2(4f, 0f)},
            {ContainerSprite.ChestMetalBroken,      new Vector2(5f, 0f)},
            {ContainerSprite.ChestShiny,            new Vector2(6f, 0f)},
            {ContainerSprite.ChestShinyBroken,      new Vector2(7f, 0f)},
            {ContainerSprite.BarrelBlue,            new Vector2(0f, 1f)},
            {ContainerSprite.BarrelBlueBroken,      new Vector2(1f, 1f)},
            {ContainerSprite.BarrelGold,            new Vector2(2f, 1f)},
            {ContainerSprite.BarrelGoldBroken,      new Vector2(3f, 1f)},
            {ContainerSprite.SackBrown,             new Vector2(0f, 2f)},
            {ContainerSprite.SackBrownGlow,         new Vector2(1f, 2f)},
            {ContainerSprite.SackRed,               new Vector2(2f, 2f)},
            {ContainerSprite.SackGreen,             new Vector2(3f, 2f)},
        };
    }
}