﻿/********************************************************************
* Contains dictionary with pest sprites keys and associated         *
*   Vector2() positions (unscaled) in pest (character)sprite        *
*   atlas texture                                                   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class EnemyPestSpriteDictionary
    {
        // Map PlayerSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<EnemyPestSprite, Vector2> SpritesPest = new Dictionary<EnemyPestSprite, Vector2>()
        {
            {EnemyPestSprite.BeetleSmall,       new Vector2(0f, 0f)},
            {EnemyPestSprite.BeetleLargeBlack,  new Vector2(1f, 0f)},        
            {EnemyPestSprite.BeetleMedium,      new Vector2(2f, 0f)},
            {EnemyPestSprite.BeetleLargeBrown,  new Vector2(3f, 0f)},
            {EnemyPestSprite.BeetleLong,        new Vector2(4f, 0f)},

            {EnemyPestSprite.WormBrown,         new Vector2(6f, 0f)},        
            {EnemyPestSprite.WormTan,           new Vector2(7f, 0f)},

            {EnemyPestSprite.FlyLarva,          new Vector2(0f, 1f)},
            {EnemyPestSprite.Fly,               new Vector2(1f, 1f)},
            {EnemyPestSprite.BeeLarva,          new Vector2(2f, 1f)},
            {EnemyPestSprite.Bee,               new Vector2(3f, 1f)},
            {EnemyPestSprite.MosquitoLarva,     new Vector2(4f, 1f)},
            {EnemyPestSprite.Mosquito,          new Vector2(5f, 1f)},

            {EnemyPestSprite.SpiderSmall,       new Vector2(0f, 2f)},
            {EnemyPestSprite.SpiderMediumBrown, new Vector2(1f, 2f)},
            {EnemyPestSprite.SpiderMediumGrey,  new Vector2(0f, 9f)},
            {EnemyPestSprite.SpiderLarge,       new Vector2(2f, 2f)},
            {EnemyPestSprite.ScorpionSmall,     new Vector2(3f, 2f)},
            {EnemyPestSprite.ScorpionMedium,    new Vector2(4f, 2f)},
            {EnemyPestSprite.ScorpionLarge,     new Vector2(5f, 2f)},

            {EnemyPestSprite.LeechTanSmall,     new Vector2(0f, 3f)},
            {EnemyPestSprite.LeechTan,          new Vector2(1f, 3f)},
            {EnemyPestSprite.LeechRedSmall,     new Vector2(3f, 3f)},
            {EnemyPestSprite.LeechRed,          new Vector2(4f, 3f)},

            {EnemyPestSprite.AntTan,            new Vector2(0f, 4f)},
            {EnemyPestSprite.AntBlack,          new Vector2(1f, 4f)},
            {EnemyPestSprite.AntRed,            new Vector2(2f, 4f)},
            {EnemyPestSprite.AntBlue,           new Vector2(3f, 4f)},

            {EnemyPestSprite.GrasshopperSmall,  new Vector2(0f, 5f)},
            {EnemyPestSprite.GrasshopperLarge,  new Vector2(1f, 5f)},
            {EnemyPestSprite.LocustSmall,       new Vector2(2f, 5f)},
            {EnemyPestSprite.LocustLarge,       new Vector2(3f, 5f)},

            {EnemyPestSprite.BugGlowWings,      new Vector2(1f, 6f)},
            {EnemyPestSprite.BugGlowSparkle,    new Vector2(2f, 6f)},
            {EnemyPestSprite.BugSparkleSmall,   new Vector2(0f, 8f)},
            {EnemyPestSprite.BugSparkleLarge,   new Vector2(1f, 8f)},

            {EnemyPestSprite.SlugSmall,         new Vector2(0f, 7f)},
            {EnemyPestSprite.SlugLarge,         new Vector2(1f, 7f)},
            {EnemyPestSprite.SnailSmall,        new Vector2(2f, 7f)},
            {EnemyPestSprite.SnailLarge,        new Vector2(3f, 7f)},

            {EnemyPestSprite.LadyBugRed,        new Vector2(0f, 10f)},
            {EnemyPestSprite.LadyBugBrown,      new Vector2(1f, 10f)},
        };
    }
}