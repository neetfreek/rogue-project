﻿/********************************************************************
* Contains dictionary with rat sprites keys and associated          *
*   Vector2() positions (unscaled) in rodent sprite                 *
*   atlas texture                                                   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class EnemyRodentSpriteDictionary
    {
        // Map rodent sprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<EnemyRodentSprite, Vector2> SpritesRodent = new Dictionary<EnemyRodentSprite, Vector2>()
        {
            {EnemyRodentSprite.RatBrownLarge,       new Vector2(2f, 1f)},
            {EnemyRodentSprite.RatBrownMedium,      new Vector2(1f, 1f)},
            {EnemyRodentSprite.RatBrownSmall,       new Vector2(0f, 1f)},
            {EnemyRodentSprite.RatGreen,            new Vector2(0f, 2f)},
            {EnemyRodentSprite.RatGreyLarge,        new Vector2(3f, 1f)},
            {EnemyRodentSprite.RatLeech,            new Vector2(2f, 2f)},
            {EnemyRodentSprite.RatTan,              new Vector2(1f, 2f)},
        };
    }
}