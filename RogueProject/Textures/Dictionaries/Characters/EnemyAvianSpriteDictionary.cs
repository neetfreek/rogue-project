﻿/********************************************************************
* Contains dictionary with avian sprites keys and associated        *
*   Vector2() positions (unscaled) in avian sprite                  *
*   atlas texture                                                   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;
namespace RogueProject.Textures
{
    static class EnemyAvianSpriteDictionary
    {
        // Map avian sprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<EnemyAvianSprite, Vector2> SpritesAvian = new Dictionary<EnemyAvianSprite, Vector2>()
        {
            {EnemyAvianSprite.BatBrownSmall,        new Vector2(0f, 11f)},
            {EnemyAvianSprite.BatBrownLarge,        new Vector2(1f, 11f)},
            {EnemyAvianSprite.BatBlackLarge,        new Vector2(2f, 11f)},
            {EnemyAvianSprite.BatTanLarge,          new Vector2(3f, 11f)},
            {EnemyAvianSprite.BatTanSmall,          new Vector2(4f, 11f)},

            {EnemyAvianSprite.BirdFluffSnow,        new Vector2(4f, 4f)},

            {EnemyAvianSprite.ChickenSnowLarge,     new Vector2(4f, 0f)},
            {EnemyAvianSprite.ChickenSnowSmall,     new Vector2(5f, 0f)},

            {EnemyAvianSprite.EagleSnowLarge,       new Vector2(2f, 9f)},
            {EnemyAvianSprite.EagleSnowSmall,       new Vector2(3f, 9f)},

            {EnemyAvianSprite.KiteSnowSmall,        new Vector2(4f, 1f)},
            {EnemyAvianSprite.KiteSnowLarge,        new Vector2(5f, 1f)},
            {EnemyAvianSprite.KiteSnowTanSmall,     new Vector2(4f, 2f)},
            {EnemyAvianSprite.KiteSnowTanLarge,     new Vector2(5f, 2f)},
        };
    }
}