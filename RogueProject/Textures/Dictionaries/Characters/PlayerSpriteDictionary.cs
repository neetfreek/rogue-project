﻿/********************************************************************
* Contains dictionary with player sprites keys and associated       *
*   Vector2() positions (unscaled) in player (character)sprite      *
*   atlas texture                                                   *
*********************************************************************/
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RogueProject.Textures
{
    static class PlayerSpriteDictionary
    {
        // Map PlayerSprite enum to (unscaled) Vector2 coordinate position on sprite atlas texture
        public static Dictionary<PlayerSprite, Vector2> SpritesPlayer = new Dictionary<PlayerSprite, Vector2>()
        {
            {PlayerSprite.Leather,  new Vector2(2f, 0f)},
            {PlayerSprite.NoneFemale,  new Vector2(2f, 5f)},
            {PlayerSprite.NoneMale,  new Vector2(0f, 3f)},
            {PlayerSprite.Robe,  new Vector2(6f, 0f)},
            {PlayerSprite.RobeHooded,  new Vector2(6f, 3f)},
            {PlayerSprite.Chainmail,  new Vector2(1f, 0f)},
            {PlayerSprite.Plate,  new Vector2(1f, 3f)},

            {PlayerSprite.PortraitFemale,  new Vector2(1f, 2f)},
            {PlayerSprite.PortraitMale,  new Vector2(0f, 2f)},

        };
    }
}