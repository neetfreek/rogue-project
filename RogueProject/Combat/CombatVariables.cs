﻿namespace RogueProject.Combat
{
    public static class CombatVariables
    {
        public const int MULTIPLIER_HIT_CRITICAL = 2;

        public const int BASE_DEFENCE = 20;
    }
}