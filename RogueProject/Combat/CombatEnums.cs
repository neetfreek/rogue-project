﻿namespace RogueProject.Combat
{
    public enum HitRollType
    {
        None,
        Miss,
        MissCritical,
        Hit,
        HitCritical,
    }
}