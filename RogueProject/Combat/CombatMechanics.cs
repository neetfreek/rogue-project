﻿/********************************************************
* Combat-related helper functionality for combatants    *
*********************************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Terrain;

namespace RogueProject.Combat
{
    public static class CombatMechanics
    {
        private static readonly Random random = new Random();

        // Determine, return HitRollType
        public static HitRollType RollToHit(int ratingHit, int ratingDefence)
        {
            HitRollType hitRoll = HitRollType.None;

            int roll = random.Next(0, 101);
            int localRatingHit = ratingHit * 10;
            int localRatingDefence = ratingDefence;

            if (roll >= 95)
            {
                hitRoll = HitRollType.HitCritical;
            }
            else if (localRatingHit + roll >= localRatingDefence)
            {
                hitRoll = HitRollType.Hit;
            }

            if (roll <= 5)
            {
                hitRoll = HitRollType.MissCritical;
            }
            else if (localRatingHit + roll < localRatingDefence)
            {
                hitRoll = HitRollType.Miss;
            }

            return hitRoll;
        }

        // Process hits and misses 
        public static int ProcessHit(HitRollType hitType, Vector2 damageAttacker)
        {
            int damageRoll = random.Next((int)damageAttacker.X, (int)damageAttacker.Y + 1);
            int damage = 0;
            switch (hitType)
            {
                case HitRollType.Hit:
                    damage = damageRoll;
                    break;
                case HitRollType.HitCritical:
                    damage = damageRoll * CombatVariables.MULTIPLIER_HIT_CRITICAL;
                    break;
                case HitRollType.MissCritical:
                    damage = (int)(-Math.Round((decimal)(damageRoll * ((random.Next(15, 50)) * 0.01f)), 0, MidpointRounding.AwayFromZero));
                    if (damage == 0)
                    {
                        damage = -1;
                    }
                    break;
            }

            return damage;
        }

        // Return whether attack was obstructed or not
        public static bool AttackObstructed(Tile tileAttacker, Tile tileDefender, Direction directionToTarget)
        {
            Tile tileCheck = tileAttacker.Neighbour(directionToTarget, 1);
            Tile tileTarger = tileDefender;
            Direction directionBetweenTiles = directionToTarget;

            while (tileCheck != tileDefender)
            {
                if (tileCheck.TileType == TileType.Wall || tileCheck.TileType == TileType.Tree ||
                    tileCheck.TileType == TileType.Door)
                {
                    return true;
                }
                else
                {
                    tileCheck = tileCheck.Neighbour(directionBetweenTiles, 1);
                    directionBetweenTiles = Helper.DirectionBetween(tileTarger, tileCheck);
                }
            }
            return false;
        }
    }
}