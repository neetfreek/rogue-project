﻿/********************************************************************
* The camera object returns a transform matrix which is used in     *
*   SpriteBatch.Begin() calls for any in-game textures. The         *
*   matrix updates the transform of sprites inversely, meaning      *
*   that the camera moving Up one unit == all sprites being drawn   *
*   Down one unit.*                                                 *
*********************************************************************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;
using RogueProject.Terrain;

namespace RogueProject
{
    public class Camera2D
    {
        private readonly GraphicsDevice graphicsDevice;

        // Bounds restricting camera from moving beyond current game area's limits
        public Rectangle Bounds { get => bounds; }
        // Matrix transform for SpriteBatch.Begin() to move sprites inverse to camera position change
        public Matrix Matrix { get => matrixTransform; }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        private Matrix matrixTransform;
        private Vector2 position;
        private Rectangle bounds;

        public Camera2D(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
        }

        /********
        * Setup * 
        ********/
        public void SetupCamera(Vector2 sizeArea)
        {
            SetBounds(sizeArea);
        }

        private void SetBounds(Vector2 sizeArea)
        {
            bounds = new Rectangle(0 - GlobalVariables.WIDTH_HEIGHT_SPRITE, 0  - GlobalVariables.WIDTH_HEIGHT_SPRITE,
                (int)sizeArea.X * GlobalVariables.ScaledSprite,
                (int)sizeArea.Y * GlobalVariables.ScaledSprite);
        }

        // Called by Player.Move()
        public void Move(Tile tile)
        {
            Position = tile.Position;
        }
        // Called by opening GUI components
        public void Move(Vector2 position)
        {
            Position = position;
        }

        // Return MatrixTransform for parameter in SpriteBatch.Begin for drawing sprites
        public Matrix MatrixTransform(GraphicsDevice graphicsDevice)
        {
            matrixTransform = Matrix.CreateTranslation(new Vector3(-Position.X, -Position.Y, 0f)) *
                Matrix.CreateTranslation(new Vector3(graphicsDevice.Viewport.Width * 0.5f, graphicsDevice.Viewport.Height * 0.5f, 0f));

            return matrixTransform;
        }
    }
}