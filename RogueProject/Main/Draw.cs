﻿/************************************************************
* Contain all draw functionality, called by Game1.Draw()    * 
*************************************************************/
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.Common;
using RogueProject.Enemies;
using RogueProject.GUI;
using RogueProject.Items;
using RogueProject.Objects;
using RogueProject.PlayerCharacter;
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject
{
    public class Draw
    {
        private Game1 game;
        public Area Area;
        private Camera2D camera;
        private Player player;
        private PlayerAction playerAction;

        public Vector2 MousePos;

        private readonly GraphicsDevice graphicsDevice;
        private SpriteBatch spriteBatch;
        private TextureLoader textures;
        private Color colourInteractable = Color.LightGreen;
        private Color colourUnInteractable = Color.PaleVioletRed;
        private Color colourMouseOverIcon = Color.LightGray;
        private Color colourDefault = Color.White;


        public Draw(Game1 game)
        {
            this.game = game;
            graphicsDevice = game.GraphicsDevice;
            spriteBatch = game.SpriteBatch;
            textures = game.Textures;
            camera = game.Camera;
            player = game.Player;
            playerAction = game.PlayerAction;
        }


        public void DrawGame()
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                SamplerState.PointWrap, null, null, null, camera.MatrixTransform(graphicsDevice));

            DrawGrid();
            DrawCursor();

            spriteBatch.End();
        }
        public void DrawGUI()
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                SamplerState.PointWrap, null, null, null, camera.MatrixTransform(graphicsDevice));

            if (game.GUIAction.CurrentComponentsList != null)
            {
                DrawGUIComponents();
            }
            DrawCursor();
            spriteBatch.End();
        }


        /************************************************
        * Routine handles drawing for all tiles in grid * 
        *************************************************/
        private void DrawGrid()
        {
            DrawCursor();
            for (int counterRows = 0; counterRows < Area.Grid.GetLength(1); counterRows++)
            {
                for (int counterCols = 0; counterCols < Area.Grid.GetLength(0); counterCols++)
                {
                    Tile tile = Area.Grid[counterCols, counterRows];

                    if (tile.TileType == TileType.Door)
                    {
                        if (tile.Door.DoorSpriteOrientation == DoorSpriteOrientation.Vertical)
                        {
                            DrawFloorDoorHalves(tile);
                        }
                        DrawDoor(tile.Door);
                    }

                    if (tile.TileType == TileType.Floor)
                    {
                        DrawFloor(tile);
                    }

                    if (tile.TileType == TileType.Tree)
                    {
                        DrawTree(tile);
                    }

                    if (tile.TileType == TileType.Wall)
                    {
                        DrawWall(tile);
                    }

                    if (tile.Entrance != null)
                    {
                        DrawEntrance(tile.Entrance);
                    }

                    if (tile.Item != null)
                    {
                        DrawItemTile(tile);
                    }

                    if (tile.character != Characters.None)
                    {
                        HandleDrawCharacter(tile);
                    }

                    if (tile.DecorationGroundCategory != DecorationGroundSpriteCategory.None)
                    {
                        DrawDecoration(tile);
                    }
                }
            }
        }


        /********************
        * Draw floor tiles  * 
        *********************/
        private void DrawFloor(Tile tile)
        {
            spriteBatch.Draw(textures.TextureFloor, new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X,
            (int)tile.Size.Y),
            textures.FloorAtlas.SourceRectangleSprite(tile.FloorCategory, tile.FloorType),
            Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_FLOOR);
        }
        private void DrawFloorDoorHalves(Tile tile)
        {
            // Draw normal tile if both sides' sprites the same
            if (tile.FloorCategory == tile.Door.FloorCategoryNeighbour)
            {
                DrawFloor(tile);
            }
            // Else draw half inner, half outer sprites under door
            else
            {
                // Draw innerFloor half
                spriteBatch.Draw(textures.TextureFloor, InnerDoorFloor(tile),
                textures.FloorAtlas.SourceRectangleSprite(tile.FloorCategory, tile.FloorType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_FLOOR);
                // Draw outerFloor half
                spriteBatch.Draw(textures.TextureFloor, OuterDoorFloor(tile),
                textures.FloorAtlas.SourceRectangleSprite(tile.Door.FloorCategoryNeighbour, tile.FloorType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_FLOOR);
            }
        }
        private void DrawDoorFloors()
        {
            for (int counterRows = 0; counterRows < Area.Grid.GetLength(1); counterRows++)
            {
                for (int counterCols = 0; counterCols < Area.Grid.GetLength(0); counterCols++)
                {
                    Tile tile = Area.Grid[counterCols, counterRows];

                    if (tile.TileType == TileType.Door)
                    {
                        DrawFloorDoorHalves(tile);
                    }
                }
            }
        }
        private Rectangle InnerDoorFloor(Tile tile)
        {
            Rectangle innerDoorFloor = 
                new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X, (int)tile.Size.Y)
                {
                    Width = (int)(GlobalVariables.ScaledSprite * 0.5f),
                    Size = new Point((int)(GlobalVariables.ScaledSprite), GlobalVariables.ScaledSprite)
                };
            if (tile.Door.NeighbourDirection == Direction.Left)
            {
                innerDoorFloor.Offset(GlobalVariables.ScaledSprite * 0.5f, 0);
            }
            else
            {
                innerDoorFloor.Offset(-GlobalVariables.ScaledSprite * 0.5f, 0);
            }

            return innerDoorFloor; 
        }
        private Rectangle OuterDoorFloor(Tile tile)
        {
            Rectangle outerDoorFloor = 
                new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X, (int)tile.Size.Y)
                {
                    Width = (int)(GlobalVariables.ScaledSprite * 0.5f),
                    Size = new Point((int)(GlobalVariables.ScaledSprite), GlobalVariables.ScaledSprite)
                };
            if (tile.Door.NeighbourDirection == Direction.Left)
            {
                outerDoorFloor.Offset(-GlobalVariables.ScaledSprite * 0.5f, 0);
            }
            else
            {
                outerDoorFloor.Offset(GlobalVariables.ScaledSprite * 0.5f, 0);
            }

            return outerDoorFloor;
        }


        /************************
        * Draw terrain tiles    * 
        *************************/
        private void DrawTree(Tile tile)
        {
            spriteBatch.Draw(textures.TextureFloor, new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X,
                (int)tile.Size.Y),
                textures.FloorAtlas.SourceRectangleSprite(tile.FloorCategory, tile.FloorType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, tile.Layer - 0.1f);

            spriteBatch.Draw(textures.TextureTree,
                new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X,
            (int)tile.Size.Y),
                textures.TreeSpriteAtlas.SourceRectangleSprite(tile.TreeCategory, tile.TreeType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, tile.Layer + 0.1f);
        }
        private void DrawWall(Tile tile)
        {
            spriteBatch.Draw(textures.TextureWall,
                new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X,
            (int)tile.Size.Y),
                textures.WallAtlas.SourceRectangleSprite(tile.WallCategory, tile.WallType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_WALL);
        }
        private void DrawDecoration(Tile tile)
        {
            spriteBatch.Draw(textures.TextureDecorationGround,
                new Rectangle((int)tile.Position.X, (int)tile.Position.Y, (int)tile.Size.X,
            (int)tile.Size.Y),
                textures.DecorationGroundAtlas.SourceRectangleSprite(tile.DecorationGroundCategory, tile.DecorationGroundType),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_DECORATIONS);
        }

        /****************
        * Draw objects  * 
        *****************/
        // Contains highlight door as well
        private void DrawDoor(Door door)
        {
            Color colour = Color.White;
            if (playerAction.HighlightTile == door.Tile)
            {
                colour = colourInteractable;
                if (door.Locked)
                {
                    colour = colourUnInteractable;
                }
            }

            spriteBatch.Draw(textures.TextureDoor,
                new Rectangle((int)door.Tile.Position.X, (int)door.Tile.Position.Y, (int)door.Tile.Size.X,
            (int)door.Tile.Size.Y),
                textures.DoorAtlas.SourceRectangleSprite(door.DoorSpriteCategory, door.DoorSpriteOrientation, DoorSprite.None, door.DoorState),
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_DOOR);

            if (door.DoorState == DoorState.Open && door.DoorSpriteOrientation == DoorSpriteOrientation.Horizontal)
            {
                DrawFloor(door.Tile);
            }
        }
        // Contains highlight entrance as well
        private void DrawEntrance(Entrance entrance)
        {
            Color colour = Color.White;
            if (playerAction.HighlightTile == entrance.Tile)
            {
                colour = colourInteractable;
            }

            if (entrance.Tile.EntranceSprite == EntranceSprite.None)
            {
                spriteBatch.Draw(textures.TextureFloor,
                    new Rectangle((int)entrance.Tile.Position.X, (int)entrance.Tile.Position.Y, (int)entrance.Tile.Size.X,
                (int)entrance.Tile.Size.Y),
                    textures.FloorAtlas.SourceRectangleSprite(Area.FloorCategory, FloorSpriteType.MidM),
                    colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_DOOR);
            }
            else
            {
                spriteBatch.Draw(textures.TextureEntrance,
                    new Rectangle((int)entrance.Tile.Position.X, (int)entrance.Tile.Position.Y, (int)entrance.Tile.Size.X,
                (int)entrance.Tile.Size.Y),
                    textures.EntranceAtlas.SourceRectangleSprite(entrance.Tile.EntranceSprite),
                    colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_DOOR);
            }
        }        
        private void DrawItemTile(Tile tile)
        {
            Item item = tile.Item;

            Color colour = Color.White;
            if (playerAction.HighlightTile == tile)
            {
                colour = colourInteractable;
            }
            Rectangle sourceRectangle = new Rectangle(0, 0, 0, 0);
            Texture2D texture = textures.TextureArmour;
            switch (item.CategoryItem)
            {                
                case CategoryItem.Armour:
                    sourceRectangle = textures.ArmourSpriteAtlas.Sprite(item.SpriteArmour);
                    break;
                case CategoryItem.Weapon:
                    sourceRectangle = textures.WeaponSpriteAtlas.Sprite(item.SpriteWeapon);
                    texture = textures.TextureWeapon;
                    break;
                //case CategoryItem.Consumable:
                //    break;
            }

            spriteBatch.Draw(texture,
                new Rectangle((int)item.Tile.Position.X, (int)item.Tile.Position.Y, (int)item.Tile.Size.X,
            (int)item.Tile.Size.Y),
                sourceRectangle,
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_ITEM);
        }
        private void DrawItemGrid(Item item, GUIRectangle cellGrid)
        {
            Color colour = colourDefault;
            if (cellGrid == game.GUIAction.CurrentSubComponent)
            {
                colour = colourMouseOverIcon;
            }
            if (item == game.GUIAction.itemDragged)
            {
                colour = colourUnInteractable;
            }
            Rectangle sourceRectangle = new Rectangle(0, 0, 0, 0);
            Texture2D texture = textures.TextureArmour;
            switch (item.CategoryItem)
            {
                case CategoryItem.Armour:
                    sourceRectangle = textures.ArmourSpriteAtlas.Sprite(item.SpriteArmour);
                    break;
                case CategoryItem.Weapon:
                    sourceRectangle = textures.WeaponSpriteAtlas.Sprite(item.SpriteWeapon);
                    texture = textures.TextureWeapon;
                    break;
                    //case CategoryItem.Consumable:
                    //    break;
            }

            spriteBatch.Draw(texture,
                new Rectangle((int)cellGrid.Position.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, (int)cellGrid.Position.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE),
                sourceRectangle,
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GUIVariables.LAYER_SUBCOMPONENT + 0.1f);
        }


        /************************
        * Draw GUI components   * 
        *************************/
        private void DrawInventory(GUIRectangle component)
        {
            int counter = 0;
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                DrawGUICellGrid(subComponent);
                if (game.PlayerInventory.Inventory[counter] != null)
                {
                    DrawItemGrid(game.PlayerInventory.Inventory[counter], subComponent);
                }
                counter++;
            }
        }
        private void DrawEquipment(GUIRectangle component)
        {
            int counter = 0;
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                DrawGUICellGrid(subComponent);
                if (game.PlayerInventory.Equipment[counter] != null)
                {
                    DrawItemGrid(game.PlayerInventory.Equipment[counter], subComponent);
                }
                else
                {
                    spriteBatch.Draw(textures.TextureArmour,
                    new Rectangle((int)subComponent.Position.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, (int)subComponent.Position.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE),
                    textures.ArmourSpriteAtlas.Sprite(SpritePlaceholderEquipment(component, counter)),
                    colourDefault, 0f, new Vector2(0f, 0f), SpriteEffects.None, GUIVariables.LAYER_SUBCOMPONENT + 0.1f);
                }
                counter++;
            }
        }
        private void DrawTrinkets(GUIRectangle component)
        {
            int counter = 0;
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                DrawGUICellGrid(subComponent);
                if (game.PlayerInventory.Trinkets[counter] != null)
                {
                    DrawItemGrid(game.PlayerInventory.Trinkets[counter], subComponent);
                }
                else
                {
                    spriteBatch.Draw(textures.TextureArmour,
                    new Rectangle((int)subComponent.Position.X * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, (int)subComponent.Position.Y * GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE, GlobalVariables.WIDTH_HEIGHT_SPRITE * (int)GlobalVariables.SCALE_SPRITE),
                    textures.ArmourSpriteAtlas.Sprite(SpritePlaceholderEquipment(component, counter)),
                    colourDefault, 0f, new Vector2(0f, 0f), SpriteEffects.None, GUIVariables.LAYER_SUBCOMPONENT + 0.1f);
                }
                counter++;
            }
        }

        private ArmourSprite SpritePlaceholderEquipment(GUIRectangle component, int index)
        {
            ArmourSprite sprite = ArmourSprite.None;
            if (component.TextHeader == GUIVariables.HEADING_EQUIPMENT)
            {
                switch (index)
                {
                    case 0:
                        sprite = ArmourSprite.SlotEquipmentWeapon;
                        break;
                    case 1:
                        sprite = ArmourSprite.SlotEquipmentOffhand;
                        break;
                    case 2:
                        sprite = ArmourSprite.SlotEquipmentHead;
                        break;
                    case 3:
                        sprite = ArmourSprite.SlotEquipmentChest;
                        break;
                    case 4:
                        sprite = ArmourSprite.SlotEquipmentHands;
                        break;
                    case 5:
                        sprite = ArmourSprite.SlotEquipmentFeet;
                        break;
                }
            }
            if (component.TextHeader == GUIVariables.HEADING_TRINKETS)
            {
                switch (index)
                {
                    case 0:
                        sprite = ArmourSprite.SlotEquipmentRing;
                        break;
                    case 1:
                        sprite = ArmourSprite.SlotEquipmentRing;
                        break;
                    case 2:
                        sprite = ArmourSprite.SlotEquipmentAmulet;
                        break;
                    case 3:
                        sprite = ArmourSprite.SlotEquipmentCharm;
                        break;
                }
            }

            return sprite;
        }

        /********************
        * Draw characters   * 
        *********************/
        private void HandleDrawCharacter(Tile tile)
        {
            switch (tile.character)
            {
                case Characters.Player:
                    DrawPlayer();
                    break;
                case Characters.Enemy:
                    HandleDrawEnemy(tile.Enemy);
                    break;
            }
        }
        private void DrawPlayer()
        {
            SpriteEffects spriteEffects = SpriteEffects.None;
            if (player.Direction == Direction.UpRight || player.Direction == Direction.Right || player.Direction == Direction.DownRight)
            {
                spriteEffects = SpriteEffects.FlipHorizontally;
            }

            spriteBatch.Draw(textures.TexturePlayer,
                new Rectangle((int)player.Tile.Position.X, (int)player.Tile.Position.Y, (int)player.Tile.Size.X,
            (int)player.Tile.Size.Y),
                textures.PlayerAtlas.SourceRectangleSprite(player.Sprite),
                Color.White, 0f, new Vector2(0f, 0f), spriteEffects, GlobalVariables.LAYER_DRAW_CHARACTER);
        }

        private void HandleDrawEnemy(Enemy enemy)
        {
            Color colour = Color.White;
            if (playerAction.HighlightTile == enemy.Tile)
            {
                colour = colourInteractable;
            }

            switch (enemy.EnemyCategory)
            {
                case EnemyCategory.Avian:
                    DrawEnemyAvian(enemy, colour);
                    break;
                case EnemyCategory.Pest:
                    DrawEnemyPest(enemy, colour);
                    break;
                case EnemyCategory.Rodent:
                    DrawEnemyRodent(enemy, colour);
                    break;
            }
        }
        private void DrawEnemyAvian(Enemy enemy, Color colour)
        {
            spriteBatch.Draw(textures.TextureEnemyAvian,
                new Rectangle((int)enemy.Tile.Position.X, (int)enemy.Tile.Position.Y, (int)enemy.Tile.Size.X,
            (int)enemy.Tile.Size.Y),
                textures.EnemyAvianAtlas.SourceRectangleSprite(enemy.EnemyType),
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_CHARACTER);
        }
        private void DrawEnemyPest(Enemy enemy, Color colour)
        {
            spriteBatch.Draw(textures.TextureEnemyPest,
                new Rectangle((int)enemy.Tile.Position.X, (int)enemy.Tile.Position.Y, (int)enemy.Tile.Size.X,
            (int)enemy.Tile.Size.Y),
                textures.EnemyPestAtlas.SourceRectangleSprite(enemy.EnemyType),
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_CHARACTER);
        }
        private void DrawEnemyRodent(Enemy enemy, Color colour)
        {
            spriteBatch.Draw(textures.TextureEnemyRodent,
                new Rectangle((int)enemy.Tile.Position.X, (int)enemy.Tile.Position.Y, (int)enemy.Tile.Size.X,
            (int)enemy.Tile.Size.Y),
                textures.EnemyRodentAtlas.SourceRectangleSprite(enemy.EnemyType),
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_CHARACTER);
        }


        /********************************************
        * Routine handles draw GUI component parts  * 
        *********************************************/
        private void DrawCursor()
        {
            Vector2 posDraw = CursorDrawPosition();

            spriteBatch.Draw(textures.TextureGUI,
                new Rectangle((int)(posDraw.X), (int)(posDraw.Y),
                (int)(GUIVariables.WIDTH_HEIGHT_TILE * 0.75f), (int)(GUIVariables.WIDTH_HEIGHT_TILE * 0.75f)),
                textures.GUIAtlas.SourceRectangleSprite(GUISpriteCategory.Cursor, GUISpriteType.Cursor),
                Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, GlobalVariables.LAYER_DRAW_CURSOR);
        }
        private Vector2 CursorDrawPosition()
        {
            Vector2 cursorDrawPosition = new Vector2(MousePos.X - GlobalVariables.WidthScreenActual * 0.5f,
                MousePos.Y - GlobalVariables.HeightScreenActual * 0.5f);

            if (Game1.InGame)
            {
                cursorDrawPosition += game.Player.Tile.Position;
            }

            return cursorDrawPosition;
        }

        private void DrawGUIComponents()
        {
            foreach (GUIRectangle component in game.GUIAction.CurrentComponentsList)
            {
                // Panels, menus, textbox
                DrawGUIRectComponent(component);
            }
        }

        private void DrawGUIRectComponent(GUIRectangle component)
        {
            // Draw component
            for (int counterCol = 0; counterCol < component.Size.X; counterCol++)
            {
                for (int counterRow = 0; counterRow < component.Size.Y; counterRow++)
                {
                    spriteBatch.Draw(textures.TextureGUI,
                        new Rectangle((int)component.PositionScaled.X + (counterCol * GUIVariables.WIDTH_HEIGHT_TILE),
                        (int)component.PositionScaled.Y + (counterRow * GUIVariables.WIDTH_HEIGHT_TILE),
                        GUIVariables.WIDTH_HEIGHT_TILE, GUIVariables.WIDTH_HEIGHT_TILE),
                        textures.GUIAtlas.SourceRectangleSprite(component.SpriteCategory, GetGUIRectPartType(component.Size, counterCol, counterRow)),
                        Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, component.Layer);

                    if (component.Text != "" && component.ShowText)
                    {
                        DrawText(component);
                    }
                    if (component.TextHeader != "" && component.ShowTextHeader)
                    {
                        DrawTextHeader(component);
                    }
                }
            }

            if (component.TextHeader == GUIVariables.HEADING_INVENTORY)
            {
                DrawInventory(component);
            }
            if (component.TextHeader == GUIVariables.HEADING_EQUIPMENT)
            {
                DrawEquipment(component);
            }
            if (component.TextHeader == GUIVariables.HEADING_TRINKETS)
            {
                DrawTrinkets(component);
            }
            // Draw subcomponents
            foreach (GUIRectangle subComponent in component.Subcomponents)
            {
                if (subComponent.IsIcon)
                {
                    DrawGUIIcon(subComponent);
                }
                else if (subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_HEALTH ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_MANA ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_BORDER_EXPERIENCE)
                     {
                         DrawGUIBorderComponent(subComponent);
                     }
                else if (subComponent.TextHeader == GUIVariables.HEADING_BAR_HEALTH ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_MANA ||
                    subComponent.TextHeader == GUIVariables.HEADING_BAR_EXPERIENCE)
                     {
                         DrawGUIBarComponent(subComponent);
                     }
                else
                {
                    DrawGUIRectComponent(subComponent);
                }

            }
        }
        private void DrawGUIBorderComponent(GUIRectangle component)
        {
            for (int counterCol = 0; counterCol < component.Size.X; counterCol++)
            {
                for (int counterRow = 0; counterRow < component.Size.Y; counterRow++)
                {
                    spriteBatch.Draw(textures.TextureGUI,
                        new Rectangle((int)component.PositionScaled.X + (counterCol * GUIVariables.WIDTH_HEIGHT_TILE),
                        (int)component.PositionScaled.Y + (counterRow * GUIVariables.WIDTH_HEIGHT_TILE),
                        GUIVariables.WIDTH_HEIGHT_TILE, GUIVariables.WIDTH_HEIGHT_TILE),
                        textures.GUIAtlas.SourceRectangleSprite(component.SpriteCategory, GetGUIBarBorderType((int)component.Size.X, counterCol)),
                        Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, component.Layer);
                }
            }
        }
        private void DrawGUIBarComponent(GUIRectangle component)
        {
            for (int counterCol = 0; counterCol < component.Size.X; counterCol++)
            {
                for (int counterRow = 0; counterRow < component.Size.Y; counterRow++)
                {
                    spriteBatch.Draw(textures.TextureGUI,
                        new Rectangle((int)component.PositionScaled.X + (counterCol * GUIVariables.WIDTH_HEIGHT_TILE),
                        (int)component.PositionScaled.Y + (counterRow * GUIVariables.WIDTH_HEIGHT_TILE),
                        GUIVariables.WIDTH_HEIGHT_TILE, GUIVariables.WIDTH_HEIGHT_TILE),
                        textures.GUIAtlas.SourceRectangleSprite(component.SpriteCategory, HandleGUIBarType(component, (int)component.Size.X, counterCol)),
                        Color.White, 0f, new Vector2(0f, 0f), SpriteEffects.None, component.Layer);
                }
            }
        }
        private void DrawGUIIcon(GUIRectangle subComponent)
        {
            Texture2D texture = textures.GUIIconSpriteAtlas.TextureSprite(subComponent.TextHeader);
            Rectangle source;
            if (subComponent.TextHeader == GUIVariables.TEXT_ICON_CHARACTER_SHEET)
            {
                source = textures.GUIIconSpriteAtlas.SourceRectangleCharacterSheet(game.PlayerStats.Gender);
            }
            else
            {
                source = textures.GUIIconSpriteAtlas.SourceRectangleSprite(subComponent.TextHeader);
            }
            Color colour = colourDefault;
            if (subComponent == game.GUIAction.CurrentSubComponent && subComponent.IsIcon)
            {
                colour = colourMouseOverIcon;
            }
            spriteBatch.Draw(texture,
                new Rectangle((int)subComponent.PositionScaled.X,
                (int)subComponent.PositionScaled.Y,
                GUIVariables.WIDTH_HEIGHT_TILE, GUIVariables.WIDTH_HEIGHT_TILE),
                source, colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, subComponent.Layer);
        }
        private void DrawGUICellGrid(GUIRectangle subComponent)
        {
            Color colour = colourDefault;
            if (subComponent == game.GUIAction.CurrentSubComponent)
            {
                colour = colourMouseOverIcon;
            }
            spriteBatch.Draw(textures.TextureGUI,
                new Rectangle((int)subComponent.PositionScaled.X,
                (int)subComponent.PositionScaled.Y,
                GUIVariables.WIDTH_HEIGHT_TILE, GUIVariables.WIDTH_HEIGHT_TILE),
                textures.GUIAtlas.SourceRectangleSprite(GUISpriteCategory.Square, CellGridType(subComponent)),
                colour, 0f, new Vector2(0f, 0f), SpriteEffects.None, subComponent.Layer);
        }

        private void DrawText(GUIRectangle component)
        {
            spriteBatch.DrawString(textures.Font, component.Text, component.PositionText, component.TextColour, 0f, CenterText(component), ScaleText(component), SpriteEffects.None, GUIVariables.LAYER_TEXT);
        }
        private void DrawTextHeader(GUIRectangle component)
        {
            spriteBatch.DrawString(textures.Font, component.TextHeader, component.PositionTextHeader, component.TextHeaderColour, 0f, CenterText(component), ScaleText(component), SpriteEffects.None, GUIVariables.LAYER_TEXT);
        }


        /***'****************
        * GUI draw helpers  * 
        *********************/
        private GUISpriteType GetGUIRectPartType(Vector2 sizeRectange, int counterCol, int counterRow)
        {
            Vector2 size = sizeRectange;
            GUISpriteType spriteType = GUISpriteType.MidM;

            // Top row,corners
            if (counterRow == 0)
            {
                if (counterCol == 0)
                {
                    spriteType = GUISpriteType.TopL;
                }
                else if (counterCol == sizeRectange.X - 1)
                {
                    spriteType = GUISpriteType.TopR;
                }
                else
                {
                    spriteType = GUISpriteType.TopM;
                }
            }
            // Bottom row, corners
            else if (counterRow == sizeRectange.Y -1)
            {
                if (counterCol == 0)
                {
                    spriteType = GUISpriteType.BotL;
                }
                else if (counterCol == sizeRectange.X - 1)
                {
                    spriteType = GUISpriteType.BotR;
                }
                else
                {
                    spriteType = GUISpriteType.BotM;
                }
            }
            // Left column
            else if (counterCol == 0)
            {
                spriteType = GUISpriteType.MidL;
            }

            // Right column
            else if (counterCol == sizeRectange.X - 1)
            {
                spriteType = GUISpriteType.MidR;
            }

            return spriteType;
        }

        private GUISpriteType GetGUIBarBorderType(int numberColumns, int counterCol)
        {
            GUISpriteType spriteType = GUISpriteType.BarBorderM;
            if (counterCol == 0)
            {
                spriteType = GUISpriteType.BarBorderL;
            }
            if (counterCol == numberColumns - 1)
            {
                spriteType = GUISpriteType.BarBorderR;
            }
            return spriteType;

        }
        private GUISpriteType HandleGUIBarType(GUIRectangle bar, int numberColumns, int counterCol)
        {
            GUISpriteType spriteType = GUISpriteType.Bar0;

            if (bar.TextHeader == GUIVariables.HEADING_BAR_HEALTH ||
                bar.TextHeader == GUIVariables.HEADING_BAR_MANA ||
                 bar.TextHeader == GUIVariables.HEADING_BAR_EXPERIENCE)
            {
                spriteType = GUIBarType(bar, numberColumns, counterCol);
            }

            return spriteType;
        }
        private GUISpriteType GUIBarType(GUIRectangle bar, int numberColumns, int counterCol)
        {
            float singleBarPortionOfTotal =
                    (float)(Math.Round((decimal)((100f / numberColumns) * 0.01f), 3, MidpointRounding.AwayFromZero));
            float currentBarPortionOfTotal =
                    (float)(Math.Round((decimal)((counterCol * singleBarPortionOfTotal) + singleBarPortionOfTotal), 3, MidpointRounding.AwayFromZero));
            float remainderPortionBar = 0f;
            float numberPercent = 0f;

            if (bar.TextHeader == GUIVariables.HEADING_BAR_HEALTH)
            {
                remainderPortionBar =
                    (float)(Math.Round((decimal)(singleBarPortionOfTotal + game.PlayerStats.HealthPercent - currentBarPortionOfTotal), 3, MidpointRounding.AwayFromZero));
                numberPercent = game.PlayerStats.HealthPercent;

            }
            if (bar.TextHeader == GUIVariables.HEADING_BAR_MANA)
            {
                remainderPortionBar =
                    (float)(Math.Round((decimal)(singleBarPortionOfTotal + game.PlayerStats.ManaPercent - currentBarPortionOfTotal), 3, MidpointRounding.AwayFromZero));
                numberPercent = game.PlayerStats.ManaPercent;

            }
            if (bar.TextHeader == GUIVariables.HEADING_BAR_EXPERIENCE)
            {
                remainderPortionBar =
                    (float)(Math.Round((decimal)(singleBarPortionOfTotal + game.PlayerStats.ExperiencePercent - currentBarPortionOfTotal), 3, MidpointRounding.AwayFromZero));
                numberPercent = game.PlayerStats.ExperiencePercent;

            }
            GUISpriteType spriteType = GUISpriteType.Bar0;

            if (numberPercent == currentBarPortionOfTotal)
            {
                spriteType = GUISpriteType.Bar100End;
            }
            else if (numberPercent > currentBarPortionOfTotal)
            {
                spriteType = GUISpriteType.Bar100;
            }
            else if (numberPercent < currentBarPortionOfTotal &&
                    numberPercent > currentBarPortionOfTotal - singleBarPortionOfTotal)
                 {
                    spriteType = GUIBarSectionType(remainderPortionBar);
                 }
            return spriteType;
        }

        private GUISpriteType GUIBarSectionType(float portionHealthInBar)
        {
            GUISpriteType sectionType = GUISpriteType.Bar0;

            if (portionHealthInBar > 0.10f)
            {
                sectionType = GUISpriteType.Bar75;
            }
            else if (portionHealthInBar > 0.05f)
            {
                sectionType = GUISpriteType.Bar50;
            }
            else
            {
                sectionType = GUISpriteType.Bar25;
            }

            return sectionType;
        }

        private GUISpriteType CellGridType(GUIRectangle panel)
        {
            GUISpriteType type = GUISpriteType.None;

            if (panel.TextHeader == GUIVariables.HEADING_CELL_INVENTORY || panel.TextHeader == GUIVariables.HEADING_CELL_EQUIPMENT ||
                panel.TextHeader == GUIVariables.HEADING_CELL_TRINKETS)
            {
                type = GUISpriteType.SquareGreyDark;
            }

            return type;
        }

        private float ScaleText(GUIRectangle component)
        {
            float scale = GUIVariables.SCALE_STANDARD;

            if (component.GuiType == GUIType.Menu)
            {
                scale = GUIVariables.SCALE_HEADING;
            }
            if (component.GuiType == GUIType.TextBox)
            {
                scale = GUIVariables.SCALE_TEXT_BOX;
            }
            if (component.TextHeader == GUIVariables.HEADING_INSEPCTION)
            {
                scale = GUIVariables.SCALE_INSPECTION;
            }

            if (component.TextHeader == GUIVariables.HEADING_PANEL_NEWGAME)
            {
                scale = GUIVariables.SCALE_HEADING;
            }

            if (component.Parent != null)
            {
                if (component.Parent.TextHeader == GUIVariables.HEADING_CHARACTER_SHEET)
                {
                    scale = component.SizeText;
                }
            }


            return scale;
        }
        private Vector2 CenterText(GUIRectangle component)
        {
            Vector2 centeredPosition = new Vector2();

            if (component.GuiType == GUIType.Button)
            {
                centeredPosition = new Vector2(0f, 0f) + (textures.Font.MeasureString(component.TextHeader) * 0.5f);
            }
            if (component.GuiType == GUIType.Menu)
            {
                centeredPosition = new Vector2(0f, 0f) + (textures.Font.MeasureString(component.TextHeader) * 0.5f);
                centeredPosition.Y -= textures.Font.MeasureString(component.TextHeader).Y * (GUIVariables.SCALE_HEADING + 1f);
            }
            if (component.TextHeader == GUIVariables.HEADING_INSEPCTION)
            {
                centeredPosition = new Vector2(1f, 0f);
                centeredPosition.X -= (component.Size.X * 0.666f) * GlobalVariables.SCALE_SPRITE * GlobalVariables.WIDTH_HEIGHT_SPRITE;
                centeredPosition.X += textures.Font.MeasureString(component.Text).X * 0.5f;
                centeredPosition.Y -= textures.Font.MeasureString(component.TextHeader).Y * (GUIVariables.SCALE_INSPECTION + 1f);
            }
            if (component.TextHeader == GUIVariables.HEADING_FIELD_NAME && Game1.InGame)
            {
                centeredPosition.X += textures.Font.MeasureString(component.Text).X * 0.5f;
            }
            return centeredPosition;
        }
    }
}