﻿/********************************************************************
* Entry-point of program                                            *
* Create new game object (handles game loop) and call game.Run()    * 
*********************************************************************/
using System;

namespace RogueProject
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
}
