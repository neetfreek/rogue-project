﻿/********************
* Handles gameloop  *
********************/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueProject.PlayerCharacter;
using RogueProject.Textures;
using RogueProject.Common;
using RogueProject.Terrain;
using RogueProject.Input;
using RogueProject.Data;
using RogueProject.GUI;
using RogueProject.Enemies;

namespace RogueProject
{
    public class Game1 : Game
    {
        // Graphics
        public SpriteBatch SpriteBatch;
        public TextureLoader Textures;
        public Camera2D Camera;
        public Draw draw;
        private GraphicsDeviceManager graphicsDeviceManager;
        private RenderTarget2D renderTarget;
        // Game components
        public Area Area;
        public AreaMaker AreaMaker;
            // Player
        public Player Player;
        public PlayerStats PlayerStats;
        public PlayerAction PlayerAction;
        public PlayerMove PlayerMove;
        public PlayerSetup PlayerSetup;
        public PlayerInventory PlayerInventory;
            // Enemies
        public EnemySetup EnemySetup;
        public EnemyAction EnemyAction;
        public EnemyMove EnemyMove;
        // Game states
        public static int CurrentAreaIndex;
        public static int HighestAreaPlanned;
        public static bool InGame;
        public static bool InMainMenu;
        public static bool GameStarted;
        public static bool LoadingGame;
        public static bool DestroyingEnemiesForLoad;
        public static int PlannedArea;
        public static int PreviousArea;
        public SaveGame SaveGame;
        // Input
        public InputKeyboard InputKeyboard;
        public InputMouse InputMouse;
        // GUI
        public GUIMaker GUIMaker;
        public GUIAction GUIAction;
        public TextBoxes TextBoxes = new TextBoxes();

        public Game1()
        {            
            graphicsDeviceManager = new GraphicsDeviceManager(this)
            {
                // Set BackBuffer size to virtual (development size); will be re-scaled, projected, onto RenderTarget
                PreferredBackBufferWidth = GlobalVariables.WIDTH_SCREEN_VIRTUAL,
                PreferredBackBufferHeight = GlobalVariables.HEIGHT_SCREEN_VIRTUAL,
                IsFullScreen = true
            };

            Content.RootDirectory = "Content";
        }


        /************
        * Game loop *
        *************/
        protected override void Initialize()
        {
            // Initialise draw-related objects
            SpriteBatch = new SpriteBatch(graphicsDeviceManager.GraphicsDevice);
            Textures = new TextureLoader();
            Camera = new Camera2D(GraphicsDevice);
            PresentationParameters presentationParameters = graphicsDeviceManager.GraphicsDevice.PresentationParameters;
                // Takes BackBuffer sprites, re-sized by LetterPillarBox(), used in drawing in SpriteBatch
            renderTarget = new RenderTarget2D(graphicsDeviceManager.GraphicsDevice,
                GlobalVariables.WIDTH_SCREEN_VIRTUAL, GlobalVariables.HEIGHT_SCREEN_VIRTUAL, false,
                SurfaceFormat.Color, DepthFormat.None, presentationParameters.MultiSampleCount,
                RenderTargetUsage.DiscardContents);
            // Initialise global variables
            GlobalVariables.WidthScreenActual = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            GlobalVariables.HeightScreenActual = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            // Initialise game components
            TileCategoryLists tileCategoryLists = new TileCategoryLists();
            AreaMaker = new AreaMaker(this);
                // Player
            Player = new Player(this);
            PlayerStats = new PlayerStats(this);
            PlayerMove = new PlayerMove(this);
            PlayerAction = new PlayerAction(this);
            PlayerSetup = new PlayerSetup(this);
            PlayerInventory = new PlayerInventory(this);
            // Enemies
            EnemySetup = new EnemySetup(this);
            EnemyMove = new EnemyMove(this);
            EnemyAction = new EnemyAction(this);
            // Initialise draw
            draw = new Draw(this);
            // Initialise state
            SaveGame = new SaveGame();
            GUIMaker = new GUIMaker(this);
            GUIAction = new GUIAction(this);

            StartGame();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            Textures.LoadContent(Content);
        }
        protected override void UnloadContent()
        {
        }
        protected override void Update(GameTime gameTime)
        {
            InputKeyboard.Update();
            InputMouse.Update();

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            // Set draw to renderTarget
            graphicsDeviceManager.GraphicsDevice.SetRenderTarget(renderTarget);
            // Draw background
            GraphicsDevice.Clear(Color.Black);

            if (InGame)
            {
                // Call draw methods
                draw.DrawGame();
                draw.DrawGUI();
            }
            if (InMainMenu)
            {
                // Call draw methods
                draw.DrawGUI();
            }

            // Stop draw to renderTarget
            graphicsDeviceManager.GraphicsDevice.SetRenderTarget(null);

            // Draw letter/pillarbox 
            SpriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend,
                SamplerState.PointWrap);
            SpriteBatch.Draw(renderTarget, LetterPillarBox(), Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);
        }        
        // Return position, size, set colour, to draw renderTarget for letter/pillarboxing
        private Rectangle LetterPillarBox()
        {
            Rectangle destinationDraw;

            // Background colour i.e. letter/pillarbox 
            graphicsDeviceManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1f, 0);

            // If actual width larger relative to height compared to virtual, letterbox
            if (GlobalVariables.AspectActual <= GlobalVariables.AspectVirtual)
            {
                int heightScreen = (int)(GlobalVariables.WidthScreenActual / GlobalVariables.AspectVirtual);
                int widthPillarBox = (int)((GlobalVariables.HeightScreenActual - heightScreen) * 0.5f);

                return destinationDraw = new Rectangle(0, widthPillarBox, GlobalVariables.WidthScreenActual, heightScreen);
            }
            //If actual width smaller relative to height compared to virtual, pillarbox
            else
            {
                int widthScreen = (int)(GlobalVariables.HeightScreenActual * GlobalVariables.AspectVirtual);
                int widthBar = (int)((GlobalVariables.WidthScreenActual - widthScreen) * 0.5f);

                return destinationDraw = new Rectangle(widthBar, 0, widthScreen, GlobalVariables.HeightScreenActual);
            }
        }


        private void StartGame()
        {
            GUIAction.ToggleMainMenu(true);
            InMainMenu = true;
            // Initialise input
            IsMouseVisible = false;
            InputKeyboard = new InputKeyboard(this);
            InputMouse = new InputMouse(this);
            
        }
        public void NewGame()
        {
            ResetStates();
            CreateFirstArea();
        }


        /****************
        * Menu methods  *
        *****************/
        public void Save(string nameSaveFile)
        {
            SaveGame.SaveGameData(nameSaveFile);
        }
        public void Load(string nameSaveFile)
        {
            ResetStates();

            LoadGame loadGame = new LoadGame(this);

            loadGame.LoadGameData(nameSaveFile);
            PlayerSetup.SetupNewPlayer("LOAD TEST CHARACTER", Gender.Female, 2, 4, 6);

            GUIAction.ToggleMainMenu(false);
            InMainMenu = false;
            InGame = true;
            GameStarted = true;

            PlayerSetup.SetPlayerPositionNoEntrance();
            GUIAction.ToggleHUD(true);
        }
        public void Quit()
        {
            Exit();
        }

        // Reset game states for loading, new game, in same session
        private void ResetStates()
        {
            HighestAreaPlanned = 0;
            if (TextBoxes.TextBoxMessages != null)
            {
                TextBoxes.TextBoxMessages.ResetList();
            }
            if (TextBoxes.TextBoxStatus != null)
            {
                TextBoxes.TextBoxStatus.ResetList();
            }
            AreaStatesList.AreaStates.Clear();
            PlayerInventory.Inventory = new Items.Item[(int)GUIVariables.CAPACITY_INVENTORY];
            PlayerInventory.Equipment = new Items.Item[(int)GUIVariables.CAPACITY_EQUIPMENT];
        }


        /****************
        * Area methods  *
        *****************/
        public void StartArea(int areaIndex)
        {
            bool areaExists = false;

            foreach (AreaState areaState in AreaStatesList.AreaStates)
            {
                if (areaState.AreaIndex == areaIndex)
                {
                    areaExists = true;
                    break;
                }
            }

            if (areaExists)
            {
                CreateExistingArea(PlannedArea);
            }
            else
            {
                CreateNewArea();
            }
        }
        public void CreateFirstArea()
        {
            AreaState areaState = new AreaState(this);
            AreaStatesList.AreaStates.Add(areaState);

            AreaMaker.MakeArea(areaState);

            GUIAction.ToggleMainMenu(false);
            InMainMenu = false;
            InGame = true;
            GameStarted = true;

            PlayerSetup.SetPlayerPositionInRoom();
            GUIAction.ToggleHUD(true);
        }
        public void CreateNewArea()
        {
            AreaState areaState = new AreaState(this);
            AreaStatesList.AreaStates.Add(areaState);

            AreaMaker.MakeArea(areaState);

            if (AreaStatesList.AreaStates.Count == 1)
            {
                PlayerSetup.SetPlayerPositionNoEntrance();
            }
            else
            {
                PlayerSetup.SetPlayerPositionEntrance();
            }
            CurrentAreaIndex = Area.AreaIndex;
        }
        public void CreateExistingArea(int areaIndex)
        {
            AreaMaker.MakeArea(Helper.AreaStateNumber(areaIndex, this));
            PlayerSetup.SetPlayerPositionEntrance();
            CurrentAreaIndex = Area.AreaIndex;
        }
    }
}