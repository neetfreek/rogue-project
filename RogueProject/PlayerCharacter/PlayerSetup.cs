﻿/************************************
* Set player start position, sprite * 
*************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Common;
using RogueProject.Items;
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject.PlayerCharacter
{
    public class PlayerSetup
    {
        private readonly Game1 game;
        private readonly Random random;

        private readonly Player player;
        private readonly PlayerAction playerAction;
        private readonly PlayerMove playerMove;

        private Tile[,] startGrid = new Tile[5, 5]; 

        public PlayerSetup(Game1 game)
        {
            this.game = game;
            random = new Random();
            player = game.Player;
            playerMove = game.PlayerMove;
            playerAction = game.PlayerAction;            
        }


        public void SetupNewPlayer(string name, Gender gender, int agility, int magic, int strength)
        {
            game.PlayerStats.SetupPlayerStats(name, gender, agility, magic, strength);
            SetSprite(null);
        }


        /****************
        * Player sprite * 
        *****************/
        public void SetSprite(Item armour)
        {
            PlayerSprite sprite = PlayerSprite.None;

            if (armour == null)
            {
                if (game.PlayerStats.Gender == Gender.Female)
                {
                    sprite = PlayerSprite.NoneFemale;
                }
                else
                {
                    sprite = PlayerSprite.NoneMale;
                }
            }
            else
            {
                if (armour.TypeArmour == TypeArmour.Leather || armour.TypeArmour == TypeArmour.LeatherHardened)
                {
                    sprite = PlayerSprite.Leather;
                }
                if (armour.TypeArmour == TypeArmour.Robe)
                {
                    sprite = PlayerSprite.Robe;
                }
                if (armour.TypeArmour == TypeArmour.RobeHooded)
                {
                    sprite = PlayerSprite.RobeHooded;
                }
                if (armour.TypeArmour == TypeArmour.Chainmail || armour.TypeArmour == TypeArmour.ChainmailFull)
                {
                    sprite = PlayerSprite.Chainmail;
                }
                if (armour.TypeArmour == TypeArmour.Platemail || armour.TypeArmour == TypeArmour.PlatemailFull)
                {
                    sprite = PlayerSprite.Plate;
                }
            }
            game.Player.SetSprite(sprite);
        }


        /************************************************
        * Player start position on new game, load game  * 
        *************************************************/
        public void SetPlayerPositionNoEntrance()
        {
            // Set start direction (side of area)
            Random random = new Random();
            int enumLength = Enum.GetNames(typeof(Direction)).Length;
            int numberSelection = random.Next(1, 5); // Exclude None, diagonal Directions (0, 5-8)
            Direction startDirection = (Direction)Enum.ToObject(typeof(Direction), numberSelection);

            // Set start tile
            Tile startTile = game.Area.Grid[0, 0];

            switch (startDirection)
            {
                case Direction.Up:
                    startTile = game.Area.Grid[(int)(game.Area.Grid.GetLength(0) * 0.5f), 0 + 1];
                    startGrid[0, 0] = startTile.Neighbour(Direction.Left, (int)(startGrid.GetLength(0) * 0.5f));
                    break;
                case Direction.Right:
                    startTile = game.Area.Grid[(int)game.Area.Grid.GetLength(0) - 1 - 1,
                        (int)(game.Area.Grid.GetLength(1) * 0.5f)];
                    startGrid[0, 0] = startTile.Neighbour(Direction.Left, startGrid.GetLength(0));
                    startGrid[0, 0] = startGrid[0, 0].Neighbour(Direction.Up, (int)(startGrid.GetLength(1) * 0.5f));
                    break;
                case Direction.Down:
                    startTile = game.Area.Grid[(int)(game.Area.Grid.GetLength(0) * 0.5f),
                        (int)game.Area.Grid.GetLength(1) - 1 - 1];
                    startGrid[0, 0] = startTile.Neighbour(Direction.Up, startGrid.GetLength(0));
                    startGrid[0, 0] = startGrid[0, 0].Neighbour(Direction.Left, (int)(startGrid.GetLength(0) * 0.5f));
                    break;
                case Direction.Left:
                    startTile = game.Area.Grid[0 + 1, (int)(game.Area.Grid.GetLength(1) * 0.5f)];
                    startGrid[0, 0] = startTile.Neighbour(Direction.Up, startGrid.GetLength(1));
                    break;
            }

            // Iterate start grid if default start tile wall
            if (!startTile.Walkable)
            {
                PopulateStartGrid(startGrid);
                startTile = EmptyStartGridTile(startGrid);
            }
            playerMove.Move(startTile);
            DestroyEnemiesAroundStartPosition(startTile);
        }
        // Assign respective areaGrid tiles to startGrid
        private void PopulateStartGrid(Tile[,] grid)
        {
            Tile tile = grid[0, 0];

            for (int rowCounter = 0; rowCounter < grid.GetLength(0); rowCounter++)
            {
                for (int columnCounter = 0; columnCounter < grid.GetLength(0); columnCounter++)
                {
                    grid[columnCounter, rowCounter] = game.Area.Grid[(int)(tile.PositionGrid.X) + columnCounter,
                        (int)(tile.PositionGrid.Y + rowCounter)];
                }
            }
        }
        // Iterate through grid, return first floor tile
        private Tile EmptyStartGridTile(Tile[,] grid)
        {
            Tile tile = grid[0, 0];

                for (int rowCounter = 0; rowCounter < grid.GetLength(0); rowCounter++)
                {
                    for (int columnCounter = 0; columnCounter < grid.GetLength(1); columnCounter++)
                    {
                        if (grid[columnCounter, rowCounter].Walkable)
                        {
                            tile = grid[columnCounter, rowCounter];
                            break;
                        }
                    }
                }

            return tile;
        }


        /****************************************
        * Player start position specific places *
        *****************************************/
        public void SetPlayerPositionInRoom()
        {
            Room roomStart = game.Area.Rooms[random.Next(0, game.Area.Rooms.Count)];
            Tile tileStart = roomStart.FloorTiles[random.Next(0, roomStart.FloorTiles.Count)];
            while (!tileStart.Walkable)
            {
                tileStart = roomStart.FloorTiles[random.Next(0, roomStart.FloorTiles.Count)];
            }

            playerMove.MovePlayerPositionIndoorEntrnace(tileStart);

            Game1.DestroyingEnemiesForLoad = true;
            DestroyEnemiesInStartRoom(roomStart);
            Game1.DestroyingEnemiesForLoad = false;

        }


        /****************************************************************
        * Player start position beside entrance after using entrance    * 
        *****************************************************************/
        public void SetPlayerPositionEntrance()
        {
            Tile tileEntrance = game.Area.Grid[0,0];

            if (game.Area.AreaType == AreaType.Outdoor)
            {
                // if player clicked to travel back to previous area
                if (AreaStatesList.AreaStates.Count > 1 && player.ClickedPreviousOutdoorEntrance == true)
                {
                    tileEntrance = game.Area.areaState.Entrances[0].Tile;
                    playerMove.MovePlayerPositionOutdoorEntrance(tileEntrance);
                }
                // if player clicked to travel forward to later area
                else if (player.ClickedNextOutdoorEntrance == true)
                {
                    tileEntrance = game.Area.areaState.Entrances[game.Area.areaState.Entrances.Count - 1].Tile;
                    playerMove.MovePlayerPositionOutdoorEntrance(tileEntrance);
                }
                // if player clicked to travel outdoor from indoor
                else if (player.ClickedpreviousIndoorEntrance)
                {
                    tileEntrance = game.Area.areaState.Entrances[1].Tile;
                    playerMove.MovePlayerPositionIndoorEntrnace(tileEntrance);
                }

                DestroyEnemiesAroundStartPosition(tileEntrance);
            }
            else if (game.Area.AreaType == AreaType.Indoor)
            {
                if (player.ClickedpreviousIndoorEntrance)
                {
                    tileEntrance = game.Area.areaState.Entrances[0].Tile;
                    playerMove.MovePlayerPositionIndoorEntrnace(tileEntrance);
                }
                else
                {
                    tileEntrance = game.Area.areaState.Entrances[game.Area.areaState.Entrances.Count - 1].Tile;
                    playerMove.MovePlayerPositionIndoorEntrnace(tileEntrance);
                }

                DestroyEnemiesInStartRoom(tileEntrance.Room);
            }
        }

        private void DestroyEnemiesAroundStartPosition(Tile tilePlayer)
        {
            Game1.DestroyingEnemiesForLoad = true;

            // Determine start (top-left) position of grid to destroy enemies in
            int gridStartX = (int)(tilePlayer.PositionGrid.X - GlobalVariables.TILES_SCREEN_X * 0.5f);
            int gridStartY = (int)(tilePlayer.PositionGrid.Y - GlobalVariables.TILES_SCREEN_Y * 0.5f);
            if (gridStartX <= 0)
            {
                gridStartX = 1;
            }
            if (gridStartY <= 0)
            {
                gridStartY = 1;
            }
            // Determine end (bottom-right) position of grid to destroy enemies in
            int gridEndX = (int)(tilePlayer.PositionGrid.X +  GlobalVariables.TILES_SCREEN_X * 0.5f);
            int gridEndY = (int)(tilePlayer.PositionGrid.Y + (GlobalVariables.TILES_SCREEN_Y * 0.5f + 1));
            if (gridEndX >= game.Area.Grid.GetLength(0) - 1)
            {
                gridEndX = game.Area.Grid.GetLength(0) - 1;
            }
            if (gridEndY >= game.Area.Grid.GetLength(1) - 1)
            {
                gridEndY = game.Area.Grid.GetLength(1) - 1;
            }

            int gridWidth = gridEndX - gridStartX;
            int gridHeight = gridEndY - gridStartY;

            // Iterate grid around screen, destroy enemies in grid
            for (int counterRows = 0; counterRows < gridHeight; counterRows++)
            {
                for (int counterCols = 0; counterCols < gridWidth; counterCols++)
                {
                    if (game.Area.Grid[gridStartX + counterCols, gridStartY + counterRows].Enemy != null)
                    {
                        game.Area.Grid[gridStartX + counterCols, gridStartY + counterRows].Enemy.DestroyEnemy();
                    }
                }
            }

            Game1.DestroyingEnemiesForLoad = false;
        }
        private void DestroyEnemiesInStartRoom(Room roomStart)
        {
            foreach (Tile tile in roomStart.FloorTiles)
            {
                if (tile.Enemy != null)
                {
                    tile.Enemy.DestroyEnemy();
                }
            }
        }
    }
}