﻿/****************************************************************
* Handle player actions / player interactions with game         *
* SetLeftClickSelect() sets selections (left click, space bar), *
* SetKeyboardHighlight(), SetMouseHighlight sets tile to        *
*   hilight based on tile faced, moused-over                    *
* HandleTileInteract() handles and calls subroutines for           *
*   interactions with game objects including:                   *
*   DoorAction() for doors                                      *
*   EntranceAction() for entrances                              *
*****************************************************************/
using RogueProject.Common;
using RogueProject.GUI;
using RogueProject.Terrain;
using RogueProject.Combat;
using Microsoft.Xna.Framework;
using RogueProject.Input;

namespace RogueProject.PlayerCharacter
{
    public class PlayerAction
    {
        private Game1 game;
        private Player player;
        private PlayerMove playerMove;

        public Tile HighlightTile;

        public bool KeyboardMove;
        public int MovesRemaining { get => movesRemaining; }
        public int ActionsRemaining { get => actionsRemaining; }

        // Turn states
        private int movesRemaining;
        private int actionsRemaining;

        private bool mouseOverHighlight;
        private bool keyboardHighlight;
        

        public PlayerAction(Game1 game)
        {
            this.game = game;
            player = game.Player;
            playerMove = game.PlayerMove;
        }


        public void Action(Direction direction)
        {
            if (game.GUIAction.inventoryOpen)
            {
                game.GUIAction.NavigateHUDComponentKeyboard(direction);
            }
            else
            {
                ActionTiles(direction);
            }
        }
        private void ActionTiles(Direction direction)
        {
            Direction directionOld = player.Direction;
            player.Direction = direction;

            if (KeyboardMove)
            {
                if (player.Direction == directionOld)
                {
                    Tile nextTile = player.Tile.Neighbour(player.Direction, 1);
                    HandleTileInteract(nextTile);
                }
                KeyboardMove = false;
            }
            else
            {
                Tile nextTile = player.Tile.Neighbour(player.Direction, 1);

                // if mousing overEnemy call HandleInteract with enemy's tile as parameter
                if (game.Player.InAttackRange(game.InputMouse.TileClicked) && game.InputMouse.TileClicked.Enemy != null)
                {
                    nextTile = game.InputMouse.TileClicked;
                }
                HandleTileInteract(nextTile);

            }

            // Highlight adjacent pc-faced tile if contains interactable if not mousing over something
            if (!mouseOverHighlight)
            {
                SetKeyboardHighlight(direction);
            }
        }

        // Highlight adjacent tile for keyboard select on mouse move
        public void SetLeftClickSelect(Tile tile)
        {
            player.SetLeftClickSelect(tile);
        }
        // Highlight adjacent tile for keyboard select on keyboard move
        public void SetLeftClickSelect(Direction direction)
        {
            player.SetLeftClickSelect(player.Tile.Neighbour(direction, 1));
        }

        // Keyboard highlight; highlight whatever player is facing within one tile (and no mouse highlight)
        public void SetKeyboardHighlight(Direction tileDirection)
        {
            if (player.Tile.Neighbour(tileDirection, 1).ContainsInteractable() == true)
            {
                keyboardHighlight = true;
                HighlightTile = player.Tile.Neighbour(tileDirection, 1);
            }
            else if (keyboardHighlight == true)
            {
                HighlightTile = null;
            }
        }
        // Mouse highlight; highlight whatever player is mousing over within one tile
        public void SetMouseHighlight(Tile tile)
        {
            if (Helper.InRange(tile, player.Tile, 1) && tile.ContainsInteractable()
                && Helper.DistanceBetween(tile, game.Player.Tile) != new Vector2(0f, 0f))
            {
                mouseOverHighlight = true;
                HighlightTile = tile;
            }
            // Hilight mouseOver enemy in range, not obstructed
            if (game.Player.InAttackRange(tile) && tile.Enemy != null)
            {
                if (!CombatMechanics.AttackObstructed(game.Player.Tile, tile, InputMouse.MousedOverDirection))
                {
                    mouseOverHighlight = true;
                    HighlightTile = tile;
                }
            }

            else
            {
                if (mouseOverHighlight == true)
                {
                    HighlightTile = null;
                }
                mouseOverHighlight = false;
            }
        }

        public bool InteractableInRange()
        {
            if (HighlightTile != null)
            {
                return true;
            }
            return false;
        }


        // Handle turn
        public void NewTurn()
        {
            movesRemaining = game.PlayerStats.Moves;
            actionsRemaining = game.PlayerStats.Actions;

            UpdateStatusTextNewTurn();
        }
        public void UpdateStatusTextNewTurn()
        {
            if (game.TextBoxes.TextBoxStatus != null)
            {
                string separatorMoves = $"{game.TextBoxes.TextBoxStatus.SeparatorSpaces(game.PlayerAction.MovesRemaining.ToString().Length)}/{game.TextBoxes.TextBoxStatus.SeparatorSpaces(game.PlayerStats.Moves.ToString().Length)}";
                string separatorAttacks = $"{game.TextBoxes.TextBoxStatus.SeparatorSpaces(game.PlayerAction.ActionsRemaining.ToString().Length)}/{game.TextBoxes.TextBoxStatus.SeparatorSpaces(game.PlayerStats.Actions.ToString().Length)}";
                string message = $"{System.Environment.NewLine}{System.Environment.NewLine}";
                if (actionsRemaining == 0)
                {
                    message += HUDVariables.NEXT_ACTION_END;
                }
                if (movesRemaining == 0)
                {
                    message += HUDVariables.NEXT_MOVE_END;
                }
                game.TextBoxes.TextBoxStatus.ReplaceMessage(
                    $"{HUDVariables.MOVES_STATUS} {game.PlayerAction.MovesRemaining}{separatorMoves}{game.PlayerStats.Moves} {System.Environment.NewLine}{System.Environment.NewLine}" +
                    $"{HUDVariables.ACTIONS_STATUS} {game.PlayerAction.ActionsRemaining}{separatorAttacks}{game.PlayerStats.Actions}" +
                    $"{message}"
                    );
            }

        }
        private void UpdateStatusTextEndTurn()
        {
        }
        private void HandleTurn()
        {
            if (movesRemaining <= 0 && actionsRemaining <= 0)
            {
                EndTurn();
            }
        }
        public void EndTurn()
        {
            UpdateStatusTextEndTurn();
            game.EnemyAction.UpdateEnemies();
        }


        // Determine appropriate action, move for left-clicked tile
        public void HandleTileInteract(Tile tile)
        {
            if (!HandleAction(tile))
            {
                HandleMove(tile);
            }

            HandleTurn();
        }

        private bool HandleAction(Tile tile)
        {
            bool performedAction = false;

            if (tile == player.TileLeftClicked && tile.ContainsInteractable() &&
                !EndTurnIfNoActionsLeft())
            {
                if (tile.Door != null)
                {
                    if (tile.Door.Opened)
                    {
                        playerMove.StartMove(tile);
                    }
                    else
                    {
                        ActionDoor(tile);
                        performedAction = true;
                    }
                }

                if (tile.Entrance != null)
                {
                    ActionEntrance(tile);
                }

                if (tile.Enemy != null && !CombatMechanics.AttackObstructed(player.Tile, tile, player.Direction))
                {
                    ActionEnemy(tile);
                    actionsRemaining--;
                    UpdateStatusTextNewTurn();
                    performedAction = true;
                }

                if (tile.Item != null)
                {
                    ActionItem(tile);
                    performedAction = true;
                }
            }

            return performedAction;
        }
        private void HandleMove(Tile tile)
        {
            if (tile.Walkable && !EndTurnIfNoMovesLeft())
            {
                if (movesRemaining == 0)
                {
                    EndTurn();
                }
                else
                {
                    movesRemaining--;
                    UpdateStatusTextNewTurn();

                    playerMove.StartMove(tile);
                }
            }
        }

        private bool EndTurnIfNoActionsLeft()
        {
            if (actionsRemaining == 0)
            {
                EndTurn();
                return true;
            }
            return false;
        }
        private bool EndTurnIfNoMovesLeft()
        {
            if (movesRemaining == 0)
            {
                EndTurn();
                return true;
            }
            return false;
        }


        private void ActionDoor(Tile tile)
        {
            if (!tile.Door.Locked)
            {
                tile.Door.Open();
                game.Area.areaState.UpdateObjectState(tile.Door);
                game.TextBoxes.TextBoxMessages.UpdateMessage($"{HUDVariables.EMPHASIS_ACTION}{game.PlayerStats.Name} {HUDVariables.OPEN_DOOR}{HUDVariables.EMPHASIS_ACTION}");
                actionsRemaining--;
            }
            else
            {
                TryUnlock(tile);
            }
        }
        private void TryUnlock(Tile tile)
        {
            System.Console.WriteLine($"Try unlock door at {tile.PositionGrid}");
        }

        private void ActionEnemy(Tile tile)
        {
            HitRollType hitRoll = CombatMechanics.RollToHit(game.PlayerStats.RatingHit, tile.Enemy.RatingDefence);
            if (hitRoll == HitRollType.Miss)
            {
                game.TextBoxes.TextBoxMessages.UpdateAttackerMiss(game.PlayerStats.Name, tile.Enemy.DisplayName);
            }

            else
            {
                int damage = CombatMechanics.ProcessHit(hitRoll, game.PlayerStats.Damage);

                if (damage < 0)
                {
                    game.PlayerStats.TakeDamage(-damage);
                    game.TextBoxes.TextBoxMessages.UpdateCriticalMiss(game.PlayerStats.Name, tile.Enemy.DisplayName, damage);
                }
                else if (damage > 0)
                {
                    tile.Enemy.TakeDamageFromPlayer(damage);
                    if (tile.Enemy != null)
                    {
                        game.TextBoxes.TextBoxMessages.UpdateAttackerHit(game.PlayerStats.Name, tile.Enemy.DisplayName, damage, tile.Enemy.HealthPercent);
                    }
                }
            }

        }

        private void ActionEntrance(Tile tile)
        {
            game.TextBoxes.TextBoxMessages.UpdateMessage($"{HUDVariables.ENTRANCE} {"Area Name"}");
            Game1.PlannedArea = tile.Entrance.AreaToLoadIndex;
            Game1.PreviousArea = Game1.CurrentAreaIndex;
            game.StartArea(Game1.PlannedArea);
            keyboardHighlight = false;
            mouseOverHighlight = false;
            NewTurn();
        }

        private void ActionItem(Tile tile)
        {
            game.PlayerInventory.AddItemToInventory(tile);
        }
    }
}