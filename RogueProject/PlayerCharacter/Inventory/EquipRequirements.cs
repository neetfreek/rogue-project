﻿/*******************************************************************
* Responsible for ensuring items can equipped, i.e. placed into    *
*   equipment GUI component grid cells (children)                  *
********************************************************************/
using RogueProject.Items;

namespace RogueProject.PlayerCharacter
{
    public class EquipRequirements
    {
        private readonly Game1 game;

        public EquipRequirements(Game1 game)
        {
            this.game = game;
        }


        public bool AbleToEquipItemEquipment(int indexCellDestination, Item item)
        {
            bool canEquip = true;
            // Check item category appropriate for cell (slot)
            canEquip = SlotAppropriateForItemCategoryEquipment(indexCellDestination, item);
            // Check stats,ability unlocked, whatever
            // ADD THING FOR 2H, 1H WEAPONS AND SHIELDS
            return canEquip;
        }
        public bool AbleToEquipItemTrinket(int indexCellDestination, Item item)
        {
            bool canEquip = true;
            // Check item category appropriate for cell (slot)
            canEquip = SlotAppropriateForItemCategoryTrinket(indexCellDestination, item);
            return canEquip;
        }

        private bool SlotAppropriateForItemCategoryEquipment(int indexCellDestination, Item item)
        {
            bool slotAppropriate = true;
            switch (indexCellDestination)
            {
                case 0:
                    if (item.CategoryItem != CategoryItem.Weapon)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 1:
                    if (item.CategoryArmour != CategoryArmour.Shield)
                    {
                        slotAppropriate = false;
                        // Dual-weild unlocked: 
                        //if (game.PlayerAbilities.DualWeild.level > 0 && item.CategoryItem == CategoryItem.Weapon)
                        //{
                        //    slotAppropriate = true;
                        //}
                    }
                    break;
                case 2:
                    if (item.CategoryArmour != CategoryArmour.Helmet)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 3:
                    if (item.CategoryArmour != CategoryArmour.Chest)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 4:
                    if (item.CategoryArmour != CategoryArmour.Gloves)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 5:
                    if (item.CategoryArmour != CategoryArmour.Boots)
                    {
                        slotAppropriate = false;
                    }
                    break;
            }

            return slotAppropriate;
        }
        private bool SlotAppropriateForItemCategoryTrinket(int indexCellDestination, Item item)
        {
            bool slotAppropriate = true;
            switch (indexCellDestination)
            {
                case 0:
                    if (item.TypeArmour != TypeArmour.Ring)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 1:
                    if (item.TypeArmour != TypeArmour.Ring)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 2:
                    if (item.TypeArmour != TypeArmour.Amulet)
                    {
                        slotAppropriate = false;
                    }
                    break;
                case 3:
                    if (item.TypeArmour != TypeArmour.Charm)
                    {
                        slotAppropriate = false;
                    }
                    break;
            }

            return slotAppropriate;
        }

    }
}