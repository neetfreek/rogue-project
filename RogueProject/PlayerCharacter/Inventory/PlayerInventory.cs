﻿/*******************************************************************
* Handles player's picked up items.                                *
* 1. Handles adding, removing items from the player's inventory.   *
* 2. Handles equipping, unequipping player items.                  *
********************************************************************/
using RogueProject.GUI;
using RogueProject.Items;
using RogueProject.Terrain;
using System.Collections.Generic;
using System.Linq;

namespace RogueProject.PlayerCharacter
{
    public class PlayerInventory
    {
        private Game1 game;
        public EquipRequirements EquipRequirements;
        public HUDInspectionItem hudInspectionItem;

        public Item[] Inventory = new Item[(int)GUIVariables.CAPACITY_INVENTORY];
        public Item[] Equipment = new Item[(int)GUIVariables.CAPACITY_EQUIPMENT];
        public Item[] Trinkets = new Item[(int)GUIVariables.CAPACITY_TRINKETS];

        public PlayerInventory(Game1 game)
        {
            this.game = game;
            EquipRequirements = new EquipRequirements(game);
            hudInspectionItem = new HUDInspectionItem();
        }


        // Picking up item from tile
        public void AddItemToInventory(Tile tile)
        {
            bool itemPlaced = false;
            Item item = tile.Item;

            for (int counter = 0; counter < Inventory.GetLength(0); counter ++)
            {
                if (Inventory[counter] == null)
                {
                    itemPlaced = true;
                    Inventory[counter] = item;
                    game.TextBoxes.TextBoxMessages.UpdateMessage($"{HUDVariables.PICKUP} {tile.Item.Name}");
                    tile.Item = null;
                    break;
                }
            }
            if (!itemPlaced)
            {
                game.TextBoxes.TextBoxMessages.UpdateMessage($"{HUDVariables.PICKUP_NO_SPACE} {tile.Item.Name}");
            }
        }
        // Move item to empty grid cell
        public void MoveItemToInventoryCell(Item item, int indexOrigin, int indexDestination, string nameDraggedItemComponent)
        {
            Inventory[indexDestination] = item;
            if (nameDraggedItemComponent == GUIVariables.HEADING_INVENTORY)
            {
                Inventory[indexOrigin] = null;
            }
            if (nameDraggedItemComponent == GUIVariables.HEADING_EQUIPMENT)
            {
                Equipment[indexOrigin] = null;
                if (item.CategoryArmour == CategoryArmour.Chest)
                {
                    game.PlayerSetup.SetSprite(null);
                }
            }
            if (nameDraggedItemComponent == GUIVariables.HEADING_TRINKETS)
            {
                Trinkets[indexOrigin] = null;
            }
        }
        public void MoveItemToEquipmentCell(Item item, int indexOrigin, int indexDestination, string nameDraggedItemComponent)
        {
            Equipment[indexDestination] = item;
            if (nameDraggedItemComponent == GUIVariables.HEADING_INVENTORY)
            {
                Inventory[indexOrigin] = null;
                if (item.CategoryArmour == CategoryArmour.Chest)
                {
                    game.PlayerSetup.SetSprite(item);
                }
            }
            if (nameDraggedItemComponent == GUIVariables.HEADING_EQUIPMENT)
            {
                Equipment[indexOrigin] = null;
            }
        }
        public void MoveItemToTrinketCell(Item item, int indexOrigin, int indexDestination, string nameDraggedItemComponent)
        {
            Trinkets[indexDestination] = item;
            if (nameDraggedItemComponent == GUIVariables.HEADING_INVENTORY)
            {
                Inventory[indexOrigin] = null;
            }
            if (nameDraggedItemComponent == GUIVariables.HEADING_TRINKETS)
            {
                Trinkets[indexOrigin] = null;
            }
        }

        // Switch items in grid cells
        public void SwitchItemsInventory(Item itemDragged, Item itemToSwitch, int indexOrigin, int indexDestination, string nameComponentOrigin)
        {
            if (nameComponentOrigin == GUIVariables.HEADING_INVENTORY)
            {
                Inventory[indexDestination] = itemDragged;
                Inventory[indexOrigin] = itemToSwitch;
            }
            if (nameComponentOrigin == GUIVariables.HEADING_EQUIPMENT)
            {
                Inventory[indexDestination] = itemDragged;
                Equipment[indexOrigin] = itemToSwitch;
            }
        }
        public void SwitchItemsEquipment(Item itemDragged, Item itemToSwitch, int indexOrigin, int indexDestination, string nameComponentOrigin)
        {
            if (nameComponentOrigin == GUIVariables.HEADING_INVENTORY)
            {
                Equipment[indexDestination] = itemDragged;
                Inventory[indexOrigin] = itemToSwitch;
            }
            if (nameComponentOrigin == GUIVariables.HEADING_EQUIPMENT)
            {
                Equipment[indexDestination] = itemDragged;
                Equipment[indexOrigin] = itemToSwitch;
            }
        }
        public void SwitchItemsTrinket(Item itemDragged, Item itemToSwitch, int indexOrigin, int indexDestination, string nameComponentOrigin)
        {
            if (nameComponentOrigin == GUIVariables.HEADING_INVENTORY)
            {
                Trinkets[indexDestination] = itemDragged;
                Inventory[indexOrigin] = itemToSwitch;
            }
            if (nameComponentOrigin == GUIVariables.HEADING_TRINKETS)
            {
                Trinkets[indexDestination] = itemDragged;
                Trinkets[indexOrigin] = itemToSwitch;
            }
        }

        public void RemoveItemFromInventory(Item item)
        {
            List<Item> inventoryList = Inventory.ToList();
            inventoryList.Remove(item);
            Inventory = inventoryList.ToArray();        
        }
    }
}