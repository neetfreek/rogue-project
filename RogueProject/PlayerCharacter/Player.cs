﻿/****************************************************************************
* Player game object                                                        *
* Contains functionality for setting tile properities:                      *
*   SetNewTile() for tile currently occupied                                *
*   SetLeftClickSelect() for tile currently selected through left-click     *
*     or spacebar ()                                                        *
* Contains functionality for setting what kind of entrance player has       *
*   selected through:                                                       *
*   CheckClickOutdoorEntrance() and its subroutine SetClickOutdoorEntrance()*
*   CheckClickIndoorEntrance() and its subroutine SetClickIndoorEntrance()  * 
*****************************************************************************/
using RogueProject.Common;
using RogueProject.Terrain;
using RogueProject.Textures;

namespace RogueProject.PlayerCharacter
{
    public class Player
    {
        private Game1 game;

        // Player interactions
        public bool ClickedNextOutdoorEntrance { get => clickedNextOutdoorEntrance; }
        public bool ClickedPreviousOutdoorEntrance { get => clickedPreviousOutdoorEntrance; }
        public bool ClickedNextIndoorEntrance { get => clickedNextIndoorEntrance; }
        public bool ClickedpreviousIndoorEntrance { get => clickedPreviousIndoorEntrance; }

        // Navigation
        public Tile Tile { get => tile; }
        public Tile TilePrevious { get => tilePrevious; set => tilePrevious = value; }
        public Tile TileLeftClicked { get => tileLeftClicked; }

        public Direction Direction;

        // Sprite
        public PlayerSprite Sprite { get => sprite; }

        private PlayerSprite sprite;
        private bool clickedNextOutdoorEntrance;
        private bool clickedPreviousOutdoorEntrance;
        private bool clickedNextIndoorEntrance;
        private bool clickedPreviousIndoorEntrance;
        private Tile tile;
        private Tile tilePrevious;
        private Tile tileLeftClicked;


        public Player(Game1 game)
        {
            this.game = game;
        }


        // Appearance
        public void SetSprite(PlayerSprite sprite)
        {
            this.sprite = sprite;
        }

        // Set argument tile as player's tile
        public void SetNewTile(Tile tile)
        {
            this.tile = tile;
            Tile.character = Characters.Player;
        }

        // Set tileLeftClicked to argument tile 
        public void SetLeftClickSelect(Tile tile)
        {
            tileLeftClicked = tile;
            CheckClickOutdoorEntrance(tile);
            CheckClickIndoorEntrance(tile);
        }

        // Entrance clicks
        private void CheckClickOutdoorEntrance(Tile tile)
        {
            if (tile.Entrance != null && game.Area.AreaType == AreaType.Outdoor)
            {
                SetClickOutdoorEntrance(tile);
            }
            else
            {
                clickedNextOutdoorEntrance = false;
                clickedPreviousOutdoorEntrance = false;
            }
        }
        private void SetClickOutdoorEntrance(Tile tile)
        {
            int entranceIndex = game.Area.Entrances.IndexOf(tile.Entrance);

            switch (game.Area.Entrances.Count)
            {
                // First area
                case 1: // Next outdoor
                    clickedNextOutdoorEntrance = true;
                    clickedPreviousOutdoorEntrance = false;
                    break;

                case 2:
                    // First area
                    if (Game1.CurrentAreaIndex == 0)
                    {
                        if (entranceIndex == 0) // Next outdoor
                        {
                            clickedNextOutdoorEntrance = true;
                            clickedPreviousOutdoorEntrance = false;
                        }
                        else // Indoor
                        {
                            clickedNextOutdoorEntrance = false;
                            clickedPreviousOutdoorEntrance = false;
                            clickedNextIndoorEntrance = true;
                            clickedPreviousIndoorEntrance = false;
                        }
                    }
                    // Other areas
                    else
                    {
                        if (entranceIndex == 0) // Next outdoor
                        {
                            clickedNextOutdoorEntrance = true;
                            clickedPreviousOutdoorEntrance = false;
                        }
                        else // Previous outdoor
                        {
                            clickedNextOutdoorEntrance = false; 
                            clickedPreviousOutdoorEntrance = true;
                        }
                    }
                    break;

                case 3:
                    if (entranceIndex == 0) // Next outdoor
                    {
                        clickedNextOutdoorEntrance = true;
                        clickedPreviousOutdoorEntrance = false;
                    }
                    if (entranceIndex == 1) // Indoor
                    {
                        clickedNextOutdoorEntrance = false;
                        clickedPreviousOutdoorEntrance = false;
                        clickedNextIndoorEntrance = true;
                        clickedPreviousIndoorEntrance = false;
                    }
                    if (entranceIndex == 2) // Previous outdoor
                    {
                        clickedNextOutdoorEntrance = false;
                        clickedPreviousOutdoorEntrance = true;
                    }
                    break;
            }
        }
        private void CheckClickIndoorEntrance(Tile tile)
        {
            if (tile.Entrance != null && game.Area.AreaType == AreaType.Indoor)
            {
                SetClickIndoorEntrance(tile);
            }
            else if (game.Area.Entrances.IndexOf(tile.Entrance) != 1 && game.Area.AreaType == AreaType.Outdoor) // if not clicking outdoor to indoor 
            {
                clickedNextIndoorEntrance = false;
                clickedPreviousIndoorEntrance = false;
            }
        }
        private void SetClickIndoorEntrance(Tile tile)
        {
            if(game.Area.areaState.Entrances.IndexOf(tile.Entrance) == game.Area.areaState.Entrances.Count -1)
            {
                clickedPreviousIndoorEntrance = true;
                clickedNextIndoorEntrance = false;
            }
            else
            {
                clickedNextIndoorEntrance = true;
                clickedPreviousIndoorEntrance = false;
            }
        }

        // Helpers
        public bool InAttackRange(Tile tileDestination)
        {
            if (Helper.DistanceBetween(tileDestination, Tile).X < game.PlayerStats.RangeAttack &&
                Helper.DistanceBetween(tileDestination, Tile).Y < game.PlayerStats.RangeAttack)
            {
                return true;
            }

            return false;
        }
    }
}