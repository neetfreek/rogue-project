﻿/****************************************
* Check tile valid for move, determine  *
*   direction to move.                  *
* Moves player, camera to new tile      * 
*****************************************/
using System.Threading.Tasks;
using RogueProject.Common;
using RogueProject.GUI;
using RogueProject.Terrain;

namespace RogueProject.PlayerCharacter
{
    public class PlayerMove
    {
        private Camera2D camera;
        private Game1 game;
        private Player player;

        public Direction DirectionMoved { get => directionMoved; }
        private Direction directionMoved;
        private bool cooldownMoveRunning;
        private bool moved;


        public PlayerMove(Game1 game)
        {
            camera = game.Camera;
            this.game = game;
            player = game.Player;
        }


        /************
        * Movement  * 
        *************/
        // Call Move if move cooldown not running
        public void StartMove(Tile tile)
        {
            if (moved == false)
            {
                if (!cooldownMoveRunning)
                {
                    moved = true;
                    StartCooldownMove();

                    if (tile.Walkable)
                    {
                        Move(tile);
                    }
                }
            }
        }
        public void Move(Tile tile)
        {
            // Old tile
            if (player.Tile != null)
            {
                player.TilePrevious = player.Tile;
                player.Tile.character = Characters.None;
                directionMoved = Helper.DirectionBetween(player.TilePrevious, tile);
            }
            // New tile
            player.SetNewTile(tile);
            camera.Move(tile);
            HUDMove.MoveHUD(tile.PositionGrid);
        }

        public void MovePlayerPositionOutdoorEntrance(Tile tile)
        {
            if (tile.PositionGrid.X == 2)
            {
                Move(tile.Neighbour(Direction.Right, 1));
            }
            if (tile.PositionGrid.X == game.Area.Grid.GetLength(0) - 3)
            {
                Move(tile.Neighbour(Direction.Left, 1));
            }
            if (tile.PositionGrid.Y == 2)
            {
                Move(tile.Neighbour(Direction.Down, 1));
            }
            if (tile.PositionGrid.Y == game.Area.Grid.GetLength(1) - 3)
            {
                Move(tile.Neighbour(Direction.Up, 1));
            }
        }
        public void MovePlayerPositionIndoorEntrnace(Tile tile)
        {
            Tile tileToMove = tile;
            bool appropriatePositionFound = false;
            int counter = 1;

            while (!appropriatePositionFound)
            {
                if (tile.Neighbour(Direction.Right, counter).Walkable)
                {
                    tileToMove = tile.Neighbour(Direction.Right, counter);
                    appropriatePositionFound = true;
                }
                else if (tile.Neighbour(Direction.Left, counter).Walkable)
                {
                    tileToMove = tile.Neighbour(Direction.Left, counter);
                    appropriatePositionFound = true;
                }
                else if (tile.Neighbour(Direction.Up, counter).Walkable)
                {
                    tileToMove = tile.Neighbour(Direction.Up, counter);
                    appropriatePositionFound = true;
                }
                else if (tile.Neighbour(Direction.Down, counter).Walkable)
                {
                    tileToMove = tile.Neighbour(Direction.Down, counter);
                    appropriatePositionFound = true;
                }
                counter++;
            }
            Move(tile);
        }


        /************************************
        * Cooldown, prevents rapid movement * 
        *************************************/
        private void StartCooldownMove()
        {
            cooldownMoveRunning = true;
            Task.Delay(GlobalVariables.COOLDOWN_MOVE).ContinueWith(t => EndCoolddownMove());
        }
        private void EndCoolddownMove()
        {
            moved = false;
            cooldownMoveRunning = false;
        }
    }
}