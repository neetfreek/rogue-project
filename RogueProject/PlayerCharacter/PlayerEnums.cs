﻿namespace RogueProject.PlayerCharacter
{
    public enum Gender
    {
        None,
        Female,
        Male,
    }

    public enum Stat
    {
        Agility,
        Magic,
        Strength
    }
}