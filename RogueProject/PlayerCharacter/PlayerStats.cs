﻿/************************************************
* Contains all state-related player information * 
*************************************************/
using System;
using Microsoft.Xna.Framework;
using RogueProject.Items;

namespace RogueProject.PlayerCharacter
{
    public class PlayerStats
    {
        private Game1 game;
        public CharacterSheet characterSheet;

        // General stats
        public string Name { get => name; }
        public Gender Gender { get => gender; }
        public int Level { get => level; }
        public float ExperienceCurrent { get => experienceCurrent;}
        public float ExperienceFull { get => experienceFull; }
        public float ExperiencePercent { get => (float)(Math.Round((decimal)(experienceCurrent / experienceFull), 3, MidpointRounding.AwayFromZero)); }
        // Vital stats
        public float HealthCurrent { get => healthCurrent; }
        public float HealthTotal { get => healthTotal; }
        public float HealthPercent {get => (float)(Math.Round((decimal)(healthCurrent / healthTotal), 3, MidpointRounding.AwayFromZero)); }
        public int Strength { get => strength; set => strength = value; }
        public int Magic { get => magic; set => magic = value; }
        public int Agility { get => agility; set => agility = value; }
        public float ManaCurrent { get => manaCurrent; }
        public float ManaTotal { get => manaTotal; }
        public float ManaPercent { get => (float)(Math.Round((decimal)(manaCurrent / manaTotal), 3, MidpointRounding.AwayFromZero)); }
        // CombatMechanics stats
        public int RangeAttack { get => EquipmentRange(); }
        public int RatingDefence { get => ratingDefence + EquipmentRatingDefence(); set => ratingDefence = value; }
        public int RatingHit { get => ratingHit; set => ratingHit = value; }
        public Vector2 Damage { get => damage + EquipmentDamage(); set => damage = value; }
        // Interaction stats
        public int Moves { get => moves; }
        public int Actions { get => actions; }
        // Inventory stats
        public int SizeInventory { get => sizeInventory; set => sizeInventory = value; }

        public bool GameOver { get => gameOver; }

        private bool gameOver;

        private int actions;
        private int moves;

        private int sizeInventory;

        private Gender gender;
        private float healthTotal;
        private float healthCurrent;
        private int strength;
        private int magic;
        private int agility;
        private float manaTotal;
        private float manaCurrent;
        private string name;
        private int ratingDefence;
        private int ratingHit;
        private Vector2 damage;
        private float experienceCurrent;
        private float experienceFull;
        private int level;

        public PlayerStats(Game1 game)
        {
            this.game = game;
            characterSheet = new CharacterSheet(this);
        } 


        public void SetupPlayerStats(string name, Gender gender, int agility, int magic, int strength)
        {
            this.name = name;
            this.gender = gender;
            this.agility = agility;
            this.magic = magic;
            this.strength = strength;

            healthTotal = 100;
            healthCurrent = 100;
            manaTotal = 100;
            manaCurrent = 100;
            moves = 2;
            actions = 1;
            ratingDefence = Combat.CombatVariables.BASE_DEFENCE;
            ratingHit = 2;
            experienceFull = 100;
            experienceCurrent = 0;
            damage = new Vector2(1f, 4f);
            sizeInventory = (int)GUI.GUIVariables.CAPACITY_INVENTORY;
            game.PlayerAction.NewTurn();
        }



        public void TakeDamage(int amountDamage)
        {
            healthCurrent -= amountDamage;

            if (healthCurrent < 1)
            {
                Defeat();
            }
        }

        public void GainExperience(int amountExperience)
        {
            experienceCurrent += amountExperience;

            if (experienceCurrent > ExperienceFull)
            {
                GainLevel();
            }
        }

        private void Defeat()
        {
            gameOver = true;
            game.TextBoxes.TextBoxMessages.UpdatePlayerDefeated(Name);
        }

        private void GainLevel()
        {
            level++;
            experienceCurrent -= ExperienceFull;
            healthCurrent = HealthTotal;
            manaCurrent = ManaTotal;
            experienceFull *= 1.4f;
        }

        private Vector2 EquipmentDamage()
        {
            Vector2 bonusDamage = new Vector2(0f, 0f);
            // Add weapon damage
            int counter = 0;
            // Iterate 1st (weapon) 2nd (offhand) equipment slots
            while (counter < 2)
            {
                if (game.PlayerInventory.Equipment[counter] != null)
                {
                    if (game.PlayerInventory.Equipment[counter].CategoryItem == CategoryItem.Weapon)
                    {
                        Weapon weapon = (Weapon)game.PlayerInventory.Equipment[0];
                        bonusDamage += weapon.Damage;
                    }
                }
                counter++;
            }

            return bonusDamage;
        }

        private int EquipmentRatingDefence()
        {
            int bonusDefenceRating = 0;
            // Start with shield slot by default
            int counter = 1;
            while (counter < GUI.GUIVariables.CAPACITY_EQUIPMENT)
            {
                if (game.PlayerInventory.Equipment[counter] != null)
                {
                    if (game.PlayerInventory.Equipment[counter].CategoryItem == CategoryItem.Armour)
                    {
                        Armour armour = (Armour)game.PlayerInventory.Equipment[counter];
                        bonusDefenceRating += armour.RatingArmour;
                    }
                }
                counter++;
            }
            return bonusDefenceRating;
        }

        public int EquipmentRange()
        {
            int counter = 0;
            int rangeEquipment = 1;
            // Iterate 1st (weapon) 2nd (offhand) equipment slots
            while (counter < 2)
            {
                if (game.PlayerInventory.Equipment[counter] != null)
                {
                    if (game.PlayerInventory.Equipment[counter].CategoryItem == CategoryItem.Weapon)
                    {
                        Weapon weapon = (Weapon)game.PlayerInventory.Equipment[counter];
                        rangeEquipment = weapon.Range;
                    }
                }
                counter++;
            }
            return rangeEquipment;
        }

        public void EquipmentSetStats()
        {
            // Add weapon damage
            int counter = 0;
            // Iterate equipment
            while (counter < 6)
            {
                if (game.PlayerInventory.Equipment[counter] != null)
                {

                }
                counter++;
            }
            // Iterate trinkets
            while (counter < 4)
            {
                if (game.PlayerInventory.Equipment[counter] != null)
                {

                }
                counter++;
            }
        }
    }
}