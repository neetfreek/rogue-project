﻿/************************************************************
* Handles assigning player character values to Text values  *
*   of different character sheet field sub-components.      *
* This class is instantiated by PlayerStats.cs in its       *
*   constructor.                                            *
* AssignCharacterSheetComponents() assigns the character    *
*   sheet sub-components so as to be able to change their   *
*   Text values. Called by GUIMaker.CreateCharacterSheet(). *
* PopulateCharacterSheetFields() assigns the values of      *
*   different player fields (e.g. mana, name) to the Text   *
*   properties of the various character sheet components.   *
*   Called by GUIAction.ToggleCharacterSheet()              *
*                                                           *
*************************************************************/
using RogueProject.Combat;
using RogueProject.Common;
using RogueProject.GUI;

namespace RogueProject.PlayerCharacter
{
    public class CharacterSheet
    {
        private PlayerStats playerStats;

        private GUIRectangle componentAgility;
        private GUIRectangle componentMagic;
        private GUIRectangle componentStrength;
        private GUIRectangle componentExperience;
        private GUIRectangle componentArea;
        private GUIRectangle componentHealth;
        private GUIRectangle componentMana;
        private GUIRectangle componentDamage;
        private GUIRectangle componentDefence;

        public CharacterSheet(PlayerStats playerStats)
        {
            this.playerStats = playerStats;
        }

        public void AssignCharacterSheetComponents(GUIRectangle characterSheet)
        {
            foreach (GUIRectangle subComponent in characterSheet.Subcomponents)
            {
                switch (subComponent.TextHeader)
                {
                    case GUIVariables.HEADING_FIELD_AGILITY:
                        componentAgility = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_MAGIC:
                        componentMagic = subComponent;
                        break;
                    case GUIVariables.HEADING_FIELD_STRENGTH:
                        componentStrength = subComponent;
                        break;
                    case GUIVariables.FIELD_AREA:
                        componentArea = subComponent;
                        break;
                    case GUIVariables.FIELD_EXPERIENCE:
                        componentExperience = subComponent;
                        break;

                    case GUIVariables.FIELD_HEALTH:
                        componentHealth = subComponent;
                        break;
                    case GUIVariables.FIELD_MANA:
                        componentMana = subComponent;
                        break;
                    case GUIVariables.FIELD_DAMAGE:
                        componentDamage = subComponent;
                        break;
                    case GUIVariables.FIELD_DEFENCE:
                        componentDefence = subComponent;
                        break;
                }
            }
        }

        public void PopulateCharacterSheetFields()
        {
            componentAgility.SetText(playerStats.Agility.ToString());
            componentMagic.SetText(playerStats.Magic.ToString());
            componentStrength.SetText(playerStats.Strength.ToString());
            componentExperience.SetText($"{playerStats.ExperienceCurrent.ToString()}{GUIVariables.SEPARATOR}{playerStats.ExperienceFull.ToString()}");
            componentHealth.SetText($"{playerStats.HealthCurrent.ToString()}{GUIVariables.SEPARATOR}{playerStats.HealthTotal.ToString()}");
            componentMana.SetText($"{playerStats.ManaCurrent.ToString()}{GUIVariables.SEPARATOR}{playerStats.ManaTotal.ToString()}");
            componentDamage.SetText(Helper.Vector2RangeToString(playerStats.Damage));
            int ratingDefenceDisplay = playerStats.RatingDefence - CombatVariables.BASE_DEFENCE;
            componentDefence.SetText(ratingDefenceDisplay.ToString());
            componentArea.SetText("PLACEHOLDER AREA");
        }
    }
}