﻿namespace RogueProject.PlayerCharacter
{
    public static class PlayerVariables
    {
        public const int MINIMUM_STAT = 0;
        public const int MAXIMUM_STAT = 20;
        public const int MAXIMUM_CHARS_NAME = 15;
    }
}