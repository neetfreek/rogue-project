NeetFreek 2019 - All Rights Reserved
Proprietary and confidential

This file constitutes a part of *rogue-project*. 
Unauthorized copying of this, or any other file in *rogue-project*, via any medium is strictly prohibited without the express permission of Jonathan Widdowson.

Written by Jonathan Widdowson <jonathan.widdowson@neetfreek.net>, May 2019
